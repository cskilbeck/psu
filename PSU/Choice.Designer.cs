﻿namespace Locomo
{
    partial class Choice
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.text_label = new System.Windows.Forms.Label();
            this.yes_button = new System.Windows.Forms.Button();
            this.no_button = new System.Windows.Forms.Button();
            this.nag_checkbox = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // text_label
            // 
            this.text_label.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.text_label.AutoSize = true;
            this.text_label.Location = new System.Drawing.Point(12, 9);
            this.text_label.MaximumSize = new System.Drawing.Size(700, 0);
            this.text_label.Name = "text_label";
            this.text_label.Size = new System.Drawing.Size(108, 13);
            this.text_label.TabIndex = 0;
            this.text_label.Text = "Question goes here...";
            // 
            // yes_button
            // 
            this.yes_button.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.yes_button.DialogResult = System.Windows.Forms.DialogResult.Yes;
            this.yes_button.Location = new System.Drawing.Point(243, 66);
            this.yes_button.Name = "yes_button";
            this.yes_button.Size = new System.Drawing.Size(75, 23);
            this.yes_button.TabIndex = 1;
            this.yes_button.Text = "Yes";
            this.yes_button.UseVisualStyleBackColor = true;
            // 
            // no_button
            // 
            this.no_button.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.no_button.DialogResult = System.Windows.Forms.DialogResult.No;
            this.no_button.Location = new System.Drawing.Point(162, 66);
            this.no_button.Name = "no_button";
            this.no_button.Size = new System.Drawing.Size(75, 23);
            this.no_button.TabIndex = 2;
            this.no_button.Text = "No";
            this.no_button.UseVisualStyleBackColor = true;
            // 
            // nag_checkbox
            // 
            this.nag_checkbox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.nag_checkbox.AutoSize = true;
            this.nag_checkbox.Location = new System.Drawing.Point(12, 70);
            this.nag_checkbox.Name = "nag_checkbox";
            this.nag_checkbox.Size = new System.Drawing.Size(100, 17);
            this.nag_checkbox.TabIndex = 3;
            this.nag_checkbox.Text = "Don\'t ask again";
            this.nag_checkbox.UseVisualStyleBackColor = true;
            // 
            // Choice
            // 
            this.AcceptButton = this.yes_button;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.no_button;
            this.ClientSize = new System.Drawing.Size(330, 101);
            this.Controls.Add(this.nag_checkbox);
            this.Controls.Add(this.no_button);
            this.Controls.Add(this.yes_button);
            this.Controls.Add(this.text_label);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(346, 103);
            this.Name = "Choice";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Choice";
            this.TopMost = true;
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label text_label;
        private System.Windows.Forms.Button yes_button;
        private System.Windows.Forms.Button no_button;
        private System.Windows.Forms.CheckBox nag_checkbox;
    }
}