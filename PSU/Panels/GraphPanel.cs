﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using System.Diagnostics;
using System.Linq;
using System.Xml;
using static Locomo.Extensions;
using System.Collections.Generic;
using Microsoft.VisualBasic.FileIO;

namespace Locomo.Panels
{
    [Ctrls.ChildPanel("Graph")]
    public partial class GraphPanel : Ctrls.ChildPanel
    {
        private bool show_zero_lines = true;

        public GraphPanel() : base()
        {
            Title = "Graph";
            InitializeComponent();
            graph_control.RightLockChanged += Graph1_OnRightLockChanged;
            graph_control.PausedChanged += (sender, e) =>
            {
                toolStripButtonPause.Checked = graph_control.Paused;
            };
        }

        public override Ctrls.ToolStripEx MyToolStrip()
        {
            return toolStrip2;
        }

        private void Graph1_OnRightLockChanged(object sender, PropertyChangedEventArgs e)
        {
            toolStripButtonGoNow.Checked = graph_control.RightLock;
        }

        public Ctrls.GraphControl GraphControl
        {
            get
            {
                return graph_control;
            }
        }

        private void toolStripButtonGoNow_CheckedChanged(object sender, EventArgs e)
        {
            graph_control.RightLock = toolStripButtonGoNow.Checked;
            if (graph_control.RightLock)
            {
                graph_control.Paused = false;
                toolStripButtonPause.Checked = false;
            }
        }

        private void toolStripButtonPause_CheckedChanged(object sender, EventArgs e)
        {
            graph_control.Paused = toolStripButtonPause.Checked;
        }

        public override void LoadFrom(XmlElement e)
        {
            foreach (XmlElement g in e.SelectNodes("Graph"))
            {
                int id = g.Load("ConnectionID", -1);
                Device d = Device.FindDeviceByConnectionID(id);
                Channel channel = Channel.None;
                g.Load("Channel", ref channel);
                if (d != null && channel != Channel.None)
                {
                    var s = d.GetStream(channel);
                    if (s != null)
                    {
                        Color c = s.color;
                        double low = g.Load("Low", 0.0);
                        double high = g.Load("High", s.Range);
                        g.LoadColor("Color", ref c);
                        var dg = AddOne(d, s, c);
                        dg.ZoomTo(low, high);
                    }
                    graph_control.LoadFrom(e);
                }
            }
            show_zero_lines = e.Load("ShowZeroLines", show_zero_lines);
            base.LoadFrom(e);
        }

        public override void SaveTo(XmlElement e)
        {
            e.Save("ShowZeroLines", show_zero_lines);
            GraphControl.SaveTo(e);
            base.SaveTo(e);
        }

        public Ctrls.Graph AddOne(Stream source)
        {
            source?.Device?.StartMonitor();
            var r = GraphControl.AddNewGraph(source?.Device, source, global_timebase);
            GraphControl.SelectGraph(r);
            return r;
        }

        public Ctrls.Graph AddOne(Device d, Stream source, Color color)
        {
            d.StartMonitor();
            return GraphControl.AddNewGraph(d, source, global_timebase, color);
        }

        private void RemoveOne(Device d, Stream source)
        {
            GraphControl.RemoveDataSet(d, source);
        }

        private static void SetupAddMenu(Ctrls.GraphControl Graph, ToolStripMenuItem m, EventHandler event_handler)
        {
            foreach (Device d in Device.Devices)
            {
                ToolStripMenuItem i = new ToolStripMenuItem(d.DisplayName);
                m.DropDownItems.Add(i);
                foreach (Stream s in d.Streams)
                {
                    ToolStripMenuItem a = new ToolStripMenuItem(s.Name);
                    a.Checked = Graph.IsShowing(s);
                    a.Tag = s;
                    a.Click += event_handler;
                    i.DropDownItems.Add(a);
                }
            }
        }

        private void MenuItemClicked(object sender, EventArgs e)
        {
            var item = sender as ToolStripMenuItem;
            if (item != null)
            {
                var p = item.Tag as Stream;
                if (!item.Checked)
                {
                    AddOne(p);
                }
                else
                {
                    RemoveOne((SerialDevice)p.Device, p);
                }
            }
        }

        public override bool ConfigMenu(ToolStripItemCollection c)
        {
            var settings = new ToolStripMenuItem("Settings");
            var show_zero_lines_menuitem = new ToolStripMenuItem("Always show zero lines", null, (sender, event_args) => {
                show_zero_lines = !show_zero_lines;
                graph_control.show_zero_lines = show_zero_lines;
            });
            show_zero_lines_menuitem.Checked = show_zero_lines;
            settings.DropDownItems.Add(show_zero_lines_menuitem);
            c.Add(settings);
            return true;
        }

        private void showdata_toolStripButton_Click(object sender, EventArgs e)
        {
            var chooser = new Ctrls.ContainerChooser(FindForm(), typeof(Ctrls.ChildPanel), true);
            chooser.Click += (s, e2) =>
            {
                Ctrls.ContainerChooser.SplitType split_type;
                var chosen = chooser.End(out split_type);
                if ((e2 as MouseEventArgs)?.Button == MouseButtons.Left)
                {
                    var new_panel = (chosen?.ActivePanel) as DataGridPanel;
                    if(new_panel == null || split_type != Ctrls.ContainerChooser.SplitType.Fill)
                    {
                        new_panel = new DataGridPanel();
                        chosen.AddControl(new_panel, true);
                    }
                    // what panel do they want to drop it on?
                    List<Stream> streams = new List<Stream>();
                    streams.AddRange(graph_control.AllStreams);
                    new_panel.AddStreams(streams);
                }
            };
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            StreamChooser s = new StreamChooser();
            s.Text = "Add stream";
            s.AddingStreamNode += (sendr, eva) =>
            {
                if (graph_control.AllStreams.Contains(eva.stream))
                {
                    eva.node.ForeColor = Color.Blue;
                }
            };
            if (s.ShowDialog(this) == DialogResult.OK)
            {
                AddOne(s.SelectedStream);
            }
        }

        private void load_toolStripButton_Click(object sender, EventArgs e)
        {
            var f = new OpenFileDialog();
            f.CheckPathExists = true;
            f.AddExtension = true;
            f.DefaultExt = "csv";
            f.Filter = "CSV Files (*.csv)|*.csv|Stream Files (*.locomo_stream)|*.locomo_stream|All Files (*.*)|*.*";
            if(f.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    using (TextFieldParser parser = new TextFieldParser(f.FileName))
                    {
                        parser.SetDelimiters(",");
                        string[] line_one = parser.ReadFields();
                        if (line_one.Length != 1 || line_one[0] != "LOCOMO")
                        {
                            throw new CSVParseException("LOCOMO at line 1 not found");
                        }
                        string[] line_two = parser.ReadFields();
                        if (line_two.Length != 2 || line_two[0] != "Channels")
                        {
                            throw new CSVParseException("Channel count at line 2 not found");
                        }
                        int channel_count;
                        if (!int.TryParse(line_two[1], out channel_count))
                        {
                            throw new CSVParseException("Channel count invalid");
                        }
                        var line_three = parser.ReadFields();
                        if (line_three.Length != channel_count * 2)
                        {
                            throw new CSVParseException("Channel count mismatch from column header line");
                        }
                        for(int i=0; i<channel_count; ++i)
                        {
                            if (line_three[i * 2] != "Time")
                            {
                                throw new CSVParseException("Column " + i * 2 + " is not 'Time' (and it should be)");
                            }
                        }
                        // streams 0..N-1 are line_three[1..N]
                        // Now create some streams (with Device = NULL!)
                        // then fill in the datasets
                        List<DummyStream<double>> dummies = new List<DummyStream<double>>();
                        for(int i=0; i<channel_count; ++i)
                        {
                            Channel c;
                            if (!Enum.TryParse(line_three[i * 2 + 1], out c))
                            {
                                throw new CSVParseException("Unknown channel " + line_three[i + 1]);
                            }
                            var ds = new DummyStream<double>(null, c, 100000, new TimeSpan(), global_timebase);
                            ds.StartNewSection();
                            dummies.Add(ds);
                        }
                        while (!parser.EndOfData)
                        {
                            string[] current_line = parser.ReadFields();
                            for (int i = 0; i < channel_count; ++i)
                            {
                                string time = current_line[i * 2];
                                string s = current_line[i * 2 + 1];
                                if (string.IsNullOrEmpty(time) || string.IsNullOrEmpty(s))
                                {
                                    dummies[i].ResetPacketCount();
                                }
                                else
                                {
                                    TimeSpan t;
                                    double d;
                                    if (!TimeSpan.TryParse(time, out t))
                                    {
                                        throw new CSVParseException("Invalid TimeSpan at line " + parser.LineNumber);
                                    }
                                    if (!double.TryParse(s, out d))
                                    {
                                        throw new CSVParseException("Invalid number '" + s + "' at line " + parser.LineNumber);
                                    }
                                    dummies[i].AddReading(t, d);
                                }
                            }
                        }
                        foreach(Stream s in dummies)
                        {
                            AddOne(s);
                        }
                    }
                }
                catch(CSVParseException err)
                {
                    MessageBox.Show("Error loading " + f.FileName + "\n\n" + err.Message);
                }
            }
        }

        private void export_toolStripButton_Click(object sender, EventArgs e)
        {
            ExportAllStreams(this, graph_control.AllStreams);
        }
    }

    public class CSVParseException: Exception
    {
        public CSVParseException(string message): base(message)
        {

        }
    }
}
