﻿namespace Locomo.Panels
{
    partial class GraphPanel
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();

            this.toolStrip2 = new Locomo.Ctrls.ToolStripEx();
            this.toolStripButtonGoNow = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonPause = new System.Windows.Forms.ToolStripButton();
            this.showdata_toolStripButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.load_toolStripButton = new System.Windows.Forms.ToolStripButton();
            this.graph_control = new Locomo.Ctrls.GraphControl();
            this.export_toolStripButton = new System.Windows.Forms.ToolStripButton();
            this.toolStrip2.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStrip2
            // 
            this.toolStrip2.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStrip2.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButtonGoNow,
            this.toolStripButtonPause,
            this.showdata_toolStripButton,
            this.toolStripButton1,
            this.load_toolStripButton,
            this.export_toolStripButton});
            this.toolStrip2.Location = new System.Drawing.Point(1, 0);
            this.toolStrip2.Name = "toolStrip2";
            this.toolStrip2.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.toolStrip2.Size = new System.Drawing.Size(172, 25);
            this.toolStrip2.TabIndex = 4;
            this.toolStrip2.Text = "toolStrip2";
            this.toolStrip2.Visible = false;
            // 
            // toolStripButtonGoNow
            // 
            this.toolStripButtonGoNow.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripButtonGoNow.CheckOnClick = true;
            this.toolStripButtonGoNow.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonGoNow.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.toolStripButtonGoNow.Image = global::Locomo.Properties.Resources.forward;
            this.toolStripButtonGoNow.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonGoNow.Name = "toolStripButtonGoNow";
            this.toolStripButtonGoNow.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonGoNow.ToolTipText = "Go to now";
            this.toolStripButtonGoNow.CheckedChanged += new System.EventHandler(this.toolStripButtonGoNow_CheckedChanged);
            // 
            // toolStripButtonPause
            // 
            this.toolStripButtonPause.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripButtonPause.CheckOnClick = true;
            this.toolStripButtonPause.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonPause.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.toolStripButtonPause.Image = global::Locomo.Properties.Resources.pause;
            this.toolStripButtonPause.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonPause.Name = "toolStripButtonPause";
            this.toolStripButtonPause.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonPause.CheckedChanged += new System.EventHandler(this.toolStripButtonPause_CheckedChanged);
            // 
            // showdata_toolStripButton
            // 
            this.showdata_toolStripButton.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.showdata_toolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.showdata_toolStripButton.Image = global::Locomo.Properties.Resources.data;
            this.showdata_toolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.showdata_toolStripButton.Name = "showdata_toolStripButton";
            this.showdata_toolStripButton.Size = new System.Drawing.Size(23, 22);
            this.showdata_toolStripButton.Text = "Show Data";
            this.showdata_toolStripButton.ToolTipText = "Show Data";
            this.showdata_toolStripButton.Click += new System.EventHandler(this.showdata_toolStripButton_Click);
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton1.Image = global::Locomo.Properties.Resources.PlusButton;
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton1.Text = "toolStripButton1";
            this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // load_toolStripButton
            // 
            this.load_toolStripButton.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.load_toolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.load_toolStripButton.Image = global::Locomo.Properties.Resources.open_file2;
            this.load_toolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.load_toolStripButton.Name = "load_toolStripButton";
            this.load_toolStripButton.Size = new System.Drawing.Size(23, 22);
            this.load_toolStripButton.Text = "Load data";
            this.load_toolStripButton.ToolTipText = "Load data";
            this.load_toolStripButton.Click += new System.EventHandler(this.load_toolStripButton_Click);
            // 
            // graph_control
            // 
            this.graph_control.AllowDrop = true;
            this.graph_control.BackColor = System.Drawing.Color.Black;
            this.graph_control.Dock = System.Windows.Forms.DockStyle.Fill;
            this.graph_control.Location = new System.Drawing.Point(0, 0);
            this.graph_control.Name = "graph_control";
            this.graph_control.Paused = false;
            this.graph_control.RightLock = true;
            this.graph_control.Size = new System.Drawing.Size(460, 433);
            this.graph_control.TabIndex = 3;
            // 
            // export_toolStripButton
            // 
            this.export_toolStripButton.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.export_toolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.export_toolStripButton.Image = global::Locomo.Properties.Resources.export;
            this.export_toolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.export_toolStripButton.Name = "export_toolStripButton";
            this.export_toolStripButton.Size = new System.Drawing.Size(23, 22);
            this.export_toolStripButton.Text = "toolStripButton2";
            this.export_toolStripButton.Click += new System.EventHandler(this.export_toolStripButton_Click);
            // 
            // GraphPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.toolStrip2);
            this.Controls.Add(this.graph_control);
            this.MinimumSize = new System.Drawing.Size(200, 128);
            this.Name = "GraphPanel";
            this.Size = new System.Drawing.Size(460, 433);
            this.toolStrip2.ResumeLayout(false);
            this.toolStrip2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Ctrls.GraphControl graph_control;
        private Ctrls.ToolStripEx toolStrip2;
        private System.Windows.Forms.ToolStripButton toolStripButtonPause;
        private System.Windows.Forms.ToolStripButton toolStripButtonGoNow;
        private System.Windows.Forms.ToolStripButton showdata_toolStripButton;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.ToolStripButton load_toolStripButton;
        private System.Windows.Forms.ToolStripButton export_toolStripButton;
    }
}
