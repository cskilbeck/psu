﻿namespace Locomo.Panels
{
    partial class VoltsPanel
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.rangeSlider1 = new Locomo.Ctrls.RangeSlider();
            this.SuspendLayout();
            // 
            // rangeSlider1
            // 
            this.rangeSlider1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rangeSlider1.HeightPadding = 4;
            this.rangeSlider1.Location = new System.Drawing.Point(0, 0);
            this.rangeSlider1.Name = "rangeSlider1";
            this.rangeSlider1.RangeLimit = 0.0099999997764825821D;
            this.rangeSlider1.RangeMax = 20D;
            this.rangeSlider1.RangeMin = 0D;
            this.rangeSlider1.ShowMinMax = true;
            this.rangeSlider1.Size = new System.Drawing.Size(223, 624);
            this.rangeSlider1.TabIndex = 0;
            this.rangeSlider1.ValueBackColor = System.Drawing.SystemColors.WindowText;
            this.rangeSlider1.ValueBorderColor = System.Drawing.SystemColors.ControlDark;
            this.rangeSlider1.ValueFont = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold);
            this.rangeSlider1.ValueTextColor = System.Drawing.Color.MediumSpringGreen;
            // 
            // VoltsPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.Controls.Add(this.rangeSlider1);
            this.Name = "VoltsPanel";
            this.Size = new System.Drawing.Size(223, 624);
            this.ResumeLayout(false);

        }

        #endregion

        private Ctrls.RangeSlider rangeSlider1;
    }
}
