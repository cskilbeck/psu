﻿using System;
using System.Windows.Forms;
using Locomo.Devices;
using System.Diagnostics;

namespace Locomo.Panels
{
    [Ctrls.ChildPanel("Volts")]
    public partial class VoltsPanel : DevicePanel
    {
        [Ctrls.SetupMenu]
        public static void SetupMenu(ToolStripMenuItem m, Ctrls.PanelContainer parent, Type type)
        {
            SetupDeviceMenu(m, parent, type);
        }

        public VoltsPanel()
        {
            panel_title = "PSU";
            InitializeComponent();
            ValueStuffer volts_stuff = new ValueStuffer { value_send_command = "volt" };
            volts_stuff.Stuffing += (is_final) => { ((Device_6612C)device).SetTracking(Device_6612C.TRACK_SETVOLTS, is_final); };
            rangeSlider1.OnNewValue += (v, finalValue) => { volts_stuff.StuffIt(v, finalValue, device); };
        }

        public override void Connect(SCPIDevice scpi_device)
        {
            base.Connect(scpi_device);
            if(device != null)
            {
                var volts_data_source = device.GetStream(Channel.VoltsDC);
                var setvolts_data_source = device.GetStream(Channel.SetVolts);
                volts_data_source.DataReady += (sender, rc) =>
                {
                    var result = (Result<double>)rc;
                    UIThread(() => { rangeSlider1.SetTickValue(result.value); });
                };
                setvolts_data_source.DataReady += (sender, rc) =>
                {
                    var result = (Result<double>)rc;
                    UIThread(() => { rangeSlider1.SetValue(result.value, true, false); });
                };
            }
        }
    }
}
