﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;
using System.Xml;

namespace Locomo.Panels
{
    [Ctrls.ChildPanel("PSU Readout")]
    public partial class PSUReadout : DevicePanel
    {
        [Ctrls.SetupMenu]
        public static void SetupMenu(ToolStripMenuItem m, Ctrls.PanelContainer parent, Type type)
        {
            SetupDeviceMenu(m, parent, type);
        }

        const int CC_MINUS = 2048;
        const int CC_PLUS = 1024;
        const int CV = 256;

        private bool power_is_on = false;

        string[] cc_texts = new string[3]
        {
                "CC-", "CC", "CC+"
        };

        Color[] cc_colors = new Color[3]
        {
                Color.Aqua,
                Color.FromArgb(64, 64, 64),
                Color.Aqua
        };

        public PSUReadout()
        {
            panel_title = "PSU";
            InitializeComponent();
        }

        static string Formatter(double value)
        {
            string s = Math.Abs(value) >= 10 ? "{0: 00.0000000;-00.0000000}" : "{0:  0.0000000;- 0.0000000}";
            return string.Format(s, value);
        }

        public override void Connect(SCPIDevice device)
        {
            base.Connect(device);
            if (device != null)
            {
                var volts_data_source = base.device.GetStream(Channel.VoltsDC);
                var amps_data_source = base.device.GetStream(Channel.AmpsDC);
                var output_data_source = base.device.GetStream(Channel.Output);
                var cc_data_source = base.device.GetStream(Channel.ConstantCurrent);
                var cv_data_source = base.device.GetStream(Channel.ConstantVoltage);

                volts_data_source.DataReady += (sender, result) =>
                {
                    UIThread(() => { voltsLabel.Text = Formatter(((Result<double>)result).value); });
                };

                amps_data_source.DataReady += (sender, result) =>
                {
                    UIThread(() => { currentLabel.Text = Formatter(((Result<double>)result).value); });
                };

                output_data_source.DataReady += (sender, rc) =>
                {
                    UIThread(() =>
                    {
                        var result = (Result<bool>)rc;
                        power_is_on = ((Result<bool>)result).value;
                        power_toggle_button.Enabled = true;
                        power_toggle_button.ForeColor = result.value ? Color.FromArgb(0, 255, 0) : Color.FromArgb(255, 0, 0);
                        power_toggle_button.Text = result.value ? "ON" : "OFF";
                        power_toggle_button.ImageIndex = result.value ? 0 : 1;
                    });
                };

                cc_data_source.DataReady += (sender, rc) =>
                {
                    UIThread(() =>
                    {
                        var result = (Result<int>)rc;
                        int r = result.value + 1;
                        if (r >= 0 && r < 3)
                        {
                            ccLabel.Text = cc_texts[r];
                            ccLabel.ForeColor = cc_colors[r];
                        }
                    });
                };

                cv_data_source.DataReady += (sender, rc) =>
                {
                    UIThread(() =>
                    {
                        var result = (Result<bool>)rc;
                        cvLabel.ForeColor = result.value ? Color.MediumSpringGreen : Color.FromArgb(64, 64, 64);
                    });
                };
            }
        }

        private void offButton_Click(object sender, EventArgs e)
        {
            PowerOff();
        }

        private void onButton_Click(object sender, EventArgs e)
        {
            PowerOn();
        }

        private void powerToggleButtonClick(object sender, EventArgs e)
        {
            PowerToggle();
        }

        void DisablePowerButton()
        {
            power_toggle_button.ForeColor = Color.White;
            power_toggle_button.Enabled = false;
            power_toggle_button.Refresh();
        }

        void PowerOff()
        {
            if (device != null)
            {
                DisablePowerButton();
                device.PutString("outp 0");
            }
        }

        void PowerOn()
        {
            if (device != null)
            {
                DisablePowerButton();
                device.PutString("outp 1");
            }
        }

        void PowerToggle()
        {
            if (device != null)
            {
                DisablePowerButton();
                device.PutString("outp " + (power_is_on ? "0" : "1"));
            }
        }
    }
}
