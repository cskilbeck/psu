﻿namespace Locomo.Panels
{
    partial class PSUPanel
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.rangeSlider1 = new Locomo.Ctrls.RangeSlider();
            this.rangeSlider2 = new Locomo.Ctrls.RangeSlider();
            this.panel4 = new System.Windows.Forms.Panel();
            this.cvLabel = new System.Windows.Forms.Label();
            this.ccLabel = new System.Windows.Forms.Label();
            this.powerPanel = new System.Windows.Forms.Panel();
            this.power_toggle_button = new System.Windows.Forms.Button();
            this.onButton = new System.Windows.Forms.Button();
            this.offButton = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.currentLabel = new System.Windows.Forms.Label();
            this.voltsLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.panel4.SuspendLayout();
            this.powerPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer1.Location = new System.Drawing.Point(0, 77);
            this.splitContainer1.Margin = new System.Windows.Forms.Padding(0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.rangeSlider1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.rangeSlider2);
            this.splitContainer1.Size = new System.Drawing.Size(428, 543);
            this.splitContainer1.SplitterDistance = 243;
            this.splitContainer1.SplitterWidth = 1;
            this.splitContainer1.TabIndex = 45;
            // 
            // rangeSlider1
            // 
            this.rangeSlider1.BackColor = System.Drawing.SystemColors.Control;
            this.rangeSlider1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rangeSlider1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.rangeSlider1.HeightPadding = 2;
            this.rangeSlider1.Location = new System.Drawing.Point(0, 0);
            this.rangeSlider1.Margin = new System.Windows.Forms.Padding(0);
            this.rangeSlider1.MinimumSize = new System.Drawing.Size(59, 200);
            this.rangeSlider1.Name = "rangeSlider1";
            this.rangeSlider1.RangeLimit = 0.10000000149011612D;
            this.rangeSlider1.RangeMax = 20D;
            this.rangeSlider1.RangeMin = 0D;
            this.rangeSlider1.ShowMinMax = true;
            this.rangeSlider1.Size = new System.Drawing.Size(243, 543);
            this.rangeSlider1.TabIndex = 28;
            this.rangeSlider1.ValueBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.rangeSlider1.ValueBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(160)))), ((int)(((byte)(160)))), ((int)(((byte)(160)))));
            this.rangeSlider1.ValueFont = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rangeSlider1.ValueTextColor = System.Drawing.Color.Cyan;
            // 
            // rangeSlider2
            // 
            this.rangeSlider2.BackColor = System.Drawing.SystemColors.Control;
            this.rangeSlider2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rangeSlider2.HeightPadding = 2;
            this.rangeSlider2.Location = new System.Drawing.Point(0, 0);
            this.rangeSlider2.Margin = new System.Windows.Forms.Padding(0);
            this.rangeSlider2.MinimumSize = new System.Drawing.Size(59, 200);
            this.rangeSlider2.Name = "rangeSlider2";
            this.rangeSlider2.RangeLimit = 0.0099999997764825821D;
            this.rangeSlider2.RangeMax = 2D;
            this.rangeSlider2.RangeMin = 0D;
            this.rangeSlider2.ShowMinMax = false;
            this.rangeSlider2.Size = new System.Drawing.Size(184, 543);
            this.rangeSlider2.TabIndex = 30;
            this.rangeSlider2.ValueBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.rangeSlider2.ValueBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(160)))), ((int)(((byte)(160)))), ((int)(((byte)(160)))));
            this.rangeSlider2.ValueFont = new System.Drawing.Font("Microsoft Sans Serif", 13.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rangeSlider2.ValueTextColor = System.Drawing.Color.MediumSpringGreen;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.Black;
            this.panel4.Controls.Add(this.cvLabel);
            this.panel4.Controls.Add(this.ccLabel);
            this.panel4.Location = new System.Drawing.Point(369, -1);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(55, 78);
            this.panel4.TabIndex = 44;
            // 
            // cvLabel
            // 
            this.cvLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cvLabel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.cvLabel.Font = new System.Drawing.Font("Consolas", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cvLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.cvLabel.Location = new System.Drawing.Point(3, 40);
            this.cvLabel.Name = "cvLabel";
            this.cvLabel.Size = new System.Drawing.Size(49, 23);
            this.cvLabel.TabIndex = 1;
            this.cvLabel.Text = "CV";
            this.cvLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // ccLabel
            // 
            this.ccLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.ccLabel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.ccLabel.Font = new System.Drawing.Font("Consolas", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ccLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.ccLabel.Location = new System.Drawing.Point(3, 10);
            this.ccLabel.Name = "ccLabel";
            this.ccLabel.Size = new System.Drawing.Size(49, 23);
            this.ccLabel.TabIndex = 0;
            this.ccLabel.Text = "CC";
            this.ccLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // powerPanel
            // 
            this.powerPanel.BackColor = System.Drawing.SystemColors.WindowText;
            this.powerPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.powerPanel.Controls.Add(this.power_toggle_button);
            this.powerPanel.Controls.Add(this.onButton);
            this.powerPanel.Controls.Add(this.offButton);
            this.powerPanel.Location = new System.Drawing.Point(240, -1);
            this.powerPanel.Name = "powerPanel";
            this.powerPanel.Size = new System.Drawing.Size(128, 78);
            this.powerPanel.TabIndex = 43;
            // 
            // power_toggle_button
            // 
            this.power_toggle_button.BackColor = System.Drawing.Color.Black;
            this.power_toggle_button.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.power_toggle_button.FlatAppearance.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.power_toggle_button.FlatAppearance.BorderSize = 0;
            this.power_toggle_button.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.power_toggle_button.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.power_toggle_button.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.power_toggle_button.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.power_toggle_button.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.power_toggle_button.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.power_toggle_button.ImageIndex = 1;
            this.power_toggle_button.Location = new System.Drawing.Point(33, 0);
            this.power_toggle_button.Name = "power_toggle_button";
            this.power_toggle_button.Size = new System.Drawing.Size(64, 78);
            this.power_toggle_button.TabIndex = 28;
            this.power_toggle_button.TabStop = false;
            this.power_toggle_button.Text = "OFF";
            this.power_toggle_button.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.power_toggle_button.UseVisualStyleBackColor = false;
            this.power_toggle_button.Click += new System.EventHandler(this.powerToggleButtonClick);
            // 
            // onButton
            // 
            this.onButton.BackColor = System.Drawing.Color.Black;
            this.onButton.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.onButton.FlatAppearance.BorderSize = 0;
            this.onButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.onButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Black;
            this.onButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.onButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.onButton.Image = global::Locomo.Properties.Resources.On;
            this.onButton.Location = new System.Drawing.Point(96, 0);
            this.onButton.Name = "onButton";
            this.onButton.Size = new System.Drawing.Size(32, 78);
            this.onButton.TabIndex = 27;
            this.onButton.TabStop = false;
            this.onButton.UseVisualStyleBackColor = false;
            this.onButton.Click += new System.EventHandler(this.onButton_Click);
            // 
            // offButton
            // 
            this.offButton.BackColor = System.Drawing.Color.Black;
            this.offButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.offButton.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.offButton.FlatAppearance.BorderSize = 0;
            this.offButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.offButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Black;
            this.offButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.offButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.offButton.Image = global::Locomo.Properties.Resources.Off;
            this.offButton.Location = new System.Drawing.Point(0, 0);
            this.offButton.Name = "offButton";
            this.offButton.Size = new System.Drawing.Size(32, 78);
            this.offButton.TabIndex = 26;
            this.offButton.TabStop = false;
            this.offButton.UseVisualStyleBackColor = false;
            this.offButton.Click += new System.EventHandler(this.offButton_Click);
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.Color.Black;
            this.label4.Font = new System.Drawing.Font("Consolas", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.MediumSpringGreen;
            this.label4.Location = new System.Drawing.Point(209, 39);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(30, 38);
            this.label4.TabIndex = 42;
            this.label4.Text = "A";
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.Black;
            this.label2.Font = new System.Drawing.Font("Consolas", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Aqua;
            this.label2.Location = new System.Drawing.Point(209, 0);
            this.label2.Margin = new System.Windows.Forms.Padding(0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(30, 38);
            this.label2.TabIndex = 41;
            this.label2.Text = "V";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // currentLabel
            // 
            this.currentLabel.BackColor = System.Drawing.Color.Black;
            this.currentLabel.Font = new System.Drawing.Font("Lucida Sans Typewriter", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.currentLabel.ForeColor = System.Drawing.Color.MediumSpringGreen;
            this.currentLabel.Location = new System.Drawing.Point(0, 39);
            this.currentLabel.Name = "currentLabel";
            this.currentLabel.Size = new System.Drawing.Size(208, 38);
            this.currentLabel.TabIndex = 40;
            this.currentLabel.Text = "-0.00000000000";
            this.currentLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // voltsLabel
            // 
            this.voltsLabel.BackColor = System.Drawing.Color.Black;
            this.voltsLabel.Font = new System.Drawing.Font("Lucida Sans Typewriter", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.voltsLabel.ForeColor = System.Drawing.Color.Aqua;
            this.voltsLabel.Location = new System.Drawing.Point(0, 0);
            this.voltsLabel.Margin = new System.Windows.Forms.Padding(0);
            this.voltsLabel.Name = "voltsLabel";
            this.voltsLabel.Size = new System.Drawing.Size(208, 38);
            this.voltsLabel.TabIndex = 39;
            this.voltsLabel.Text = "-0.000000000";
            this.voltsLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // PSUPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.powerPanel);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.currentLabel);
            this.Controls.Add(this.voltsLabel);
            this.Margin = new System.Windows.Forms.Padding(0);
            this.Name = "PSUPanel";
            this.Size = new System.Drawing.Size(428, 620);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.powerPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label currentLabel;
        private System.Windows.Forms.Label voltsLabel;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label cvLabel;
        private System.Windows.Forms.Label ccLabel;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private Ctrls.RangeSlider rangeSlider1;
        private Ctrls.RangeSlider rangeSlider2;
        private System.Windows.Forms.Button offButton;
        private System.Windows.Forms.Button onButton;
        private System.Windows.Forms.Button power_toggle_button;
        private System.Windows.Forms.Panel powerPanel;
    }
}
