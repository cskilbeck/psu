﻿using System;
using System.Reflection;
using System.Windows.Forms;
using static Locomo.Extensions;

namespace Locomo.Panels
{
    [Ctrls.ChildPanel("Settings")]
    public class SettingsPanel: Ctrls.ChildPanel
    {
        private CheckBox checkBox2;
        private CheckBox checkBox1;
        private BindingSource configBindingSource;
        private System.ComponentModel.IContainer components;
        private CheckBox checkBox3;
        private Label label1;
        private Button save_button;
        private Button save_as_button;
        private CheckBox titlebar_checkbox;
        private CheckBox resizable_checkbox;
        private CheckBox ontop_checkbox;
        private FlowLayoutPanel flowLayoutPanel1;

        public SettingsPanel()
        {
            panel_title = "Settings";
            InitializeComponent();
            configBindingSource.DataSource = Config.Settings;
            Config.SettingsLoaded += () =>
            {
                configBindingSource.DataSource = Config.Settings;
            };
        }

        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.configBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.checkBox3 = new System.Windows.Forms.CheckBox();
            this.titlebar_checkbox = new System.Windows.Forms.CheckBox();
            this.resizable_checkbox = new System.Windows.Forms.CheckBox();
            this.ontop_checkbox = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.save_button = new System.Windows.Forms.Button();
            this.save_as_button = new System.Windows.Forms.Button();
            this.flowLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.configBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.AutoSize = true;
            this.flowLayoutPanel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.flowLayoutPanel1.BackColor = System.Drawing.SystemColors.Window;
            this.flowLayoutPanel1.Controls.Add(this.checkBox2);
            this.flowLayoutPanel1.Controls.Add(this.checkBox1);
            this.flowLayoutPanel1.Controls.Add(this.checkBox3);
            this.flowLayoutPanel1.Controls.Add(this.titlebar_checkbox);
            this.flowLayoutPanel1.Controls.Add(this.resizable_checkbox);
            this.flowLayoutPanel1.Controls.Add(this.ontop_checkbox);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.flowLayoutPanel1.Margin = new System.Windows.Forms.Padding(20);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Padding = new System.Windows.Forms.Padding(10, 30, 10, 40);
            this.flowLayoutPanel1.Size = new System.Drawing.Size(303, 226);
            this.flowLayoutPanel1.TabIndex = 0;
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.DataBindings.Add(new System.Windows.Forms.Binding("CheckState", this.configBindingSource, "AlwaysPin", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.checkBox2.Location = new System.Drawing.Point(13, 33);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(116, 17);
            this.checkBox2.TabIndex = 1;
            this.checkBox2.Text = "Always pin toolbars";
            this.checkBox2.UseVisualStyleBackColor = true;
            // 
            // configBindingSource
            // 
            this.configBindingSource.DataSource = typeof(Locomo.Config);
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.DataBindings.Add(new System.Windows.Forms.Binding("CheckState", this.configBindingSource, "AutoShowToolBar", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.checkBox1.Location = new System.Drawing.Point(13, 56);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(116, 17);
            this.checkBox1.TabIndex = 0;
            this.checkBox1.Text = "Auto-show toolbars";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // checkBox3
            // 
            this.checkBox3.AutoSize = true;
            this.checkBox3.DataBindings.Add(new System.Windows.Forms.Binding("CheckState", this.configBindingSource, "AutoHideToolBar", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.checkBox3.Location = new System.Drawing.Point(13, 79);
            this.checkBox3.Name = "checkBox3";
            this.checkBox3.Size = new System.Drawing.Size(111, 17);
            this.checkBox3.TabIndex = 2;
            this.checkBox3.Text = "Auto-hide toolbars";
            this.checkBox3.UseVisualStyleBackColor = true;
            // 
            // titlebar_checkbox
            // 
            this.titlebar_checkbox.AutoSize = true;
            this.titlebar_checkbox.DataBindings.Add(new System.Windows.Forms.Binding("CheckState", this.configBindingSource, "TitleBar", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.titlebar_checkbox.Location = new System.Drawing.Point(13, 102);
            this.titlebar_checkbox.Name = "titlebar_checkbox";
            this.titlebar_checkbox.Size = new System.Drawing.Size(65, 17);
            this.titlebar_checkbox.TabIndex = 3;
            this.titlebar_checkbox.Text = "Title Bar";
            this.titlebar_checkbox.UseVisualStyleBackColor = true;
            // 
            // resizable_checkbox
            // 
            this.resizable_checkbox.AutoSize = true;
            this.resizable_checkbox.DataBindings.Add(new System.Windows.Forms.Binding("CheckState", this.configBindingSource, "Resizable", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.resizable_checkbox.Location = new System.Drawing.Point(13, 125);
            this.resizable_checkbox.Name = "resizable_checkbox";
            this.resizable_checkbox.Size = new System.Drawing.Size(72, 17);
            this.resizable_checkbox.TabIndex = 4;
            this.resizable_checkbox.Text = "Resizable";
            this.resizable_checkbox.UseVisualStyleBackColor = true;
            // 
            // ontop_checkbox
            // 
            this.ontop_checkbox.AutoSize = true;
            this.ontop_checkbox.DataBindings.Add(new System.Windows.Forms.Binding("CheckState", this.configBindingSource, "OnTop", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.ontop_checkbox.Location = new System.Drawing.Point(13, 148);
            this.ontop_checkbox.Name = "ontop_checkbox";
            this.ontop_checkbox.Size = new System.Drawing.Size(62, 17);
            this.ontop_checkbox.TabIndex = 5;
            this.ontop_checkbox.Text = "On Top";
            this.ontop_checkbox.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(6, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(76, 20);
            this.label1.TabIndex = 1;
            this.label1.Text = "Settings";
            // 
            // save_button
            // 
            this.save_button.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.save_button.Location = new System.Drawing.Point(7, 191);
            this.save_button.Name = "save_button";
            this.save_button.Size = new System.Drawing.Size(75, 23);
            this.save_button.TabIndex = 5;
            this.save_button.Text = "Save";
            this.save_button.UseVisualStyleBackColor = true;
            this.save_button.Click += new System.EventHandler(this.save_button_Click);
            // 
            // save_as_button
            // 
            this.save_as_button.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.save_as_button.Location = new System.Drawing.Point(88, 191);
            this.save_as_button.Name = "save_as_button";
            this.save_as_button.Size = new System.Drawing.Size(75, 23);
            this.save_as_button.TabIndex = 6;
            this.save_as_button.Text = "Save As";
            this.save_as_button.UseVisualStyleBackColor = true;
            this.save_as_button.Click += new System.EventHandler(this.save_as_button_Click);
            // 
            // SettingsPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScroll = true;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.Controls.Add(this.save_button);
            this.Controls.Add(this.save_as_button);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.flowLayoutPanel1);
            this.MinimumSize = new System.Drawing.Size(175, 100);
            this.Name = "SettingsPanel";
            this.Size = new System.Drawing.Size(303, 226);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.configBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        private void save_button_Click(object sender, EventArgs e)
        {
            ((MainForm)FindForm()).SaveSettings(null);
        }

        private void save_as_button_Click(object sender, EventArgs e)
        {
            SaveFileDialog f = new SaveFileDialog();
            f.AddExtension = true;
            f.AutoUpgradeEnabled = true;
            f.DefaultExt = ".locomo";
            f.Filter = "Locomo files|*.locomo||";
            f.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            f.OverwritePrompt = true;
            if (f.ShowDialog() == DialogResult.OK)
            {
                string fullName = Assembly.GetEntryAssembly().Location;
                if(!IsFileAssociationSet(".locomo", "locomo_file", fullName))
                {
                    bool nag = Config.Settings.SupressFileAssociationPrompt;
                    Config.Settings.SetFileAssociation = Choice.Ask(
                        "Set File Association?",
                        "Would you like to associate .locomo files with Locmo?\n\n" + fullName,
                        "Yes", "No",
                        ref nag,
                        Config.Settings.SetFileAssociation, FindForm());
                    Config.Settings.SupressFileAssociationPrompt = nag;
                    if (Config.Settings.SetFileAssociation)
                    {
                        SetFileAssociation(".locomo", "locomo_file", fullName);
                    }
                }
                ((MainForm)FindForm()).SaveSettings(f.FileName);
            }
        }
    }
}
