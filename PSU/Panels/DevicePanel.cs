﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows.Forms;
using System.Xml;
using Logging;

namespace Locomo.Panels
{
    public class DevicePanel: Ctrls.ChildPanel
    {
        public SCPIDevice device;

        public virtual void Connect(SCPIDevice scpi_device)
        {
            device = scpi_device;
            if(device != null)
            {
                device.Connection.ConnectionStateChange -= psu_OnConnectionStateChange;
                device.Connection.ConnectionStateChange += psu_OnConnectionStateChange;
                UpdatePSUConnectionStatus();
                if (device.IsConnected)
                {
                    base.Title = Name + ":" + device.PortName;
                    device.Get("*idn?", (Result<string> r1) =>
                    {
                        if (r1.success)
                        {
                            device.PutString("*cls");
                            StartMonitor();
                        }
                        else
                        {
                            Log.Error("Couldn't get IDN");
                        }
                    });
                }
                else
                {
                    Log.Warning("Device " + Name + " is not connected!");
                    base.Title = Name + ": -";
                }
                StartMonitor();
            }
            else
            {
                Log.Error("Null device!?");
            }
        }

        void UpdatePSUConnectionStatus()
        {
            if (device.Connection.State == ConnectionState.Connecting || device.Connection.State == ConnectionState.Connected)
            {
                Title = Name + ":" + device.PortName;
            }
            else
            {
                Title = Name + ": -";
            }
        }

        void psu_OnConnectionStateChange(object sender)
        {
            UpdatePSUConnectionStatus();
        }

        public virtual void StartMonitor()
        {
            device?.StartMonitor();
        }

        protected virtual void StopMonitor()
        {
            device?.StopMonitor();
        }

        public override void SaveTo(XmlElement e)
        {
            if (device != null)
            {
                e.Save("ConnectionID", device.ConnectionID);
            }
            base.SaveTo(e);
        }

        public override void LoadFrom(XmlElement e)
        {
            Connect((SCPIDevice)Device.FindDeviceByConnectionID(e.Load("ConnectionID", -1)));
        }
    }
}
