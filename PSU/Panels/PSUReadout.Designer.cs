﻿namespace Locomo.Panels
{
    partial class PSUReadout
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.voltsLabel = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.currentLabel = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.cvLabel = new System.Windows.Forms.Label();
            this.power_toggle_button = new System.Windows.Forms.Button();
            this.onButton = new System.Windows.Forms.Button();
            this.ccLabel = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.flowLayoutPanel1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.AutoSize = true;
            this.flowLayoutPanel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.flowLayoutPanel1.BackColor = System.Drawing.Color.Black;
            this.flowLayoutPanel1.Controls.Add(this.panel1);
            this.flowLayoutPanel1.Controls.Add(this.panel2);
            this.flowLayoutPanel1.Controls.Add(this.panel3);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.flowLayoutPanel1.Margin = new System.Windows.Forms.Padding(0);
            this.flowLayoutPanel1.MinimumSize = new System.Drawing.Size(240, 38);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(812, 120);
            this.flowLayoutPanel1.TabIndex = 50;
            // 
            // panel1
            // 
            this.panel1.AutoSize = true;
            this.panel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.voltsLabel);
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(0, 0, 1, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(198, 38);
            this.panel1.TabIndex = 51;
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.Black;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Aqua;
            this.label2.Location = new System.Drawing.Point(170, 0);
            this.label2.Margin = new System.Windows.Forms.Padding(0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(28, 38);
            this.label2.TabIndex = 47;
            this.label2.Text = "V";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // voltsLabel
            // 
            this.voltsLabel.BackColor = System.Drawing.Color.Black;
            this.voltsLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.voltsLabel.ForeColor = System.Drawing.Color.Aqua;
            this.voltsLabel.Location = new System.Drawing.Point(0, 0);
            this.voltsLabel.Margin = new System.Windows.Forms.Padding(0);
            this.voltsLabel.Name = "voltsLabel";
            this.voltsLabel.Size = new System.Drawing.Size(170, 38);
            this.voltsLabel.TabIndex = 46;
            this.voltsLabel.Text = "-0.000000000";
            this.voltsLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel2
            // 
            this.panel2.AutoSize = true;
            this.panel2.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panel2.Controls.Add(this.currentLabel);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Location = new System.Drawing.Point(199, 0);
            this.panel2.Margin = new System.Windows.Forms.Padding(0, 0, 1, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(201, 38);
            this.panel2.TabIndex = 51;
            // 
            // currentLabel
            // 
            this.currentLabel.BackColor = System.Drawing.Color.Black;
            this.currentLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.currentLabel.ForeColor = System.Drawing.Color.MediumSpringGreen;
            this.currentLabel.Location = new System.Drawing.Point(0, 0);
            this.currentLabel.Margin = new System.Windows.Forms.Padding(0);
            this.currentLabel.Name = "currentLabel";
            this.currentLabel.Size = new System.Drawing.Size(170, 38);
            this.currentLabel.TabIndex = 44;
            this.currentLabel.Text = "-0.000000000";
            this.currentLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.Color.Black;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.MediumSpringGreen;
            this.label4.Location = new System.Drawing.Point(170, 0);
            this.label4.Margin = new System.Windows.Forms.Padding(0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(31, 38);
            this.label4.TabIndex = 46;
            this.label4.Text = "A";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel3
            // 
            this.panel3.AutoSize = true;
            this.panel3.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panel3.Controls.Add(this.cvLabel);
            this.panel3.Controls.Add(this.power_toggle_button);
            this.panel3.Controls.Add(this.onButton);
            this.panel3.Controls.Add(this.ccLabel);
            this.panel3.Controls.Add(this.button1);
            this.panel3.Location = new System.Drawing.Point(401, 0);
            this.panel3.Margin = new System.Windows.Forms.Padding(0, 0, 1, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(225, 38);
            this.panel3.TabIndex = 51;
            // 
            // cvLabel
            // 
            this.cvLabel.BackColor = System.Drawing.Color.Black;
            this.cvLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cvLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.cvLabel.Location = new System.Drawing.Point(173, 0);
            this.cvLabel.Margin = new System.Windows.Forms.Padding(0);
            this.cvLabel.Name = "cvLabel";
            this.cvLabel.Size = new System.Drawing.Size(52, 38);
            this.cvLabel.TabIndex = 1;
            this.cvLabel.Text = "CV";
            this.cvLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // power_toggle_button
            // 
            this.power_toggle_button.BackColor = System.Drawing.Color.Black;
            this.power_toggle_button.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.power_toggle_button.FlatAppearance.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.power_toggle_button.FlatAppearance.BorderSize = 0;
            this.power_toggle_button.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.power_toggle_button.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.power_toggle_button.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.power_toggle_button.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.power_toggle_button.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.power_toggle_button.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.power_toggle_button.ImageIndex = 1;
            this.power_toggle_button.Location = new System.Drawing.Point(32, 0);
            this.power_toggle_button.Margin = new System.Windows.Forms.Padding(0);
            this.power_toggle_button.Name = "power_toggle_button";
            this.power_toggle_button.Size = new System.Drawing.Size(54, 38);
            this.power_toggle_button.TabIndex = 28;
            this.power_toggle_button.TabStop = false;
            this.power_toggle_button.Text = "OFF";
            this.power_toggle_button.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.power_toggle_button.UseVisualStyleBackColor = false;
            this.power_toggle_button.Click += new System.EventHandler(this.powerToggleButtonClick);
            // 
            // onButton
            // 
            this.onButton.BackColor = System.Drawing.Color.Black;
            this.onButton.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.onButton.FlatAppearance.BorderSize = 0;
            this.onButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.onButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Black;
            this.onButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.onButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.onButton.Image = global::Locomo.Properties.Resources.On;
            this.onButton.Location = new System.Drawing.Point(86, 0);
            this.onButton.Margin = new System.Windows.Forms.Padding(0);
            this.onButton.Name = "onButton";
            this.onButton.Size = new System.Drawing.Size(32, 38);
            this.onButton.TabIndex = 27;
            this.onButton.TabStop = false;
            this.onButton.UseVisualStyleBackColor = false;
            this.onButton.Click += new System.EventHandler(this.onButton_Click);
            // 
            // ccLabel
            // 
            this.ccLabel.BackColor = System.Drawing.Color.Black;
            this.ccLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ccLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.ccLabel.Location = new System.Drawing.Point(118, 0);
            this.ccLabel.Margin = new System.Windows.Forms.Padding(0);
            this.ccLabel.Name = "ccLabel";
            this.ccLabel.Size = new System.Drawing.Size(55, 38);
            this.ccLabel.TabIndex = 0;
            this.ccLabel.Text = "CC";
            this.ccLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Black;
            this.button1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.button1.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.button1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Black;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Image = global::Locomo.Properties.Resources.Off;
            this.button1.Location = new System.Drawing.Point(0, 0);
            this.button1.Margin = new System.Windows.Forms.Padding(0);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(32, 38);
            this.button1.TabIndex = 26;
            this.button1.TabStop = false;
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.offButton_Click);
            // 
            // PSUReadout
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.Controls.Add(this.flowLayoutPanel1);
            this.Name = "PSUReadout";
            this.Size = new System.Drawing.Size(812, 120);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label currentLabel;
        private System.Windows.Forms.Button onButton;
        private System.Windows.Forms.Label cvLabel;
        private System.Windows.Forms.Label ccLabel;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label voltsLabel;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button power_toggle_button;
    }
}
