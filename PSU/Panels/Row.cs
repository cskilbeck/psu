﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Locomo.Panels
{
    public class Row
    {
        public TimeSpan time;
        public int index;
        public Stream stream;
    }
}
