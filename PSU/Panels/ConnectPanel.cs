﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.IO.Ports;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;
using static Locomo.Extensions;

// If they click on:
//  a device:
//     if Active
//          show the correct connection config control in Panel2 (eg SerialPortConfig etc), and enable Connect/Disconnect, disable Add
//     else
//          enable the 'Add' button, disable Connect/Disconnect
//  a channel:
//     if device parent is active
//          show the stream config control in Panel2
//     else
//          show the stream config, but disabled
//     

namespace Locomo.Panels
{
    [Ctrls.ChildPanel("Connections")]
    public partial class ConnectPanel : Ctrls.ChildPanel
    {
        private Type current_device_type = null;
        private TreeNode active_treenode = null;
        private TreeNode available_treenode = null;

        public ConnectPanel()
        {
            panel_title = "Connections";
            InitializeComponent();
            DoubleBuffered = true;
            device_treeview.EnableDoubleBuffer();
            active_treenode = new TreeNode("Active");
            available_treenode = new TreeNode("Available");

            device_treeview.Nodes.Add(active_treenode);
            device_treeview.Nodes.Add(available_treenode);

            SetupDevicesTreeView();

            //sources_listView.MouseClick += sources_clicked;
        }

        public void DeviceConnectionChanged(object sender)
        {
            Device d = (sender as Connection).Device;
            TreeNode n = d?.tree_node;
            if (n != null && Created)
            {
                BeginInvoke((MethodInvoker)delegate
                {
                    RefreshConnectionStatus(n, d);
                });
            }
        }

        void SetupDeviceNode(TreeNode n)
        {
            Device d = n.Tag as Device;
            if(n.Nodes.Count != 0)
            {
                Debugger.Break();
            }
            if(Device.GetChannels(d.GetType()) != null)
            {
                foreach (var channel in Device.GetChannels(d.GetType()))
                {
                    var channel_node = n.Nodes.Add(channel.ToString());
                    channel_node.Tag = d.GetStream(channel);
                }
                if(d.Connection != null)
                {
                    d.Connection.ConnectionStateChange -= DeviceConnectionChanged;
                    d.Connection.ConnectionStateChange += DeviceConnectionChanged;
                }
            }
            RefreshConnectionStatus(n, d);
        }

        public void RefreshActiveDevices()
        {
            active_treenode.Nodes.Clear();
            foreach (Device device in Device.Devices)
            {
                if (device is SerialDevice)
                {
                    SerialDevice d = (SerialDevice)device;
                    if (d.PortName != null)
                    {
                        var device_node = new TreeNode(string.Format("{0}:{1}", d.Name, d.PortName));
                        device_node.Tag = d;
                        active_treenode.Nodes.Add(device_node);
                        d.tree_node = device_node;
                        SetupDeviceNode(device_node);
                    }
                }
                else if(device is Locomo.Devices.Device_Virtual)
                {
                    Locomo.Devices.Device_Virtual d = (Locomo.Devices.Device_Virtual)device;
                    var device_node = new TreeNode(d.Name);
                    device_node.Tag = d;
                    active_treenode.Nodes.Add(device_node);
                    d.tree_node = device_node;
                    SetupDeviceNode(device_node);
                }
            }
            active_treenode.Expand();
        }

        public void AddDevicesToTreeView(TreeView treeview)
        {
            foreach(var kind in Device.AllKinds)
            {
                TreeNode kind_node = available_treenode.Nodes.Add(kind);
                //kind_node.BackColor = SystemColors.Control;
                foreach (var manufacturer in Device.AllManufacturers)
                {
                    TreeNode manufacturer_node = null;
                    foreach (var type in Device.DeviceTypes.Where(t => Device.DeviceAttributes[t].kind == kind && Device.DeviceAttributes[t].manufacturer == manufacturer))
                    {
                        if (manufacturer_node == null)
                        {
                            manufacturer_node = new TreeNode(manufacturer);
                            kind_node.Nodes.Add(manufacturer_node);
                        }
                        TreeNode device_node = manufacturer_node.Nodes.Add(Device.GetDeviceName(type));
                        device_node.Tag = type;
                    }
                }
            }
        }

        static Type current_image_type = null;

        void SetImage(Type t)
        {
            if(t != current_image_type)
            {
                string s = Directory.GetCurrentDirectory();
                string filename = $"{s}\\DeviceImages\\" + t.Name + ".jpg";
                Bitmap b = null;
                try
                {
                    b = new Bitmap(filename);
                }
                catch (ArgumentException)
                {
                    // File not found!?
                    b = new Bitmap(128, 128);
                    Graphics g = Graphics.FromImage(b);
                    g.FillRectangle(Brushes.Yellow, 0, 0, 128, 128);
                    Pen p = new Pen(Color.Blue, 4);
                    g.DrawLine(p, 0, 0, 128, 128);
                    g.DrawLine(p, 128, 0, 0, 128);
                }
                if (b != null)
                {
                    ImageHelper.RotateImageByExifOrientationData(b);
                    b = ImageHelper.resizeImage(b, device_pictureBox.Width, device_pictureBox.Height);
                    device_pictureBox.SizeMode = PictureBoxSizeMode.Zoom;
                    device_pictureBox.Image = b;
                }
                current_image_type = t;
            }
        }

        void SetText(Type t)
        {
            device_label.Text = Device.FullName(t);
            device_label.Visible = true;
        }

        void SetText(Device d)
        {
            if(d != null)
            {
                UIThread(() =>
                {
                    device_label.Text = d.Name + " - " + d.ConnectionStateString;
                    device_label.Visible = true;
                });
            }
            else
            {
                Debugger.Break();
            }
        }

        void ClearImage()
        {
            device_label.Visible = false;
            device_pictureBox.Image = null;
            current_image_type = null;
        }

        Stream CurrentStream;
        Ctrls.StreamConfigControl StreamControl;

        void SetConnectionText(object sender)
        {
            SetText((sender as Connection).Device);
        }

        Ctrls.ActiveDeviceControl SetupActiveDeviceControl(Device device)
        {
            var ntt = new Ctrls.ActiveDeviceControl();
            if (device.Connection != null)
            {
                device.Connection.ConnectionStateChange -= SetConnectionText;
                device.Connection.ConnectionStateChange += SetConnectionText;
                ntt.auto_connect_checkBox.Checked = device.Connection.AutoConnect;
                ntt.auto_connect_checkBox.CheckedChanged += (s2, e2) =>
                {
                    device.Connection.AutoConnect = ntt.auto_connect_checkBox.Checked;
                };
                ntt.connect_button.Click += (s2, e2) =>
                {
                    device.Connect();
                };
                ntt.disconnect_button.Click += (s2, e2) =>
                {
                    ntt.auto_connect_checkBox.Checked = false;
                    device.Disconnect();
                };
            }
            ntt.delete_button.Click += (s2, e2) =>
            {
                device.Disconnect();
                Device.Devices.Remove(device);
                RefreshActiveDevices();
                top_panel.Controls.Clear();
                bottom_panel.Controls.Clear();
            };
            return ntt;
        }

        public void SetupDevicesTreeView()
        {
            RefreshActiveDevices();
            AddDevicesToTreeView(device_treeview);

            device_treeview.AfterSelect += (object sender, TreeViewEventArgs e) =>
            {
                Control new_control = null;
                Control new_top_control = null;
                if (e.Node.Tag == null)
                {
                    e.Node.Expand();
                    current_device_type = null;
                    ClearImage();
                }
                else if(e.Node.Tag is Stream)
                {
                    Stream s = e.Node.Tag as Stream;
                    CurrentStream = s;
                    StreamControl = new Ctrls.StreamConfigControl();
                    StreamControl.rate_trackBar.Value = IntFromTimeSpan(s.resolution);
                    StreamControl.stream_color_button.BackColor = s.color;
                    StreamControl.rate_trackBar.ValueChanged += rate_trackBar_ValueChanged;
                    StreamControl.stream_color_button.Click += stream_button_Click;
                    StreamControl.groupBox.Text = s.Name;
                    new_control = StreamControl;
                    new_top_control = SetupActiveDeviceControl(s.Device);
                    SetImage(s.Device.GetType());
                    SetText(s.Device);
                }
                else if(e.Node.Tag is Device)
                {
                    var device = e.Node.Tag as Device;
                    SetImage(device.GetType());
                    SetText(device);
                    new_top_control = SetupActiveDeviceControl(device);
                }
                else if(e.Node.Tag is Type)
                {
                    Type t = (Type)e.Node.Tag;
                    SetImage(t);
                    SetText(t);
                    string s = Device.GetDeviceName(t);
                    current_device_type = t;

                    Control scc = Device.GetConfigControl(t);

                    // Set the serial form enablement here...
                    Device d = Device.CreateDevice(t, -1, e.Node);
                    if (d != null)
                    {
                        d.InitConfigControl(scc);
                    }
                    if (d != null)
                    {
                        d.Delete();
                    }
                    new_control = scc as Control;
                    new_top_control = new Ctrls.AddControl();
                    ((Ctrls.AddControl)new_top_control).add_button.Click += (s2, e2) =>
                    {
                        Device new_device = Device.CreateDevice(t, -1, e.Node);
                        if (new_device != null)
                        {
                            new_device.Setup(scc);
                            if(new_device.Connection != null && new_device.Connection.Connect())
                            {
                                new_device.StartMonitor();
                            }
                            var tn = new TreeNode(new_device.DisplayName);
                            tn.Tag = new_device;
                            SetupDeviceNode(tn);
                            active_treenode.Nodes.Add(tn);
                            device_treeview.SelectedNode = tn;
                            tn.Expand();
                        }
                    };
                }
                top_panel.Controls.Clear();
                bottom_panel.Controls.Clear();
                if (new_control != null)
                {
                    new_control.Dock = DockStyle.Fill;
                    new_control.Margin = new Padding(0);
                    new_control.Padding = new Padding(0);
                    bottom_panel.Controls.Add(new_control);
                }
                if(new_top_control != null)
                {
                    new_top_control.Dock = DockStyle.Fill;
                    new_top_control.Margin = new Padding(0);
                    new_top_control.Padding = new Padding(0);
                    top_panel.Controls.Add(new_top_control);
                }
            };
        }

        void RefreshConnectionStatus(TreeNode t, Device d)
        {
            if(d.Connection != null)
            {
                t.ImageIndex = device_state_icons[d.Connection.State] + 1;
                device_treeview.Refresh();
            }
        }

        private static Dictionary<ConnectionState, int> device_state_icons = new Dictionary<ConnectionState, int>()
        {
            { ConnectionState.Initializing, 3 },
            { ConnectionState.Connecting, 0 },
            { ConnectionState.Connected, 1 },
            { ConnectionState.Disconnected, 2 }
        };

        private void SetupSerialSettingsFromConfig(SerialPortConfig config, SerialDevice.SerialSettingsMask settings_mask, Type device_type)
        {
            // 
        }

        private ListViewItem SelectedItem(ListView view)
        {
            return (view == null || view.SelectedItems.Count == 0) ? null : view.SelectedItems[0];
        }

        private void connections_SelectedIndexChanged(object sender, EventArgs e)
        {
        }

        private void device_treeview_DrawNode(object sender, DrawTreeNodeEventArgs e)
        {
            if (e.Node != null && e.State.HasFlag(TreeNodeStates.Selected))
            {
                var font = e.Node.NodeFont ?? e.Node.TreeView.Font;
                Brush brush = SystemBrushes.Highlight;
                Color text_color = SystemColors.HighlightText;
                if (e.Node.Tag == null)
                {
                    brush = SystemBrushes.Control;
                    text_color = SystemColors.WindowText;
                }
                e.Graphics.FillRectangle(brush, e.Bounds);
                if (e.Node.Tag == null)
                {
                    e.Graphics.DrawRectangle(SystemPens.HotTrack, new Rectangle(e.Bounds.Left, e.Bounds.Top, e.Bounds.Width - 1, e.Bounds.Height - 1));
                }
                TextRenderer.DrawText(e.Graphics, e.Node.Text, font, e.Bounds, text_color, TextFormatFlags.GlyphOverhangPadding);
            }
            else
            {
                e.DrawDefault = true;
            }
        }

        private TimeSpan TimeSpanFromInt(int n)
        {
            int seconds = n;
            if(n > 0)
            {
                seconds = 1 << (n - 1);
            }
            return new TimeSpan(0, 0, seconds);
        }

        private int IntFromTimeSpan(TimeSpan t)
        {
            return (int)t.TotalSeconds;
        }

        private void rate_trackBar_ValueChanged(object sender, EventArgs e)
        {
            var stream = CurrentStream;
            if (stream != null)
            {
                stream.resolution = TimeSpanFromInt((sender as TrackBar).Value);
                StreamControl.rate_label.Text = stream.resolution.TotalSeconds == 0 ? "Fastest" : stream.resolution.TotalSeconds + " seconds";
            }
        }

        private void stream_button_Click(object sender, EventArgs e)
        {
            ColorDialog d = new ColorDialog();
            var stream = CurrentStream;
            if (stream != null)
            {
                d.Color = stream.color;
                if (d.ShowDialog() == DialogResult.OK)
                {
                    stream.color = d.Color;
                    (sender as Button).BackColor = d.Color;
                }
            }
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
        }

        private void sources_listView_ItemDrag(object sender, ItemDragEventArgs e)
        {
            //sources_listView.DoDragDrop(sources_listView.SelectedItems, DragDropEffects.Move);
        }

        private static Brush back_brush;
        private static Brush fore_brush;
        private static Brush back_selected_brush;
        private static Brush fore_selected_brush;

        private static void InitBrushes(TreeView t)
        {
            if(back_brush == null)
            {
                back_brush = new SolidBrush(t.BackColor);
                fore_brush = new SolidBrush(t.ForeColor);
                back_selected_brush = SystemBrushes.MenuHighlight;
                fore_selected_brush = SystemBrushes.HighlightText;
            }
        }

        protected void device_treeview_DrawNode_1(object sender, DrawTreeNodeEventArgs e)
        {
            string text = e.Node.Text;
            Rectangle itemRect = e.Bounds;
            if (e.Bounds.Height < 1 || e.Bounds.Width < 1)
                return;

            int cIndentBy = 19;                 // TODO - support Indent value
            int cMargin = 6;                    // TODO - this is a bit random, it's slaved off the Indent in some way
            int cTwoMargins = cMargin * 2;

            int indent = (e.Node.Level * cIndentBy) + cMargin;
            int iconLeft = indent;              // Where to draw parentage lines & icon/checkbox
            int textLeft = iconLeft + 16;       // Where to draw text

            Color leftColour = e.Node.BackColor;
            Color textColour = e.Node.ForeColor;
            if(textColour.A == 0)
            {
                textColour = e.Node.TreeView.ForeColor;
            }

            if (e.State.On(TreeNodeStates.Grayed))
                textColour = Color.FromArgb(255, 128, 128, 128);

            // Grad-fill the background
            Brush backBrush = new SolidBrush(leftColour);
            e.Graphics.FillRectangle(backBrush, itemRect);

            // Bodged to use Button styles as Treeview styles not available on my laptop...
            if (!e.Node.TreeView.HideSelection)
            {
                if (e.State.Any(TreeNodeStates.Selected | TreeNodeStates.Hot))
                {
                    Rectangle selRect = new Rectangle(textLeft, itemRect.Top, itemRect.Right - textLeft, itemRect.Height);
                    VisualStyleRenderer renderer = new VisualStyleRenderer((ContainsFocus) ? VisualStyleElement.Button.PushButton.Hot
                                                                                           : VisualStyleElement.Button.PushButton.Normal);
                    renderer.DrawBackground(e.Graphics, selRect);

                    // Bodge to make VisualStyle look like Explorer selections - overdraw with alpha'd white rectangle to fade the colour a lot
                    //Brush bodge = new SolidBrush(Color.FromArgb(e.State.On(TreeNodeStates.Hot) ? 224 : 128, 255, 255, 255));
                    //e.Graphics.FillRectangle(bodge, selRect);
                }
            }

            Pen dotPen = new Pen(Color.FromArgb(128, 128, 128));
            dotPen.DashStyle = System.Drawing.Drawing2D.DashStyle.Dot;

            int midY = (itemRect.Top + itemRect.Bottom) / 2;

            // Draw parentage lines
            if (e.Node.TreeView.ShowLines)
            {
                int x = cMargin * 2;

                if (e.Node.Level == 0 && e.Node.PrevNode == null)
                {
                    // The very first node in the tree has a half-height line
                    e.Graphics.DrawLine(dotPen, x, midY, x, itemRect.Bottom);
                }
                else
                {
                    TreeNode testNode = e.Node;         // Used to only draw lines to nodes with Next Siblings, as in normal TreeViews
                    for (int iLine = e.Node.Level; iLine >= 0; iLine--)
                    {
                        if (testNode.NextNode != null)
                        {
                            x = (iLine * cIndentBy) + (cMargin * 2);
                            e.Graphics.DrawLine(dotPen, x, itemRect.Top, x, itemRect.Bottom);
                        }

                        testNode = testNode.Parent;
                    }

                    x = (e.Node.Level * cIndentBy) + cTwoMargins;
                    e.Graphics.DrawLine(dotPen, x, itemRect.Top, x, midY);
                }

                e.Graphics.DrawLine(dotPen, iconLeft + cMargin, midY, iconLeft + cMargin + 10, midY);
            }

            // Draw Expand (plus/minus) icon if required
            if (e.Node.TreeView.ShowPlusMinus && e.Node.Nodes.Count > 0)
            {
                // Use the VisualStyles renderer to use the proper OS-defined glyphs
                Rectangle expandRect = new Rectangle(iconLeft - 1, midY - 7, 16, 16);

                VisualStyleElement element = (e.Node.IsExpanded) ? VisualStyleElement.TreeView.Glyph.Opened
                                                                 : VisualStyleElement.TreeView.Glyph.Closed;

                VisualStyleRenderer renderer = new VisualStyleRenderer(element);
                renderer.DrawBackground(e.Graphics, expandRect);
            }

            // Draw the text, which is separated into columns by | characters
            Point textStartPos = new Point(itemRect.Left + textLeft, itemRect.Top);
            Point textPos = new Point(textStartPos.X, textStartPos.Y);

            if(e.Node.ImageIndex > 0)
            {
                Image i = imageList1.Images[e.Node.ImageIndex - 1];
                e.Graphics.DrawImage(i, textPos.Add(new Point(1,1)));
                textPos.X += 18;
            }

            Font textFont = e.Node.NodeFont;    // Get the font for the item, or failing that, for this control
            if (textFont == null)
                textFont = Font;

            StringFormat drawFormat = new StringFormat();
            drawFormat.Alignment = StringAlignment.Near;
            drawFormat.LineAlignment = StringAlignment.Center;
            drawFormat.FormatFlags = StringFormatFlags.NoWrap;

            //string[] columnTextList = text.Split('|');
            //for (int iCol = 0; iCol < columnTextList.GetLength(0); iCol++)
            //{
            //    Rectangle textRect = new Rectangle(textPos.X, textPos.Y, itemRect.Right - textPos.X, itemRect.Bottom - textPos.Y);
            //    if (mColumnImageList != null && mColumnImageList[iCol] != null)
            //    {
            //        // This column has an imagelist assigned, so we use the column text as an integer zero-based index
            //        // into the imagelist to indicate the icon to display
            //        int iImage = 0;
            //        try
            //        {
            //            iImage = MathUtils.Clamp(Convert.ToInt32(columnTextList[iCol]), 0, mColumnImageList[iCol].Images.Count);
            //        }
            //        catch (Exception)
            //        {
            //            iImage = 0;
            //        }

            //        e.Graphics.DrawImageUnscaled(mColumnImageList[iCol].Images[iImage], textRect.Left, textRect.Top);
            //    }
            //    else
            //        e.Graphics.DrawString(columnTextList[iCol], textFont, new SolidBrush(textColour), textRect, drawFormat);

            //    textPos.X += mColumnWidthList[iCol];
            //}
            Rectangle textRect = new Rectangle(textPos.X, textPos.Y, itemRect.Right - textPos.X, itemRect.Bottom - textPos.Y);
            e.Graphics.DrawString(text, textFont, new SolidBrush(textColour), textRect, drawFormat);

            // Draw Focussing box around the text
            if (e.State == TreeNodeStates.Focused)
            {
                SizeF size = e.Graphics.MeasureString(text, textFont);
                size.Width = e.Node.TreeView.ClientRectangle.Width - textStartPos.X - 2;
                size.Height += 1;
                Rectangle rect = new Rectangle(textStartPos, size.ToSize());
                e.Graphics.DrawRectangle(dotPen, rect);
                //          ControlPaint.DrawFocusRectangle(e.Graphics, Rect);
            }
        }

        private void device_treeview_DrawNode_1_OLD(object sender, DrawTreeNodeEventArgs e)
        {
            Rectangle r = e.Node.Bounds;
            InitBrushes(e.Node.TreeView);
            Brush back = back_brush;
            Brush fore = fore_brush;
            if ((e.State & TreeNodeStates.Selected) != 0)
            {
                back = back_selected_brush;
                fore = fore_selected_brush;
            }
            var back_rect = new Rectangle(r.Left, r.Top, e.Bounds.Right - r.Left, r.Height);
            e.Graphics.FillRectangle(back, back_rect);
            if (e.Node.Tag != null)
            {
                if (e.Node.Tag is Device)
                {
                    e.Graphics.DrawImage(imageList1.Images[0], r.Location.Add(new Point(1, 1)));
                    r.Offset(18, 0);
                }
            }
            e.Graphics.DrawString(e.Node.Text, device_treeview.Font, fore, Rectangle.Inflate(r, 2, 0));
        }

        private void device_treeview_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
//            e.Node.TreeView.SelectedNode = e.Node;
        }
    }
}
