﻿namespace Locomo.Panels
{
    partial class AmpsPanel
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.rangeSlider1 = new Locomo.Ctrls.RangeSlider();
            this.SuspendLayout();
            // 
            // rangeSlider1
            // 
            this.rangeSlider1.BackColor = System.Drawing.SystemColors.Window;
            this.rangeSlider1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rangeSlider1.HeightPadding = 4;
            this.rangeSlider1.Location = new System.Drawing.Point(0, 0);
            this.rangeSlider1.Name = "rangeSlider1";
            this.rangeSlider1.RangeLimit = 0.0099999997764825821D;
            this.rangeSlider1.RangeMax = 20D;
            this.rangeSlider1.RangeMin = 0D;
            this.rangeSlider1.ShowMinMax = false;
            this.rangeSlider1.Size = new System.Drawing.Size(167, 608);
            this.rangeSlider1.TabIndex = 0;
            this.rangeSlider1.ValueBackColor = System.Drawing.Color.Black;
            this.rangeSlider1.ValueBorderColor = System.Drawing.Color.DarkGray;
            this.rangeSlider1.ValueFont = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rangeSlider1.ValueTextColor = System.Drawing.Color.Aqua;
            // 
            // AmpsPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.rangeSlider1);
            this.Name = "AmpsPanel";
            this.Size = new System.Drawing.Size(167, 608);
            this.ResumeLayout(false);

        }

        #endregion

        private Ctrls.RangeSlider rangeSlider1;
    }
}
