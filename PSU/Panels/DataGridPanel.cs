﻿using System.Windows.Forms;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Diagnostics;
using Locomo.Ctrls;
using System.IO;
using System.Xml;

// Columns can be all sorts of shit!
// 

namespace Locomo.Panels
{
    [ChildPanel("DataGrid")]
    public partial class DataGridPanel : ChildPanel
    {
        List<Row> rows = new List<Row>();
        List<Stream> streams = new List<Stream>();

        bool auto_scroll = true;

        public DataGridPanel()
        {
            Title = "Data";
            InitializeComponent();

            AllowDrop = true;
            DragEnter += stream_DragEnter;
            DragDrop += stream_DragDrop;

            // Connect the virtual-mode events to event handlers. 
            dataGridView1.CellValueNeeded += new DataGridViewCellValueEventHandler(dataGridView1_CellValueNeeded);

            dataGridView1.RowCount = 0;

            // enable auto_scroll if user mashes scrollbar to the bottom of the datagridview
            dataGridView1.UserScroll += (o, ended) =>
            {
                int lastVisible = dataGridView1.FirstDisplayedScrollingRowIndex + dataGridView1.DisplayedRowCount(true);
                auto_scroll = (dataGridView1.RowCount - lastVisible) < 3 && !ended;
            };

            var c = new DataGridViewTextBoxColumn();
            c.DefaultCellStyle.BackColor = System.Drawing.Color.FromArgb(240, 240, 250);
            c.HeaderText = "Time";
            dataGridView1.ColumnHeadersDefaultCellStyle.BackColor = System.Drawing.Color.FromArgb(240, 250, 240);
            dataGridView1.EnableHeadersVisualStyles = false;
            dataGridView1.Columns.Add(c);
        }

        private void stream_DragDrop(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent("System.Windows.Forms.ListView+SelectedListViewItemCollection", false))
            {
                foreach (ListViewItem item in e.Data.GetData("System.Windows.Forms.ListView+SelectedListViewItemCollection") as ListView.SelectedListViewItemCollection)
                {
                    AddStream(item.Tag as Stream);
                }
            }
        }

        private void stream_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent("System.Windows.Forms.ListView+SelectedListViewItemCollection", false))
            {
                e.Effect = DragDropEffects.Move;
            }
        }

        public override ToolStripEx MyToolStrip()
        {
            return toolStrip1;
        }

        // stream being added, we have to zip all readings together into a new Rows list
        public void AddOneStream(Stream s)
        {
            if (s != null && !streams.Contains(s))
            {
                streams.Add(s);
                s.SampleAdded += S_SampleAdded;
                var column = new DataGridViewTextBoxColumn();
                column.Tag = s;
                column.HeaderText = s.Name;
                dataGridView1.Columns.Add(column);
            }
        }

        private void S_SampleAdded(Stream d, Reading result)
        {
            lock (dataGridView1)
            {
                BeginInvoke((MethodInvoker)delegate {
                    rows.Add(new Row { stream = d, index = d.DataLength - 1, time = result.Time });
                    dataGridView1.RowCount = rows.Count;
                    if (auto_scroll)
                    {
                        dataGridView1.FirstDisplayedScrollingRowIndex = dataGridView1.RowCount - 1;
                    }
                    Invalidate(true);
                });
            }
        }

        public void AddStream(Stream s)
        {
            if(s != null)
            {
                dataGridView1.RowCount = 0;
                rows.Clear();
                AddOneStream(s);
                RefreshStreams();
            }
        }

        public void AddStreams(List<Stream> s)
        {
            dataGridView1.RowCount = 0;
            rows.Clear();
            for (int i=0; i<s.Count; ++i)
            {
                AddOneStream(s[i]);
            }
            RefreshStreams();
        }

        public void RefreshStreams()
        {
            int[] index = new int[streams.Count];
            while (true)
            {
                TimeSpan t = new TimeSpan();
                int winner = -1;
                for (int i = streams.Count-1; i >= 0; --i)
                {
                    if (index[i] < streams[i].DataLength)
                    {
                        Reading r = streams[i].Get(index[i]);
                        if (r.Time < t || winner == -1)
                        {
                            t = r.Time;
                            winner = i;
                        }
                    }
                }
                if (winner != -1)
                {
                    rows.Add(new Row { stream = streams[winner], time = t, index = index[winner] });
                    index[winner]++;
                }
                else
                {
                    break;
                }
            }
            dataGridView1.RowCount = rows.Count;
            dataGridView1.Refresh();
        }

        private void dataGridView1_CellValueNeeded(object sender, DataGridViewCellValueEventArgs e)
        {
            e.Value = string.Empty;
            if (e.RowIndex < rows.Count)
            {
                if (e.ColumnIndex == 0)
                {
                    e.Value = rows[e.RowIndex].time.ToString();
                }
                else
                {
                    Stream s = streams[e.ColumnIndex - 1];
                    if (rows[e.RowIndex].stream == s)
                    {
                        e.Value = s.Get(rows[e.RowIndex].index).Max.ToString();
                    }
                }
            }
        }

        public override bool ConfigMenu(ToolStripItemCollection c)
        {
            var streams = new ToolStripMenuItem("Streams");
            SetupAddMenu(streams, MenuItemClicked);
            c.Add(streams);
            return true;
        }

        private void MenuItemClicked(object sender, EventArgs e)
        {
            var item = sender as ToolStripMenuItem;
            if (item != null)
            {
                var p = item.Tag as Stream;
                AddStream(p);
            }
        }
        private static void SetupAddMenu(ToolStripMenuItem m, EventHandler event_handler)
        {
            foreach (Device d in Device.Devices)
            {
                ToolStripMenuItem i = new ToolStripMenuItem(d.DisplayName);
                m.DropDownItems.Add(i);
                foreach (Stream s in d.Streams)
                {
                    ToolStripMenuItem a = new ToolStripMenuItem(s.Name);
                    a.Tag = s;
                    a.Click += event_handler;
                    i.DropDownItems.Add(a);
                }
            }
        }

        private void export_toolStripButton_Click(object sender, EventArgs e)
        {
            Extensions.ExportAllStreams(this, streams);
        }

        private void add_toolStripButton_Click(object sender, EventArgs e)
        {
            StreamChooser s = new StreamChooser();
            s.Text = "Add stream";
            s.AddingStreamNode += (sendr, eva) =>
            {
                eva.suppress_node = streams.Contains(eva.stream);
            };
            if(s.ShowDialog(this) == DialogResult.OK)
            {
                AddStream(s.SelectedStream);
            }
        }

        private void dataGridView1_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if(e.Button == MouseButtons.Right && e.ColumnIndex > 0)
            {
                ContextMenuStrip m = new ContextMenuStrip();
                var delete = new ToolStripMenuItem("Remove");
                m.Items.Add(delete);
                m.Click += (s, ev) =>
                {
                    // remove all rows where this is the only entry

                    lock (dataGridView1)
                    {
                        Stream stream = streams[e.ColumnIndex - 1];
                        stream.SampleAdded -= S_SampleAdded;
                        streams.Remove(stream);
                        rows.RemoveAll((r) => { return r.stream == stream; });
                        dataGridView1.RowCount = rows.Count;
                        dataGridView1.Columns.RemoveAt(e.ColumnIndex);
                    }
                };
                m.Show(this, this.MousePos());
            }
        }

        public override void SaveTo(XmlElement e)
        {
            foreach(Stream s in streams)
            {
                XmlElement elem = e.Elem("Stream");
                s.SaveID(elem);
                e.AppendChild(elem);
            }
            base.SaveTo(e);
        }

        public override void LoadFrom(XmlElement e)
        {
            foreach(XmlNode node in e.SelectNodes("Stream"))
            {
                Channel c = node.Load("Channel", Channel.None);
                Type device_type = null;
                node.LoadType("Type", ref device_type);
                var d = Device.FindDeviceByConnectionID(node.Load("ConnectionID", -1));
                if(d != null)
                {
                    Stream s = d.GetStream(c);
                    if(s != null)
                    {
                        AddStream(s);
                    }
                }
            }
            base.LoadFrom(e);
        }
    }
}
