﻿using System.Windows.Forms;
using System;
using Locomo.Ctrls;
using Logging;
using SharpDX.Direct2D1;
using System.Collections.Generic;

namespace Locomo.Panels
{
    [ChildPanel("LCD Debug")]
    public partial class LCDDebugPanel : ChildPanel
    {
        public Locomo.Devices.DMM.IDMM current_device = null;
        public LCDDebugView bits_view = null;

        public const int history_length = 50;

        public List<byte[]> buffers = new List<byte[]>(history_length);

        public LCDDebugPanel()
        {
            Title = "LCD Debug";
            InitializeComponent();
            bits_view = new LCDDebugView();
            bits_view.Left = 5;
            bits_view.Top = 5;
            bits_view.Width = 23 * 8 * LCDDebugView.scale;
            bits_view.Height = history_length * LCDDebugView.scale;
            bits_view.parent = this;
            Controls.Add(bits_view);
        }

        public override bool ConfigMenu(ToolStripItemCollection c)
        {
            c.Clear();
            var device_menu = new ToolStripMenuItem("Device");
            foreach(Device device in Device.Devices)
            {
                if (device.IsConnected)
                {
                    if (device.Kind.Equals("Multimeter", StringComparison.OrdinalIgnoreCase))
                    {
                        var device_menuitem = new ToolStripMenuItem($"{device.Name}", null, (sender, event_args) =>
                        {
                            if (current_device != null)
                            {
                                current_device.on_data -= got_data;
                            }
                            current_device = (Devices.DMM.IDMM)device;
                            current_device.on_data += got_data;
                            Log.Debug($"They chose {device.Name}");
                        });
                        device_menuitem.Checked = false;
                        device_menu.DropDownItems.Add(device_menuitem);
                    }
                }
            }
            if (device_menu.DropDownItems.Count != 0)
            {
                c.Add(device_menu);
            }
            else
            {
                c.Add(new ToolStripMenuItem("Nothing connected"));
            }
            return true;
        }

        public override void OnBeingClosed(PanelContainer parent, ToolStripButton button)
        {
            if (current_device != null)
            {
                current_device.on_data -= got_data;
            }
            current_device = null;
            base.OnBeingClosed(parent, button);
        }

        void got_data(byte[] buffer)
        {
            byte[] n = new byte[buffer.Length];
            buffer.CopyTo(n, 0);

            Log.Debug($"Got {buffer.Length} bytes");
            buffers.Add(n);
            if(buffers.Count > history_length)
            {
                buffers.RemoveAt(0);
            }
            bits_view.Invalidate();
        }
    }
}
