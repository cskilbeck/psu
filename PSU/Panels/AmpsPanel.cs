﻿using System;
using System.Windows.Forms;
using Locomo.Devices;

namespace Locomo.Panels
{
    [Ctrls.ChildPanel("Amps")]
    public partial class AmpsPanel : DevicePanel
    {
        [Ctrls.SetupMenu]
        public static void SetupMenu(ToolStripMenuItem m, Ctrls.PanelContainer parent, Type type)
        {
            SetupDeviceMenu(m, parent, type);
        }

        public AmpsPanel()
        {
            panel_title = "PSU";
            InitializeComponent();
            var amps_stuff = new ValueStuffer { value_send_command = "curr" };
            amps_stuff.Stuffing += (is_final) => { ((Device_6612C)device).SetTracking(Device_6612C.TRACK_SETAMPS, is_final); };
            rangeSlider1.OnNewValue += (v, finalValue) => { amps_stuff.StuffIt(v, finalValue, device); };
        }

        public override void Connect(SCPIDevice device)
        {
            base.Connect(device);
            if (device != null)
            {
                var amps_data_source = base.device.GetStream(Channel.AmpsDC);
                rangeSlider1.RangeMax = amps_data_source.Range;
                var setamps_data_source = base.device.GetStream(Channel.SetAmps);
                amps_data_source.DataReady += (sender, result) => { UIThread(() => { rangeSlider1.SetTickValue(((Result<double>)result).value); }); };
                setamps_data_source.DataReady += (sender, result) => { UIThread(() => { rangeSlider1.SetValue(((Result<double>)result).value, true, false); }); };
            }
        }
    }
}
