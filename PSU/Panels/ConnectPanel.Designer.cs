﻿namespace Locomo.Panels
{
    partial class ConnectPanel
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ConnectPanel));
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.label5 = new System.Windows.Forms.Label();
            this.splitContainer1 = new Locomo.Ctrls.SplitContainerEx();
            this.device_treeview = new System.Windows.Forms.TreeView();
            this.panel1 = new System.Windows.Forms.Panel();
            this.device_label = new System.Windows.Forms.Label();
            this.device_pictureBox = new System.Windows.Forms.PictureBox();
            this.bottom_panel = new System.Windows.Forms.Panel();
            this.top_panel = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.device_pictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "amber.png");
            this.imageList1.Images.SetKeyName(1, "green.png");
            this.imageList1.Images.SetKeyName(2, "red.png");
            this.imageList1.Images.SetKeyName(3, "blue.png");
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(6, 11);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(53, 13);
            this.label5.TabIndex = 22;
            this.label5.Text = "Devices";
            // 
            // splitContainer1
            // 
            this.splitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer1.FixedPanelEx = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer1.Location = new System.Drawing.Point(0, 30);
            this.splitContainer1.Margin = new System.Windows.Forms.Padding(0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.BackColor = System.Drawing.SystemColors.Control;
            this.splitContainer1.Panel1.Controls.Add(this.device_treeview);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.BackColor = System.Drawing.SystemColors.Control;
            this.splitContainer1.Panel2.Controls.Add(this.panel1);
            this.splitContainer1.Panel2.Controls.Add(this.device_pictureBox);
            this.splitContainer1.Panel2.Controls.Add(this.bottom_panel);
            this.splitContainer1.Panel2.Controls.Add(this.top_panel);
            this.splitContainer1.Size = new System.Drawing.Size(817, 395);
            this.splitContainer1.SplitterDistance = 337;
            this.splitContainer1.TabIndex = 44;
            // 
            // device_treeview
            // 
            this.device_treeview.BackColor = System.Drawing.SystemColors.Window;
            this.device_treeview.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.device_treeview.Dock = System.Windows.Forms.DockStyle.Fill;
            this.device_treeview.DrawMode = System.Windows.Forms.TreeViewDrawMode.OwnerDrawAll;
            this.device_treeview.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.device_treeview.FullRowSelect = true;
            this.device_treeview.HideSelection = false;
            this.device_treeview.Location = new System.Drawing.Point(0, 0);
            this.device_treeview.Margin = new System.Windows.Forms.Padding(0);
            this.device_treeview.Name = "device_treeview";
            this.device_treeview.Size = new System.Drawing.Size(337, 395);
            this.device_treeview.TabIndex = 9;
            this.device_treeview.DrawNode += new System.Windows.Forms.DrawTreeNodeEventHandler(this.device_treeview_DrawNode_1);
            this.device_treeview.NodeMouseClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.device_treeview_NodeMouseClick);
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackColor = System.Drawing.SystemColors.Window;
            this.panel1.Controls.Add(this.device_label);
            this.panel1.Location = new System.Drawing.Point(-1, 96);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(477, 23);
            this.panel1.TabIndex = 3;
            // 
            // device_label
            // 
            this.device_label.Dock = System.Windows.Forms.DockStyle.Fill;
            this.device_label.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.device_label.Location = new System.Drawing.Point(0, 0);
            this.device_label.Margin = new System.Windows.Forms.Padding(0);
            this.device_label.Name = "device_label";
            this.device_label.Size = new System.Drawing.Size(477, 23);
            this.device_label.TabIndex = 0;
            this.device_label.Text = "label1";
            this.device_label.Visible = false;
            // 
            // device_pictureBox
            // 
            this.device_pictureBox.BackColor = System.Drawing.SystemColors.Window;
            this.device_pictureBox.Location = new System.Drawing.Point(-1, 0);
            this.device_pictureBox.Name = "device_pictureBox";
            this.device_pictureBox.Size = new System.Drawing.Size(108, 96);
            this.device_pictureBox.TabIndex = 2;
            this.device_pictureBox.TabStop = false;
            // 
            // bottom_panel
            // 
            this.bottom_panel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.bottom_panel.BackColor = System.Drawing.SystemColors.Window;
            this.bottom_panel.Location = new System.Drawing.Point(-1, 118);
            this.bottom_panel.Name = "bottom_panel";
            this.bottom_panel.Size = new System.Drawing.Size(477, 276);
            this.bottom_panel.TabIndex = 1;
            // 
            // top_panel
            // 
            this.top_panel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.top_panel.BackColor = System.Drawing.SystemColors.Window;
            this.top_panel.Location = new System.Drawing.Point(107, 0);
            this.top_panel.Name = "top_panel";
            this.top_panel.Padding = new System.Windows.Forms.Padding(4);
            this.top_panel.Size = new System.Drawing.Size(368, 96);
            this.top_panel.TabIndex = 0;
            // 
            // ConnectPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.AutoScrollMinSize = new System.Drawing.Size(320, 300);
            this.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.label5);
            this.MinimumSize = new System.Drawing.Size(100, 100);
            this.Name = "ConnectPanel";
            this.Size = new System.Drawing.Size(817, 425);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.device_pictureBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ImageList imageList1;
        private Locomo.Ctrls.SplitContainerEx splitContainer1;
        private System.Windows.Forms.TreeView device_treeview;
        private System.Windows.Forms.Panel bottom_panel;
        private System.Windows.Forms.Panel top_panel;
        private System.Windows.Forms.PictureBox device_pictureBox;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label device_label;
    }
}
