﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;
using System.Xml;
using Locomo.Devices;

namespace Locomo.Panels
{
    [Ctrls.ChildPanel("PSU Control")]
    public partial class PSUPanel : DevicePanel
    {
        [Ctrls.SetupMenu]
        public static void SetupMenu(ToolStripMenuItem m, Ctrls.PanelContainer parent, Type type)
        {
            SetupDeviceMenu(m, parent, type);
        }

        const int CC_MINUS = 2048;
        const int CC_PLUS = 1024;
        const int CV = 256;

        double voltage = 0;

        ValueStuffer volts_stuff;
        ValueStuffer amps_stuff;

        private bool power_is_on = false;

        string[] cc_texts = new string[3]
        {
                "CC-", "CC", "CC+"
        };

        Color[] cc_colors = new Color[3]
        {
                Color.Aqua,
                Color.FromArgb(64, 64, 64),
                Color.Aqua
        };

        public override void SaveTo(XmlElement e)
        {
            rangeSlider1.Save(e, "Volts");
            rangeSlider2.Save(e, "Amps");
            base.SaveTo(e);
        }

        public override void LoadFrom(XmlElement e)
        {
            rangeSlider1.Load(e, "Volts", 20);
            rangeSlider2.Load(e, "Amps", 2);
            base.LoadFrom(e);
        }

        static string Formatter(double value)
        {
            string s = Math.Abs(value) >= 10 ? "{0: 00.0000000;-00.0000000}" : "{0:  0.0000000;- 0.0000000}";
            return string.Format(s, value);
        }

        void UpdateVoltage(Result<double> result)
        {
            voltage = result.value;
            rangeSlider1.SetTickValue(voltage);
            voltsLabel.Text = Formatter(result.value);
        }

        void UpdateAmps(Result<double> result)
        {
            if (result.value < -1 || result.value > 3)
            {
                return; // ignore out of range readings!?
            }
            rangeSlider2.SetTickValue(result.value);
            currentLabel.Text = Formatter(result.value); ;
        }

        private void UpdatePowerButtons(Result<bool> result)
        {
            power_is_on = result.value;
            power_toggle_button.Enabled = true;
            power_toggle_button.ForeColor = result.value ? Color.FromArgb(0, 255, 0) : Color.FromArgb(255, 0, 0);
            power_toggle_button.Text = result.value ? "ON" : "OFF";
            power_toggle_button.ImageIndex = result.value ? 0 : 1;
        }

        void UpdateCCStatus(Result<int> result)
        {
            int r = result.value + 1;
            if (r >= 0 && r < 3)
            {
                ccLabel.Text = cc_texts[r];
                ccLabel.BackColor = cc_colors[r];
            }
        }

        void UpdateCVStatus(Result<bool> result)
        {
            cvLabel.ForeColor = result.value ? Color.MediumSpringGreen : Color.FromArgb(64, 64, 64);
        }

        public PSUPanel()
        {
            panel_title = "PSU";
            InitializeComponent();
            splitContainer1.Panel1MinSize = rangeSlider1.MinWidth;
            splitContainer1.Panel2MinSize = rangeSlider2.MinWidth;
            rangeSlider1.OnNewValue += rangeSlider1_OnNewValue;
            rangeSlider2.OnNewValue += rangeSlider2_OnNewValue;

            volts_stuff = new ValueStuffer { value_send_command = "volt" };
            amps_stuff = new ValueStuffer { value_send_command = "curr" };

            volts_stuff.Stuffing += (is_final) =>
            {
                if (is_final) ((Device_6612C)device).StartTracking(Device_6612C.TRACK_SETVOLTS);
                else ((Device_6612C)device).StopTracking(Device_6612C.TRACK_SETVOLTS);
            };

            amps_stuff.Stuffing += (is_final) =>
            {
                if (is_final) ((Device_6612C)device).StartTracking(Device_6612C.TRACK_SETAMPS);
                else ((Device_6612C)device).StopTracking(Device_6612C.TRACK_SETAMPS);
            };
        }

        public override void Connect(SCPIDevice device)
        {
            StopMonitor();
            base.device = device;
            base.device.Connection.ConnectionStateChange -= psu_OnConnectionStateChange;
            base.device.Connection.ConnectionStateChange += psu_OnConnectionStateChange;
            UpdatePSUConnectionStatus();
            if (base.device.Connection.State == ConnectionState.Connecting || base.device.Connection.State == ConnectionState.Connected)
            {
                base.Title = Name + ":" + base.device.PortName;
                base.device.Get<string>("*idn?", (Result<string> r1) =>
                {
                    if (r1.success)
                    {
                        base.device.PutString("*cls");
                        StartMonitor();
                    }
                });
            }
            else
            {
                base.Title = Name + ": -";
            }
        }

        // when we send a new value to the device, ignore all reports which come back until we know that the sent value has been consumed

        void rangeSlider1_OnNewValue(double v, bool finalValue)
        {
            volts_stuff.StuffIt(v, finalValue, device);
        }

        void rangeSlider2_OnNewValue(double v, bool finalValue)
        {
            amps_stuff.StuffIt(v, finalValue, device);
        }

        public override void StartMonitor()
        {
            base.StartMonitor();

            var volts_data_source = device.GetStream(Channel.VoltsDC);
            var setvolts_data_source = device.GetStream(Channel.SetVolts);
            var amps_data_source = device.GetStream(Channel.AmpsDC);
            var setamps_data_source = device.GetStream(Channel.SetAmps);
            var output_data_source = device.GetStream(Channel.Output);
            var cc_data_source = device.GetStream(Channel.ConstantCurrent);
            var cv_data_source = device.GetStream(Channel.ConstantVoltage);

            volts_data_source.DataReady += VoltsDataSource_OnDataReady;
            setvolts_data_source.DataReady += SetVoltsDataSource_OnDataReady;
            amps_data_source.DataReady += AmpsDataSource_OnDataReady;
            setamps_data_source.DataReady += SetAmpsDataSource_OnDataReady;
            output_data_source.DataReady += OutputDataSource_OnDataReady;
            cc_data_source.DataReady += CcDataSource_OnDataReady;
            cv_data_source.DataReady += CvDataSource_OnDataReady;
        }

        private void CvDataSource_OnDataReady(Stream sender, ResultBase value)
        {
            UIThread(() => { UpdateCVStatus((Result<bool>)value); });
        }

        private void CcDataSource_OnDataReady(Stream sender, ResultBase value)
        {
            UIThread(() => { UpdateCCStatus((Result<int>)value); });
        }

        private void SetAmpsDataSource_OnDataReady(Stream sender, ResultBase value)
        {
            UIThread(() => { rangeSlider2.SetValue(((Result<double>)value).value, true, true); });
        }

        private void SetVoltsDataSource_OnDataReady(Stream sender, ResultBase value)
        {
            UIThread(() => { rangeSlider1.SetValue(((Result<double>)value).value, true, true); });
        }

        private void OutputDataSource_OnDataReady(Stream sender, ResultBase value)
        {
            UIThread(() => { UpdatePowerButtons((Result<bool>)value); });
        }

        private void AmpsDataSource_OnDataReady(Stream sender, ResultBase value)
        {
            UIThread(() => { UpdateAmps((Result<double>)value); });
        }

        private void VoltsDataSource_OnDataReady(Stream sender, ResultBase value)
        {
            UIThread(() => { UpdateVoltage((Result<double>)value); });
        }

        void UpdatePSUConnectionStatus()
        {
            if (device.IsConnected)
            {
                Title = Name + ":" + device.PortName;
            }
            else
            {
                Title = Name + ": -";
            }
        }

        void psu_OnConnectionStateChange(object sender)
        {
            UpdatePSUConnectionStatus();
        }

        private void offButton_Click(object sender, EventArgs e)
        {
            PowerOff();
        }

        private void onButton_Click(object sender, EventArgs e)
        {
            PowerOn();
        }

        private void powerToggleButtonClick(object sender, EventArgs e)
        {
            PowerToggle();
        }

        void DisablePowerButton()
        {
            power_toggle_button.ForeColor = Color.White;
            power_toggle_button.Enabled = false;
            power_toggle_button.Refresh();
        }

        void PowerOff()
        {
            if (device != null)
            {
                DisablePowerButton();
                device.PutString("outp 0");
            }
        }

        void PowerOn()
        {
            if (device != null)
            {
                DisablePowerButton();
                device.PutString("outp 1");
            }
        }

        void PowerToggle()
        {
            if (device != null)
            {
                DisablePowerButton();
                device.PutString("outp " + (power_is_on ? "0" : "1"));
            }
        }

        void Set(string cmd, double v, Device.Completed<bool> onCompleted)
        {
            if (device != null)
            {
                string c = string.Format("{0} {1}", cmd, v);
                device.PutString(c, onCompleted);
            }
        }

        void SetVolts(double v, Device.Completed<bool> onCompleted)
        {
            Set("volt", v, onCompleted);
        }

        void SetAmps(double v, Device.Completed<bool> onCompleted)
        {
            Set("curr", v, onCompleted);
        }
    }
}
