﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Reflection;
using System.Linq;
using System.IO;
using Microsoft.Win32;
using Locomo.Devices;
using System.Drawing.Drawing2D;
using System.ComponentModel;

namespace Locomo
{
    public sealed class StringWriterWithEncoding : StringWriter
    {
        private readonly Encoding encoding;

        public StringWriterWithEncoding(Encoding encoding)
        {
            this.encoding = encoding;
        }

        public override Encoding Encoding
        {
            get { return encoding; }
        }
    }

    public static class Extensions
    {
        public static DateTime global_timebase = DateTime.Now;  // when the program was launched (roughly)

        public static Point TopLeft(this Rectangle r)
        {
            return new Point(r.Left, r.Top);
        }
        public static Point TopRight(this Rectangle r)
        {
            return new Point(r.Right, r.Top);
        }
        public static Point BottomLeft(this Rectangle r)
        {
            return new Point(r.Left, r.Bottom);
        }
        public static Point BottomRight(this Rectangle r)
        {
            return new Point(r.Right, r.Bottom);
        }
        public static PointF BottomRight(this RectangleF r)
        {
            return new PointF(r.Right, r.Bottom);
        }

        public static int MidX(this Rectangle r)
        {
            return (r.Left + r.Right) / 2;
        }

        public static int MidY(this Rectangle r)
        {
            return (r.Top + r.Bottom) / 2;
        }

        public static Point MidTop(this Rectangle r)
        {
            return new Point((r.Left + r.Right) / 2, r.Top);
        }
        public static Point MidBottom(this Rectangle r)
        {
            return new Point((r.Left + r.Right) / 2, r.Bottom);
        }
        public static Point MidLeft(this Rectangle r)
        {
            return new Point(r.Left, (r.Top + r.Bottom) / 2);
        }
        public static Point MidRight(this Rectangle r)
        {
            return new Point(r.Right, (r.Top + r.Bottom) / 2);
        }

        public static SizeF MeasureString(this Font font, string s)
        {
            using (var image = new Bitmap(1, 1))
            {
                using (var g = Graphics.FromImage(image))
                {
                    return g.MeasureString(s, font);
                }
            }
        }

        public static double Scale10Down(double a)
        {
            return Math.Pow(10.0, Math.Floor(Math.Log10(a)));
        }

        public static double Scale10Up(double a)
        {
            return Math.Pow(10.0, Math.Ceiling(Math.Log10(a)));
        }

        public static double RoundDown(double a)
        {
            var p = Scale10Down(a);
            return (int)(a / p) * p;
        }

        public static double RoundUp(double a)
        {
            var p = Scale10Down(a);
            return (int)(a / p + 0.5) * p;
        }

        public static Color Interpolate(this Color a, Color b, int i)
        {
            if (i < 0)
            {
                i = 0;
            }
            else if (i > 256)
            {
                i = 256;
            }
            int j = 256 - i;
            return Color.FromArgb((a.A * i + b.A * j) / 256,
                                    (a.R * i + b.R * j) / 256,
                                    (a.G * i + b.G * j) / 256,
                                    (a.B * i + b.B * j) / 256);
        }

        public static PointF PointF(double a, double b)
        {
            return new PointF((float)a, (float)b);
        }

        public static PointF PointF(int a, int b)
        {
            return new PointF(a, b);
        }

        public static PointF PointF(Point p)
        {
            return new PointF(p.X, p.Y);
        }

        public static double Clamp(double v, double min, double max)
        {
            return Math.Min(max, Math.Max(min, v));
        }

        public static float Clamp(float v, float min, float max)
        {
            return Math.Min(max, Math.Max(min, v));
        }

        static public int MeasureDisplayStringWidth(Graphics graphics, string text, Font font)
        {
            StringFormat format = new StringFormat();
            RectangleF rect = new RectangleF(0, 0, 1000, 1000);
            CharacterRange[] ranges = { new CharacterRange(0, text.Length) };
            Region[] regions = new Region[1];
            format.SetMeasurableCharacterRanges(ranges);
            regions = graphics.MeasureCharacterRanges(text, font, rect, format);
            rect = regions[0].GetBounds(graphics);
            return (int)(rect.Right + 1.0f);
        }

        static public SizeF MeasureDisplayString(Graphics graphics, string text, Font font)
        {
            StringFormat format = new StringFormat();
            RectangleF rect = new RectangleF(0, 0, 1000, 1000);
            CharacterRange[] ranges = { new CharacterRange(0, text.Length) };
            Region[] regions = new Region[1];
            format.SetMeasurableCharacterRanges(ranges);
            regions = graphics.MeasureCharacterRanges(text, font, rect, format);
            rect = regions[0].GetBounds(graphics);
            return rect.Size;
        }

        static public Point MousePos(this Control c)
        {
            return c.PointToClient(Control.MousePosition);
        }

        static public Managed.Graphics.Direct2D.PointF MousePosF(this Control c)
        {
            Point p = c.MousePos();
            return new Managed.Graphics.Direct2D.PointF(p.X, p.Y);
        }

        static public bool ColorFromARGBString(string s, ref Color c)
        {
            if (s == null) return false;
            if (s.Length == 0) return false;
            string[] parts = s.Split(',');
            if (parts.Count() != 4) return false;
            int[] v = new int[4];
            for(int i=0; i<4; ++i)
            {
                if(!int.TryParse(parts[i], out v[i]))
                {
                    return false;
                }
            }
            c = Color.FromArgb(v[0], v[1], v[2], v[3]);
            return true;
        }

        static public string ColorToARGBString(Color c)
        {
            return string.Format("{0},{1},{2},{3}", c.A, c.R, c.G, c.B);
        }

        public static string DefaultFontFamily
        {
            get
            {
                return "Lucida Sans Typewriter";
            }
        }

        public static string DragTextFontFamily
        {
            get
            {
                return SystemFonts.DefaultFont.FontFamily.ToString();
            }
        }

        public static uint ToRGB(this Color c)
        {
            uint r = c.R;
            uint g = c.G;
            uint b = c.B;
            return b << 16 | g << 8 | r;
        }

        public static bool CompareTo(this Size a, Size b)
        {
            return a.Width == b.Width && a.Height == b.Height;
        }

        public static bool On(this Enum f, Enum mask)
        {
            return f.HasFlag(mask);
        }

        public static bool Any(this Enum f, Enum mask)
        {
            ulong m = Convert.ToUInt64(mask);
            return (Convert.ToUInt64(f) & m) != 0;
        }

        public static bool Off(this Enum f, Enum mask)
        {
            ulong m = Convert.ToUInt64(mask);
            return (Convert.ToUInt64(f) & m) == 0;
        }

        public static bool InRange<T>(this int a, T[] array)
        {
            return a >= 0 && a < array.Length;
        }

        public static bool OutOfRange<T>(this int a, T[] array)
        {
            return a < 0 || a >= array.Length;
        }

        public static IEnumerable<Type> FindAllDerivedTypes<T>()
        {
            return FindAllDerivedTypes<T>(Assembly.GetAssembly(typeof(T)));
        }

        private static IEnumerable<Type> FindAllDerivedTypes<T>(Assembly assembly)
        {
            var derivedType = typeof(T);
            return assembly.GetTypes().Where(t => t != derivedType && derivedType.IsAssignableFrom(t));
        }

        public class Pair<U, V>
        {
            public U First;
            public V Second;

            public Pair(U u, V v)
            {
                First = u;
                Second = v;
            }
        }

        public static IEnumerable<Pair<Type, T>> FindTypesWithAttribute<T>()
        {
            Assembly assembly = Assembly.GetExecutingAssembly();
            foreach (Type type in assembly.GetTypes())
            {
                var attrs = type.GetCustomAttributes(typeof(T), true);
                if (attrs.Length > 0)
                {
                    yield return new Pair<Type, T>(type, (T)attrs[0]);
                }
            }
        }

        public static IEnumerable<MethodInfo> FindStaticMethodsWithAttribute<T>(Type type) where T: Attribute
        {
            foreach (var method in type.GetRuntimeMethods())
            {
                if (method.IsStatic && method.GetCustomAttribute(typeof(T), true) != null)
                {
                    yield return method;
                }
            }
        }

        public static V Get<K, V>(this Dictionary<K, V> d, K key, V default_value)
        {
            V value;
            if (!d.TryGetValue(key, out value))
            {
                value = default_value;
            }
            return value;
        }

        private const int WM_SETREDRAW = 11;

        public static void SuspendDrawing(this Control parent)
        {
            SendMessage(parent.Handle, WM_SETREDRAW, 0, 0);
        }

        public static void ResumeDrawing(this Control parent)
        {
            SendMessage(parent.Handle, WM_SETREDRAW, 1, 0);
            parent.Refresh();
        }

        public const int HTLEFT = 10;
        public const int HTRIGHT = 11;
        public const int HTTOP = 12;
        public const int HTTOPLEFT = 13;
        public const int HTTOPRIGHT = 14;
        public const int HTBOTTOM = 15;
        public const int HTBOTTOMLEFT = 0x10;
        public const int HTBOTTOMRIGHT = 17;
        public const int WM_LBUTTONDOWN = 0x0201;
        public const int WM_NCLBUTTONDOWN = 0x00A1;
        public const int WM_NCCALCSIZE = 0x0083;
        public const int WS_EX_TRANSPARENT = 0x20;

        public enum ListViewExtendedStyles
        {
            /// <summary>
            /// LVS_EX_GRIDLINES
            /// </summary>
            GridLines = 0x00000001,
            /// <summary>
            /// LVS_EX_SUBITEMIMAGES
            /// </summary>
            SubItemImages = 0x00000002,
            /// <summary>
            /// LVS_EX_CHECKBOXES
            /// </summary>
            CheckBoxes = 0x00000004,
            /// <summary>
            /// LVS_EX_TRACKSELECT
            /// </summary>
            TrackSelect = 0x00000008,
            /// <summary>
            /// LVS_EX_HEADERDRAGDROP
            /// </summary>
            HeaderDragDrop = 0x00000010,
            /// <summary>
            /// LVS_EX_FULLROWSELECT
            /// </summary>
            FullRowSelect = 0x00000020,
            /// <summary>
            /// LVS_EX_ONECLICKACTIVATE
            /// </summary>
            OneClickActivate = 0x00000040,
            /// <summary>
            /// LVS_EX_TWOCLICKACTIVATE
            /// </summary>
            TwoClickActivate = 0x00000080,
            /// <summary>
            /// LVS_EX_FLATSB
            /// </summary>
            FlatsB = 0x00000100,
            /// <summary>
            /// LVS_EX_REGIONAL
            /// </summary>
            Regional = 0x00000200,
            /// <summary>
            /// LVS_EX_INFOTIP
            /// </summary>
            InfoTip = 0x00000400,
            /// <summary>
            /// LVS_EX_UNDERLINEHOT
            /// </summary>
            UnderlineHot = 0x00000800,
            /// <summary>
            /// LVS_EX_UNDERLINECOLD
            /// </summary>
            UnderlineCold = 0x00001000,
            /// <summary>
            /// LVS_EX_MULTIWORKAREAS
            /// </summary>
            MultilWorkAreas = 0x00002000,
            /// <summary>
            /// LVS_EX_LABELTIP
            /// </summary>
            LabelTip = 0x00004000,
            /// <summary>
            /// LVS_EX_BORDERSELECT
            /// </summary>
            BorderSelect = 0x00008000,
            /// <summary>
            /// LVS_EX_DOUBLEBUFFER
            /// </summary>
            DoubleBuffer = 0x00010000,
            /// <summary>
            /// LVS_EX_HIDELABELS
            /// </summary>
            HideLabels = 0x00020000,
            /// <summary>
            /// LVS_EX_SINGLEROW
            /// </summary>
            SingleRow = 0x00040000,
            /// <summary>
            /// LVS_EX_SNAPTOGRID
            /// </summary>
            SnapToGrid = 0x00080000,
            /// <summary>
            /// LVS_EX_SIMPLESELECT
            /// </summary>
            SimpleSelect = 0x00100000
        }

        public enum ListViewMessages
        {
            First = 0x1000,
            SetExtendedStyle = (First + 54),
            GetExtendedStyle = (First + 55),
        }

        // P/Invoke declarations
        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern IntPtr GetSystemMenu(IntPtr hWnd, bool bRevert);

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern bool AppendMenu(IntPtr hMenu, int uFlags, int uIDNewItem, string lpNewItem);

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern bool InsertMenu(IntPtr hMenu, int uPosition, int uFlags, int uIDNewItem, string lpNewItem);

        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        public static extern int SendMessage(IntPtr handle, int messg, int wparam, int lparam);

        public static void SetExtendedStyle(this Control control, ListViewExtendedStyles exStyle)
        {
            ListViewExtendedStyles styles;
            styles = (ListViewExtendedStyles)SendMessage(control.Handle, (int)ListViewMessages.GetExtendedStyle, 0, 0);
            styles |= exStyle;
            SendMessage(control.Handle, (int)ListViewMessages.SetExtendedStyle, 0, (int)styles);
        }

        public static void EnableDoubleBuffer(this ListView control)
        {
            ListViewExtendedStyles styles;
            // read current style
            styles = (ListViewExtendedStyles)SendMessage(control.Handle, (int)ListViewMessages.GetExtendedStyle, 0, 0);
            // enable double buffer and border select
            styles |= ListViewExtendedStyles.DoubleBuffer | ListViewExtendedStyles.BorderSelect;
            // write new style
            SendMessage(control.Handle, (int)ListViewMessages.SetExtendedStyle, 0, (int)styles);
        }

        public static void EnableDoubleBuffer(this TreeView control)
        {
            int TVS_EX_DOUBLEBUFFER = 4;
            int TVM_SETEXTENDEDSTYLE = 0x1100 + 44;
            SendMessage(control.Handle, TVM_SETEXTENDEDSTYLE, TVS_EX_DOUBLEBUFFER, TVS_EX_DOUBLEBUFFER);
        }

        public static void DisableDoubleBuffer(this ListView control)
        {
            ListViewExtendedStyles styles;
            // read current style
            styles = (ListViewExtendedStyles)SendMessage(control.Handle, (int)ListViewMessages.GetExtendedStyle, 0, 0);
            // disable double buffer and border select
            styles -= styles & ListViewExtendedStyles.DoubleBuffer;
            styles -= styles & ListViewExtendedStyles.BorderSelect;
            // write new style
            SendMessage(control.Handle, (int)ListViewMessages.SetExtendedStyle, 0, (int)styles);
        }

        public static int Clamp(int a, int min, int max)
        {
            if (a < min)
            {
                return min;
            }
            else if (a >= max)
            {
                return max;
            }
            else
            {
                return a;
            }
        }

        public static Rectangle Coordinates(this Control control)
        {
            // Extend System.Windows.Forms.Control to have a Coordinates property.
            // The Coordinates property contains the control's form-relative location.
            Rectangle coordinates;
            Control form = control.TopLevelControl;

            if (control == form)
                coordinates = form.ClientRectangle;
            else
                coordinates = form.RectangleToClient(control.Parent.RectangleToScreen(control.Bounds));

            return coordinates;
        }

        public static T GetKeyValue<T>(RegistryHive hive, string key, string name, T default_value)
        {
            using (RegistryKey k = RegistryKey.OpenBaseKey(RegistryHive.CurrentUser, RegistryView.Default))
            {
                using (RegistryKey l = k.OpenSubKey(key))
                {
                    if(l != null)
                    {
                        return (T)l.GetValue(name, default_value);
                    }
                }
            }
            return default_value;
        }

        public static void SetKeyValue<T>(RegistryHive hive, string key, string name, T value)
        {
            using (RegistryKey k = RegistryKey.OpenBaseKey(RegistryHive.CurrentUser, RegistryView.Default))
            {
                using (RegistryKey l = k.CreateSubKey(key))
                {
                    l.SetValue(name, value);
                }
            }
        }

        public static bool IsFileAssociationSet(string extension, string association_name, string executable_path)
        {
            string assoc = "SOFTWARE\\Classes\\" + extension;
            string setter = "SOFTWARE\\Classes\\" + association_name + "\\shell\\open\\command";
            string exe = executable_path + " \"%1\"";
            RegistryHive hive = RegistryHive.CurrentUser;
            return GetKeyValue(hive, assoc, string.Empty, string.Empty) == association_name &&
                    GetKeyValue(hive, setter, string.Empty, string.Empty) == exe;
        }

        public static void SetFileAssociation(string extension, string association_name, string executable_path)
        {
            string assoc = "SOFTWARE\\Classes\\" + extension;
            string setter = "SOFTWARE\\Classes\\" + association_name + "\\shell\\open\\command";
            string exe = executable_path + " \"%1\"";
            RegistryHive hive = RegistryHive.CurrentUser;
            SetKeyValue(hive, assoc, string.Empty, association_name);
            SetKeyValue(hive, setter, string.Empty, exe);
        }

        public static string InputString(string text, string caption, string value)
        {
            Prompt p = new Prompt();
            p.Text = caption;
            p.label.Text = text;
            p.textbox.Text = value;
            return p.ShowDialog() == DialogResult.OK ? p.textbox.Text : value;
        }

        public static Point PointFromMessage(this Control control, Message m)
        {
            int x = (Int16)(m.LParam.ToInt32() & 0xffff);
            int y = (Int16)(m.LParam.ToInt32() >> 16);
            if (control.Parent != null)
            {
                return control.Parent.PointToClient(control.PointToScreen(new Point(x, y)));
            }
            else
            {
                return new Point(x, y);
            }
        }

        private static Random random_color_engine = new Random();

        public static Color RandomColor
        {
            get
            {
                return Color.FromArgb(random_color_engine.Next() | unchecked((int)0xff000000));
            }
        }


        public static GraphicsPath RoundedRect(RectangleF bounds, int radius)
        {
            int diameter = radius * 2;
            Size size = new Size(diameter, diameter);
            RectangleF arc = new RectangleF(bounds.Location, size);
            GraphicsPath path = new GraphicsPath();

            if (radius == 0)
            {
                path.AddRectangle(bounds);
                return path;
            }

            // top left arc  
            path.AddArc(arc, 180, 90);

            // top right arc  
            arc.X = bounds.Right - diameter;
            path.AddArc(arc, 270, 90);

            // bottom right arc  
            arc.Y = bounds.Bottom - diameter;
            path.AddArc(arc, 0, 90);

            // bottom left arc 
            arc.X = bounds.Left;
            path.AddArc(arc, 90, 90);

            path.CloseFigure();
            return path;
        }

        public static void DrawRoundedRectangle(this Graphics graphics, Pen pen, Rectangle bounds, int cornerRadius)
        {
            if (graphics == null)
                throw new ArgumentNullException("graphics");
            if (pen == null)
                throw new ArgumentNullException("pen");

            using (GraphicsPath path = RoundedRect(bounds, cornerRadius))
            {
                graphics.DrawPath(pen, path);
            }
        }

        public static void FillRoundedRectangle(this Graphics graphics, Brush brush, Rectangle bounds, int cornerRadius)
        {
            if (graphics == null)
                throw new ArgumentNullException("graphics");
            if (brush == null)
                throw new ArgumentNullException("brush");

            using (GraphicsPath path = RoundedRect(bounds, cornerRadius))
            {
                graphics.FillPath(brush, path);
            }
        }

        public static bool IsSplitterChildPanelOrMainForm(Control c)
        {
            return c is MainForm || c is SplitterPanel && c.Parent is Ctrls.PanelSplitter;
        }

        public static IEnumerable<T> GetAllControlsOfType<T>(this Control control) where T : Control
        {
            foreach (Control c in control.Controls)
            {
                foreach (T c1 in c.GetAllControlsOfType<T>()) yield return c1;
            }
            if (control is T) yield return (T)control;
        }

        public static void RemoveEvents(this Control b, string name)
        {
            FieldInfo f1 = typeof(Control).GetField(name, BindingFlags.Static | BindingFlags.NonPublic);
            object obj = f1.GetValue(b);
            PropertyInfo pi = b.GetType().GetProperty("Events", BindingFlags.NonPublic | BindingFlags.Instance);
            EventHandlerList list = (EventHandlerList)pi.GetValue(b, null);
            list.RemoveHandler(obj, list[obj]);
        }

        public static Managed.Graphics.Direct2D.Color COLOR(System.Drawing.Color color)
        {
            return Managed.Graphics.Direct2D.Color.FromRGB(color.R, color.G, color.B, color.A);
        }

        public static System.Drawing.Color COLOR(Managed.Graphics.Direct2D.Color color)
        {
            byte a = (byte)(color.A * 255.0);
            byte r = (byte)(color.R * 255.0);
            byte g = (byte)(color.G * 255.0);
            byte b = (byte)(color.B * 255.0);
            return Color.FromArgb(a, r, g, b);
        }

        public static T[] GetUnderlyingArray<T>(this List<T> list)
        {
            var field = list.GetType().GetField("_items", BindingFlags.Instance | BindingFlags.NonPublic);
            return (T[])field.GetValue(list);
        }

        public static void DisposeOf<T>(ref T obj) where T : IDisposable
        {
            obj?.Dispose();
            obj = default(T);
        }

        public static void ExportStreamsEx(List<Stream> streams, string filename)
        {
            while (true)
            {
                try
                {
                    using (var stream = new StreamWriter(filename))
                    {
                        int row_count = streams.Count;
                        stream.WriteLine("\"LOCOMO\"");
                        stream.WriteLine("Channels," + row_count.ToString());

                        var headers = new List<string>() { "Time" };
                        var cells = new List<string>() { "00" };
                        for (int i = 0; i < row_count; ++i)
                        {
                            headers.Add("\"" + streams[i].Name + "\"");
                            cells.Add(string.Empty);
                        }
                        stream.WriteLine(string.Join(",", headers));

                        int[] index = new int[streams.Count];
                        int[] sections = new int[streams.Count];    // current section for each stream
                        while (true)
                        {
                            TimeSpan t = new TimeSpan();
                            int winner = -1;
                            for (int i = streams.Count - 1; i >= 0; --i)
                            {
                                int section = streams[i].SectionBase(sections[i]);
                                if (index[i] < streams[i].DataLength)
                                {
                                    // Check which section we're in and if it's a new one, output a blank for this stream
                                    Reading r = streams[i].Get(index[i]);
                                    if (r.Time < t || winner == -1)
                                    {
                                        t = r.Time;
                                        winner = i;
                                    }
                                }
                            }
                            if (winner != -1)
                            {
                                Reading r = streams[winner].Get(index[winner]);
                                cells[0] = r.Time.ToString();
                                cells[winner + 1] = r.Max.ToString();
                                index[winner]++;
                                stream.WriteLine(string.Join(",", cells));
                            }
                            else
                            {
                                break;
                            }
                        }
                    }
                    break;
                }
                catch (IOException ex)
                {
                    var r = Choice.ShowOnce("Error saving " + filename + "\n\n" + ex.Message + "\n\n Would you like to retry?");
                    if (r != DialogResult.Yes)
                    {
                        break;
                    }
                }
            }
        }

        public static void ExportStreams(List<Stream> streams, string filename)
        {
            while (true)
            {
                try
                {
                    using (var stream = new StreamWriter(filename))
                    {
                        int colunm_count = streams.Count;
                        stream.WriteLine("\"LOCOMO\"");
                        stream.WriteLine("Channels," + colunm_count.ToString());

                        // need to lock the streams here, really...
                        var cells = new List<string>();
                        for (int i = 0; i < colunm_count; ++i)
                        {
                            cells.Add("\"Time\"");
                            cells.Add("\"" + streams[i].Name + "\"");
                        }
                        stream.WriteLine(string.Join(",", cells));

                        int[] index = new int[streams.Count];
                        int[] section = new int[streams.Count];
                        while (true)
                        {
                            bool got = false;
                            for (int i = 0; i < streams.Count; ++i)
                            {
                                cells[i * 2] = string.Empty;
                                cells[i * 2 + 1] = string.Empty;
                                Stream s = streams[i];
                                if (section[i] < s.SectionCount)
                                {
                                    if (index[i] >= s.SectionEnd(section[i])) ++section[i];
                                    else
                                    {
                                        Reading r = s.Get(index[i]);
                                        cells[i * 2] = r.Time.ToString();
                                        cells[i * 2 + 1] = r.Max.ToString();
                                        ++index[i];
                                    }
                                    got = true;
                                }
                            }
                            if (got) stream.WriteLine(string.Join(",", cells));
                            else break;
                        }
                    }
                    break;
                }
                catch (IOException ex)
                {
                    var r = Choice.ShowOnce("Error saving " + filename + "\n\n" + ex.Message + "\n\n Would you like to retry?");
                    if (r != DialogResult.Yes)
                    {
                        break;
                    }
                }
            }
        }

        public static void ExportAllStreams(IWin32Window window, IEnumerable<Stream> streams_enumerable)
        {
            List<Stream> streams = new List<Stream>();
            streams.AddRange(streams_enumerable);
            if (streams.Count > 0)
            {
                SaveFileDialog s = new SaveFileDialog();
                s.CheckPathExists = true;
                s.AddExtension = true;
                s.DefaultExt = "csv";
                s.OverwritePrompt = true;
                s.Filter = "CSV Files (*.csv)|*.csv|All Files (*.*)|*.*";
                if (s.ShowDialog(window) == DialogResult.OK)
                {
                    ExportStreams(streams, s.FileName);
                }
            }
            else
            {
                MessageBox.Show("There are no streams to save...", "Save Streams");
            }
        }
    }

    public class Visitor<T>
    {
        public delegate void Function(Control c);

        private Function function;

        public Visitor(Function f)
        {
            function = f;
        }

        public void Execute(Control control)
        {
            foreach (Control c in control.Controls)
            {
                if (c is T)
                {
                    function(c);
                }
                Execute(c);
            }
        }
    }
}
