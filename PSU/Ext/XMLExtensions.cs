﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using Logging;

namespace Locomo
{
    public static class XmlExtensions
    {
        public interface IXmlDeserializationCallback
        {
            void OnXmlDeserialization(object sender);
        }

        public class Serializer : XmlSerializer
        {
            public Serializer(Type t) : base(t)
            {
            }

            protected override object Deserialize(XmlSerializationReader reader)
            {
                var result = base.Deserialize(reader);

                var deserializedCallback = result as IXmlDeserializationCallback;
                if (deserializedCallback != null)
                {
                    deserializedCallback.OnXmlDeserialization(this);
                }
                return result;
            }
        }

        public static XmlElement AddElement(this XmlDocument x, string name)
        {
            return x.CreateElement(name);
        }

        public static XmlElement Elem(this XmlElement x, string name)
        {
            return ((XmlNode)x).Elem(name);
        }

        public static XmlElement Elem(this XmlNode x, string name)
        {
            return x.OwnerDocument.CreateElement(name);
        }

        public static void SetAttr_FOO<T>(this XmlElement x, string name, T value)
        {
            x.SetAttribute(name, value.ToString());
        }

        static bool TryParseString<T>(string stringValue, ref T value)
        {
            Type typeT = typeof(T);

            if(typeT == typeof(TimeSpan))
            {
                value = (T)Convert.ChangeType(TimeSpan.Parse(stringValue), typeT);
            }
            if (typeT.IsPrimitive || typeT == typeof(string))
            {
                try
                {
                    value = (T)Convert.ChangeType(stringValue, typeT);
                }
                catch (FormatException)
                {
                }
            }
            else if (typeT.IsEnum)
            {
                try
                {
                    value = (T)Enum.Parse(typeT, stringValue); // Yeah, we're making an assumption
                }
                catch (ArgumentException)
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
            return true;

        }

        public static T Attr_FOO<T>(this XmlElement elem, string name)
        {
            return ((XmlNode)elem).Attr_FOO<T>(name);
        }

        public static T Attr_FOO<T>(this XmlNode elem, string name)
        {
            T rc = default(T);
            var attr = elem.Attributes[name];
            if (attr != null)
            {
                TryParseString<T>(attr.Value, ref rc);
            }
            return rc;
        }

        public static T Attr_FOO<T>(this XmlElement elem, string name, T default_value)
        {
            return ((XmlNode)elem).Attr_FOO<T>(name, default_value);
        }

        public static T Attr_FOO<T>(this XmlNode elem, string name, T default_value)
        {
            T rc = default_value;
            var attr = elem.Attributes[name];
            if (attr != null)
            {
                TryParseString<T>(attr.Value, ref rc);
            }
            return rc;
        }

        public static T Load<T>(this XmlNode elem, string name)
        {
            XmlElement e = (XmlElement)elem.SelectSingleNode(name);
            T rc = default(T);
            if (e != null)
            {
                TryParseString<T>(e.InnerText, ref rc);
            }
            return rc;
        }

        public static T Load<T>(this XmlNode elem, string name, T default_value)
        {
            XmlElement e = (XmlElement)elem.SelectSingleNode(name);
            T rc = default_value;
            if (e != null)
            {
                TryParseString<T>(e.InnerText, ref rc);
            }
            return rc;
        }

        public static bool Load<T>(this XmlNode elem, string name, ref T value)
        {
            XmlElement e = (XmlElement)elem.SelectSingleNode(name);
            if (e != null)
            {
                return TryParseString(e.InnerText, ref value);
            }
            return false;
        }

        public static bool LoadColor(this XmlNode elem, string name, ref Color value)
        {
            XmlElement e = (XmlElement)elem.SelectSingleNode(name);
            if(e != null)
            {
                return Extensions.ColorFromARGBString(e.InnerText, ref value);
            }
            return false;
        }

        public static bool LoadType(this XmlNode elem, string name, ref Type value)
        {
            XmlElement e = (XmlElement)elem.SelectSingleNode(name);
            if (e != null)
            {
                value = Type.GetType(e.InnerText);
                return value != null;
            }
            return false;
        }

        public static XmlElement Save<T>(this XmlNode elem, string name, T value)
        {
            XmlElement e = elem.Elem(name);
            e.InnerText = value.ToString();
            elem.AppendChild(e);
            return e;
        }

        public static XmlElement Serialize(object o)
        {
            XmlDocument doc = new XmlDocument();
            using (XmlWriter writer = doc.CreateNavigator().AppendChild())
            {
                var x = new Serializer(o.GetType());
                XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
                ns.Add(string.Empty, string.Empty);
                x.Serialize(writer, o, ns);
            }
            return doc.DocumentElement;
        }

        public static XmlElement Serialize(object o, string name)
        {
            XmlDocument doc = new XmlDocument();
            using (XmlWriter writer = doc.CreateNavigator().AppendChild())
            {
                XmlAttributes myAttributes = new XmlAttributes();
                myAttributes.XmlRoot = new XmlRootAttribute(name);

                XmlAttributeOverrides myOverrides = new XmlAttributeOverrides();
                myOverrides.Add(o.GetType(), myAttributes);

                var x = new XmlSerializer(o.GetType(), myOverrides);
                XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
                ns.Add(string.Empty, string.Empty);
                x.Serialize(writer, o, ns);
            }
            return doc.DocumentElement;
        }

        public static void SerializeInto<T>(XmlElement e, T thing)
        {
            e.AppendChild(e.OwnerDocument.ImportNode(Serialize(thing), true));
        }

        public static void SerializeInto<T>(XmlElement e, T thing, string name)
        {
            e.AppendChild(e.OwnerDocument.ImportNode(Serialize(thing, name), true));
        }

        public static T Deserialize<T>(XmlNode element)
        {
            try
            {
                XmlNode node = element.SelectSingleNode(typeof(T).Name);
                if(node != null)
                {
                    var serializer = new Serializer(typeof(T));
                    return (T)serializer.Deserialize(new XmlNodeReader(node));
                }
            }
            catch(NullReferenceException)
            {
            }
            catch (XmlException e)
            {
                Log.Error(e.Message + ": " + e.InnerException);
            }
            catch (InvalidOperationException e)
            {
                Log.Error(e.Message);
            }
            return default(T);
        }

        public static T Deserialize<T>(XmlNode element, string name)
        {
            XmlNode node = element.SelectSingleNode(name);
            if (node != null)
            {
                var serializer = new XmlSerializer(typeof(T), new XmlRootAttribute() { ElementName = name });
                try
                {
                    return (T)serializer.Deserialize(new XmlNodeReader(node));
                }
                catch (XmlException)
                {
                }
                catch (InvalidOperationException e)
                {
                    Log.Error(e.Message);
                }
            }
            return default(T);
        }
    }
}
