﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Reflection;
using System.Resources;

namespace Locomo
{
    class AssemblyLoader
    {
        public AssemblyLoader()
        {
            AppDomain.CurrentDomain.AssemblyResolve += new ResolveEventHandler(CurrentDomain_AssemblyResolve);
        }

        Assembly CurrentDomain_AssemblyResolve(object sender, ResolveEventArgs args)
        {
            string dllName = args.Name.Contains(',') ? args.Name.Substring(0, args.Name.IndexOf(',')) : args.Name.Replace(".dll", string.Empty);

            if (!dllName.EndsWith(".resources"))
            {
                dllName = dllName.Replace(".", "_");

                var assembly = Assembly.GetExecutingAssembly();
                var namesp = GetType().Namespace + ".Properties.Resources";
                var rm = new ResourceManager(namesp, assembly);

                byte[] bytes = (byte[])rm.GetObject(dllName);

                if (bytes != null)
                {
                    return Assembly.Load(bytes);
                }
            }
            return null;
        }
    }
}
