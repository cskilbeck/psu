﻿using System;
using Managed.Graphics.Direct2D;

namespace Locomo
{
    public static class PointFExtensions
    {
        public static double DistanceSquaredFrom(this PointF p, PointF o)
        {
            double x = p.X - o.X;
            double y = p.Y - o.Y;
            return x * x + y * y;
        }

        public static double DistanceFrom(this PointF p, PointF o)
        {
            return Math.Sqrt(p.DistanceSquaredFrom(o));
        }

        public static double Dot(PointF a, PointF b)
        {
            return a.X * b.X + a.Y * b.Y;
        }

        public static PointF Add(this PointF a, PointF b)
        {
            return new PointF(a.X + b.X, a.Y + b.Y);
        }

        public static PointF Sub(this PointF a, PointF b)
        {
            return new PointF(a.X - b.X, a.Y - b.Y);
        }

        public static PointF Sub(this PointF a, SizeF b)
        {
            return new PointF(a.X - b.Width, a.Y - b.Height);
        }

        public static PointF Sub(this PointF a, float x, float y)
        {
            return new PointF(a.X - x, a.Y - y);
        }

        public static PointF Scale(this PointF a, double b)
        {
            return new PointF((float)(a.X * b), (float)(a.Y * b));
        }

        public static double DistanceAlong(this PointF p, PointF a, PointF b)
        {
            double l2 = a.DistanceSquaredFrom(b);
            if (l2 == 0)
            {
                return 0;
            }
            return Dot(p.Sub(a), b.Sub(a)) / l2;
        }

        public static PointF ClosestPoint(this PointF p, PointF a, PointF b)
        {
            double t = Math.Max(0, Math.Min(1, p.DistanceAlong(a, b)));
            return a.Add((b.Sub(a).Scale(t)));
        }

        public static double DistanceToLine(this PointF p, PointF a, PointF b)
        {
            return p.DistanceFrom(p.ClosestPoint(a, b));
        }
    }
}
