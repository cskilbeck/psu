﻿using Managed.Graphics.Direct2D;

namespace Locomo
{
    public static class RectFExtensions
    {
        public static float Bottom(this RectF rect)
        {
            return rect.Y + rect.Height;
        }

        public static float Right(this RectF rect)
        {
            return rect.X + rect.Width;
        }

        public static RectF Shrink(this RectF r, float x, float y)
        {
            return new RectF(r.X + x, r.Y + y, r.Width - x * 2, r.Height - y * 2);
        }
    }
}
