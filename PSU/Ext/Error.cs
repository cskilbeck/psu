﻿using System;

namespace Locomo
{
    public class Error : Exception
    {
        public Error(string s) : base(s)
        {
        }
    }

    public class SilentError : Exception
    {
        public SilentError(string s) : base(s)
        {
        }
    }

    public static class Throw
    {
        public static void If(bool condition, string message)
        {
            if (condition)
            {
                throw new Error(message);
            }
        }

        public static void IfNull<T>(T ptr, string message)
        {
            if (ptr == null)
            {
                throw new Error(message);
            }
        }
    }
}
