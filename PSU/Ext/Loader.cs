﻿using System;
using System.Runtime.InteropServices;
using System.Text;

namespace Locomo
{
    public static class Loader
    {
        public static T Load<T>(byte[] buffer)
        {
            T packet = default(T);
            int objSize = Marshal.SizeOf(typeof(T));
            if (buffer != null && buffer.Length >= objSize)
            {
                IntPtr ptrObj = IntPtr.Zero;
                try
                {
                    ptrObj = Marshal.AllocHGlobal(objSize);
                    if (ptrObj != IntPtr.Zero)
                    {
                        Marshal.Copy(buffer, 0, ptrObj, objSize);
                        packet = (T)Marshal.PtrToStructure(ptrObj, typeof(T));
                    }
                }
                finally
                {
                    if (ptrObj != IntPtr.Zero)
                    {
                        Marshal.FreeHGlobal(ptrObj);
                    }
                }
            }
            return packet;
        }

        // defaults to 1252, eg 8 bit bytes
        public static T Load<T>(string line)
        {
            return Load<T>(Encoding.GetEncoding(1252).GetBytes(line));
        }

        // or override with, eg ASCII (7 bits/byte) or whatever
        public static T Load<T>(string line, Encoding encoding)
        {
            return Load<T>(encoding.GetBytes(line));
        }
    }
}
