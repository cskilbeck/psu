﻿using System;

namespace Locomo
{
    public class DataExpiredException : Exception
    {
        public DataExpiredException(string s) : base(s)
        {

        }
    }

    public class CircularList<T>
    {
        public T[] items;
        public int head;
        public int size;
        public int capacity;
        public int base_index;

        public CircularList(int capacity)
        {
            items = new T[capacity];
            this.capacity = capacity;
        }

        public int Capacity
        {
            get
            {
                return capacity;
            }
        }

        public int Length
        {
            get
            {
                return size;
            }
        }

        public bool IsEmpty
        {
            get
            {
                return Length == 0;
            }
        }

        private int add(int a, int b)
        {
            return (a + b) % capacity;
        }

        public void Reset()
        {
            head = 0;
            size = 0;
            base_index = 0;
        }

        public void Put(T o)
        {
            items[add(head, size)] = o;
            if (++size > capacity)
            {
                head = add(head, 1);
                size = capacity;
                base_index += 1;
            }
        }

        public T Get()
        {
            if (size == 0)
            {
                throw new InvalidOperationException("Circular Buffer is empty");
            }
            size -= 1;
            T item = items[head];
            head = add(head, 1);
            base_index += 1;
            return item;
        }

        public T GetAt(int index)
        {
            if (size == 0)
            {
                throw new InvalidOperationException("Circular Buffer is empty");
            }
            if(index < 0)
            {
                throw new IndexOutOfRangeException(string.Format("Index {0} is negative", index));
            }
            if (index < base_index)
            {
                throw new DataExpiredException(string.Format("Data at Index {0} has expired", index));
            }
            if(index >= size)
            {
                throw new IndexOutOfRangeException(string.Format("Index {0} is not in [0..{1}]", index, size - 1));
            }
            return items[add(head, index - base_index)];
        }
    }

}
