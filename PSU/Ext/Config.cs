﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace Locomo
{
    // NONE OF THIS SHIT IS THREAD SAFE AT ALL, NOT EVEN A BIT
    // __ONLY__ LOAD/SAVE THE GLOBAL CONFIG FROM THE UI THREAD

    [XmlType(Namespace = null, TypeName = null)]
    public class Config
    {
        public static Config Settings = new Config();

        public static event PropertyChangedEventHandler PropertyChanged;

        public delegate void SettingsLoadedDelegate();
        public static SettingsLoadedDelegate SettingsLoaded;

        private bool always_pin = false;
        private bool auto_show_toolbar = false;
        private bool auto_hide_toolbar = false;
        private bool supress_file_association_prompt = false;
        private bool set_file_association = true;
        private bool titlebar_on = true;
        private bool resizable = true;
        private bool ontop = false;

        public override string ToString()
        {
            return "AlwaysPin = " + AlwaysPin + ", " +
                    "AutoShowToolBar = " + AutoShowToolBar + ", " +
                    "AutoHideToolBar = " + AutoHideToolBar + ", " +
                    "SupressFileAssociationPrompt = " + SupressFileAssociationPrompt + ", " +
                    "SetFileAssociation = " + SetFileAssociation + ", " +
                    "TitleBar = " + TitleBar + ", " +
                    "Resizable = " + Resizable + ", " +
                    "OnTop = " + OnTop;
        }

        protected bool Set<T>(ref T field, T value, string propertyName)
        {
            if (EqualityComparer<T>.Default.Equals(field, value))
            {
                return false;
            }
            field = value;
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
            return true;
        }

        public bool AlwaysPin
        {
            get { return always_pin; }
            set { Set(ref always_pin, value, "AlwaysPin"); }
        }

        public bool AutoShowToolBar
        {
            get { return auto_show_toolbar; }
            set { Set(ref auto_show_toolbar, value, "AutoShowToolBar"); }
        }

        public bool AutoHideToolBar
        {
            get { return auto_hide_toolbar; }
            set { Set(ref auto_hide_toolbar, value, "AutoHideToolBar"); }
        }

        public bool SupressFileAssociationPrompt
        {
            get { return supress_file_association_prompt; }
            set { Set(ref supress_file_association_prompt, value, "SupressFileAssociationPrompt"); }
        }

        public bool SetFileAssociation
        {
            get { return set_file_association; }
            set { Set(ref set_file_association, value, "SetFileAssociation"); }
        }

        public bool TitleBar
        {
            get { return titlebar_on; }
            set { Set(ref titlebar_on, value, "TitleBar"); }
        }

        public bool Resizable
        {
            get { return resizable; }
            set { Set(ref resizable, value, "Resizable"); }
        }

        public bool OnTop
        {
            get { return ontop; }
            set { Set(ref ontop, value, "OnTop"); }
        }

        public static void SaveSettings(XmlElement e)
        {
            XmlExtensions.SerializeInto(e, Settings);
        }

        public static Config Load(XmlElement e)
        {
            var new_settings = XmlExtensions.Deserialize<Config>(e);
            if (new_settings != null)
            {
                Settings = new_settings;
                SettingsLoaded?.Invoke();
                PropertyChanged?.Invoke(Settings, new PropertyChangedEventArgs("*"));
            }
            return Settings;
        }
    }
}
