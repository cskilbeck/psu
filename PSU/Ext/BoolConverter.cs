﻿using System;
using System.ComponentModel;
using System.Globalization;

namespace Locomo
{
    public class BoolConverter : TypeConverter
    {
        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
        {
            return sourceType == typeof(string);
        }

        public override bool CanConvertTo(ITypeDescriptorContext context, Type destinationType)
        {
            return destinationType == typeof(bool);
        }

        public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
        {
            int rc;
            int.TryParse((string)value, out rc);
            return rc != 0;
        }
    }

    public class DoubleConverter : TypeConverter
    {
        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
        {
            return sourceType == typeof(Boolean) || sourceType == typeof(Int32) || sourceType == typeof(Double);
        }

        public override bool CanConvertTo(ITypeDescriptorContext context, Type destinationType)
        {
            return destinationType == typeof(double);
        }

        public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
        {
            if (value is bool)
            {
                return Convert.ToBoolean(value) ? 1.0 : 0.0;
            }
            if (value is int)
            {
                return Convert.ToDouble(value);
            }
            if (value is double)
            {
                return value;
            }
            return null;
        }
    }
}
