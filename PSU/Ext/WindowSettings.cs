﻿using System;
using System.Configuration;
using System.Diagnostics;
using System.Drawing;
using System.Reflection;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;

namespace Locomo
{
    public class WindowSettings
    {
        public Point Location;
        public Size Size;
        public bool Maximized;
        public bool Minimized;

        public WindowSettings()
        {
        }

        public WindowSettings(Form f)
        {
            switch (f.WindowState)
            {
                case FormWindowState.Maximized:
                    Location = f.RestoreBounds.Location;
                    Size = f.RestoreBounds.Size;
                    Maximized = true;
                    break;
                case FormWindowState.Minimized:
                    Location = f.RestoreBounds.Location;
                    Size = f.RestoreBounds.Size;
                    Minimized = true;
                    break;
                default:
                    Location = f.Location;
                    Size = f.Size;
                    break;
            }
        }

        public void Set(Form f, bool set_window_state)
        {
            // we start minimized, which messes up the location/size setting for maximizing to the right monitor
            // so briefly set to Normal before setting location/size then the State
            f.WindowState = FormWindowState.Normal;

            f.Location = Location;
            f.Size = Size;

            if (Maximized)
            {
                f.WindowState = FormWindowState.Maximized;
            }
            else if (Minimized)
            {
                f.WindowState = FormWindowState.Minimized;
            }
            else
            {
                f.WindowState = FormWindowState.Normal;
            }
        }

        public static WindowSettings Load(XmlElement element, Form form)
        {
            return XmlExtensions.Deserialize<WindowSettings>(element, "WindowSettings");
        }

        public static void SaveSettings(Form form, XmlElement settings)
        {
            XmlExtensions.SerializeInto(settings, new WindowSettings(form));
        }
    }
}
