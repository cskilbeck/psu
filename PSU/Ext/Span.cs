﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Locomo
{
    public class Span
    {
        public double Low;
        public double High;

        public Span(double low, double high)
        {
            Low = low;
            High = high;
        }

        public Span(Span s)
        {
            Set(s);
        }

        public Span()
        {
            Low = 0;
            High = 1;
        }

        public double Range
        {
            get
            {
                return High - Low;
            }
        }

        public void Set(double low, double high)
        {
            Low = low;
            High = high;
        }

        public void Set(Span s)
        {
            Low = s.Low;
            High = s.High;
        }

        public double MidPoint
        {
            get
            {
                return (Low + High) / 2;
            }
        }

        public double Normalize(double v)
        {
            return (v - Low) / Range;
        }

        public double Clamp(double v)
        {
            if (v < Low)
            {
                v = Low;
            }
            if (v > High)
            {
                v = High;
            }
            return v;
        }

        public void Offset(double o)
        {
            Low += o;
            High += o;
        }

        public static Span Diff(Span a, Span b)
        {
            return new Span(a.Low - b.Low, a.High - b.High);
        }
    }
}
