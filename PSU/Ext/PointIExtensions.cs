﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Locomo
{
    public static class PointIExtensions
    {
        public static double DistanceSquaredFrom(this Point p, Point o)
        {
            double x = p.X - o.X;
            double y = p.Y - o.Y;
            return x * x + y * y;
        }

        public static double DistanceFrom(this Point p, Point o)
        {
            return Math.Sqrt(p.DistanceSquaredFrom(o));
        }

        public static double Dot(Point a, Point b)
        {
            return a.X * b.X + a.Y * b.Y;
        }

        public static Point Add(this Point a, Point b)
        {
            return new Point(a.X + b.X, a.Y + b.Y);
        }

        public static Point Sub(this Point a, Point b)
        {
            return new Point(a.X - b.X, a.Y - b.Y);
        }

        public static Point Sub(this Point a, SizeF b)
        {
            return new Point((int)(a.X - b.Width), (int)(a.Y - b.Height));
        }

        public static Point Sub(this Point a, float x, float y)
        {
            return new Point((int)(a.X - x), (int)(a.Y - y));
        }

        public static Point Scale(this Point a, double b)
        {
            return new Point((int)(a.X * b), (int)(a.Y * b));
        }

        public static double DistanceAlong(this Point p, Point a, Point b)
        {
            double l2 = a.DistanceSquaredFrom(b);
            if (l2 == 0)
            {
                return 0;
            }
            return Dot(p.Sub(a), b.Sub(a)) / l2;
        }

        public static Point ClosestPoint(this Point p, Point a, Point b)
        {
            double t = Math.Max(0, Math.Min(1, p.DistanceAlong(a, b)));
            return a.Add((b.Sub(a).Scale(t)));
        }

        public static double DistanceToLine(this Point p, Point a, Point b)
        {
            return p.DistanceFrom(p.ClosestPoint(a, b));
        }
    }
}
