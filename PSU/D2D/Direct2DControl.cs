﻿/* 
* 
* Authors: 
*  Dmitry Kolchev <dmitrykolchev@msn.com>
*  
* Copyright (C) 2010 Dmitry Kolchev
*
* This sourcecode is licenced under The GNU Lesser General Public License
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
* OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
* NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
* DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
* OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
* USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
using System;
using System.Diagnostics;
using System.Windows.Forms;
using Managed.Graphics.Direct2D;
using Managed.Graphics.DirectWrite;
using Managed.Graphics.Imaging;

namespace Managed.Graphics.Forms
{
    public partial class Direct2DControl : Control
    {
        private static float _dpiScaleX = 1;
        private static float _dpiScaleY = 1;

        static Direct2DControl()
        {
            DirectWriteFactory.GetDpiScale(out _dpiScaleX, out _dpiScaleY);
        }

        public static float xdips(float x)
        {
            return x / _dpiScaleX;
        }

        public static float ydips(float y)
        {
            return y / _dpiScaleY;
        }

        public static PointF pdips(PointF p)
        {
            return new PointF(xdips(p.X), ydips(p.Y));
        }

        public Direct2DFactory D2DFactory;
        public DirectWriteFactory DirectWriteFactory;
        public WicImagingFactory WICImagingFactory;
        public WindowRenderTarget RenderTarget;

        protected bool resourcesCreated;

        public event EventHandler<RenderTargetEventArgs> CreateDeviceResources;
        public event EventHandler CreateDeviceIndependentResources;
        public event EventHandler<RenderTargetEventArgs> Render;
        public event EventHandler CleanUpDeviceResources;
        public event EventHandler CleanUpDeviceIndependentResources;

        public Direct2DControl()
        {
            SetStyle(
                ControlStyles.AllPaintingInWmPaint |
                ControlStyles.Opaque |
                ControlStyles.UserPaint, true);
            InitializeComponent();
            Disposed += new EventHandler(Direct2DControl_Disposed);
        }

        void Direct2DControl_Disposed(object sender, EventArgs e)
        {
            Disposed -= new EventHandler(Direct2DControl_Disposed);
            CleanUpDeviceResourcesInternal();
            CleanUpDeviceIndependentResourcesInternal();
        }

        public Direct2DFactory D2D
        {
            get
            {
                return D2DFactory;
            }
        }

        public DirectWriteFactory DW
        {
            get
            {
                if (DirectWriteFactory == null)
                {
                    DirectWriteFactory = DirectWriteFactory.Create(DirectWriteFactoryType.Shared);
                }
                return DirectWriteFactory;
            }
        }

        public WicImagingFactory WIC
        {
            get
            {
                if (WICImagingFactory == null)
                {
                    WICImagingFactory = WicImagingFactory.Create();
                }
                return WICImagingFactory;
            }
        }

        protected WindowRenderTarget RT
        {
            get
            {
                return RenderTarget;
            }
        }

        private void CreateDeviceIndependentResourcesInternal()
        {
            D2DFactory = Direct2DFactory.CreateFactory(FactoryType.SingleThreaded, DebugLevel.None);
            OnCreateDeviceIndependentResources(D2DFactory);
        }

        private void CleanUpDeviceIndependentResourcesInternal()
        {
            OnCleanUpDeviceIndependentResources();
            if (WICImagingFactory != null)
            {
                WICImagingFactory.Dispose();
                WICImagingFactory = null;
            }
            if (DirectWriteFactory != null)
            {
                DirectWriteFactory.Dispose();
                DirectWriteFactory = null;
            }
            if (D2DFactory != null)
            {
                D2DFactory.Dispose();
                D2DFactory = null;
            }
            resourcesCreated = false;
        }

        private void CleanUpDeviceResourcesInternal()
        {
            if (RenderTarget != null)
            {
                OnCleanUpDeviceResources();
                RenderTarget.Dispose();
                RenderTarget = null;
            }
        }

        private void CreateDeviceResourcesInternal()
        {
            if (RenderTarget == null)
            {
                RenderTarget = D2DFactory.CreateWindowRenderTarget(this);
                OnCreateDeviceResources(RenderTarget);
            }
        }

        protected virtual void OnCleanUpDeviceIndependentResources()
        {
            CleanUpDeviceIndependentResources?.Invoke(this, EventArgs.Empty);
        }

        protected virtual void OnCleanUpDeviceResources()
        {
            CleanUpDeviceResources?.Invoke(this, EventArgs.Empty);
        }

        protected virtual void OnCreateDeviceResources(WindowRenderTarget renderTarget)
        {
            CreateDeviceResources?.Invoke(this, new RenderTargetEventArgs(renderTarget));
        }

        protected virtual void OnCreateDeviceIndependentResources(Direct2DFactory factory)
        {
            CreateDeviceIndependentResources?.Invoke(this, EventArgs.Empty);
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            if (!resourcesCreated)
            {
                CreateDeviceIndependentResourcesInternal();
                resourcesCreated = true;
            }
            CreateDeviceResourcesInternal();
            RenderInternal(RenderTarget);
        }

        private void RenderInternal(WindowRenderTarget renderTarget)
        {
            var old_handle = renderTarget.Handle;
            renderTarget.BeginDraw();
            try
            {
                renderTarget.Transform = Matrix3x2.Identity;
                OnRender(renderTarget);
            }
            finally
            {
                if(renderTarget.Handle != old_handle)
                {
                    Debugger.Break();
                }
                if (!renderTarget.EndDraw())
                {
                    CleanUpDeviceResourcesInternal();
                }
            }
        }

        public void InvokeRender()
        {
            OnPaint(null);
        }

        protected virtual void OnRender(WindowRenderTarget renderTarget)
        {
            Render?.Invoke(this, new RenderTargetEventArgs(renderTarget));
        }

        protected override void OnResize(EventArgs e)
        {
            base.OnResize(e);
            if (RT != null)
            {
                RT.Resize(new SizeU { Width = (uint)ClientSize.Width, Height = (uint)ClientSize.Height });
                Invalidate();
            }
        }
    }
}
