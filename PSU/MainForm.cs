﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.ComponentModel;
using System.Reflection;
using Logging;

using Locomo.Ctrls;

using static Locomo.Extensions;

// GRAPH
// PANELCONTAINER/TOOLBAR
// DEVICE/CONNECTION

// BUG! (chs): resizing PSUController when it's below min_width = no redrawing
// BUG! (chs): - zoom in/out - never squish the ratio of horiz:vertical when hitting the limits
// BUG! (chs): - recenter graph when window resized
// BUG! (chs): - toolbar hide/show not working after panel drag (handlers tied to the wrong panel!)
// BUG! (chs): panel naming is broken
// BUG! (chs): window border settings on startup (or without config file, not sure which or why)
// BUG! (chs): value leader lines from min/max dragtextboxes overridden by value dragtextbox painting
// BUG! (chs): can't drag panel labels from dropdown menu where they go when the toolbar is too narrow to fit them on as buttons
// BUG! (chs): toolbar layout

// TODO (chs): virtual channel (2 channels combined with a function eg volts * amps = watts)
// TODO (chs): line graph with 2 channels as the XY inputs (can't fill, but can bezier the points)

// TODO (chs): proper logging function with channels etc

// TODO (chs): graph things other than doubles (bools, ints)
// TODO (chs): different types of graph (bar?)
// TODO (chs): somehow show min/max/mean in graph
// TODO (chs): identify graphs more easily (which device is that graph of?)

// TODO (chs): Support USB?, LAN, GPIB as well as Serial devices
// TODO (chs): virtualize common PSU controls like PowerOff, PowerOn etc so when we add more it's easier
// TODO (chs): set OV/OCP
// TODO (chs): save/restore settings slots for SCPIDevice
// TODO (chs): handle multiple output channels

// TODO (chs): Generic DMM display / compnentized DMM display
// TODO (chs): deal with crazy range of ohms (from 0 to bazillions)
// TODO (chs): serial parameter ranges (eg limited baud rates for 6612C) ?

// DONE (chs): different kinds of settings panes for different kinds of connections (eg: Serial/TCP/GPIB/etc)
// DONE (chs): different channels for AC/DC volts and amps?

// TODO (chs): triggers [when reading from stream X goes into/out of range Y, do Z then stop checking for W]
// TODO (chs): triggering with external program launch (eg if amps goes below 0.1ma, run alerter.exe (for the first N transitions where N = [1..∞] )

// TODO (chs): UI
// TODO (chs): - drag things onto panel buttons (like graphs...)
// TODO (chs): - toolbar design
// TODO (chs): - allocate mouse buttons
// TODO (chs): - auto-scroll rangeslider
// TODO (chs): - remove all scroll velocity crap
// TODO (chs): - when fixing panels, fix me and my parent (if it's a splitter too) (or something) (somehow)

// FIXED (chs): connecting to device after it loaded disconnected is broken (kinda fixed, autoconnect is lame)
// FIXED (chs): serial device frontpanel and graph not reconnecting sometimes, make them try harder! (kinda fixed, will have to do for now)
// FIXED (chs): it doesn't quit sometimes! (fixed yet again, orphaned CommandProcessor this time)
// FIXED (chs): connection to 6612C not good after restart
// FIXED (chs): moved graph still attached to old graph_control (or something?)
// FIXED (chs): horizontal axis lines are off by one tick sometimes
// FIXED (chs): maximized window position save / restore bust
// FIXED (chs): number entry for RangeSlider not working when you press Enter key
// FIXED (chs): ValueAxis mouseclicks not getting through
// FIXED (chs): ValueRange min max panel wrong when origin != 0
// FIXED (chs): 2nd graphcontrol crashes [twat, label brushes were static]
// FIXED (chs): can't hover most recent datapoint
// FIXED (chs): graph is unresponsive when paused [lamely, invalidates on mousemove]
// FIXED (chs): marker gets dropped in the wrong place
// FIXED (chs): - auto-show toolbar [on|off], always pin toolbar [on|off] ; these are fucked [not really fixed]
// FIXED (chs): - correct offset when grabbing/dragging panel buttons
// FIXED (chs): - fix flickering when dragging panels
// FIXED (chs): graph causing flicker to all childpanels in that container

// DONE (chs): BF117 decoder/class
// DONE (chs): DT-4000ZC decoder/class
// DONE (chs): draw a line which shows the current time
// DONE (chs): load/save log (and browse non-live dataset in a graph)
// DONE (chs): set graph sample frequency
// DONE (chs): export log to CSV
// DONE (chs): log datagridview thing
// DONE (chs): Direct2D for rendering graphs and axes
// DONE (chs): move linegraphs from one graph panel to another
// DONE (chs): remove graphs from graphpanels
// DONE (chs): markers show lines when hovered
// DONE (chs): set and save graph colors
// DONE (chs): sub second scale on the time axis might be nice
// DONE (chs): linegraphs break when device goes offline or stream changes in some major way
// DONE (chs): split out PSU controller panel into [readout, control volts, control amps]
// DONE (chs): - save active panel first so it shows before the others when loading settings (I think)
// DONE (chs): - make PSU panel readout layout stack/rearrange on resize?
// DONE (chs): - all ChildPanels should be resizable ?? YES, even PSUControl (and DMMPanel when it's done) [kinda done, more to do]
// DONE (chs): - draw popup value box after all graphs
// DONE (chs): - little value tag popup in the graph window for hovered datapoint
// DONE (chs): - save rangeslider stuff in the settings file
// DONE (chs): - graph axes - show for 'selected' graph rather than 0,1
// DONE (chs): - no-titlebar mode
// DONE (chs): - select graphs in the graph window by clicking on them
// DONE (chs): - finish main settings page
// DONE (chs): - rename panels
// DONE (chs): settings: Save As file and load from commandline parameter
// DONE (chs): - move 'Pinned' attribute to [Layout, Panel1, Panel2] (it's redundantly set in all panes)
// DONE (chs): - icon for status of connection in listview
// DONE (chs): reconnect button somewhere ? Oh yes, on the connections panel
// DONE (chs): - make splitter stick width of certain ChildPanels?
// DONE (chs): - color (icon?) for connection status in title button of PSUController ChildPanel
// DONE (chs): - allow dragging panes around the place
// DONE (chs): - persist which tab was active in each panel
// DONE (chs): - fix the special 'Panel' thing in ContainerControl, make a DefaultEmptyChildPanelWhichCan'tBeClosed
// DONE (chs): - middle mouse button click on the toolstripbutton
// DONE (chs): add the rest of the serial parameters to the serialform
// DONE (chs): save/restore: Graph zooms and offsets, min/max voltage range etc, connected devices, with their ports etc
// DONE (chs): serialize layout
// DONE (chs): error checking/recovery
// DONE (chs): UT61E
// DONE (chs): make graph scrolling/drawing performant somehow [fixed for now, using a Forms.Timer instead of System.Timer
// DONE (chs): SerialForm: show/hide some parameters depending on device (eg for UT61E baud/parity etc are all fixed) (also, valid baud rate list...)
// DONE (chs): doublebuffer a bunch of things (vertical axis labels slow to draw)
// DONE (chs): device select/connect dialog
// DONE (chs): split graph horizontally and vertically
// DONE (chs): tabbed multi-Graph thingy
// DONE (chs): graph doesn't track min values well
// DONE (chs): toolbar above the graph
// DONE (chs): hide (autohide?) toolbar in ContainerControl (how to show it again? mouse hover near the top of the panel?)
// DONE (chs): make ValueTextBox Owner drawn so the ticks don't lag
// DONE (chs): bug where dragging in the rangeslider doesn't send last value if mouse isn't moved
// DONE (chs): rewrite graph renderer (VERY slow when dataset is large)
// DONE (chs): special zoomed out Graph drawing mode which is quick but maintains min/max spikes
// DONE (chs): integrate RangeSlider labels with Graph axes labels (TODO: wide lines)
// DONE (chs): drag a box on the graph with the right mouse button to zoom in
// DONE (chs): single click status = reconnect to same COM port, double click = config dialog
// DONE (chs): fix Graph scale axes labels
// DONE (chs): graph: scale (auto/zoom), scroll (auto/pan), axes (H: time, V: one per dataset), grid, what about when it's full??
// DONE (chs): rangeslider drag release bug
// DONE (chs): fix rangeslider mouseup bug
// DONE (chs): current control
// DONE (chs): listen to CV/CC
// DONE (chs): feedback / status
// DONE (chs): save/restore config
// DONE (chs): serial port config dialog

// NOPE (chs): - maximize panel?

namespace Locomo
{
    partial class MainForm : Managed.Graphics.Forms.Direct2DWindow
    {
        public static MainForm Instance;

        public MainForm()
        {
            Log.Info("Startup!", "MainForm");
            Instance = this;

            string[] args = Environment.GetCommandLineArgs();
            if (args.Length > 1)
            {
                LayoutFilename = args[1];
            }
            else
            {
                LayoutFilename = DefaultLayoutFilename;
            }

            Device.RegisterClasses();
            ChildPanel.RegisterClasses();
            Visible = false;
            InitializeComponent();
            KeyPreview = true;
            KeyUp += MainForm_KeyUp;
            DoubleBuffered = true;
        }

        private const int SYSMENU_ABOUT_ID = 0x1;

        protected override void OnHandleCreated(EventArgs e)
        {
            const int MF_STRING = 0x0;
            const int MF_SEPARATOR = 0x800;

            base.OnHandleCreated(e);
            IntPtr hSysMenu = GetSystemMenu(this.Handle, false);
            AppendMenu(hSysMenu, MF_SEPARATOR, 0, string.Empty);
            AppendMenu(hSysMenu, MF_STRING, SYSMENU_ABOUT_ID, "&Settings");
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            Visible = false;
            base.OnClosing(e);
        }

        const int WM_SHOWWINDOW = 0x0018;
            
        protected override void WndProc(ref Message m)
        {
            const int WM_SYSCOMMAND = 0x112;

            switch (m.Msg)
            {
                case WM_SYSCOMMAND:
                    if ((int)m.WParam == SYSMENU_ABOUT_ID)
                    {
                    }
                    base.WndProc(ref m);
                    break;

                //case WM_SHOWWINDOW:
                //    if (!Visible)
                //    {
                //        m.WParam = IntPtr.Zero;
                //    }
                //    base.WndProc(ref m);
                //    break;

                default:
                    base.WndProc(ref m);
                    break;
            }
        }

        protected override CreateParams CreateParams
        {
            get
            {
                var cp = base.CreateParams;
                cp.ExStyle |= 0x02000000;    // Turn on WS_EX_COMPOSITED
                return cp;
            }
        }

        private Control FindChild<T>(Control c, Point p)
        {
            foreach (Control child in c.Controls)
            {
                if (child is T)
                {
                    Point top_left = c.Parent.PointToScreen(c.Location);
                    int x = p.X - top_left.X;
                    int y = p.Y - top_left.Y;
                    if (x >= 0 && x < child.Width && y >= 0 && y < child.Height)
                    {
                        return child;
                    }
                }
                Control d = FindChild<T>(child, p);
                if (d != null)
                {
                    return d;
                }
            }
            return null;
        }

        private void MainForm_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Menu)
            {
                Point pos = Cursor.Position;
                ChildPanel c = (ChildPanel)FindChild<ChildPanel>(this, pos);
                if (c != null)
                {
                    c.host_control.ToggleToolbar();
                }
            }
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            LoadSettings();
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            SaveSettings(null);
            Device.DisconnectAll();
        }

        private static string GetElementName(Control c)
        {
            if (c is MainForm)
            {
                return "Layout";
            }
            else if (c is SplitterPanel && c.Parent is PanelSplitter)
            {
                return (c == (c.Parent as PanelSplitter).Panel1) ? "Panel1" : "Panel2";
            }
            else
            {
                return null;
            }
        }

        private static void WriteLayout(Control c, XmlElement writer)
        {
            if (IsSplitterChildPanelOrMainForm(c))
            {
                var name = GetElementName(c);
                var child = writer.Elem(name);
                writer.AppendChild(child);
                writer = child;
                if(c.Controls.Count > 0)    // @YUCK
                {
                    PanelContainer container_control = c.Controls[0] as PanelContainer;
                    if (container_control != null)
                    {
                        child.Save("Pinned", container_control.Pinned);
                    }
                }
            }
            else if (c is PanelSplitter)
            {
                PanelSplitter sc = c as PanelSplitter;
                var child = writer.Elem("Split");
                child.Save("Orientation", sc.Orientation);
                child.Save("Distance", sc.SplitterDistance);
                child.Save("Fixed", sc.FixedPanel);
                writer.AppendChild(child);
                writer = child;
            }
            else if (c is ChildPanel)
            {
                PanelContainer p = (PanelContainer)(c.Parent);
                ChildPanel cc = c as ChildPanel;
                if (cc != null)
                {
                    var child = writer.Elem("Pane");
                    child.Save("Type", cc.GetType().ToString());
                    cc.SaveTo(child);
                    writer.AppendChild(child);
                }
            }
            foreach (Control child_control in c.Controls)
            {
                WriteLayout(child_control, writer);
            }
        }

        private void SaveLayout(XmlElement settings)
        {
            WriteLayout(this, settings);
        }

        public static string LayoutFilename;

        static T GetAttr<T>() where T: Attribute
        {
            var asm = Assembly.GetExecutingAssembly();
            return Attribute.GetCustomAttribute(asm, typeof(T), false) as T;
        }

        static string GetVersion()
        {
            return typeof(MainForm).Assembly.GetName().Version.ToString();
        }

        public static string DefaultLayoutFilename
        {
            get
            {
                string company = GetAttr<AssemblyCompanyAttribute>().Company;
                string app = GetAttr<AssemblyProductAttribute>().Product;
                string version = GetVersion();
                string app_data = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);
                return app_data + "\\" + app + "\\" + version + "\\settings.locomo";
            }
        }

        // Inside a node 'Layout' or 'Panel1' or 'Panel2' there can be either [one or more 'Pane's] or [one 'Split']

        private static List<string> valid_names = new List<string>() { "Layout", "Panel1", "Panel2" };

        private void LoadLayout(XmlNode item, PanelContainer root)
        {
            LoadNode(item.SelectSingleNode("Layout"), root);
        }

        private void LoadNode(XmlNode item, PanelContainer current_container)
        {
            Throw.IfNull(item, "Invalid node encountered, expected " + string.Join(" or ", valid_names));

            var split_element = (XmlElement)item.SelectSingleNode("Split");
            if (split_element != null)
            {
                var o = split_element.Load("Orientation", Orientation.Horizontal);
                var split = current_container.DoSplit(o, false);
                split.SplitterDistance = split_element.Load("Distance", split.SplitterDistance);
                LoadNode(split_element.SelectSingleNode("Panel1"), (PanelContainer)(split.Panel1.Controls[0]));
                LoadNode(split_element.SelectSingleNode("Panel2"), (PanelContainer)(split.Panel2.Controls[0]));
                split.FixedPanelEx = split_element.Load("Fixed", FixedPanel.None);
            }
            else
            {
                current_container.Pinned = item.Load("Pinned", false);
                bool shown = false;
                foreach (XmlElement elem in item.SelectNodes("Pane"))
                {
                    try
                    {
                        var s = elem.Load<string>("Type");
                        Throw.IfNull(s, "Missing 'Type' element, blank panel created");

                        Type panel_type = Type.GetType(s);
                        Throw.IfNull(panel_type, "Unknown panel type " + s + " ignored");

                        ChildPanel chp = Ctrls.ChildPanel.CreateChildPanel(panel_type);
                        Throw.IfNull(chp, "Can't create panel of type " + s + ", so... huh...");

                        chp.LoadFrom(elem);
                        bool active = elem.Load("Active", false);
                        shown |= active;
                        current_container.AddControl(chp, active);
                        chp.SetName(elem.Load("Name", chp.Name));
                    }
                    catch (Error e)
                    {
                        MessageBox.Show("Error loading " + LayoutFilename + "\r\n\r\n" + e.Message);
                    }
                }
                if (!shown && current_container.ChildPanels.Count > 0)
                {
                    current_container.ShowControl(current_container.ChildPanels[0]);
                }
            }
        }

        private void LoadSettings()
        {
            try
            {
                XmlDocument doc = new XmlDocument();
                if (!File.Exists(LayoutFilename))
                {
                    LayoutFilename = DefaultLayoutFilename;
                }
                doc.Load(LayoutFilename);

                XmlElement settings = (XmlElement)doc.SelectSingleNode("Settings");
                if (settings != null)
                {
                    ApplySettings(Config.Load(settings));
                    Device.LoadSettings(settings);
                    var window_settings = WindowSettings.Load(settings, this);
                    window_settings?.Set(this, true);
                    LoadLayout(settings, rootContainer);
                }
            }
            catch (FileNotFoundException)
            {
                // No biggie
            }
            catch (DirectoryNotFoundException)
            {
                // No biggie
            }
            catch (Error e)
            {
                MessageBox.Show("Error loading " + LayoutFilename + "\r\n\r\n" + e.Message);
            }
            catch (XmlException e)
            {
                MessageBox.Show("Error loading " + LayoutFilename + "\r\n\r\n" + e.Message);
            }
        }

        void ApplySettings(Config config)
        {
            if(config != null && config != Config.Settings)
            {
                Config.Settings = config;
            }

            MaximizeBox = Config.Settings.Resizable;
            MinimizeBox = Config.Settings.Resizable;
            ControlBox = Config.Settings.TitleBar;
            Text = Config.Settings.TitleBar ? "Locomo" : string.Empty;
            TopMost = Config.Settings.OnTop;
            ShowIcon = Config.Settings.TitleBar;
            FormBorderStyle = border_styles[(Config.Settings.Resizable ? 1 : 0) | (Config.Settings.TitleBar ? 2 : 0)];

            Config.PropertyChanged -= Settings_PropertyChanged;
            Config.PropertyChanged += Settings_PropertyChanged;
            PerformLayout();
        }

        ///////////////////////////////////
        /// TITLE BAR //  RESIZABLE //  //
        /// FALSE         FALSE     0   FixedDialog
        /// FALSE         TRUE      1   Sizable
        /// TRUE          FALSE     2   FixedSingle
        /// TRUE          TRUE      3   Sizable

        static List<FormBorderStyle> border_styles = new List<FormBorderStyle>()
        {
            { FormBorderStyle.FixedToolWindow },
            { FormBorderStyle.Sizable },
            { FormBorderStyle.FixedSingle },
            { FormBorderStyle.Sizable }
        };

        private void Settings_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            Log.Debug(e.PropertyName + " changed");
            ApplySettings(Config.Settings);
        }

        public void SaveSettings(string filename)
        {
            if (filename == null)
            {
                filename = LayoutFilename;
            }
            XmlDocument d = new XmlDocument();
            XmlWriter xw = null;
            StreamWriter w = null;
            try
            {
                d.AppendChild(d.CreateXmlDeclaration("1.0", "utf-8", null));
                XmlElement settings = d.AddElement("Settings");
                WindowSettings.SaveSettings(this, settings);
                Config.SaveSettings(settings);
                Device.SaveSettings(settings);
                SaveLayout(settings);
                d.AppendChild(settings);
                string path = Path.GetDirectoryName(filename);
                try
                {
                    Directory.CreateDirectory(path);
                }
                catch (Exception)
                {
                    throw new Error("Can't create output folder " + path);
                }
                w = new StreamWriter(filename, false, Encoding.UTF8);
                XmlWriterSettings xws = new XmlWriterSettings { Indent = true, NewLineOnAttributes = false, CloseOutput = false };
                xw = XmlWriter.Create(w, xws);
                d.WriteTo(xw);
                LayoutFilename = filename;
            }
            catch (Error e)
            {
                MessageBox.Show("Error saving " + filename + "\r\n\r\n" + e.Message);
            }
            catch (IOException e)
            {
                MessageBox.Show("File error saving " + filename + "\r\n\r\n" + e.Message);
            }
            catch (XmlException e)
            {
                MessageBox.Show("XML error saving " + filename + "\r\n\r\n" + e.Message);
            }
            finally
            {
                if (xw != null)
                {
                    xw.Flush();
                }
                if (w != null)
                {
                    w.Flush();
                    w.Close();
                }
            }
        }

        private void MainForm_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 27)
            {
                Close();
            }
        }
        protected override void OnInvalidated(InvalidateEventArgs e)
        {
            base.OnInvalidated(e);
        }
    }
}
