﻿using System.Windows.Forms;

namespace Locomo
{
    public partial class Choice : Form
    {
        int diffX;
        int diffY;

        public Choice()
        {
            InitializeComponent();
            diffY = Height - text_label.Height;
            diffX = Width - text_label.Width;
        }

        public void ResizeToFit()
        {
            Height = text_label.Height + diffY;
            Width = text_label.Width + 64;
            PerformLayout();
            FormBorderStyle = FormBorderStyle.FixedDialog;
        }

        public static bool Ask(string caption, string text, string oktext, string canceltext, ref bool nag_setting, bool previous_choice, Form owner)
        {
            bool choice = previous_choice;
            if (!nag_setting)
            {
                Choice c = new Choice();
                c.Text = caption;
                c.text_label.Text = text;
                c.ResizeToFit();
                choice = c.ShowDialog(owner) == DialogResult.Yes;
                nag_setting = c.nag_checkbox.Checked;
            }
            return choice;
        }

        public static DialogResult Show(string text)
        {
            Choice c = new Choice();
            c.text_label.Text = text;
            c.ResizeToFit();
            return c.ShowDialog();
        }

        public static DialogResult ShowOnce(string text)
        {
            Choice c = new Choice();
            c.nag_checkbox.Visible = false;
            c.text_label.Text = text;
            c.ResizeToFit();
            return c.ShowDialog();
        }
    }
}
