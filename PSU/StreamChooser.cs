﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Locomo
{
    public partial class StreamChooser : Form
    {
        public delegate void AddingStreamNodeDelegate(object sender, AddingChannelNodeEventArgs e);
        public event AddingStreamNodeDelegate AddingStreamNode;

        public Stream SelectedStream;

        public StreamChooser()
        {
            InitializeComponent();
        }

        public virtual bool OnAddingStreamNode(TreeNode node, Stream stream)
        {
            var e = new AddingChannelNodeEventArgs { node = node, stream = stream };
            AddingStreamNode?.Invoke(this, e);
            return e.suppress_node;
        }

        private void StreamChooser_Load(object sender, EventArgs e)
        {
            foreach (Device d in Device.Devices)
            {
                if (d.IsConnected)
                {
                    TreeNode device_node = new TreeNode();
                    device_node.Text = d.DisplayName;

                    foreach(var stream in d.Streams)
                    {
                        TreeNode stream_node = new TreeNode(stream.Name);
                        stream_node.Tag = stream;
                        if (!OnAddingStreamNode(stream_node, stream))
                        {
                            device_node.Nodes.Add(stream_node);
                        }
                    }
                    streams_treeView.Nodes.Add(device_node);
                }
            }
            streams_treeView.ExpandAll();
            streams_treeView.NodeMouseDoubleClick += Streams_treeView_NodeMouseDoubleClick;
        }

        private void Streams_treeView_NodeMouseDoubleClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            SelectedStream = e.Node.Tag as Stream;
            DialogResult = DialogResult.OK;
            Close();
        }

        private void streams_treeView_AfterSelect(object sender, TreeViewEventArgs e)
        {
            SelectedStream = e.Node.Tag as Stream;
        }
    }

    public class AddingChannelNodeEventArgs : EventArgs
    {
        public TreeNode node;
        public Stream stream;
        public bool suppress_node;
    }
}
