﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Locomo.Devices;

namespace Locomo.Ctrls
{
    public partial class VirtualInputsControl : UserControl
    {
        public VirtualDeviceControl parent;

        public VirtualInputsControl()
        {
            InitializeComponent();
        }

        private void channel_A_button_Click(object sender, EventArgs e)
        {
            string c = parent.choose_channel(VirtualDeviceControl.virtual_input.A);
            if (c != null)
            {
                channel_A_label.Text = c;
            }
        }

        private void channel_B_button_Click(object sender, EventArgs e)
        {
            string c = parent.choose_channel(VirtualDeviceControl.virtual_input.B);
            if (c != null)
            {
                channel_B_label.Text = c;
            }
        }

        private void operation_button_Click(object sender, EventArgs e)
        {
            ContextMenu m = new ContextMenu();
            foreach (var i in Enum.GetValues(typeof(Virtual_Channel.operation)))
            {
                MenuItem item = new MenuItem(Enum.GetName(typeof(Virtual_Channel.operation), i));
                item.Click += (sendr, eventargs) =>
                {
                    parent.set_operator((Virtual_Channel.operation)i);
                    operation_label.Text = i.ToString();
                };
                m.MenuItems.Add(item);
            }
            m.Show(this, channel_B_button.Location);
        }

        private void Item_Click(object sender, EventArgs e)
        {
            //throw new NotImplementedException();
        }

    }
}
