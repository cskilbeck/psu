﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Locomo.Ctrls
{
    public class ChildPanelAttribute : Attribute
    {
        public string name;

        public ChildPanelAttribute(string name)
        {
            this.name = name;
        }
    }

    public class SetupMenuAttribute : Attribute
    {
    }
}
