﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Xml;
using Managed.Graphics.Direct2D;
using Managed.Graphics.DirectWrite;

using Point = System.Drawing.Point;

using D2DColor = Managed.Graphics.Direct2D.Color;
using D2DPointF = Managed.Graphics.Direct2D.PointF;
using D2DSizeF = Managed.Graphics.Direct2D.SizeF;
using D2DBrush = Managed.Graphics.Direct2D.Brush;
using StrokeStyle = Managed.Graphics.Direct2D.StrokeStyle;
using RectF = Managed.Graphics.Direct2D.RectF;
using AntialiasMode = Managed.Graphics.Direct2D.AntialiasMode;
using DrawTextOptions = Managed.Graphics.Direct2D.DrawTextOptions;
using PathSegment = Managed.Graphics.Direct2D.PathSegment;
using FigureBegin = Managed.Graphics.Direct2D.FigureBegin;
using FigureEnd = Managed.Graphics.Direct2D.FigureEnd;
using StrokeStyleProperties = Managed.Graphics.Direct2D.StrokeStyleProperties;

using D2DFont = Managed.Graphics.DirectWrite.Font;
using WindowRenderTarget = Managed.Graphics.Direct2D.WindowRenderTarget;
using Colors = Managed.Graphics.Direct2D.Colors;
using SolidColorBrush = Managed.Graphics.Direct2D.SolidColorBrush;
using PathGeometry = Managed.Graphics.Direct2D.PathGeometry;
using GeometrySink = Managed.Graphics.Direct2D.GeometrySink;
using Direct2DFactory = Managed.Graphics.Direct2D.Direct2DFactory;

using static Locomo.Extensions;

namespace Locomo.Ctrls
{
    public class Graph : Draggable
    {
        public const int hover_threshold_distance = 25;

        bool resources_created;

        public Stream stream;
        public Device device;
        private D2DColor color;
        public Span View;
        public bool auto_range;
        public double min_range;
        public double max_range;
        public double global_min;
        public double global_max;
        public bool selected;
        public GraphControl graph_Control;
        public ValueRangeD2D axis;
        public double default_range;
        public bool hover;
        private Point drag_point;
        private SortedSet<int> marker_lookup = new SortedSet<int>();

        public StrokeStyle dashed_stroke;
        public StrokeStyle zero_strokestyle;

        public D2DBrush label_background_brush;
        public D2DBrush label_outline_brush;
        public D2DBrush label_text_brush;

        public TextFormat label_font;
        public TextFormat regular;
        public TextFormat bold;

        public TextFormat scale_normal_text;
        public TextFormat scale_bold_text;
        public D2DBrush scale_major_brush;
        public D2DBrush scale_minor_brush;
        public D2DBrush scale_major_label;
        public D2DBrush scale_minor_label;
        public D2DBrush zero_brush;

        public D2DColor major_color;
        public D2DColor minor_color;

        public D2DBrush fill_brush;

        public double best_distance = hover_threshold_distance;
        public int best_data_index = -1;
        private int draw_index = -1;

        private int total_samples = 0;

        // Now we can have a big whole list of points and draw the sections using offset/count parameters to DrawLines :)

        public class DrawableSection
        {
            public List<D2DPointF> points;
        }

        List<DrawableSection> drawable_sections = new List<DrawableSection>();

        public Graph(GraphControl parent_graph, Stream stream, Device serial_device, DateTime time_base, Color color)
        {
            Setup(parent_graph, stream, serial_device, time_base, color);
        }

        public Graph(GraphControl parent_graph, Stream stream, Device serial_device, DateTime time_base)  // DODO
        {
            Setup(parent_graph, stream, serial_device, time_base, COLOR(stream.color));
        }

        private void Setup(GraphControl parent_graph, Stream stream, Device serial_device, DateTime time_base, Color color)
        {
            this.graph_Control = parent_graph;
            this.color = color;
            this.stream = stream;
            this.device = serial_device;
            this.auto_range = false;
            double scale = stream.Range;
            max_range = scale * 100;
            default_range = scale;
            min_range = scale * 0.0001f;
            View = new Span(-scale * 0.1f, scale * 1.1f);
            axis = new ValueRangeD2D();
            axis.SetVisibleRange(View);
            axis.actual_range.Set(View.Low - View.Range, View.High + View.Range);
        }

        public D2DColor Color
        {
            get
            {
                return color;
            }
            set
            {
                color = value;
                CleanupResources();
            }
        }

        public void CreateResources(RenderTarget renderTarget)
        {
            if(!resources_created)
            {
                major_color = D2DColor.FromRGB(255, 255, 255);
                minor_color = color;

                var factory = renderTarget.GetFactory();
                var dw = graph_Control.DW;
                var ssp = new StrokeStyleProperties();
                ssp.DashCap = LineCapStyle.Round;
                ssp.DashStyle = DashStyle.Dash;
                ssp.LineJoin = LineJoin.Round;
                dashed_stroke = factory.CreateStrokeStyle(ssp, null);

                ssp.DashStyle = DashStyle.Dot;
                zero_strokestyle = factory.CreateStrokeStyle(ssp, null);

                bold = dw.CreateTextFormat("Lucida Sans Typewriter", 9 * 96.0f / 72.0f, FontWeight.Medium, FontStyle.Normal);
                label_font = dw.CreateTextFormat("Lucida Sans Typewriter", 9 * 96.0f / 72.0f, FontWeight.Medium, FontStyle.Normal);
                regular = dw.CreateTextFormat("Lucida Sans Typewriter", 9 * 96.0f / 72.0f, FontWeight.Medium, FontStyle.Normal);
                scale_normal_text = dw.CreateTextFormat("Lucida Sans Typewriter", 9 * 96.0f / 72.0f, FontWeight.Medium, FontStyle.Normal);
                scale_bold_text = dw.CreateTextFormat("Lucida Sans Typewriter", 9 * 96.0f / 72.0f, FontWeight.Medium, FontStyle.Normal);

                fill_brush = renderTarget.CreateSolidColorBrush(color);
                label_background_brush = renderTarget.CreateSolidColorBrush(D2DColor.FromRGB(0, 0, 0, 192));
                label_text_brush = renderTarget.CreateSolidColorBrush(D2DColor.FromRGB(255, 255, 255));
                label_outline_brush = renderTarget.CreateSolidColorBrush(D2DColor.FromRGB(160, 160, 160));
                scale_major_brush = renderTarget.CreateSolidColorBrush(color);
                scale_minor_brush = renderTarget.CreateSolidColorBrush(D2DColor.FromRGB(255, 255, 255));
                scale_major_label = renderTarget.CreateSolidColorBrush(color);
                scale_major_brush.Opacity = 0.35f;
                scale_minor_label = renderTarget.CreateSolidColorBrush(D2DColor.FromRGB(255, 255, 255));
                zero_brush = renderTarget.CreateSolidColorBrush(color);

                resources_created = true;
            }
        }

        public void CleanupResources()
        {
            DisposeOf(ref zero_brush);
            DisposeOf(ref zero_strokestyle);
            DisposeOf(ref scale_normal_text);
            DisposeOf(ref scale_bold_text);
            DisposeOf(ref scale_major_brush);
            DisposeOf(ref scale_minor_brush);
            DisposeOf(ref scale_major_label);
            DisposeOf(ref scale_minor_label);

            DisposeOf(ref dashed_stroke);
            DisposeOf(ref fill_brush);
            DisposeOf(ref label_background_brush);
            DisposeOf(ref label_outline_brush);
            DisposeOf(ref label_text_brush);
            DisposeOf(ref bold);
            DisposeOf(ref label_font);
            DisposeOf(ref regular);

            resources_created = false;
        }

        public int TotalSamples
        {
            get
            {
                return total_samples;
            }
        }

        public double Range
        {
            get
            {
                return View.Range;
            }
        }

        public void SaveTo(XmlElement e)
        {
            stream.SaveID(e);
            e.Save("Low", axis.Low);
            e.Save("High", axis.High);
            e.Save("Color", ColorToARGBString(COLOR(Color)));
        }

        public void MouseMoved(System.Windows.Forms.MouseEventArgs e)
        {
            int diff = e.Y - drag_point.Y;
            double delta = diff / graph_Control.GraphHeight * View.Range;
            View.Offset(delta);
            drag_point = e.Location;
            auto_range = false;
            graph_Control.DoMouseDrag(e);
        }

        public bool StartDrag(System.Windows.Forms.MouseEventArgs e, Point click_point)
        {
            drag_point = click_point;
            graph_Control.StartMouseDrag(e, click_point);
            return true;
        }

        public void EndDrag(System.Windows.Forms.MouseEventArgs e)
        {
            graph_Control.DoEndMouseDrag();
        }

        public void DoubleClick(System.Windows.Forms.MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                auto_range = true;
            }
            else
            {
                auto_range = false;
                View.Set(0, default_range);
            }
            graph_Control.Invalidate();
        }

        public void ZoomTo(int top, int bottom)
        {
            double t = (graph_Control.GraphBottom - top) / graph_Control.GraphHeight * View.Range + View.Low;
            double b = (graph_Control.GraphBottom - bottom) / graph_Control.GraphHeight * View.Range + View.Low;
            if (Math.Abs(t - b) < min_range)
            {
                double m = (t + b) / 2;
                t = m + min_range / 2;
                b = m - min_range / 2;
            }
            View.Set(b, t);
        }

        public void ZoomTo(double low, double high)
        {
            View.Set(low, high);
            axis.SetVisibleRange(low, high);
        }

        // when adding data, track min and max of all linegraphs

        public double Min
        {
            get
            {
                return global_min;
            }
        }

        public double Max
        {
            get
            {
                return global_max;
            }
        }

        public TimeSpan MaxTime
        {
            get
            {
                return new TimeSpan(0); // DODO
            }
        }

        public void SetupScale(double xpos, bool dir)
        {
            if (auto_range)
            {
                View.Set(Min, Max);
            }
            if (Range < min_range)
            {
                View.High = View.Low + min_range;
            }
            else if (Range > max_range)
            {
                View.High = View.Low + max_range;
            }
            axis.label_right_align = dir;
            float w = 1 - 16.0f / axis.Rect.Width;
            axis.minor_tick = new Span(w, 1);
            axis.label_pos = -20;
            axis.Rect = new System.Drawing.Rectangle((int)0, (int)graph_Control.GraphTop, (int)graph_Control.GraphWidth, (int)graph_Control.GraphHeight);
            axis.SetVisibleRange(View);
        }

        public void DrawZeroLine(RenderTarget rt)
        {
            CreateResources(rt);
            axis.DrawHorizontalScale(rt, zero_brush, zero_strokestyle, graph_Control.GraphRectF, true);
        }

        public void DrawScale(RenderTarget rt)
        {
            CreateResources(rt);
            axis.DrawAxis(rt, graph_Control.DW, scale_normal_text, scale_bold_text, scale_major_brush, scale_minor_brush, scale_major_label, scale_minor_label);
        }

        public bool IsHoverMarked
        {
            get
            {
                return marker_lookup.Contains(draw_index);
            }
        }

        public void AddMarker()
        {
            if(draw_index != -1)
            {
                marker_lookup.Add(draw_index);
            }
            else
            {
            }
        }

        public void RemoveMarker()
        {
            marker_lookup.Remove(draw_index);
        }

        public bool IsMarked(int index)
        {
            return marker_lookup.Contains(index);
        }

        public void DrawMarkers(RenderTarget rt)
        {
            CreateResources(rt);
            List<int> dead_markers = new List<int>();

            int w = graph_Control.Width;
            int h = graph_Control.Height;
            foreach (var i in marker_lookup)
            {
                try
                {
                    D2DPointF pos = graph_Control.PosF(stream.Get(i), this);
                    int mark_size = 5;

                    float left = pos.X - mark_size;
                    float right = pos.X + mark_size;
                    float top = pos.Y - mark_size;
                    float bottom = pos.Y + mark_size;
                    if (i == draw_index && hover)
                    {
                        left = (float)graph_Control.GraphLeft;
                        right = (float)graph_Control.GraphRight;
                        top = (float)graph_Control.GraphTop;
                        bottom = (float)graph_Control.GraphBottom;
                    }
                    else
                    {
                        rt.DrawRect(label_text_brush, 1, RectF.FromLTBR(left, top, right, bottom));
                    }
                    rt.DrawLine(label_text_brush, 1, new D2DPointF(left, pos.Y), new D2DPointF(right, pos.Y));
                    rt.DrawLine(label_text_brush, 1, new D2DPointF(pos.X, top), new D2DPointF(pos.X, bottom));
                }
                catch (DataExpiredException)
                {
                    dead_markers.Add(i);
                }
            }
            foreach (var i in dead_markers)
            {
                marker_lookup.Remove(i);
            }
        }

        public bool DrawValuePopup(RenderTarget renderTarget)
        {
            if (hover && draw_index >= 0)
            {
                CreateResources(renderTarget);
                Reading reading = stream.Get(draw_index);
                string text = string.Format("{0}\n{1} {2}\n{3}",
                                            reading.Time.ToString(@"dd\:hh\:mm\:ss\.ff"),
                                            reading.Max.ToString(),
                                            Stream.ChannelName(stream.Channel),
                                            stream.Device.DisplayName);
                using (TextLayout textLayout = graph_Control.DW.CreateTextLayout(text, bold, (float)graph_Control.GraphWidth, (float)graph_Control.GraphHeight))
                {
                    D2DSizeF s = new D2DSizeF(textLayout.Metrics.Width, textLayout.Metrics.Height);
                    D2DPointF pos = graph_Control.PosF(reading, this);
                    D2DSizeF s2 = s;
                    s2.Width /= 2;
                    s2.Height /= 2;
                    D2DPointF border = new D2DPointF(8, 8);
                    D2DPointF top_left = new D2DPointF(pos.X - s.Width - border.X * 4, pos.Y - s.Height - border.Y * 4);
                    s.Width += border.X;
                    s.Height += border.Y;
                    RectF rect = new RectF(top_left, s);
                    D2DPointF offset = new D2DPointF(rect.Width - 3, rect.Height - 3);
                    if (rect.X < graph_Control.GraphLeft)
                    {
                        rect = new RectF(rect.X + rect.Width + border.X * 6, rect.Y, rect.Width, rect.Height);
                        offset.X = 3;
                    }
                    if (rect.Y < graph_Control.GraphTop)
                    {
                        rect = new RectF(rect.X, rect.Y + rect.Height + border.Y * 6, rect.Width, rect.Height);
                        offset.Y = 3;
                    }
                    RoundedRect rr = new RoundedRect(rect, 4, 4);
                    renderTarget.FillRoundedRect(label_background_brush, rr);
                    renderTarget.DrawRoundedRect(label_outline_brush, 2, rr);
                    renderTarget.DrawLine(label_text_brush, 2, new D2DPointF(rect.X, rect.Y).Add(offset), pos);
                    renderTarget.FillEllipse(label_text_brush, new Ellipse(pos, 4, 4));
                    rect.X += 5;
                    rect.Y += 5;
                    renderTarget.DrawText(text, label_font, rect, label_text_brush, DrawTextOptions.None, MeasuringMode.GdiClassic);
                    return true;
                }
            }
            return false;
        }

        private void DrawSection(int section_index)
        {
            int base_index = stream.SectionBase(section_index);
            int end = stream.SectionEnd(section_index);
            int length = end - base_index;

            DrawableSection ds = new DrawableSection();

            List<D2DPointF> top_bit = new List<D2DPointF>();
            ds.points = top_bit;

            D2DPointF mouse_pos_f = graph_Control.MousePosF();

            if (length == 0 || graph_Control.GraphWidth < 2)
            {
                return;
            }

            // find the left, right indices
            int left = base_index;

            int len = length - 1;
            double lxp = -1;
            double nxp;
            for (int i = base_index; i < end; ++i)
            {
                nxp = graph_Control.X(stream.Get(i));
                if (nxp >= 0)
                {
                    break;
                }
                lxp = nxp;
                left = i;
            }
            if (lxp > graph_Control.GraphRight)
            {
                return;
            }

            int right = end - 1;
            for (int i = end - 1; i > left; --i)
            {
                right = i;
                if (graph_Control.X(stream.Get(i)) < graph_Control.GraphRight)
                {
                    break;
                }
            }

            right = Math.Min(right + 1, end - 1);

            if (right - left < 1)
            {
                return;
            }

            top_bit.Capacity = right - left + 3;

            // OK, plot from left to right, quarter of whole pixels only
            System.Drawing.PointF np = graph_Control.MaxPos(stream.Get(left), this);
            float min = np.Y;
            float max = np.Y;
            float last_x = np.X;
            int last_i = left;
            for (int i = left; i <= right; ++i)
            {
                Reading r = stream.Get(i);
                np = graph_Control.MaxPos(r, this);
                float nx = np.X;
                if (i == left || (nx - last_x) > 0.25f)  // it can scroll at 1/4 of a pixel, for smoothlinessness
                {
                    AddPoint(top_bit, last_x, min, max, last_i, mouse_pos_f);
                    min = np.Y;
                    max = np.Y;
                    last_x = nx;
                    last_i = i;
                }
                else
                {
                    if (np.Y < max)
                    {
                        max = np.Y;
                    }
                    if (np.Y > min)
                    {
                        min = np.Y;
                    }
                }
            }
            AddPoint(top_bit, last_x, min, max, last_i, mouse_pos_f);

            // now fuck all that and see if they're hovering near a marker
            double cheat_best_distance = hover_threshold_distance;
            foreach (int i in marker_lookup)
            {
                D2DPointF pos = graph_Control.PosF(stream.Get(i), this);
                var distance = pos.DistanceFrom(mouse_pos_f);
                if (distance < cheat_best_distance)
                {
                    cheat_best_distance = distance;
                    best_distance = distance;
                    best_data_index = i;
                }
            }

            if (top_bit != null && top_bit.Count > 2)
            {
                drawable_sections.Add(ds);
            }
        }

        private bool AddPoint(List<D2DPointF> top_bit, float x, float min, float max, int i, D2DPointF mouse_pos)
        {
            D2DPointF p = new D2DPointF(x, max);
            top_bit.Add(p);
            int b = top_bit.Count - 1;
            int a = b - 1;
            bool rc = false;
            if(a >= 0)
            {
                double along = mouse_pos.DistanceAlong(top_bit[a], top_bit[b]);
                double c = Clamp(along, 0, 1);
                D2DPointF closest = top_bit[a].Add(top_bit[b].Sub(top_bit[a]).Scale(c));
                double distance = closest.DistanceFrom(mouse_pos);
                if (distance < best_distance)
                {
                    best_distance = distance;
                    best_data_index = (c < 0.5) ? i - 1 : i;
                    rc = true;
                }
            }
            if (min != max)
            {
                top_bit.Add(new D2DPointF(x, min));
            }
            return rc;
        }

        public void PreDraw()
        {
            //draw_index = -1;
            best_data_index = -1;
            drawable_sections.Clear();
            best_distance = hover_threshold_distance;

            for(int i = 0; i<stream.SectionCount; ++i)
            {
                DrawSection(i);
            }
        }

        public void Draw(RenderTarget rt)
        {
            CreateResources(rt);

            rt.AntialiasMode = AntialiasMode.PerPrimitive;
            for (int i = 0; i < drawable_sections.Count; ++i)
            {
                var drawable_section = drawable_sections[i];
                if (drawable_section.points.Count > 1)
                {
                    float xr = drawable_section.points[drawable_section.points.Count - 1].X;
                    float xl = drawable_section.points[0].X;
                    float yz = (float)graph_Control.Y(0, this);
                    drawable_section.points.Add(new D2DPointF(xr, yz));
                    drawable_section.points.Add(new D2DPointF(xl, yz));
                    using (var geometry = rt.GetFactory().CreatePathGeometry())
                    {
                        using (var sink = geometry.Open())
                        {
                            sink.BeginFigure(drawable_section.points[0], FigureBegin.Filled);
                            sink.AddLines(drawable_section.points.GetUnderlyingArray(), 1, drawable_section.points.Count - 1);
                            sink.EndFigure(FigureEnd.Closed);
                            sink.Close();
                            using (var brush = rt.CreateSolidColorBrush(color))
                            {
                                brush.Opacity = 0.25f;
                                rt.FillGeometry(brush, geometry);
                            }
                        }
                    }
                    using (var geometry = rt.GetFactory().CreatePathGeometry())
                    {
                        using (var sink = geometry.Open())
                        {
                            sink.SetSegmentFlags(PathSegment.ForceRoundLineJoin);
                            sink.BeginFigure(drawable_section.points[0], FigureBegin.Hollow);
                            sink.AddLines(drawable_section.points.GetUnderlyingArray(), 1, drawable_section.points.Count - 3);
                            sink.EndFigure(FigureEnd.Open);
                            sink.SetSegmentFlags(PathSegment.ForceRoundLineJoin);
                            sink.Close();
                            using (var brush = rt.CreateSolidColorBrush(color))
                            {
                                rt.DrawGeometry(brush, selected ? 4 : (hover ? 3 : 2), geometry);
                            }
                        }
                    }
                }
            }
        }

        public void SnapshotHover()
        {
            draw_index = best_data_index;
        }
    }
}
