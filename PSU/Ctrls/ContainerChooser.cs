﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Locomo.Ctrls
{
    public class ContainerChooser : Panel
    {
        public ContainerChooser(Form f, Type child_type, bool allow_empty_select) : base()
        {
            this.child_type = child_type;
            this.allow_empty_select = allow_empty_select;

            Visible = false;
            opacity = 90;
            back_brush = new SolidBrush(BackColor);
            RectangleColor = Color.FromArgb(64, 128, 255);
            draw_rectangle = new Rectangle(0, 0, Width / 2, Height / 2);
            Size form_size = new Size(f.Right - f.Left, f.Bottom - f.Top);
            Rectangle inner = f.RectangleToScreen(f.ClientRectangle);
            Point top_left = new Point(inner.Left - f.Left, inner.Top - f.Top);
            snapshot = new Bitmap(form_size.Width, form_size.Height);
            client_rectangle = new Rectangle(top_left, f.ClientRectangle.Size);
            f.DrawToBitmap(snapshot, new Rectangle(0, 0, form_size.Width, form_size.Height));
            DoubleBuffered = true;
            DrawRectangle = Rectangle.Empty;
            Location = Point.Empty;
            Size = f.ClientRectangle.Size;
            Dock = DockStyle.Fill;
            f.Controls.Add(this);
            PerformLayout();
            Visible = true;
            Capture = true;
        }

        public enum SplitType
        {
            Top,
            Left,
            Bottom,
            Right,
            Fill
        }

        private Type child_type;
        private bool allow_empty_select;
        private Rectangle draw_rectangle;
        private Color rect_color = Color.Magenta;
        private SolidBrush back_brush;
        private SolidBrush rect_brush;
        private Pen rect_pen;
        private int opacity;
        private Control owner;
        private Rectangle client_rectangle;
        private PanelContainer drag_target_container; // which it's hovering on
        private Bitmap snapshot;    // snapshot of the whole form
        private SplitType split_type = SplitType.Fill;

        public void Cleanup()
        {
            Capture = false;

            if (snapshot != null) snapshot.Dispose();
            if (back_brush != null) back_brush.Dispose();
            if (rect_brush != null) rect_brush.Dispose();
            if (rect_pen != null) rect_pen.Dispose();
            snapshot = null;
            back_brush = null;
            rect_brush = null;
            rect_pen = null;
        }

        public PanelContainer TargetContainer
        {
            get
            {
                return drag_target_container;
            }
        }

        static RectangleF[] split_rectangles = new RectangleF[]
        {
            new RectangleF(0, 0, 1, 0.5f),
            new RectangleF(0, 0, 0.5f, 1),
            new RectangleF(0, 0.5f, 1, 1),
            new RectangleF(0.5f, 0, 1, 1),
            new RectangleF(0, 0, 1, 1),
        };

        private PanelContainer FindContainer(Control root, Control parent, Type t, Point p)
        {
            foreach (Control c in parent.Controls)
            {
                var cc = c as PanelContainer;
                if (cc != null && cc.Visible)
                {
                    Rectangle r = c.DisplayRectangle;
                    r.Offset(root.PointToClient(c.PointToScreen(Location)));
                    bool for_consideration = cc.ActivePanel == null && allow_empty_select;
                    if (!for_consideration)
                    {
                        Type panel_type = cc.ActivePanel?.GetType();    // could be null
                        for_consideration = panel_type == t || panel_type.IsSubclassOf(t);
                    }
                    if (for_consideration && r.Contains(p))
                    {
                        int x = (p.X - r.Left) * 5 / r.Width;
                        int y = (p.Y - r.Top) * 5 / r.Height;
                        bool topright = x > y;
                        bool bottomright = x > (r.Height - y);
                        if (y == 0) split_type = SplitType.Top;
                        else if(y == 4) split_type = SplitType.Bottom;
                        else
                        {
                            switch (x)
                            {
                                case 0: split_type = SplitType.Left; break;
                                case 4: split_type = SplitType.Right; break;
                                default: split_type = SplitType.Fill; break;
                            }
                        }
                        return cc;
                    }
                }
                Control f = FindContainer(root, c, t, p);
                if (f != null)
                {
                    return (PanelContainer)f;
                }
            }
            return null;
        }

        protected override void OnParentChanged(EventArgs e)
        {
            base.OnParentChanged(e);
            if(Parent != null)
            {
                BringToFront();
                Focus();
                Capture = true;
                Refresh();
            }
            else
            {
                Cleanup();
            }
        }

        public PanelContainer End(out SplitType split_type)
        {
            split_type = this.split_type;
            FindForm().Controls.Remove(this);

            if(split_type != SplitType.Fill)
            {
                // create a new container and return that
                Orientation o = (split_type == SplitType.Left || split_type == SplitType.Right) ? Orientation.Vertical : Orientation.Horizontal;
                var new_container = TargetContainer.DoSplit(o, split_type == SplitType.Left || split_type == SplitType.Top);
                if(new_container != null)
                {
                    PanelContainer p = new_container.Panel1.Controls[0] as PanelContainer;
                    if (split_type == SplitType.Right || split_type == SplitType.Bottom)
                    {
                        p = new_container.Panel2.Controls[0] as PanelContainer;
                    }
                    return p;
                }
            }
            return TargetContainer;
        }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            Point pos = PointToClient(Cursor.Position);
            Control f = FindForm();
            PanelContainer under = FindContainer(f, f, child_type, pos);
            if (under != null)
            {
                drag_target_container = under;
                Rectangle r = drag_target_container.DisplayRectangle;
                RectangleF s = split_rectangles[(int)split_type];
                int x = (int)(r.Left + r.Width * s.X);
                int y = (int)(r.Top + r.Height * s.Y);
                int r1 = (int)(r.Left + r.Width * s.Width);
                int b1 = (int)(r.Top + r.Height * s.Height);
                int w = r1 - x;
                int h = b1 - y;
                r = new Rectangle(x, y, w, h);
                r.Offset(f.PointToClient(drag_target_container.PointToScreen(Location)));
                DrawRectangle = r;
            }
            else
            {
                drag_target_container = null;
                DrawRectangle = Rectangle.Empty;
            }
            base.OnMouseMove(e);
        }

        public Rectangle DrawRectangle
        {
            get
            {
                return draw_rectangle;
            }
            set
            {
                draw_rectangle = value;
                Invalidate();
            }
        }

        public Control Owner
        {
            set
            {
                owner = value;
            }
        }

        public Color RectangleColor
        {
            get
            {
                return rect_color;
            }
            set
            {
                rect_color = value;
                rect_brush = new SolidBrush(Color.FromArgb(opacity * 128 / 100, rect_color));
                rect_pen = new Pen(Color.FromArgb(opacity * 255 / 100, rect_color), 4);
            }
        }

        protected override void OnPaintBackground(PaintEventArgs e)
        {
            if (snapshot != null)
            {
                e.Graphics.DrawImage(snapshot, ClientRectangle, client_rectangle, GraphicsUnit.Pixel);
            }
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            e.Graphics.FillRectangle(rect_brush, DrawRectangle);
        }
    }
}
