﻿namespace Locomo.Ctrls
{
    partial class AddControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.add_button = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // add_button
            // 
            this.add_button.Location = new System.Drawing.Point(0, 0);
            this.add_button.Margin = new System.Windows.Forms.Padding(0);
            this.add_button.Name = "add_button";
            this.add_button.Size = new System.Drawing.Size(112, 35);
            this.add_button.TabIndex = 47;
            this.add_button.Text = "Connect";
            this.add_button.UseVisualStyleBackColor = true;
            // 
            // AddControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.add_button);
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "AddControl";
            this.Size = new System.Drawing.Size(112, 35);
            this.Click += new System.EventHandler(this.AddControl_Click);
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.Button add_button;
    }
}
