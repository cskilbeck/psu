﻿using System.Diagnostics;
using System.Windows.Forms;

namespace Locomo.Ctrls
{
    public class ContainerToolsHost : ToolStripControlHost
    {
        public ContainerToolsHost() : base(new ContainerTools())
        {
            Resize();
        }

        public ContainerToolsHost(ToolStrip toolstrip) : base(new ContainerTools(toolstrip))
        {
            Resize();
        }

        public ToolStrip ToolStrip
        {
            get
            {
                ContainerTools c = Control as ContainerTools;
                return c.toolStrip1;
            }
        }

        public void Resize()
        {
            ContainerTools c = Control as ContainerTools;
            c.AutoSize = false;
            c.MinimumSize = new System.Drawing.Size(c.toolStrip1.Width, c.Height);
            c.Width = c.toolStrip1.Width;
            AutoSize = false;
            Size = c.Size;
            Width = c.Width - 52;
            //Width = 0;    // @yuck remove horizontal padding which winforms adds (?High DPI problem?)
        }
    }
}
