﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Reflection;
using System.Windows.Forms;
using System.Xml;
using Locomo.Panels;
using static Locomo.Extensions;
using Logging;

namespace Locomo.Ctrls
{
    public class ChildPanel: UserControl
    {
        ToolStripEx original_toolstrip;
        ToolStripButton caption_button;
        int old_y;
        bool inside;

        public PanelContainer host_control;

        public static int hover_height_threshold = 16;

        public delegate void ChildMouseEnterDelegate(MouseEventArgs e);
        public delegate void ChildMouseLeaveDelegate(EventArgs e);

        public delegate void BeingAddedDelegate(PanelContainer parent, ToolStripButton button);
        public delegate void BeingClosedDelegate(PanelContainer parent, ToolStripButton button);
        public delegate void TitleChangedDelegate(ChildPanel panel);

        public delegate void MenuSetupDelegate(ToolStripMenuItem m, PanelContainer c, Type panel_type);

        public event ChildMouseEnterDelegate ChildMouseEnter;
        public event ChildMouseLeaveDelegate ChildMouseLeave;
        public event BeingAddedDelegate BeingAdded;
        public event BeingClosedDelegate BeingClosed;
        public event TitleChangedDelegate TitleChanged;

        public static Dictionary<Type, MenuSetupDelegate> MenuSetupEvent = new Dictionary<Type, MenuSetupDelegate>();
        public static List<Type> Classes = new List<Type>();
        public static Dictionary<Type, string> Names = new Dictionary<Type, string>();

        protected string panel_title = "WTF";

        private bool active = false;

        public bool Active
        {
            get
            {
                return active;
            }
            set
            {
                active = value;
            }
        }

        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // ChildPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "ChildPanel";
            this.Size = new System.Drawing.Size(439, 429);
            this.ResumeLayout(false);

        }

        public ChildPanel() : base()
        {
            InitializeComponent();
        }

        protected override void SetVisibleCore(bool value)
        {
            base.SetVisibleCore(value);
        }

        public static void RegisterChildPanel(Type type, string name)
        {
            Classes.Add(type);
            Names.Add(type, name);
            foreach (MethodInfo m in FindStaticMethodsWithAttribute<SetupMenuAttribute>(type))
            {
                // @note only 1 SetupMenu delegate per ChildPanel class
                if (MenuSetupEvent.ContainsKey(type))
                {
                    MessageBox.Show("Extraneous Menu Setup callback (" + m.Name + ") in " + type.Name + " ignored...");
                    break;
                }
                MenuSetupEvent[type] = (MenuSetupDelegate)m.CreateDelegate(typeof(MenuSetupDelegate));
            }
        }

        public static void RegisterClasses()
        {
            foreach (Pair<Type, ChildPanelAttribute> type in FindTypesWithAttribute<ChildPanelAttribute>())
            {
                try
                {
                    RegisterChildPanel(type.First, type.Second.name);
                }
                catch (Exception e)
                {
                    MessageBox.Show("Error registering ChildPanel, type: " + type.First + "\n\n" + e.Message);
                }
            }
        }

        // Helper for all the panels which need a device
        public static void SetupDeviceMenu(ToolStripMenuItem m, PanelContainer parent, Type panel_type)
        {
            m.Enabled = Device.Devices.Count > 0;

            foreach (var device in Device.Devices)
            {
                if (device.Controllable)
                {
                    var ci = new ToolStripMenuItem(device.DisplayName);
                    ci.Click += (object s2, EventArgs eva2) =>
                    {
                        DevicePanel p = CreateChildPanel(panel_type) as DevicePanel;
                        if(p != null)
                        {
                            parent.AddControl(p, true);
                            p.Connect(device as SCPIDevice);
                        }
                    };
                    m.DropDownItems.Add(ci);
                }
                else
                {
                    Log.Warning("NON CONTROLLABLE!?");
                }
            }
        }

        public static bool SetupMenu(ToolStripMenuItem m, Type panel_type, PanelContainer parent)
        {
            MenuSetupDelegate d;
            if (MenuSetupEvent.TryGetValue(panel_type, out d))
            {
                d.Invoke(m, parent, panel_type);
                return true;
            }
            return false;
        }

        public static ChildPanel CreateChildPanel(Type panel_type)
        {
            if (panel_type.IsSubclassOf(typeof(ChildPanel)))
            {
                var c = panel_type.GetConstructor(Type.EmptyTypes);
                return (c != null) ? (ChildPanel)c.Invoke(null) : null;
            }
            return null;
        }

        public virtual void OnTitleChanged()
        {
            if (TitleChanged != null)
            {
                foreach (Delegate del in TitleChanged.GetInvocationList())
                {
                    del.DynamicInvoke(this);
                }
            }
        }

        public void SetName(string name)
        {
            ToolStripItem item = (ToolStripItem)Tag;
            if (item != null && name != null && name.Length > 0)
            {
                item.Text = name;
            }
        }

        public virtual void OnBeingAdded(PanelContainer parent, ToolStripButton button)
        {
            RemoveMouseMoveHandler(this);
            MouseLeave -= MouseLeaveHandler;
            MouseLeave += MouseLeaveHandler;
            AddMouseMoveHandler(this);
            caption_button = button;
            BeingAdded?.Invoke(parent, button);
        }

        public virtual void OnBeingClosed(PanelContainer parent, ToolStripButton button)
        {
            BeingClosed?.Invoke(parent, button);
        }

        public virtual void SaveTo(XmlElement e)
        {
            e.Save("Name", Title);
            e.Save("Active", Active);
            Log.Debug("Saved active = " + Active + " for " + Name);
        }

        public virtual void LoadFrom(XmlElement e)
        {
            Title = e.Load("Name", Title);
            Active = e.Load<bool>("Active");
        }

        public virtual string Title
        {
            get
            {
                return panel_title;
            }
            set
            {
                panel_title = value;
                OnTitleChanged();
            }
        }

        private void AddMouseMoveHandler(Control c)
        {
            if (c.Visible)
            {
                c.MouseMove += MouseHandler;
            }
            if (c.Controls.Count > 0)
            {
                foreach (Control ct in c.Controls)
                {
                    AddMouseMoveHandler(ct);
                }
            }
        }

        private void RemoveMouseMoveHandler(Control c)
        {
            c.MouseMove -= MouseHandler;
            foreach (Control ct in c.Controls)
            {
                RemoveMouseMoveHandler(ct);
            }
        }

        private void MouseLeaveHandler(object sender, EventArgs e)
        {
            if (GetChildAtPoint(PointToClient(MousePosition)) == null)
            {
                if (old_y < hover_height_threshold && inside)
                {
                    ChildMouseLeave?.Invoke(e);
                }
                inside = false;
            }
        }

        private void MouseHandler(object sender, MouseEventArgs e)
        {
            Point p = PointToClient((sender as Control).PointToScreen(e.Location));
            if (p.Y < hover_height_threshold && (old_y >= hover_height_threshold || !inside))
            {
                inside = true;
                ChildMouseEnter?.Invoke(e);
            }
            else if(old_y < hover_height_threshold)
            {
                inside = false;
                ChildMouseLeave?.Invoke(e);
            }
            old_y = p.Y;
        }

        public ToolStripEx GetToolStrip()
        {
            if(original_toolstrip == null)
            {
                var t = MyToolStrip();
                if (t != null)
                {
                    Controls.Remove(t);
                    t.Visible = true;
                    t.Name = Title;
                }
                original_toolstrip = t;
            }
            return original_toolstrip;
        }

        public virtual ToolStripEx MyToolStrip()
        {
            return null;
        }

        public virtual bool ConfigMenu(ToolStripItemCollection c)
        {
            return false;
        }

        public void UIThread(Action t)
        {
            BeginInvoke(t);
        }
    }
}
