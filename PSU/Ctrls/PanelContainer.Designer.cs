﻿namespace Locomo.Ctrls
{
    [System.ComponentModel.DesignerCategory("UserControl")]
    partial class PanelContainer
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.popup_toolStrip = new System.Windows.Forms.ToolStrip();
            this.vertical_split_toolStripButton = new System.Windows.Forms.ToolStripButton();
            this.horizontal_split_toolstripButton = new System.Windows.Forms.ToolStripButton();
            this.fix_toolStripButton = new System.Windows.Forms.ToolStripButton();
            this.pin_toolStripButton = new System.Windows.Forms.ToolStripButton();
            this.toolStrip = new Locomo.Ctrls.ToolStripEx();
            this.toolStripButton3 = new System.Windows.Forms.ToolStripButton();
            this.utility_toolstrip_button = new System.Windows.Forms.ToolStripDropDownButton();
            this.add_panel_toolStripDropDownButton = new System.Windows.Forms.ToolStripDropDownButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.popup_toolStrip.SuspendLayout();
            this.toolStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // popup_toolStrip
            // 
            this.popup_toolStrip.CanOverflow = false;
            this.popup_toolStrip.Dock = System.Windows.Forms.DockStyle.None;
            this.popup_toolStrip.GripMargin = new System.Windows.Forms.Padding(0);
            this.popup_toolStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.popup_toolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.vertical_split_toolStripButton,
            this.horizontal_split_toolstripButton,
            this.fix_toolStripButton,
            this.pin_toolStripButton});
            this.popup_toolStrip.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.popup_toolStrip.Location = new System.Drawing.Point(0, 0);
            this.popup_toolStrip.Name = "popup_toolStrip";
            this.popup_toolStrip.Padding = new System.Windows.Forms.Padding(0);
            this.popup_toolStrip.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.popup_toolStrip.Size = new System.Drawing.Size(94, 25);
            this.popup_toolStrip.TabIndex = 4;
            this.popup_toolStrip.Text = "toolStrip1";
            // 
            // vertical_split_toolStripButton
            // 
            this.vertical_split_toolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.vertical_split_toolStripButton.Image = global::Locomo.Properties.Resources.VerticalSplitButton;
            this.vertical_split_toolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.vertical_split_toolStripButton.Name = "vertical_split_toolStripButton";
            this.vertical_split_toolStripButton.Size = new System.Drawing.Size(23, 22);
            this.vertical_split_toolStripButton.Text = "toolStripButton6";
            this.vertical_split_toolStripButton.Click += new System.EventHandler(this.toolStripButtonSplitVertical_Click);
            // 
            // horizontal_split_toolstripButton
            // 
            this.horizontal_split_toolstripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.horizontal_split_toolstripButton.Image = global::Locomo.Properties.Resources.HorizontalSplitButton;
            this.horizontal_split_toolstripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.horizontal_split_toolstripButton.Name = "horizontal_split_toolstripButton";
            this.horizontal_split_toolstripButton.Size = new System.Drawing.Size(23, 22);
            this.horizontal_split_toolstripButton.Text = "toolStripButton7";
            this.horizontal_split_toolstripButton.Click += new System.EventHandler(this.toolStripButtonSplitHorizontal_Click);
            // 
            // fix_toolStripButton
            // 
            this.fix_toolStripButton.CheckOnClick = true;
            this.fix_toolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.fix_toolStripButton.Image = global::Locomo.Properties.Resources.fix;
            this.fix_toolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.fix_toolStripButton.Name = "fix_toolStripButton";
            this.fix_toolStripButton.Size = new System.Drawing.Size(23, 22);
            this.fix_toolStripButton.Text = "toolStripButton8";
            this.fix_toolStripButton.Click += new System.EventHandler(this.toolStripButtonFix_Clicked);
            // 
            // pin_toolStripButton
            // 
            this.pin_toolStripButton.CheckOnClick = true;
            this.pin_toolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.pin_toolStripButton.Image = global::Locomo.Properties.Resources.pin;
            this.pin_toolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.pin_toolStripButton.Name = "pin_toolStripButton";
            this.pin_toolStripButton.Size = new System.Drawing.Size(23, 22);
            this.pin_toolStripButton.Text = "toolStripButton9";
            this.pin_toolStripButton.Click += new System.EventHandler(this.toolStripButtonPin_Click);
            // 
            // toolStrip
            // 
            this.toolStrip.AllowMerge = false;
            this.toolStrip.AutoSize = false;
            this.toolStrip.BackColor = System.Drawing.Color.Transparent;
            this.toolStrip.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton3,
            this.utility_toolstrip_button,
            this.add_panel_toolStripDropDownButton,
            this.toolStripSeparator1});
            this.toolStrip.Location = new System.Drawing.Point(25, 269);
            this.toolStrip.Name = "toolStrip";
            this.toolStrip.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.toolStrip.Size = new System.Drawing.Size(251, 25);
            this.toolStrip.TabIndex = 3;
            this.toolStrip.Text = "toolStrip1";
            // 
            // toolStripButton3
            // 
            this.toolStripButton3.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripButton3.BackColor = System.Drawing.Color.Transparent;
            this.toolStripButton3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton3.Image = global::Locomo.Properties.Resources.CloseButton_disabled;
            this.toolStripButton3.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButton3.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton3.Name = "toolStripButton3";
            this.toolStripButton3.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton3.Text = "toolStripButtonClose";
            this.toolStripButton3.ToolTipText = "Close";
            this.toolStripButton3.Click += new System.EventHandler(this.toolStripButtonClose_Click);
            // 
            // utility_toolstrip_button
            // 
            this.utility_toolstrip_button.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.utility_toolstrip_button.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.utility_toolstrip_button.Image = global::Locomo.Properties.Resources.Options;
            this.utility_toolstrip_button.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.utility_toolstrip_button.Name = "utility_toolstrip_button";
            this.utility_toolstrip_button.Size = new System.Drawing.Size(32, 22);
            this.utility_toolstrip_button.Text = "toolStripSplitButton1";
            // 
            // add_panel_toolStripDropDownButton
            // 
            this.add_panel_toolStripDropDownButton.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.add_panel_toolStripDropDownButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.add_panel_toolStripDropDownButton.Image = global::Locomo.Properties.Resources.PlusButton;
            this.add_panel_toolStripDropDownButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.add_panel_toolStripDropDownButton.Name = "add_panel_toolStripDropDownButton";
            this.add_panel_toolStripDropDownButton.Size = new System.Drawing.Size(29, 22);
            this.add_panel_toolStripDropDownButton.Text = "toolStripDropDownButton1";
            this.add_panel_toolStripDropDownButton.ToolTipText = "New panel";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // ContainerControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.Controls.Add(this.popup_toolStrip);
            this.Controls.Add(this.toolStrip);
            this.Name = "ContainerControl";
            this.Size = new System.Drawing.Size(437, 470);
            this.popup_toolStrip.ResumeLayout(false);
            this.popup_toolStrip.PerformLayout();
            this.toolStrip.ResumeLayout(false);
            this.toolStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Locomo.Ctrls.ToolStripEx toolStrip;
        private System.Windows.Forms.ToolStripButton toolStripButton3;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripDropDownButton add_panel_toolStripDropDownButton;
        private System.Windows.Forms.ToolStripDropDownButton utility_toolstrip_button;
        private System.Windows.Forms.ToolStrip popup_toolStrip;
        private System.Windows.Forms.ToolStripButton vertical_split_toolStripButton;
        private System.Windows.Forms.ToolStripButton horizontal_split_toolstripButton;
        private System.Windows.Forms.ToolStripButton fix_toolStripButton;
        private System.Windows.Forms.ToolStripButton pin_toolStripButton;
    }
}
