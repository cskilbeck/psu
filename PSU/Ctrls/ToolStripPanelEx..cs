﻿using System.Windows.Forms;
using System.Windows.Forms.Layout;
using static Locomo.Extensions;

namespace Locomo.Ctrls
{
    public class ToolStripPanelEx : ToolStripPanel
    {
        private Ctrls.ToolStripLayoutEngineEx layout_engine;

        public ToolStripPanelEx() : base()
        {
        }

        public override LayoutEngine LayoutEngine
        {
            get
            {
                if (layout_engine == null)
                {
                    layout_engine = new Ctrls.ToolStripLayoutEngineEx();
                }
                return layout_engine;
            }
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
        }

        protected override void WndProc(ref Message m)
        {
            const int WM_LBUTTONDOWN = 0x0201;
            const int HTCAPTION = 0x02;
            const int WM_NCLBUTTONDOWN = 0x00A1;

            if (m.Msg == WM_LBUTTONDOWN)
            {
                SendMessage(MainForm.Instance.Handle, WM_NCLBUTTONDOWN, HTCAPTION, 0);
                return;
            }
            base.WndProc(ref m);
        }
    }
}
