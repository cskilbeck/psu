﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using static Locomo.Extensions;

namespace Locomo.Ctrls
{
    public class SplitContainerEx : SplitContainer
    {
        public SplitContainerEx() : base()
        {
            MouseDown += on_MouseDown;
            MouseUp += on_MouseUp;
            MouseMove += on_MouseMove;
        }

        private void on_MouseDown(object sender, MouseEventArgs e)
        {
            ((SplitContainerEx)sender).IsSplitterFixed = true;
        }

        private void on_MouseUp(object sender, MouseEventArgs e)
        {
            ((SplitContainerEx)sender).IsSplitterFixed = false;
        }

        private void on_MouseMove(object sender, MouseEventArgs e)
        {
            SplitContainerEx s = (SplitContainerEx)sender;
            if (s.IsSplitterFixed)
            {
                if (e.Button == MouseButtons.Left)
                {
                    s.SplitterDistance = (s.Orientation == Orientation.Vertical) ? Clamp(e.X, 0, s.Width) : Clamp(e.Y, 0, s.Height);
                    s.Refresh();
                }
                else
                {
                    s.IsSplitterFixed = false;
                }
            }
        }

        public FixedPanel FixedPanelEx
        {
            get
            {
                return FixedPanel;
            }
            set
            {
                FixedPanel = value;
                if(Panel1.Controls.Count > 0)
                {
                    var panel1 = Panel1.Controls[0] as PanelContainer;
                    if (panel1 != null)
                    {
                        panel1.Fixed = value == FixedPanel.Panel1;
                    }
                }
                if (Panel2.Controls.Count > 0)
                {
                    var panel2 = Panel2.Controls[0] as PanelContainer;
                    if (panel2 != null)
                    {
                        panel2.Fixed = value == FixedPanel.Panel2;
                    }
                }
            }
        }
    }
}
