﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace Locomo.Ctrls
{
    public class DragValue : Control
    {
        int xpos;
        public double value;
        public DragTextBox TextBox;
        RangeSlider parent;
        private int offset;
        public Span range;
        private Rectangle filler;

        int old_y;

        Point drag_offset;
        bool drag;
        public delegate void Drag(object sender, ref Point pos);
        public delegate void DragFinished(object sender, ref Point pos);
        public delegate void NewText(object sender, string text);
        public event Drag OnDrag;
        public event DragFinished OnDragFinished;

        public delegate void NewValue(double v, bool finalValue);
        public event NewValue OnNewValue;

        Point[] tick_coords;

        public void LayoutEx()
        {
            Height = ((TextBox.Height + parent.HeightPadding) | 1) + 2;
            DoubleBuffered = true;
            Controls.Add(TextBox);
            TextBox.Left = 1;
            TextBox.Top = parent.HeightPadding / 2;
            PerformLayout();
            Rectangle d = new Rectangle(0, 0, TextBox.Width + 2, Height - 1);
            //                d.Offset(2, 1);
            Point tr = new Point(d.Height / 2, 0);
            switch (offset)
            {
                case -1:
                    //                        d.Offset(0, -1);
                    tick_coords = new Point[] { d.TopRight(), d.TopRight().Add(tr), d.MidRight() };
                    break;
                case 0:
                    tick_coords = new Point[] { d.TopRight(), d.MidRight().Add(tr), d.BottomRight() };
                    break;
                case 1:
                    tick_coords = new Point[] { d.MidRight(), d.BottomRight().Add(tr), d.BottomRight() };
                    break;
            };
            filler = new Rectangle(0, 0, d.Width, Height);
        }

        public DragValue(int x, int y, int offset, RangeSlider parent, int left, int right) : base()
        {
            //SetStyle(ControlStyles.OptimizedDoubleBuffer, true);

            this.offset = offset;
            this.parent = parent;
            range = new Span(0, 1);
            xpos = x;
            BackColor = SystemColors.ButtonFace;
            Location = new Point(x, y);
            old_y = y;
            // Create the DragTextBox
            TextBox = new DragTextBox(parent.ValueFont);
            Left = left * (TextWidth + 4);
            Width = right - Left;
            LayoutEx();
            OnDrag += Value_OnDrag;
            TextBox.OnNewText += TextBox_OnNewText;
            // OnDragFinished += Value_OnDragFinished;
            BackColor = Color.Red;
            TextBox.ForeColor = parent.ValueTextColor;
            TextBox.BackColor = parent.ValueBackColor;
        }

        private void TextBox_OnNewText(object sender, string text)
        {
            double v;
            if(double.TryParse(text, out v))
            {
                SetValue(v, true, true);
            }
            else
            {
                SetValue(value, true, false);
            }
        }

        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                //cp.ExStyle = cp.ExStyle | WS_EX_TRANSPARENT;
                return cp;
            }
        }
        public void Save(XmlNode elem, string name)
        {
            elem.Save(name, value);
        }

        public void Load(XmlNode elem, string name)
        {
            value = elem.Load(name, value);
            SetValue(value, true, false);
        }

        void SetPositionFromTick(int tick)
        {
            Top = tick - tick_coords[1].Y - 1;
        }

        int PositionFromTick(int tick)
        {
            return tick = tick_coords[1].Y;
        }

        public int TextWidth
        {
            get
            {
                return TextBox.Width + ArrowSize;
            }
        }

        public int TickWidth
        {
            get
            {
                return TextBox.Width + ArrowSize;
            }
        }

        public int ArrowSize
        {
            get
            {
                return Height / 2;
            }
        }

        public void OnMouseLeftButtonDown(Point p)  // client coords
        {
            drag_offset = p;
            drag = true;
        }

        public void OnMouseLeftButtonUp(Point p)    // client coords
        {
            drag = false;
            if (OnDragFinished != null)
            {
                Point newPos = new Point(p.X - drag_offset.X, p.Y - drag_offset.Y);
                OnDragFinished(this, ref newPos);
                Location = newPos;
            }
        }

        public void OnMouseMoved(Point p)           // client coords
        {
            if (drag)
            {
                Point newPos = Parent.PointToClient(PointToScreen(new Point(p.X - drag_offset.X, p.Y - drag_offset.Y)));
                OnDrag?.Invoke(this, ref newPos);
                Location = new Point(Left, newPos.Y);
            }
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            Pen border_pen = new Pen(((RangeSlider)Parent).ValueBorderColor);
            Brush border_brush = new SolidBrush(((RangeSlider)Parent).ValueBorderColor);

            //Pen back_pen = new Pen(((RangeSlider)Parent).ValueBackColor);
            Brush back_brush = new SolidBrush(((RangeSlider)Parent).ValueBackColor);

            Rectangle r = filler;
            //                r.Width += 1;
            r.Height -= 1;
            e.Graphics.FillRectangle(back_brush, r);
            e.Graphics.DrawRectangle(border_pen, r);
            e.Graphics.FillPolygon(border_brush, tick_coords);

            e.Graphics.DrawPolygon(border_pen, tick_coords);
            e.Graphics.DrawLine(border_pen, tick_coords[1], tick_coords[1].Add(new Point(Width, 0)));
            base.OnPaint(e);
        }

        protected override void OnPaintBackground(PaintEventArgs e)
        {
            e.Graphics.FillRectangle(new SolidBrush(Parent.BackColor), DisplayRectangle);
        }

        void UpdateValue(ref Point pos)
        {
            int new_y = (int)(Math.Max(0, Math.Min(parent.Height - Height, pos.Y)));
            double v = parent.ValueFromYCoordinate(new_y + tick_coords[1].Y);
            v = parent.range.Clamp(range.Clamp(v));
            v = parent.value_axis.range.RoundValue(v, 1);
            value = v;
            pos.Y = parent.intYCoordinateFromValue(value) - tick_coords[1].Y - 1;
            UpdateText();
        }

        void Value_OnDragFinished(object sender, ref Point pos)
        {
            pos.X = xpos;
            if (pos.Y == old_y)
            {
                return;
            }
            old_y = pos.Y;
            UpdateValue(ref pos);
            OnNewValue?.Invoke(value, true);
            //parent.Invalidate(false);
            //parent.Update();
        }

        void Value_OnDrag(object sender, ref Point pos)
        {
            pos.X = xpos;
            if (pos.Y == old_y)
            {
                return;
            }
            UpdateValue(ref pos);
            old_y = pos.Y;
            double min_val = parent.range.Low;
            double max_val = parent.range.High;
            if (parent.ShowMinMax)
            {
                min_val = parent.MinValue.value;
                max_val = parent.MaxValue.value;
            }
            parent.UpdateLimits();
            OnNewValue?.Invoke(value, false);
        }

        public void UpdateText()
        {
            TextBox.Text = parent.value_axis.range.FormatValue(value, 2);
        }

        public void SetValue(double v, bool updateText, bool callCallback)
        {
            parent.UpdateLimits();
            value = range.Clamp(v);
            if (updateText)
            {
                UpdateText();
            }
            UpdatePosition();
            if (OnNewValue != null && callCallback)
            {
                OnNewValue(value, false);
            }
        }

        public void UpdatePosition()
        {
            SetPositionFromTick(parent.intYCoordinateFromValue(value));
        }
    }
}
