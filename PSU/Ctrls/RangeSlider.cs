﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;
using System.Xml;
using static Locomo.Extensions;

namespace Locomo.Ctrls
{
    [System.ComponentModel.DesignerCategory("UserControl")]
    public class RangeSlider : Panel
    {
        public Span range;
        public Span old_range;
        public DragValue MaxValue;
        public DragValue MinValue;
        public DragValue CurrentValue;

        public ValueAxis value_axis;

        public Brush range_brush;

        public Tick tick;

        public Control range_control;

        double range_limit;

        int scale_width = SystemInformation.VerticalScrollBarWidth;
        int scale_left;
        bool drag;
        int drag_offset;
        bool show_minmax;
        double tick_value;

        Font regular;
        Font bold;

        private Color value_text_color = Color.MediumSpringGreen;
        private Color value_back_color = Color.Black;
        private Color value_border_color = Color.Black;
        private Font value_font = SystemFonts.DefaultFont;

        public delegate void NewValue(double v, bool finalValue);
        public event NewValue OnNewValue;

        private Bitmap backdrop;

        private Bitmap BackDrop
        {
            get
            {
                if (backdrop == null || !backdrop.Size.CompareTo(ClientSize))
                {
                    backdrop = new Bitmap(ClientSize.Width, ClientSize.Height, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
                }
                return backdrop;
            }
        }

        public RangeSlider()
            : base()
        {
            range_limit = 0.01f;
            range = new Span(0, 20);

            old_range = new Span();

            value_axis = new ValueAxis();
            value_axis.Width = 60;
            value_axis.Dock = DockStyle.Right;
            value_axis.range.actual_range = range;
            value_axis.range.SetVisibleRange(range.Low, range.High);
            value_axis.Margin = Padding.Empty;
            value_axis.range.OnScaleChanged += valueRange_OnScaleChanged;

            Controls.Add(value_axis);

            tick = new Tick();
            tick.Width = 11;
            tick.Height = 20;
            Controls.Add(tick);

            range_control = new Control();
            range_control.Paint += Range_control_Paint;
            Controls.Add(range_control);

            MouseWheel += RangeSlider_MouseWheel;
            MouseDown += RangeSlider_MouseDown;
            MouseMove += RangeSlider_MouseMove;
            MouseUp += RangeSlider_MouseUp;
            MouseDoubleClick += RangeSlider_MouseDoubleClick;
            SizeChanged += RangeSlider_SizeChanged;

            value_axis.MouseDoubleClick += RangeSlider_MouseDoubleClick;

            regular = new Font(DefaultFontFamily, SystemFonts.DefaultFont.Size, FontStyle.Regular);
            bold = new Font(DefaultFontFamily, SystemFonts.DefaultFont.Size, FontStyle.Bold);

            scale_left = Width - 66;

            SetupValues();
        }

        private void Range_control_Paint(object sender, PaintEventArgs e)
        {
            if (range_brush == null)
            {
                range_brush = new SolidBrush(ValueBorderColor);
            }
            e.Graphics.FillRectangle(range_brush, ClientRectangle);
        }

        public void Save(XmlNode e, string name)
        {
            XmlNode elem = e.Elem(name);
            if (ShowMinMax)
            {
                MaxValue.Save(elem, "High");
                MinValue.Save(elem, "Low");
            }
            CurrentValue.Save(elem, "Value");
            XmlExtensions.SerializeInto((XmlElement)elem, value_axis.range.VisibleRange, "View");
            e.AppendChild(elem);
        }

        public void Load(XmlNode e, string name, double max_range)
        {
            XmlNode c = e.SelectSingleNode(name);
            if(c != null)
            {
                if (ShowMinMax)
                {
                    MaxValue.Load(c, "High");
                    MinValue.Load(c, "Low");
                }
                CurrentValue.Load(c, "Value");
                Span s = XmlExtensions.Deserialize<Span>(c, "View");
                if(s != null)
                {
                    value_axis.range.SetVisibleRange(s);
                }
            }
        }

        void valueRange_OnScaleChanged(ValueRangeD2D v)
        {
            UpdateValuePositions();
        }

        public bool ShowMinMax
        {
            get
            {
                return show_minmax;
            }
            set
            {
                if(show_minmax != value || CurrentValue == null)
                {
                    show_minmax = value;
                    SetupValues();
                }
            }
        }

        public int MinWidth
        {
            get
            {
                int w = CurrentValue.TextWidth + 66;
                if (ShowMinMax)
                {
                    w += MaxValue.ArrowSize;
                }
                return w;
            }
        }

        private void UpdateValuePositions()
        {
            UpdateLimits();
            scale_left = Width - 64;
            if (ShowMinMax)
            {
                MinValue.Left = 0;
                MaxValue.Left = 0;
                MaxValue.Width = scale_left;
                MinValue.Width = scale_left;
            }
            int width = CurrentValue.TextWidth + 3;
            int diff = scale_left - width;
            int leftPos = ShowMinMax ?  MinValue.ArrowSize: 0;
            if (diff < leftPos)
            {
                diff = leftPos;
            }
            CurrentValue.Left = diff;
            CurrentValue.Width = width;

            double height = Height - CurrentValue.Height * 2;
            double rtop = (Height - height) / 2;
//            value_axis.range.Rect = new Rectangle(scale_left, (int)rtop, scale_width, (int)height);
            value_axis.range.padding = rtop + regular.Height / 2;
            if (ShowMinMax)
            {
                SetRange();
                MinValue.UpdatePosition();
                MaxValue.UpdatePosition();
            }
            CurrentValue.UpdatePosition();
        }

        private void SetupValues()
        {
            int leftPos = 0;

            if (ShowMinMax)
            {
                if (MaxValue != null)
                {
                    Controls.Remove(MaxValue);
                }
                if (MinValue != null)
                {
                    Controls.Remove(MinValue);
                }
                MinValue = new Ctrls.DragValue(0, 200, -1, this, 0, scale_left);
                MinValue.BackColor = value_back_color;
                MinValue.ForeColor = value_text_color;
                MaxValue = new Ctrls.DragValue(0, 100, 1, this, 0, scale_left);
                MaxValue.BackColor = value_back_color;
                MaxValue.ForeColor = value_text_color;
                Controls.Add(MaxValue);
                Controls.Add(MinValue);
                MaxValue.range.Set(range);
                MinValue.range.Set(range);
                MaxValue.SetValue(range.High, true, true);
                MinValue.SetValue(0, true, true);
                MaxValue.OnNewValue += maxTextBox_OnNewValue;
                MinValue.OnNewValue += minTextBox_OnNewValue;
                //maxTextBox.OnNewText += maxTextBox_OnNewText;
                //minTextBox.OnNewText += minTextBox_OnNewText;
                leftPos = MaxValue.TextWidth + 4;
                SetRange();
            }
            if (CurrentValue != null)
            {
                Controls.Remove(CurrentValue);
            }
            CurrentValue = new DragValue(leftPos, 100, 0, this, show_minmax ? 1 : 0, scale_left);
            Controls.Add(CurrentValue);
            UpdateValuePositions();
            CurrentValue.BackColor = value_back_color;
            CurrentValue.ForeColor = value_text_color;
            //valueTextBox.BorderStyle = BorderStyle.None;
            CurrentValue.range.Set(range);
            CurrentValue.OnNewValue += valueTextBox_OnNewValue;
            CurrentValue.SetValue(range.MidPoint, true, true);
            range_control.SendToBack();
            CurrentValue.BringToFront();
            value_axis.BringToFront();
            tick.BringToFront();
        }

        private int height_padding;

        public int HeightPadding
        {
            get
            {
                return height_padding;
            }
            set
            {
                height_padding = Math.Max(value, 2);
                CurrentValue.LayoutEx();
                MinValue?.LayoutEx();
                MaxValue?.LayoutEx();
            }
        }

        void SetRange()
        {
            int max_y = Math.Max(0, intYCoordinateFromValue(MaxValue.value));
            int min_y = Math.Min(Height, intYCoordinateFromValue(MinValue.value));
            range_control.Location = new Point(0, (int)max_y);
            range_control.Size = new Size((int)MaxValue.TextWidth + 2, (int)(min_y - max_y));
        }

        void minTextBox_OnNewValue(double v, bool finalValue)
        {
            CurrentValue.SetValue(CurrentValue.value, true, true);
            SetRange();
        }

        void maxTextBox_OnNewValue(double v, bool finalValue)
        {
            CurrentValue.SetValue(CurrentValue.value, true, true);
            SetRange();
        }

        public double RangeMin
        {
            get
            {
                return range.Low;
            }
            set
            {
                range.Low = value;
                value_axis.range.SetVisibleLower(Math.Max(value_axis.range.Low, range.Low));
                SetupValues();
            }
        }

        public double RangeMax
        {
            get
            {
                return range.High;
            }
            set
            {
                range.High = value;
                value_axis.range.SetVisibleUpper(Math.Min(value_axis.range.High, range.High));
                SetupValues();
            }
        }

        public double RangeLimit
        {
            get
            {
                return range_limit;
            }
            set
            {
                range_limit = value;
                SetupValues();
            }
        }

        public Font ValueFont
        {
            get
            {
                return value_font;
            }
            set
            {
                value_font = value;
                SetupValues();
            }
        }

        public Color ValueTextColor
        {
            get
            {
                return value_text_color;
            }
            set
            {
                value_text_color = value;
                SetupValues();
            }
        }

        public Color ValueBackColor
        {
            get
            {
                return value_back_color;
            }
            set
            {
                value_back_color = value;
                SetupValues();
            }
        }

        public Color ValueBorderColor
        {
            get
            {
                return value_border_color;
            }
            set
            {
                value_border_color = value;
                SetupValues();
            }
        }

        void RangeSlider_SizeChanged(object sender, EventArgs e)
        {
            value_axis.range.Resize();
            UpdateValuePositions();
        }

        void valueTextBox_OnNewValue(double v, bool finalValue)
        {
            if (OnNewValue != null)
            {
                OnNewValue(v, finalValue);
            }
        }

        public double ValueFromYCoordinate(int y)
        {
            return (double)value_axis.range.ValueFromYCoord(y);
        }

        double YCoordinateFromValue(double v)
        {
            return value_axis.range.YCoordFromValue(v);
        }

        public int intYCoordinateFromValue(double v)
        {
            return (int)Math.Round(value_axis.range.YCoordFromValue(v));
        }

        double ScaleByRange(double v)
        {
            double n = v / ViewRange;
            return (int)Math.Round(n * value_axis.range.Height);
        }

        double ViewRange
        {
            get
            {
                return (double)value_axis.range.Range;
            }
        }

        void valueTextBox_OnNewText(object sender, string text)
        {
            double f;
            bool ok = double.TryParse(text, out f);
            if (ok)
            {
                CurrentValue.SetValue(f, true, true);
                UpdateValuePositions();
                CurrentValue.Text = text;   // UNDO
            }
        }

        void minTextBox_OnNewText(object sender, string text)
        {
            double f;
            bool ok = double.TryParse(text, out f);
            if (ok)
            {
                f = Clamp(f, range.Low, MaxValue.value);
                value_axis.range.SetVisibleLower(Clamp((double)Math.Min(value_axis.range.Low, MinValue.value), range.Low, range.High));
                MinValue.SetValue(f, true, true);
                UpdateValuePositions();
                MinValue.UpdateText();
            }
        }

        void maxTextBox_OnNewText(object sender, string text)
        {
            double f;
            bool ok = double.TryParse(text, out f);
            if (ok)
            {
                f = Clamp(f, MinValue.value, range.High);
                value_axis.range.SetVisibleUpper((double)Math.Min(value_axis.range.High, MaxValue.value));
                MaxValue.SetValue(f, true, true);
                CurrentValue.SetValue(CurrentValue.value, true, true);
                UpdateValuePositions();
                MinValue.UpdateText();
            }
        }

        void RangeSlider_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ZoomIn();
            }
            else
            {
                ZoomOut();
            }
        }

        public void Zoom(double low, double high)
        {
            Span s = new Span(low, high);
            double r = high - low;
            if (r < 0.1f)
            {
                double m = s.MidPoint;
                s.Set(m - RangeLimit / 2, m + RangeLimit / 2);
                if (s.High > value_axis.range.Max)
                {
                    s.High = (double)value_axis.range.Max;
                    s.Low = s.High - RangeLimit;
                }
                else if (s.Low < value_axis.range.Min)
                {
                    s.Low = (double)value_axis.range.Min;
                    s.High = s.Low + RangeLimit;
                }
            }
            value_axis.range.SetTargetRange(s, 0.5f);
        }

        public void ZoomIn()
        {
            if(ShowMinMax)
            {
                Zoom(MinValue.value, MaxValue.value);
            }
            else
            {
                Zoom(CurrentValue.value - RangeLimit / 2, CurrentValue.value + RangeLimit / 2);
            }
        }

        public void ZoomOut()
        {
            Zoom(range.Low, range.High);
        }

        public void UpdateLimits()
        {
            if (ShowMinMax)
            {
                CurrentValue.range.Set(MinValue.value, MaxValue.value);
                MinValue.range.Set(range.Low, MaxValue.value);
                MaxValue.range.Set(MinValue.value, range.High);
            }
        }

        void RangeSlider_MouseUp(object sender, MouseEventArgs e)
        {
            drag = false;
        }

        void RangeSlider_MouseMove(object sender, MouseEventArgs e)
        {
            if (drag)
            {
                double view_range = (double)value_axis.range.Range;
                double view_scale = (double)value_axis.range.Height / view_range;
                int diff = e.Y - drag_offset;
                double offset = diff / view_scale;
                double new_low = old_range.Low + offset;
                double new_high = old_range.High + offset;
                if (new_low < range.Low)
                {
                    new_low = range.Low;
                    new_high = range.Low + view_range;
                }
                else if (new_high > range.High)
                {
                    new_high = range.High;
                    new_low = range.High - view_range;
                }
                value_axis.range.SetVisibleRange(new_low, new_high);
            }
        }

        void RangeSlider_MouseDown(object sender, MouseEventArgs e)
        {
            if (!value_axis.range.Animating)
            {
                old_range.Set(value_axis.range.VisibleRange);
                drag_offset = e.Y;
                drag = true;
            }
        }

        // TODO (chs): add acceleration by recording the time (using System.Diagnostics.Stopwatch)
        // between this and the last scroll event and scaling accordingly
        void RangeSlider_MouseWheel(object sender, MouseEventArgs e)
        {
            value_axis.range.OnMouseWheel(e, RangeLimit);
        }

        public static PointF ToPointF(Point p)
        {
            return new PointF(p.X, p.Y);
        }

        public void SetTickValue(double v)
        {
            double old = tick_value;
            tick_value = v;
            int oy = intYCoordinateFromValue(old);
            int ny = intYCoordinateFromValue(v);
            int tl = TickLeft - TickSize;
            int ts = TickSize + 1;
            tick.Height = TickSize * 2;
            tick.Width = TickSize + 1;
            double min = Math.Min(oy - ts - 1, ny - ts - 1);
            double max = Math.Max(oy + ts + 1, ny + ts + 1);
            tick.Location = new Point(tl - TickSize, (int)min + 1);
        }

        public void SetValue(double v, bool updateSlider, bool callCallback)
        {
            CurrentValue.SetValue(v, updateSlider, callCallback);
        }

        public int TickSize
        {
            get
            {
                return CurrentValue.Height / 3;
            }
        }

        public int TickLeft
        {
            get
            {
                return scale_left + scale_width + TickSize + 1;
            }
        }
    }
}
