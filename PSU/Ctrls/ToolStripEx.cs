﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;

namespace Locomo.Ctrls
{
    public class ToolStripEx : ToolStrip
    {
        public ToolStripEx() : base()
        {
        }

        public ToolStripEx(params ToolStripItem[] items) : base(items)
        {
        }

        public HorizontalAlignment Align;

        public delegate void ItemMouseDownDelegate(object sender, MouseEventArgs e);
        public delegate void ItemMouseUpDelegate(object sender, MouseEventArgs e);
        public delegate void ItemMouseMoveDelegate(object sender, MouseEventArgs e);
        public delegate void ItemMouseClickDelegate(object sender, MouseEventArgs e);
        public delegate void ItemDragBeginDelegate(object sender, MouseEventArgs e, Point diff);
        public delegate void ItemDragDelegate(object sender, MouseEventArgs e);
        public delegate void ItemDragEndDelegate(object sender, MouseEventArgs e);

        public event ItemMouseDownDelegate ItemMouseDown;
        public event ItemMouseUpDelegate ItemMouseUp;
        public event ItemMouseMoveDelegate ItemMouseMove;
        public event ItemMouseClickDelegate ItemMouseClick;
        public event ItemDragBeginDelegate ItemDragBegin;
        public event ItemDragDelegate ItemDrag;
        public event ItemDragEndDelegate ItemDragEnd;

        private bool dragging;
        private ToolStripItem drag_item;
        private Point drag_point;
        private Point drag_offset;
        private ToolStripItem initial_click_item;
        private MouseButtons initial_click_button;

        public void EndDragging()
        {
            drag_item = null;
            dragging = false;
        }

        protected virtual void OnItemMouseDown(object sender, MouseEventArgs e)
        {
            ItemMouseDown?.Invoke(sender, e);
        }

        protected virtual void OnItemMouseUp(object sender, MouseEventArgs e)
        {
            ItemMouseUp?.Invoke(sender, e);
        }

        protected virtual void OnItemMouseMove(object sender, MouseEventArgs e)
        {
            ItemMouseMove?.Invoke(sender, e);
        }

        protected virtual void OnItemMouseClick(object sender, MouseEventArgs e)
        {
            ItemMouseClick?.Invoke(sender, e);
        }

        protected virtual void OnItemDragBegin(object sender, MouseEventArgs e, Point drag_offset)
        {
            ItemDragBegin?.Invoke(sender, e, drag_offset);
        }

        protected virtual void OnItemDrag(object sender, MouseEventArgs e)
        {
            ItemDrag?.Invoke(sender, e);
        }

        protected virtual void OnItemDragEnd(object sender, MouseEventArgs e)
        {
            ItemDragEnd?.Invoke(sender, e);
        }

        protected override void OnMouseUp(MouseEventArgs mea)
        {
            if (dragging)
            {
                OnItemDragEnd(drag_item, mea);
                dragging = false;
                drag_item = null;
            }
            base.OnMouseUp(mea);
        }

        protected override void OnMouseMove(MouseEventArgs mea)
        {
            if (dragging)
            {
                OnItemDrag(drag_item, mea);
            }
            else if (drag_item != null)
            {
                Point screen_pos = PointToScreen(mea.Location);
                Point diff = drag_point - (Size)screen_pos;

                int dx = diff.X < 0 ? -diff.X : diff.X;
                int dy = diff.Y < 0 ? -diff.Y : diff.Y;

                if (dx > SystemInformation.DragSize.Width || dy > SystemInformation.DragSize.Height)
                {
                    OnItemDragBegin(drag_item, mea, drag_offset);
                    dragging = true;
                }
            }
            else
            {
            }
            base.OnMouseMove(mea);
        }

        protected override void OnItemAdded(ToolStripItemEventArgs e)
        {
            base.OnItemAdded(e);
            e.Item.MouseLeave += (object sender, EventArgs eva) =>
            {
                initial_click_item = null;
            };
            e.Item.MouseDown += (object sender, MouseEventArgs eva) =>
            {
                initial_click_item = e.Item;
                initial_click_button = eva.Button;
                if (eva.Button == MouseButtons.Left)
                {
                    //eva.Location is offset within the button
                    ToolStrip t = ((sender as ToolStripItem).Owner);
                    drag_item = e.Item;
                    drag_point = PointToScreen(eva.Location.Add(drag_item.Bounds.Location));
                    drag_offset = eva.Location;
                    Capture = true;
                }
                OnItemMouseDown(e.Item, eva);
            };
            e.Item.MouseUp += (object sender, MouseEventArgs eva) =>
            {
                if (eva.Button == MouseButtons.Left && drag_item != null)
                {
                    drag_item = null;
                }
                if (initial_click_item == e.Item && initial_click_button == eva.Button)
                {
                    OnItemMouseClick(e.Item, eva);
                }
                OnItemMouseUp(e.Item, eva);
            };
            e.Item.MouseMove += (object sender, MouseEventArgs eva) =>
            {
                if (e.Item == drag_item)
                {
                    ToolStrip t = ((sender as ToolStripItem).Owner);
                    Point pos = t.PointToScreen(eva.Location);
                }
                else
                {
                }
                OnItemMouseMove(e.Item, eva);
            };
        }
    }
}
