﻿namespace Locomo.Ctrls
{
    partial class VirtualInputsControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.channel_B_label = new System.Windows.Forms.Label();
            this.operation_label = new System.Windows.Forms.Label();
            this.channel_A_label = new System.Windows.Forms.Label();
            this.channel_B_button = new System.Windows.Forms.Button();
            this.operation_button = new System.Windows.Forms.Button();
            this.channel_A_button = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // channel_B_label
            // 
            this.channel_B_label.Location = new System.Drawing.Point(59, 98);
            this.channel_B_label.Name = "channel_B_label";
            this.channel_B_label.Size = new System.Drawing.Size(188, 23);
            this.channel_B_label.TabIndex = 15;
            this.channel_B_label.Text = "None";
            // 
            // operation_label
            // 
            this.operation_label.Location = new System.Drawing.Point(59, 56);
            this.operation_label.Name = "operation_label";
            this.operation_label.Size = new System.Drawing.Size(188, 23);
            this.operation_label.TabIndex = 14;
            this.operation_label.Text = "Multiply";
            // 
            // channel_A_label
            // 
            this.channel_A_label.Location = new System.Drawing.Point(59, 14);
            this.channel_A_label.Name = "channel_A_label";
            this.channel_A_label.Size = new System.Drawing.Size(188, 23);
            this.channel_A_label.TabIndex = 13;
            this.channel_A_label.Text = "None";
            // 
            // channel_B_button
            // 
            this.channel_B_button.Location = new System.Drawing.Point(6, 90);
            this.channel_B_button.Name = "channel_B_button";
            this.channel_B_button.Size = new System.Drawing.Size(47, 36);
            this.channel_B_button.TabIndex = 12;
            this.channel_B_button.Text = "B";
            this.channel_B_button.UseVisualStyleBackColor = true;
            this.channel_B_button.Click += new System.EventHandler(this.channel_B_button_Click);
            // 
            // operation_button
            // 
            this.operation_button.Location = new System.Drawing.Point(6, 48);
            this.operation_button.Name = "operation_button";
            this.operation_button.Size = new System.Drawing.Size(47, 36);
            this.operation_button.TabIndex = 11;
            this.operation_button.Text = "op";
            this.operation_button.UseVisualStyleBackColor = true;
            this.operation_button.Click += new System.EventHandler(this.operation_button_Click);
            // 
            // channel_A_button
            // 
            this.channel_A_button.Location = new System.Drawing.Point(6, 6);
            this.channel_A_button.Name = "channel_A_button";
            this.channel_A_button.Size = new System.Drawing.Size(47, 36);
            this.channel_A_button.TabIndex = 10;
            this.channel_A_button.Text = "A";
            this.channel_A_button.UseVisualStyleBackColor = true;
            this.channel_A_button.Click += new System.EventHandler(this.channel_A_button_Click);
            // 
            // VirtualInputsControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.channel_B_label);
            this.Controls.Add(this.operation_label);
            this.Controls.Add(this.channel_A_label);
            this.Controls.Add(this.channel_B_button);
            this.Controls.Add(this.operation_button);
            this.Controls.Add(this.channel_A_button);
            this.Name = "VirtualInputsControl";
            this.Size = new System.Drawing.Size(250, 133);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label channel_B_label;
        private System.Windows.Forms.Label operation_label;
        private System.Windows.Forms.Label channel_A_label;
        private System.Windows.Forms.Button channel_B_button;
        private System.Windows.Forms.Button operation_button;
        private System.Windows.Forms.Button channel_A_button;
    }
}
