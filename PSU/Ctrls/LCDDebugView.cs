﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Managed.Graphics.Forms;
using Managed.Graphics.Direct2D;

using D2DBrush = Managed.Graphics.Direct2D.Brush;
using D2DColor = Managed.Graphics.Direct2D.Color;
using D2DPointF = Managed.Graphics.Direct2D.PointF;

using Locomo.Panels;
using Logging;

namespace Locomo.Ctrls
{
    public partial class LCDDebugView : Direct2DControl
    {
        public LCDDebugPanel parent = null;

        D2DBrush zero_brush = null;
        D2DBrush one_brush = null;
        D2DBrush one_ignore_brush = null;
        D2DBrush one_known_brush = null;
        D2DBrush line_brush = null;

        string[] bit_descriptions = new string[500];

        public const int scale = 8;

        public LCDDebugView()
        {
            InitializeComponent();
        }

        public System.Windows.Forms.AutoScaleMode AutoScaleMode { get; set; }

        protected override void OnCreateDeviceResources(WindowRenderTarget rt)
        {
            zero_brush = rt.CreateSolidColorBrush(D2DColor.FromRGB(32, 32, 32));
            one_brush = rt.CreateSolidColorBrush(D2DColor.FromRGB(0, 160, 0));
            one_ignore_brush = rt.CreateSolidColorBrush(D2DColor.FromRGB(48, 48, 48));
            one_known_brush = rt.CreateSolidColorBrush(D2DColor.FromRGB(0, 0, 120));
            line_brush = rt.CreateSolidColorBrush(D2DColor.FromRGB(120, 120, 120));
        }

        protected override void OnCleanUpDeviceResources()
        {
            Extensions.DisposeOf(ref zero_brush);
            Extensions.DisposeOf(ref one_brush);
            Extensions.DisposeOf(ref one_ignore_brush);
            Extensions.DisposeOf(ref one_known_brush);
            Extensions.DisposeOf(ref line_brush);
        }

        void set_tags(int o, int s, int e, string t)
        {
            for (int b = s; b <= e; ++b)
            {
                bit_descriptions[o + b] = t;
            }
        }

        protected override void OnMouseClick(MouseEventArgs e)
        {
            int bit_index = e.X / scale;
            int byte_index = bit_index / 8;
            int bit_start = bit_index % 8;
            int bit_end = bit_start;
            if (ModifierKeys.HasFlag(Keys.Shift))
            {
                bit_start = 0;
                bit_end = 7;
            }
            int descriptor_offset = byte_index * 8;
            if (e.Button == MouseButtons.Right)
            {
                ContextMenu m = new ContextMenu();

                foreach(ulong i in Enum.GetValues(typeof(DMM.LCD.segment)))
                {
                    string s = Enum.GetName(typeof(DMM.LCD.segment), i);
                    MenuItem mi = new MenuItem(s);
                    mi.Click += (sender, args) => {
                        Log.Debug($"They say it's {s} which is {i:X16}");
                        string tag = null;
                        if (i != 0)
                        {
                            tag = s;
                        }
                        set_tags(descriptor_offset, bit_start, bit_end, tag);
                    };
                    m.MenuItems.Add(mi);
                }
                m.Show(this, new Point(e.X, e.Y));
            }
            else if(e.Button == MouseButtons.Left)
            {
                set_tags(descriptor_offset, bit_start, bit_end, "");
            }
            base.OnMouseClick(e);
        }

        protected override void OnRender(WindowRenderTarget rt)
        {
            int y = 0;
            try
            {
                foreach (byte[] b in parent.buffers)
                {
                    int index = 0;
                    for (int i = 0; i < b.Length; ++i)
                    {
                        int x = index * scale;
                        rt.DrawLine(line_brush, 1, new D2DPointF(x, 0), new D2DPointF(x, 50 * scale));
                        for (int j = 0; j < 8; ++j)
                        {
                            D2DBrush bit = zero_brush;
                            if ((b[i] & (1 << j)) != 0)
                            {
                                bit = one_brush;
                                if (bit_descriptions[index] != null)
                                {
                                    if (bit_descriptions[index] == "")
                                    {
                                        bit = one_ignore_brush;
                                    }
                                    else
                                    {
                                        bit = one_known_brush;
                                    }
                                }
                            }
                            x = index * scale;
                            rt.FillRect(bit, new RectF(x + 1, y, scale - 1, scale - 1));
                            index += 1;
                        }
                    }
                    y += scale;
                }
            }
            catch(InvalidOperationException)
            {
                // modified buffer, abandon render
            }
        }
    }
}
