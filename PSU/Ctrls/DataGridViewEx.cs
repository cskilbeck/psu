﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Locomo.Ctrls
{
    public class DataGridViewEx : DataGridView
    {
        public bool IsUserScrolling { get; private set; }

        private const int WM_HSCROLL = 0x0114;
        private const int WM_VSCROLL = 0x0115;
        private const int SB_ENDSCROLL = 8;

        public delegate void UserScrollDelegate(object sender, bool ended);
        public event UserScrollDelegate UserScroll;

        protected virtual void OnUserScroll(bool ended)
        {
            UserScroll?.Invoke(this, ended);
        }

        protected override void OnMouseWheel(MouseEventArgs e)
        {
            base.OnMouseWheel(e);
            OnUserScroll(false);
        }

        protected override void WndProc(ref Message m)
        {
            if (m.Msg == WM_HSCROLL || m.Msg == WM_VSCROLL)
            {
                OnUserScroll((short)(m.WParam.ToInt32() & 0xFFFF) != SB_ENDSCROLL);
            }
            base.WndProc(ref m);
        }
    }
}
