﻿namespace Locomo.Ctrls
{
    partial class StreamConfigControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.stream_color_button = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.samples_textBox = new System.Windows.Forms.TextBox();
            this.rate_label = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.rate_trackBar = new System.Windows.Forms.TrackBar();
            this.reset_button = new System.Windows.Forms.Button();
            this.groupBox = new System.Windows.Forms.GroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.rate_trackBar)).BeginInit();
            this.groupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // stream_color_button
            // 
            this.stream_color_button.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.stream_color_button.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.stream_color_button.ForeColor = System.Drawing.Color.Transparent;
            this.stream_color_button.Location = new System.Drawing.Point(79, 84);
            this.stream_color_button.Name = "stream_color_button";
            this.stream_color_button.Size = new System.Drawing.Size(21, 23);
            this.stream_color_button.TabIndex = 21;
            this.stream_color_button.TabStop = false;
            this.stream_color_button.UseVisualStyleBackColor = false;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(5, 89);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(68, 13);
            this.label11.TabIndex = 20;
            this.label11.Text = "Default Color";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(6, 61);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(47, 13);
            this.label12.TabIndex = 19;
            this.label12.Text = "Samples";
            // 
            // samples_textBox
            // 
            this.samples_textBox.Location = new System.Drawing.Point(53, 58);
            this.samples_textBox.Name = "samples_textBox";
            this.samples_textBox.Size = new System.Drawing.Size(92, 20);
            this.samples_textBox.TabIndex = 18;
            // 
            // rate_label
            // 
            this.rate_label.AutoSize = true;
            this.rate_label.Location = new System.Drawing.Point(268, 34);
            this.rate_label.Name = "rate_label";
            this.rate_label.Size = new System.Drawing.Size(41, 13);
            this.rate_label.TabIndex = 17;
            this.rate_label.Text = "Fastest";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(6, 34);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(30, 13);
            this.label10.TabIndex = 16;
            this.label10.Text = "Rate";
            // 
            // rate_trackBar
            // 
            this.rate_trackBar.AutoSize = false;
            this.rate_trackBar.LargeChange = 1;
            this.rate_trackBar.Location = new System.Drawing.Point(53, 20);
            this.rate_trackBar.Name = "rate_trackBar";
            this.rate_trackBar.Size = new System.Drawing.Size(209, 36);
            this.rate_trackBar.SmallChange = 5;
            this.rate_trackBar.TabIndex = 15;
            // 
            // reset_button
            // 
            this.reset_button.Location = new System.Drawing.Point(251, 79);
            this.reset_button.Name = "reset_button";
            this.reset_button.Size = new System.Drawing.Size(77, 23);
            this.reset_button.TabIndex = 14;
            this.reset_button.Text = "Reset";
            this.reset_button.UseVisualStyleBackColor = true;
            // 
            // groupBox
            // 
            this.groupBox.Controls.Add(this.label10);
            this.groupBox.Controls.Add(this.label12);
            this.groupBox.Controls.Add(this.stream_color_button);
            this.groupBox.Controls.Add(this.rate_trackBar);
            this.groupBox.Controls.Add(this.reset_button);
            this.groupBox.Controls.Add(this.rate_label);
            this.groupBox.Controls.Add(this.samples_textBox);
            this.groupBox.Controls.Add(this.label11);
            this.groupBox.Location = new System.Drawing.Point(3, 3);
            this.groupBox.Name = "groupBox";
            this.groupBox.Size = new System.Drawing.Size(338, 123);
            this.groupBox.TabIndex = 22;
            this.groupBox.TabStop = false;
            this.groupBox.Text = "groupBox1";
            // 
            // StreamConfigControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBox);
            this.Name = "StreamConfigControl";
            this.Size = new System.Drawing.Size(347, 132);
            ((System.ComponentModel.ISupportInitialize)(this.rate_trackBar)).EndInit();
            this.groupBox.ResumeLayout(false);
            this.groupBox.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label10;
        public System.Windows.Forms.Button stream_color_button;
        public System.Windows.Forms.TextBox samples_textBox;
        public System.Windows.Forms.TrackBar rate_trackBar;
        public System.Windows.Forms.Button reset_button;
        public System.Windows.Forms.Label rate_label;
        public System.Windows.Forms.GroupBox groupBox;
    }
}
