﻿namespace Locomo.Ctrls
{
    partial class ActiveDeviceControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.delete_button = new System.Windows.Forms.Button();
            this.disconnect_button = new System.Windows.Forms.Button();
            this.connect_button = new System.Windows.Forms.Button();
            this.auto_connect_checkBox = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // delete_button
            // 
            this.delete_button.Location = new System.Drawing.Point(0, 58);
            this.delete_button.Name = "delete_button";
            this.delete_button.Size = new System.Drawing.Size(75, 23);
            this.delete_button.TabIndex = 5;
            this.delete_button.Text = "Delete";
            this.delete_button.UseVisualStyleBackColor = true;
            // 
            // disconnect_button
            // 
            this.disconnect_button.Location = new System.Drawing.Point(0, 29);
            this.disconnect_button.Name = "disconnect_button";
            this.disconnect_button.Size = new System.Drawing.Size(75, 23);
            this.disconnect_button.TabIndex = 4;
            this.disconnect_button.Text = "Disconnect";
            this.disconnect_button.UseVisualStyleBackColor = true;
            // 
            // connect_button
            // 
            this.connect_button.Location = new System.Drawing.Point(0, 0);
            this.connect_button.Name = "connect_button";
            this.connect_button.Size = new System.Drawing.Size(75, 23);
            this.connect_button.TabIndex = 3;
            this.connect_button.Text = "Connect";
            this.connect_button.UseVisualStyleBackColor = true;
            // 
            // auto_connect_checkBox
            // 
            this.auto_connect_checkBox.AutoSize = true;
            this.auto_connect_checkBox.Location = new System.Drawing.Point(82, 4);
            this.auto_connect_checkBox.Name = "auto_connect_checkBox";
            this.auto_connect_checkBox.Size = new System.Drawing.Size(90, 17);
            this.auto_connect_checkBox.TabIndex = 6;
            this.auto_connect_checkBox.Text = "Auto-connect";
            this.auto_connect_checkBox.UseVisualStyleBackColor = true;
            // 
            // ActiveDeviceControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.auto_connect_checkBox);
            this.Controls.Add(this.delete_button);
            this.Controls.Add(this.disconnect_button);
            this.Controls.Add(this.connect_button);
            this.Name = "ActiveDeviceControl";
            this.Size = new System.Drawing.Size(184, 84);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.Button delete_button;
        public System.Windows.Forms.Button disconnect_button;
        public System.Windows.Forms.Button connect_button;
        public System.Windows.Forms.CheckBox auto_connect_checkBox;
    }
}
