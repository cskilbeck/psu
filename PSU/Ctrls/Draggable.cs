﻿using System.Drawing;
using System.Windows.Forms;

namespace Locomo.Ctrls
{
    public interface Draggable
    {
        bool StartDrag(MouseEventArgs e, Point click_point);
        void MouseMoved(MouseEventArgs e);
        void EndDrag(MouseEventArgs e);
    }
}
