﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Managed.Graphics.Direct2D;
using Managed.Graphics.DirectWrite;

using static Locomo.Extensions;

using D2DColor = Managed.Graphics.Direct2D.Color;
using D2DPointF = Managed.Graphics.Direct2D.PointF;
using D2DSizeF = Managed.Graphics.Direct2D.SizeF;
using D2DBrush = Managed.Graphics.Direct2D.Brush;
using D2DFont = Managed.Graphics.DirectWrite.Font;
using StrokeStyle = Managed.Graphics.Direct2D.StrokeStyle;
using WindowRenderTarget = Managed.Graphics.Direct2D.WindowRenderTarget;
using RectF = Managed.Graphics.Direct2D.RectF;
using AntialiasMode = Managed.Graphics.Direct2D.AntialiasMode;
using Colors = Managed.Graphics.Direct2D.Colors;
using DrawTextOptions = Managed.Graphics.Direct2D.DrawTextOptions;
using SolidColorBrush = Managed.Graphics.Direct2D.SolidColorBrush;
using PathGeometry = Managed.Graphics.Direct2D.PathGeometry;
using GeometrySink = Managed.Graphics.Direct2D.GeometrySink;
using PathSegment = Managed.Graphics.Direct2D.PathSegment;
using FigureBegin = Managed.Graphics.Direct2D.FigureBegin;
using FigureEnd = Managed.Graphics.Direct2D.FigureEnd;
using StrokeStyleProperties = Managed.Graphics.Direct2D.StrokeStyleProperties;
using Direct2DFactory = Managed.Graphics.Direct2D.Direct2DFactory;
using D2DFontStyle = Managed.Graphics.DirectWrite.FontStyle;
using System.Diagnostics;

namespace Locomo.Ctrls
{
    public partial class ValueAxis : Managed.Graphics.Forms.Direct2DControl
    {
        public ValueRangeD2D range;

        D2DColor color;

        public StrokeStyle dashed_stroke;
        public StrokeStyle zero_strokestyle;

        public D2DBrush label_background_brush;
        public D2DBrush label_text_brush;

        public TextFormat label_font;
        public TextFormat regular;
        public TextFormat bold;

        public TextFormat scale_normal_text;
        public TextFormat scale_bold_text;
        public D2DBrush scale_major_brush;
        public D2DBrush scale_minor_brush;
        public D2DBrush scale_major_label;
        public D2DBrush scale_minor_label;

        public D2DColor major_color;
        public D2DColor minor_color;

        public D2DBrush fill_brush;

        public ValueAxis()
        {
            range = new ValueRangeD2D();
            InitializeComponent();
            color = D2DColor.FromRGB(255, 255, 0);
            range.Rect = new Rectangle(0, 0, 8, ClientSize.Height);
        }

        private HorizontalAlignment alignment;

        [DefaultValue(HorizontalAlignment.Left)]
        public HorizontalAlignment Alignment
        {
            get
            {
                return alignment;
            }
            set
            {
                alignment = value;
                range.label_right_align = value == HorizontalAlignment.Right;
                SetupSize();
                Refresh();
            }
        }

        void SetupSize()
        {
            int left = 0;
            if (range.label_right_align)
            {
                left = ClientSize.Width - 16;
            }
            range.Rect = new Rectangle(left, 30, 16, ClientSize.Height - 60);
        }

        protected override void OnClientSizeChanged(EventArgs e)
        {
            SetupSize();
            base.OnClientSizeChanged(e);
        }

        protected override void OnRender(WindowRenderTarget renderTarget)
        {
            
            renderTarget.Clear(COLOR(BackColor));
            range.DrawAxis(renderTarget, DW, label_font, bold, scale_major_brush, scale_minor_brush, scale_major_label, scale_minor_label);
            base.OnRender(renderTarget);
        }

        protected override void OnCreateDeviceIndependentResources(Direct2DFactory factory)
        {
            major_color = D2DColor.FromRGB(0, 0, 0);
            minor_color = D2DColor.FromRGB(64, 64, 64);
            var ssp = new StrokeStyleProperties();
            ssp.DashCap = LineCapStyle.Round;
            ssp.DashStyle = DashStyle.Dash;
            ssp.LineJoin = LineJoin.Round;
            dashed_stroke = factory.CreateStrokeStyle(ssp, null);
            ssp.DashStyle = DashStyle.Dot;
            zero_strokestyle = factory.CreateStrokeStyle(ssp, null);
            bold = DW.CreateTextFormat("Lucida Sans Typewriter", 9 * 96.0f / 72.0f, FontWeight.ExtraBold, D2DFontStyle.Normal);
            label_font = DW.CreateTextFormat("Lucida Sans Typewriter", 9 * 96.0f / 72.0f, FontWeight.Medium, D2DFontStyle.Normal);
            regular = DW.CreateTextFormat("Lucida Sans Typewriter", 9 * 96.0f / 72.0f, FontWeight.Medium, D2DFontStyle.Normal);
            scale_normal_text = DW.CreateTextFormat("Lucida Sans Typewriter", 9 * 96.0f / 72.0f, FontWeight.Medium, D2DFontStyle.Normal);
            scale_bold_text = DW.CreateTextFormat("Lucida Sans Typewriter", 9 * 96.0f / 72.0f, FontWeight.Medium, D2DFontStyle.Normal);
            base.OnCreateDeviceIndependentResources(factory);
        }

        protected override void OnCleanUpDeviceIndependentResources()
        {
            DisposeOf(ref dashed_stroke);
            DisposeOf(ref zero_strokestyle);
            DisposeOf(ref bold);
            DisposeOf(ref label_font);
            DisposeOf(ref regular);
            DisposeOf(ref scale_normal_text);
            DisposeOf(ref scale_bold_text);
            base.OnCleanUpDeviceIndependentResources();
        }

        protected override void OnCreateDeviceResources(WindowRenderTarget renderTarget)
        {
            fill_brush = renderTarget.CreateSolidColorBrush(color);
            label_background_brush = renderTarget.CreateSolidColorBrush(D2DColor.FromRGB(0, 0, 0, 192));
            label_text_brush = renderTarget.CreateSolidColorBrush(major_color);
            scale_major_brush = renderTarget.CreateSolidColorBrush(major_color);
            scale_minor_brush = renderTarget.CreateSolidColorBrush(minor_color);
            scale_major_label = renderTarget.CreateSolidColorBrush(major_color);
            scale_minor_label = renderTarget.CreateSolidColorBrush(minor_color);
            scale_minor_label.Opacity = 0.66f;
            base.OnCreateDeviceResources(renderTarget);
        }

        protected override void OnCleanUpDeviceResources()
        {
            DisposeOf(ref fill_brush);
            DisposeOf(ref label_background_brush);
            DisposeOf(ref label_text_brush);
            DisposeOf(ref scale_major_brush);
            DisposeOf(ref scale_minor_brush);
            DisposeOf(ref scale_major_label);
            DisposeOf(ref scale_minor_label);
            base.OnCleanUpDeviceResources();
        }
    }
}
