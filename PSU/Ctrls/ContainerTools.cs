﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;

namespace Locomo.Ctrls
{
    public partial class ContainerTools : UserControl
    {
        public ContainerTools()
        {
            InitializeComponent();
            AutoSize = false;
            MinimumSize = toolStrip1.Size;
            MaximumSize = toolStrip1.Size;
            Size = toolStrip1.Size;
        }

        public ContainerTools(ToolStrip toolstrip)
        {
            AutoSize = false;
            InitializeComponent();
            Controls.Remove(toolStrip1);
            toolStrip1 = toolstrip;
            toolStrip1.AutoSize = false;
            toolStrip1.MinimumSize = toolStrip1.Size;
            Controls.Add(toolStrip1);
            AutoSize = false;
            MinimumSize = Size;
            MaximumSize = Size;
            Size = toolStrip1.Size;
            Width = toolStrip1.Width;
        }
    }
}
