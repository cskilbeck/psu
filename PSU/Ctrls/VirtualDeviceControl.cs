﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Locomo.Devices;
using Logging;

namespace Locomo.Ctrls
{
    public partial class VirtualDeviceControl : UserControl
    {
        public Device_Virtual device;
        BindingSource binding = new BindingSource();

        public enum virtual_input
        {
            A,
            B
        }

        public VirtualDeviceControl()
        {
            InitializeComponent();
            virtualInputs_Control.Hide();
            virtualInputs_Control.parent = this;
        }

        public void set_device(Device_Virtual d)
        {
            device = d;
            binding.DataSource = device.virtual_channels;
            channels_listBox.DataSource = binding;
            channels_listBox.Refresh();
        }

        private void Add_button_Click(object sender, EventArgs e)
        {
            string s = Extensions.InputString("Name the channel", "Enter name", "vchannel_1");
            Virtual_Channel c = new Virtual_Channel();
            c.name = s;
            device.virtual_channels.Add(c);
            binding.ResetBindings(false);
            channels_listBox.SelectedItem = c;
        }

        private void Delete_button_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Are you SURE you want to delete the channel?", "Are you sure?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                Virtual_Channel c = (Virtual_Channel)channels_listBox.SelectedItem;
                if (c != null)
                {
                    device.virtual_channels.Remove(c);
                    binding.ResetBindings(false);
                }
            }
        }

        public string choose_channel(virtual_input i)
        {
            StreamChooser s = new StreamChooser();
            s.Text = $"Choose stream for channel {i}";
            if (s.ShowDialog(this) == DialogResult.OK)
            {
                return $"{s.SelectedStream.Name} on {s.SelectedStream.Device.Name}";
            }
            return null;
        }

        public void set_operator(Virtual_Channel.operation o)
        {
            Log.Debug(o);
        }

        private void channels_listBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            Virtual_Channel c = (Virtual_Channel)channels_listBox.SelectedItem;
            if (c != null)
            {
                virtualInputs_Control.Show();
            }
            else
            {
                virtualInputs_Control.Hide();
            }
        }
    }
}
