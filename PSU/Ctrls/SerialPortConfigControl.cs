﻿using System;
using System.Windows.Forms;
using System.IO.Ports;

namespace Locomo.Ctrls
{
    public partial class SerialPortConfigControl : UserControl
    {
        public SerialPortConfigControl()
        {
            InitializeComponent();
            portComboBox.Items.Clear();
            baudComboBox.Items.Clear();
            parityComboBox.Items.Clear();
            flowControlComboBox.Items.Clear();
            foreach (var s in SerialPort.GetPortNames())
            {
                portComboBox.Items.Add(s);
            }
            portComboBox.Items.Add("DUMMY");

            foreach (var s in Enum.GetValues(typeof(Handshake)))
            {
                flowControlComboBox.Items.Add(s);
            }

            baudComboBox.Items.Add("1200");
            baudComboBox.Items.Add("2400");
            baudComboBox.Items.Add("4800");
            baudComboBox.Items.Add("9600");
            baudComboBox.Items.Add("19200");

            foreach (var s in Enum.GetValues(typeof(Parity)))
            {
                parityComboBox.Items.Add(s);
            }

            foreach (var s in Enum.GetValues(typeof(StopBits)))
            {
                stopBitsComboBox.Items.Add(s);
            }

            dataBitsComboBox.Items.Add("7");
            dataBitsComboBox.Items.Add("8");
        }

        public void SetConfig(SerialPortConfig config, SerialDevice.SerialSettingsMask settings_mask)
        {
            portComboBox.SelectedIndex = portComboBox.FindString(config.Port);
            baudComboBox.SelectedIndex = baudComboBox.FindString(config.BaudRate.ToString());
            flowControlComboBox.SelectedIndex = flowControlComboBox.FindString(config.FlowControl.ToString());
            parityComboBox.SelectedIndex = parityComboBox.FindString(config.Parity.ToString());
            dataBitsComboBox.SelectedIndex = dataBitsComboBox.FindString(config.DataBits.ToString());
            stopBitsComboBox.SelectedIndex = stopBitsComboBox.FindString(config.StopBits.ToString());

            baudComboBox.Enabled = settings_mask.Off(SerialDevice.SerialSettingsMask.BAUDRATE);
            parityComboBox.Enabled = settings_mask.Off(SerialDevice.SerialSettingsMask.PARITY);
            flowControlComboBox.Enabled = settings_mask.Off(SerialDevice.SerialSettingsMask.FLOWCONTROL);
            dataBitsComboBox.Enabled = settings_mask.Off(SerialDevice.SerialSettingsMask.DATABITS);
            stopBitsComboBox.Enabled = settings_mask.Off(SerialDevice.SerialSettingsMask.STOPBITS);
        }

        public SerialPortConfig GetConfig()
        {
            SerialPortConfig config = new SerialPortConfig();
            config.Port = portComboBox.Text;
            config.BaudRate = int.Parse(baudComboBox.Text);
            config.Parity = (Parity)Enum.Parse(typeof(Parity), parityComboBox.Text);
            config.FlowControl = (Handshake)Enum.Parse(typeof(Handshake), flowControlComboBox.Text);
            config.StopBits = (StopBits)Enum.Parse(typeof(StopBits), stopBitsComboBox.Text);
            config.DataBits = int.Parse(dataBitsComboBox.Text);
            return config;
        }
    }
}
