﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using System.Diagnostics;
using System.Collections.Generic;
using static Locomo.Extensions;
using Logging;

namespace Locomo.Ctrls
{
    public partial class PanelContainer : UserControl
    {
        private static PanelContainer root_container = null;

        private ChildPanel active_tab;
        private Timer toolbar_timer;
        private Timer slider_timer;
        private int toolbar_default_height;
        private bool pinned = false;
        private bool toolbar_can_hide = true;

        public ToolStripPanelEx toolstrip_panel;
        public ToolStrip current_hosted_toolstrip;
        public ToolStripEx default_toolstrip;
        public ToolStripDropDownButton config_button;

        private List<ChildPanel> children = new List<ChildPanel>();

        static int toolbar_hide_delay = 500;
        static int toolbar_slide_speed = 1000 / 120;
        static int toolbar_slide_rate = 2;

        public ChildPanel ActivePanel
        {
            get
            {
                return active_tab;
            }
        }

        public PanelContainer()
        {
            if (root_container == null)
            {
                root_container = this;
            }
            InitializeComponent();

            add_panel_toolStripDropDownButton.DropDownOpening += Add_toolStripDropDownButton_DropDownOpening;
            add_panel_toolStripDropDownButton.DropDownClosed += toolstrip_menu_closed;

            ChildPanel.hover_height_threshold = toolStrip.Height / 2;

            toolstrip_panel = new ToolStripPanelEx();  // add a toolstrippanel for hosting default toolstrip and toolstrip of active panel
            toolstrip_panel.Dock = DockStyle.Top;
            toolstrip_panel.AutoSize = false;
            toolstrip_panel.Height = toolStrip.Height;
            toolstrip_panel.BackColor = Color.Cornsilk;
            toolstrip_panel.Tag = this;
            Controls.Add(toolstrip_panel);

            default_toolstrip = new ToolStripEx();
            default_toolstrip.GripStyle = ToolStripGripStyle.Hidden;
            default_toolstrip.RenderMode = ToolStripRenderMode.System;
            config_button = new ToolStripDropDownButton();
            config_button.Alignment = ToolStripItemAlignment.Left;
            config_button.DisplayStyle = ToolStripItemDisplayStyle.Image;
            config_button.Image = Properties.Resources.Options;
            config_button.Name = "config_button";
            config_button.Size = new Size(23, 22);
            config_button.Text = "Config";
            config_button.DropDownOpening += Config_button_DropDownOpening;
            config_button.DropDownClosed += toolstrip_menu_closed;
            default_toolstrip.Items.Add(config_button);
            default_toolstrip.Name = "Default";
            default_toolstrip.PerformLayout();
            default_toolstrip.Size = new Size(23, 22);
            SetupToolStrip(default_toolstrip);

            Controls.Remove(toolStrip);
            EnableMouseEvents(toolStrip);

            toolStrip.ItemMouseClick += ToolStrip_MouseClick;
            toolStrip.ItemDragBegin += ToolStrip_ItemDragBegin;
            toolStrip.Tag = null;
            toolStrip.AutoSize = true;
            toolStrip.PerformLayout();
            toolStrip.Align = HorizontalAlignment.Right;
            toolstrip_panel.Join(toolStrip);

            utility_toolstrip_button.DropDownItems.Add(new Ctrls.ContainerToolsHost(popup_toolStrip));
            utility_toolstrip_button.DropDownOpening += (s, e) => { toolbar_can_hide = false; };
            utility_toolstrip_button.DropDownOpened += (s, e) => { popup_toolStrip.Focus(); };
            utility_toolstrip_button.DropDownClosed += toolstrip_menu_closed;

            toolStrip.Renderer = new ToolStripSystemRendererEx();
            toolbar_default_height = toolStrip.Height;
            DoubleBuffered = true;
            slider_timer = new Timer();
            toolbar_timer = new Timer();
            toolbar_timer.Interval = toolbar_hide_delay;
            slider_timer.Interval = toolbar_slide_speed;
            toolbar_timer.Tick += ToolbarTimer_Tick;
            slider_timer.Tick += SliderTimer_Tick;
            toolstrip_panel.MouseEnter += (sender, e) => { ToolStrip_Hovered(); };
            toolstrip_panel.MouseLeave += (sender, e) => { PopupToolbar(false, true); };
            SetCloseButtonState();
            StartToolbarHiderTimer();
            HostToolStrip(null);

            Config.PropertyChanged += (sender, e) =>
            {
                SetCloseButtonState();
                StartToolbarHiderTimer();
                DoPin();
            };

            Config.SettingsLoaded += () => {
                SetCloseButtonState();
                StartToolbarHiderTimer();
                DoPin();
            };
        }

        protected override void SetVisibleCore(bool value)
        {
            base.SetVisibleCore(value);
        }

        private void ToolStrip_ItemDragBegin(object sender, MouseEventArgs e, Point diff)
        {
            Control c = (Control)(sender as ToolStripItem).Tag;
            if (c == null) return;

            ToolStripButton drag_toolstrip_button = sender as ToolStripButton;

            drag_toolstrip_button.AutoToolTip = false;
            PanelContainer drag_source_container = (drag_toolstrip_button.Tag as ChildPanel).host_control;
            drag_toolstrip_button.Visible = false;
            Button drag_button = new Button();
            drag_button.Size = drag_toolstrip_button.Size;// + new Size(4, 2);
            drag_button.Font = drag_toolstrip_button.Font;
            drag_button.TextAlign = ContentAlignment.MiddleCenter;
            drag_button.FlatStyle = FlatStyle.System;
            drag_button.BackColor = SystemColors.ActiveCaption;
            drag_button.ForeColor = SystemColors.ActiveCaptionText;
            drag_button.Text = drag_toolstrip_button.Text;

            var chooser = new ContainerChooser(FindForm(), typeof(ChildPanel), true);
            chooser.Controls.Add(drag_button);
            chooser.MouseMove += (object sender2, MouseEventArgs e2) =>
            {
                drag_button.Location = chooser.PointToClient(Cursor.Position).Sub(diff);
            };
            chooser.MouseUp += (object sender2, MouseEventArgs e2) =>
            {
                drag_toolstrip_button.AutoToolTip = true;
                toolStrip.EndDragging();
                drag_button = null;
                ContainerChooser.SplitType split_type;
                PanelContainer chosen = chooser.End(out split_type) ?? drag_source_container ?? this;
                chosen.Visible = true;
                ChildPanel child_panel = (ChildPanel)drag_toolstrip_button.Tag;
                if (chosen == this)
                {
                    drag_toolstrip_button.Visible = true;
                    ShowControl(child_panel);
                }
                else if (child_panel != null)
                {
                    CloseControl(child_panel);
                    chosen.AddControl(child_panel, true);
                }
            };
        }

        private void ToolStrip_MouseClick(object sender, MouseEventArgs e)
        {
            ToolStripItem item = sender as ToolStripItem;
            if (item != null)
            {
                if (e.Button == MouseButtons.Middle)
                {
                    ChildPanel c = (ChildPanel)item.Tag;
                    if (c != null)
                    {
                        CloseControl(c);
                    }
                }
                else if (e.Button == MouseButtons.Right)
                {
                    ContextMenu m = new ContextMenu();
                    MenuItem rename_item = new MenuItem("Rename");
                    rename_item.Click += (sender2, e2) =>
                    {
                        item.Text = InputString("Enter new name", "Rename panel", item.Text);
                        Control c = (Control)item.Tag;
                        c.Name = item.Text;
                    };
                    m.MenuItems.Add(rename_item);
                    m.Show(this, PointToClient(Cursor.Position));
                }
            }
        }

        private void ActiveTab_MouseEnter(MouseEventArgs e)
        {
            PopupToolbar(false, true);
        }

        public bool Pinned
        {
            get
            {
                return pinned;
            }
            set
            {
                pinned = value;
                pin_toolStripButton.Checked = pinned;
                DoPin();
            }
        }

        private void DoPin()
        {
            if (Pinned)
            {
                slider_timer.Stop();
                toolstrip_panel.Height = toolbar_default_height;
                FillContainer(toolbar_default_height);
            }
            else if(!Empty)
            {
                // not pinned, but might be scrolling up?
                FillContainer(0);
                slider_timer.Stop();
                toolstrip_panel.Height = 0;
                toolbar_timer.Stop();
                StartToolbarHiderTimer();
            }
        }

        private void toolStripButtonPin_Click(object sender, EventArgs e)
        {
            pinned = pin_toolStripButton.Checked;
            DoPin();
        }

        private bool isFixed;

        public bool Fixed
        {
            get
            {
                return isFixed;
            }
            set
            {
                isFixed = value;
                fix_toolStripButton.Checked = value;
            }
        }

        private void toolStripButtonFix_Clicked(object sender, EventArgs e)
        {
            bool is_fixed = (sender as ToolStripButton).Checked;
            SplitterPanel splitter_panel = Parent as SplitterPanel;
            if (splitter_panel != null)
            {
                PanelSplitter split_container = splitter_panel.Parent as PanelSplitter;
                if (split_container != null)
                {
                    var panel1 = split_container.Panel1.Controls[0] as PanelContainer;
                    var panel2 = split_container.Panel2.Controls[0] as PanelContainer;
                    split_container.FixedPanelEx = is_fixed ? (this == panel1 ? FixedPanel.Panel1 : FixedPanel.Panel2) : FixedPanel.None; ;
                }
            }
        }

        private void SliderTimer_Tick(object sender, EventArgs e)
        {
            toolstrip_panel.Height -= toolbar_slide_rate;
            if (toolstrip_panel.Height <= 0)
            {
                toolstrip_panel.Height = 0;
                slider_timer.Stop();
            }
        }

        private void FillContainer(int top)
        {
            foreach(ChildPanel p in children)
            {
                p.Top = top;
                p.Height = Height - top;
                p.Left = 0;
                p.Width = Width;
            }
        }

        private void ActiveTab_MouseLeave(EventArgs e)
        {
            PopupToolbar(false, true);
        }

        private bool ShouldSlideUpToolbar
        {
            get
            {
                return !pinned && Config.Settings.AutoHideToolBar;
            }
        }

        public void ToggleToolbar()
        {
            if (Pinned)
            {
                Pinned = false;
            }
            else
            {
                if (IsToolstripVisible())
                {
                    slider_timer.Start();
                }
                else
                {
                    PopupToolbar(true, false);
                }
            }
        }

        // force - show it regardless of AutoShowToolbar, hide
        public void PopupToolbar(bool force, bool auto_hide)
        {
            if (Config.Settings.AutoShowToolBar || force)
            {
                ShowToolStrip();
                slider_timer.Stop();
                if (ShouldSlideUpToolbar && auto_hide)
                {
                    toolbar_timer.Stop();
                    StartToolbarHiderTimer();
                }
            }
        }

        private void StartToolbarHiderTimer()
        {
            if(Config.Settings.AutoHideToolBar && toolbar_can_hide)
            {
                toolbar_timer.Start();
            }
        }

        private void ToolStrip_MouseLeave(object sender, EventArgs e)
        {
            PopupToolbar(false, true);
        }

        private void ShowToolStrip()
        {
            toolstrip_panel.Height = toolbar_default_height;
        }

        public bool IsToolstripVisible()
        {
            return toolstrip_panel.Height == toolbar_default_height;
        }

        private void ToolStrip_Hovered()
        {
            PopupToolbar(false, true);
            ShowToolStrip();
            slider_timer.Stop();
            toolbar_timer.Stop();
        }

        private void ToolStrip_MouseEnter(object sender, EventArgs e)
        {
            ToolStrip_Hovered();
        }

        private void ToolbarTimer_Tick(object sender, EventArgs e)
        {
            if (ShouldSlideUpToolbar && !Empty)
            {
                slider_timer.Start();
            }
            toolbar_timer.Stop();
        }

        private void DeviceConnectionStatedChanged(object sender, PropertyChangedEventArgs e)
        {
            Device device = sender as Device;
            Log.Info(string.Format("DEVICE {0} changed connection state to {1}", device.Name, device.ConnectionStateString));
        }
        
        public List<ChildPanel> ChildPanels
        {
            get
            {
                return children;
            }
        }

        public void ShowControl(ChildPanel ctrl_to_show)
        {
            if (active_tab != null)
            {
                ToolStripItem i = (ToolStripItem)active_tab.Tag;
                i.BackColor = Color.Transparent;
                i.ForeColor = SystemColors.InactiveCaptionText;
                i.Font = SystemFonts.CaptionFont;

                active_tab.ChildMouseEnter -= ActiveTab_MouseEnter;
                active_tab.ChildMouseLeave -= ActiveTab_MouseLeave;
                active_tab.Visible = false;
                active_tab.Active = false;
                active_tab = null;
            }
            active_tab = ctrl_to_show;
            if(active_tab != null)
            {
                ToolStripItem toolstrip_button = (ToolStripItem)active_tab.Tag;
                if (toolstrip_button != null)
                {
                    toolstrip_button.BackColor = SystemColors.ActiveCaption;
                    toolstrip_button.ForeColor = SystemColors.ActiveCaptionText;
                    toolstrip_button.Font = new Font(SystemFonts.CaptionFont, FontStyle.Bold);
                    active_tab.ChildMouseEnter += ActiveTab_MouseEnter;
                    active_tab.ChildMouseLeave += ActiveTab_MouseLeave;
                    active_tab.Visible = true;
                    active_tab.Active = true;
//                    active_tab.SendToBack();    // so it gets saved first and therefore loaded first
                }
                HostToolStrip(active_tab.GetToolStrip());
            }
            else
            {
                HostToolStrip(null);
            }
        }

        private void EnableMouseEvents(ToolStrip toolStrip)
        {
            DisableMouseEvents(toolStrip);
            toolStrip.Tag = this;
            toolStrip.MouseEnter += ToolStrip_MouseEnter;
            toolStrip.MouseLeave += ToolStrip_MouseLeave;
        }

        private void DisableMouseEvents(ToolStrip toolStrip)
        {
            toolStrip.MouseEnter -= ToolStrip_MouseEnter;
            toolStrip.MouseLeave -= ToolStrip_MouseLeave;
            toolStrip.Tag = null;
        }

        private ChildPanel ActiveTab
        {
            get
            {
                return active_tab;
            }
        }

        private void CloseActiveControl()
        {
            CloseControl(ActiveTab);
        }

        public bool Empty
        {
            get
            {
                return ActiveTab == null;
            }
        }

        private void CloseControl(ChildPanel c)
        {
            if (c == null)
            {
                return;
            }
            c.OnBeingClosed(this, null);
            ToolStripItem button = (ToolStripItem)c.Tag;
            toolStrip.Items.Remove(button);
            Controls.Remove(c);
            children.Remove(c);
            active_tab = null;
            bool found = false;

            // For now, just show any old other one
            foreach (ToolStripItem child in toolStrip.Items)
            {
                if (child.Tag != null)
                {
                    ShowControl((ChildPanel)child.Tag);
                    found = true;
                    break;
                }
            }
            if (!found)
            {
                HostToolStrip(null);
            }
            SetCloseButtonState();
        }

        public void AddControl(ChildPanel c, bool active)
        {
            c.Active = active;
            string text = c.Title;
            ToolStripButton nb = new ToolStripButton(text);
            c.TitleChanged += (p) => { BeginInvoke((MethodInvoker)delegate { nb.Text = p.Title; }); };
            bool was_empty = Empty;
            c.Width = Width;
            c.Height = Height;
            c.Top = 0;
            c.Left = 0;
            c.Anchor = AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top | AnchorStyles.Bottom;
            nb.Tag = c;
            nb.Alignment = ToolStripItemAlignment.Left;
            toolStrip.Items.Add(nb);
            Controls.Add(c);
            children.Add(c);
            c.OnBeingAdded(this, nb);
            c.Tag = nb;
            if (c.Active)
            {
                ShowControl(c);
            }
            else
            {
                c.Visible = false;
            }
            FillContainer(!ShouldSlideUpToolbar ? toolStrip.Height : 0);
            nb.Click += (object s, EventArgs a) =>
            {
                ChildPanel ctrl = (ChildPanel)(s as ToolStripButton).Tag;
                ShowControl(ctrl);
            };
            SetCloseButtonState();
            if (was_empty)
            {
                // tell parent that we just added our first control
            }
            c.host_control = this;
            if (Config.Settings.AlwaysPin)
            {
                Pinned = true;
            }
            PerformLayout();
        }

        private void HostToolStrip(ToolStripEx t)
        {
            if (current_hosted_toolstrip != null && current_hosted_toolstrip != t)
            {
                toolstrip_panel.Controls.Remove(current_hosted_toolstrip);
                DisableMouseEvents(current_hosted_toolstrip);
            }
            current_hosted_toolstrip = t;
            if (t != null)
            {
                SetupToolStrip(t);
            }
        }

        void SetupToolStrip(ToolStripEx t)
        {
            if (t != null)
            {
                t.Renderer = new ToolStripSystemRendererEx();
                t.BackColor = Color.Transparent;
                t.Align = HorizontalAlignment.Left;
                t.PerformLayout();
                toolstrip_panel.Join(t, 0);
                foreach (ToolStripItem item in t.Items)
                {
                    item.Tag = this;
                }
                EnableMouseEvents(t);
                toolstrip_panel.PerformLayout();
            }
        }

        public void SetCloseButtonState()
        {
            Control parent = Parent;
            bool enabled = (parent is SplitterPanel) || !Empty || !Config.Settings.TitleBar;
            toolStripButton3.Image = enabled ? Properties.Resources.CloseButton : Properties.Resources.CloseButton_disabled;
            toolStripButton3.Enabled = enabled;
        }

        public PanelSplitter DoSplit(Orientation split_type, bool order)
        {
            if (split_type == Orientation.Horizontal && Height < toolStrip.Height * 5 || split_type == Orientation.Vertical && Width < toolStrip.Height * 5)
            {
                return null;
            }
            SuspendLayout();
            PanelSplitter new_split = new PanelSplitter();
            new_split.Location = Location;
            new_split.Size = Size;
            new_split.Dock = DockStyle.Fill;
            new_split.Orientation = split_type;
            new_split.BackColor = Color.FromArgb(32, 32, 32);
            new_split.SplitterDistance = split_type == Orientation.Horizontal ? Height / 2 : Width / 2;
            new_split.SplitterWidth = 2;

            new_split.MouseHover += (object s1, EventArgs e) =>
            {
                ((PanelSplitter)s1).BackColor = SystemColors.ControlDark;
            };

            new_split.MouseLeave += (object s2, EventArgs e2) => {
                ((PanelSplitter)s2).BackColor = Color.FromArgb(32, 32, 32);
            };

            var new_container = new PanelContainer();
            new_container.Name = "SplitHoster";
            new_container.Dock = DockStyle.Fill;

            var parent = Parent;
            Parent.Controls.Remove(this);

            var a = this;
            var b = new_container;
            if (order)
            {
                var c = a;
                a = b;
                b = c;
            }
            new_split.Panel1.Controls.Add(a);
            new_split.Panel2.Controls.Add(b);

            parent.Controls.Add(new_split);

            new_container.SetCloseButtonState();
            SetCloseButtonState();
            ResumeLayout();
            return new_split;
        }

        private void toolStripButtonSplitHorizontal_Click(object sender, EventArgs e)
        {
            DoSplit(Orientation.Horizontal, (ModifierKeys & Keys.Shift) != 0);
            utility_toolstrip_button.HideDropDown();
        }

        private void toolStripButtonSplitVertical_Click(object sender, EventArgs e)
        {
            DoSplit(Orientation.Vertical, (ModifierKeys & Keys.Shift) != 0);
            utility_toolstrip_button.HideDropDown();
        }

        private void toolStripButtonClose_Click(object sender, EventArgs e)
        {
            if (Empty)
            {
                var panel = Parent as SplitterPanel;
                if (panel != null)
                {
                    var panel_parent = panel.Parent as PanelSplitter;
                    var a = panel_parent.Panel1;
                    var b = panel_parent.Panel2;

                    Control keeper = a.Contains(this) ? b.Controls[0] : a.Controls[0];
                    var parent = panel_parent.Parent;
                    parent.Controls.Remove(panel_parent);
                    parent.Controls.Add(keeper);
                    if (keeper is PanelContainer)
                    {
                        ((PanelContainer)keeper).SetCloseButtonState();
                    }
                }
                else if (!Config.Settings.TitleBar)
                {
                    MainForm.Instance.Visible = false;
                    MainForm.Instance.Close();
                }
            }
            else
            {
                CloseActiveControl();
            }
        }

        private void Add_toolStripDropDownButton_DropDownOpening(object sender, EventArgs e)
        {
            add_panel_toolStripDropDownButton.DropDownItems.Clear();
            foreach (var v in ChildPanel.Classes)
            {
                string s;
                if (ChildPanel.Names.TryGetValue(v, out s))
                {
                    var menu_item = new ToolStripMenuItem(s);

                    if (!ChildPanel.SetupMenu(menu_item, v, this))
                    {
                        menu_item.Click += (sender2, eva2) =>
                        {
                            ChildPanel c = ChildPanel.CreateChildPanel(v);
                            AddControl(c, true);
                        };
                    }

                    add_panel_toolStripDropDownButton.DropDownItems.Add(menu_item);
                }
            }
            toolbar_can_hide = false;
        }

        private void Config_button_DropDownOpening(object sender, EventArgs e)
        {
            if (active_tab != null)
            {
                config_button.DropDownItems.Clear();
                toolbar_can_hide = !active_tab.ConfigMenu(config_button.DropDownItems);
            }
            
        }

        private void toolstrip_menu_closed(object sender, EventArgs e)
        {
            toolbar_can_hide = true;
            StartToolbarHiderTimer();
        }

        protected override void OnInvalidated(InvalidateEventArgs e)
        {
            base.OnInvalidated(e);
        }
    }
}
