﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static Locomo.Extensions;

namespace Locomo.Ctrls
{
    public class ValueRange
    {
        public Span visible_range;
        public Span actual_range;
        public Span minor_tick;

        public double min_gap;            // how close in pixels the lines can be
        public bool label_right_align;    // put labels on Left (true) or Right (false) [should be an enum]
        public double label_gap;

        private double step;
        private bool step_dirty;

        private Rectangle rect;
        public double padding;       // how much extra to draw outside the rect

        // outputs
        public int significant_digits;
        public string format_string;

        public delegate void ScaleChanged(ValueRange v);
        public event ScaleChanged OnScaleChanged;

        public bool IsDirty
        {
            get
            {
                return step_dirty;
            }
        }

        public Rectangle Rect
        {
            get
            {
                return rect;
            }
            set
            {
                step_dirty = true;
                rect = value;
            }
        }

        public double Low
        {
            get
            {
                return visible_range.Low;
            }
            set
            {
                visible_range.Low = value;
                step_dirty = true;
            }
        }

        public double High
        {
            get
            {
                return visible_range.High;
            }
            set
            {
                visible_range.High = value;
                step_dirty = true;
            }
        }


        public void SetVisibleRange(double l, double h)
        {
            visible_range.Set(l, h);
            step_dirty = true;
        }

        public Span VisibleRange
        {
            get
            {
                return visible_range;
            }
        }

        public void SetVisibleRange(Span r)
        {
            VisibleRange.Set(r);
            step_dirty = true;
        }

        public void SetVisibleLower(double l)
        {
            VisibleRange.Low = l;
            step_dirty = true;
        }

        public void SetVisibleUpper(double l)
        {
            VisibleRange.High = l;
            step_dirty = true;
        }

        public double Max
        {
            get
            {
                return actual_range.High;
            }
        }

        public double Min
        {
            get
            {
                return actual_range.Low;
            }
        }

        Timer animTimer;
        DateTime animStart;
        double animEnd;

        const int animFrameSpeed = 1000 / 120;

        public ValueRange()
        {
            actual_range = new Span(0, 1);
            visible_range = new Span(0, 1);
            Rect = new Rectangle(0, 0, 1, 1);
            min_gap = 2;
            step = 1;
            minor_tick = new Span(0, 0.5f);
            label_right_align = false;
            label_gap = 10;
            step_dirty = true;
            padding = 30;
            animTimer = new Timer() { Interval = animFrameSpeed };
            animTimer.Tick += animTimer_Tick;
        }

        public double Height
        {
            get
            {
                return Rect.Height;
            }
        }

        public double Top
        {
            get
            {
                return Rect.Top;
            }
        }

        public double Range
        {
            get
            {
                return VisibleRange.Range;
            }
        }

        public double Scale
        {
            get
            {
                return Height / (double)Math.Max(Range, 0.000001);
            }
        }

        public double Left
        {
            get
            {
                return Rect.Left;
            }
        }

        public double Right
        {
            get
            {
                return Rect.Right;
            }
        }

        public double Step
        {
            get
            {
                return step;
            }
        }

        public double ValueToPixels(double v)
        {
            return (int)Math.Round(v * Scale);
        }

        public double PixelsToValue(double v)
        {
            return v / Scale;
        }

        public double YCoordFromValue(double v)
        {
            return (double)(Height - ((v - Low) * Scale) + Top);
        }

        public double ValueFromYCoord(double v)
        {
            return (Height - (v - Top)) / Scale + Low;
        }

        public void GetStep()
        {
            if (!step_dirty)
            {
                return;
            }
            // if I was less lame at maths I could integrate directly instead of using a shitty loop

            // pick a sensible starting point
            step = Math.Pow(10.0, Math.Ceiling(Math.Log10(Range)));
            double scale = 1;
            double newStep = step;
            int brk = 0;

            // find the most zoomed in it can be so labels don't overlap
            while (++brk < 20)  // TODO (chs): use label font height + some gap instead of hard coded value
            {
                double s = step / scale;
                if (s * Scale < min_gap)
                {
                    break;
                }
                newStep = s;
                scale *= 10;
            }

            // didn't find one? use some default
            if (newStep == 0)
            {
                newStep = 1;
            }

            step = newStep;
            step_dirty = false;

            // digits for string formatting (can be negative!)
            significant_digits = (int)(Math.Ceiling(Math.Log10(100000000 / step))) - 9;
            if (significant_digits > 10)
            {
                significant_digits = 10;
            }

            // actual formatting string
            format_string = "0";
            if (significant_digits > 0)
            {
                format_string += "." + new string('0', significant_digits);
            }
        }

        private double Rounder(double v, int d)
        {
            if (d > 0)
            {
                return Math.Round(v, d);
            }
            else
            {
                double x = Math.Round(v);
                double pow = Math.Pow(10, -(d + 1));
                return ((int)(x / pow) * pow);
            }
        }

        public double RoundValue(double v)
        {
            GetStep();
            return Rounder(v, significant_digits);
        }

        public double RoundValue(double v, int d)
        {
            GetStep();
            return Rounder(v, significant_digits + d);
        }

        public double PrevStep(double v, double step)
        {
            return (int)(v / step) * step;
        }

        public string FormatValue(double v)
        {
            GetStep();
            return v.ToString(format_string);
        }

        public string FormatValue(double v, int s)
        {
            v = RoundValue(v, s + 1);
            s += significant_digits;
            if (s <= 0)
            {
                return v.ToString();
            }
            return v.ToString("0." + new String('0', s - 1));
        }

        public void DrawHorizontalScale(Graphics g, Pen pen, Rectangle rect, bool zero_only)
        {
            double from = zero_only ? 0 : PrevStep(Min, step * 10);
            double to = zero_only ? 0 : Max;

            for (double y = from; y <= to; y += step * 10)
            {
                double o = YCoordFromValue(y);
                if (o < rect.Top)
                {
                    break;
                }
                if (o < rect.Bottom)
                {
                    int l = (int)Math.Round(o);
                    g.DrawLine(pen, rect.Left, l, rect.Right, l);
                }
            }
        }

        public void DrawAxis(Graphics g, Font font, Font boldFont, Pen majorPen, Pen minorPen, Color majorColor, Color minorColor, Color backColor)
        {
            GetStep();

            if (Scale < 0.1f)
            {
                return;
            }

            double pad = PixelsToValue(padding);
            double from = PrevStep(Low - pad, step * 10);   // agh, draw loads more outside the clipping area so the labels don't flick off at the edge

            int ticks = 0;
            int transparency = (int)(ValueToPixels((double)(step)) / (min_gap * 15) * 255);  // if MinGap == 2, stepper range in pixels will be 2..20
            if (transparency < 0)
            {
                transparency = 0;
            }
            else if (transparency > 255)
            {
                transparency = 255;
            }

            Pen tick = new Pen(Color.FromArgb(transparency, minorPen.Color));

            double vmin = Low - step / 2;
            double vmax = High + step / 2;

            double majorTickLeft = Left;
            double majorTickRight = Left + Rect.Width;
            double minorTickLeftPos = Left + Rect.Width * minor_tick.Low;
            double minorTickRightPos = Left + Rect.Width * minor_tick.High;

            double labelPos = Right + label_gap;    // TODO (chs): make this 8 a variable
            if(label_right_align)
            {
                labelPos = Left - label_gap;
            }

            double th = font.Height / 2;

            double maxVal = actual_range.Clamp(visible_range.High + pad) + step * 0.5f;
            double minVal = actual_range.Clamp(visible_range.Low - pad) - step * 0.5f;

            var tb = Rect.Top - padding;
            for (double y = from; true; y += step)
            {
                double tl = minorTickLeftPos;
                double tr = minorTickRightPos;
                double o = YCoordFromValue(y);
                Pen linePen = tick;
                //if ((ticks % 10) == 0 && y >= vmin)
                if ((ticks % 10) == 0)
                {
                    string s = FormatValue(y);
                    Font f = font;
                    Color labelColor = minorColor;

                    // make every pow(10) label bold

                    // bah, nasty grimmer to get round the rounding around ranges which are close to 0..10
                    int offset = -1;
                    if (significant_digits <= 0)
                    {
                        offset -= 1;
                    }
                    if (RoundValue(y, offset) == RoundValue(y, 0)) // TODO (chs): work out best/worst case ACCURACY limit here
                    {
                        f = boldFont;
                        labelColor = majorColor;
                    }

                    tl = majorTickLeft;
                    tr = majorTickRight;
                    if (y > minVal && y < maxVal)
                    {
                        double labelX = labelPos;
                        if (label_right_align)
                        {
                            labelX -= (double)TextRenderer.MeasureText(g, s, f).Width;
                        }
                        TextRenderer.DrawText(g, s, f, new Point((int)labelX, (int)(o - f.Height / 2)), labelColor, backColor);
                    }
                    linePen = majorPen;
                }
                if (o < (Rect.Bottom + padding) && o > (Rect.Top - padding) && y > minVal && y < maxVal)
                {
                    var i = (int)Math.Round(o);
                    g.DrawLine(linePen, (int)tl, (int)i, (int)tr, (int)i);
                }
                if (o < tb)
                {
                    break;
                }
                ++ticks;
            }
        }

        public void OnMouseWheel(MouseEventArgs e, double RangeLimit)
        {
            if (Range > RangeLimit || e.Delta < 0)
            {
                double v = (Height - (e.Y - Top)) / Height;
                double local = Math.Min(1.0f, Math.Max(0.0f, v));
                double mid = (Low + Range * local);
                double newRange = Math.Max(RangeLimit, Range * (1.0 - (e.Delta / 1000.0)));
                SetVisibleRange(Math.Max(Min, mid - newRange * local), Math.Min(Max, mid + newRange * (1 - local)));
                OnScaleChanged?.Invoke(this);
            }
        }

        public void Resize()
        {
            step_dirty = true;
        }

        private Span sourceView;
        private Span targetView;

        public void SetTargetRange(Span s, double seconds)   // get there in N seconds
        {
            sourceView = new Span(VisibleRange);
            targetView = new Span(s);
            animStart = DateTime.Now;
            animEnd = seconds;
            animTimer.Start();
        }
        static double easeInOut(double x, double p)
        {
            double xp = (double)Math.Pow(x, p);
            double ox = 1 - x;
            double ixp = (double)Math.Pow(ox, p);
            return xp / (xp + ixp);
        }

        static double easeOut(double x, double p)
        {
            return (easeInOut(x / 2 + 0.5f, p) - 0.5f) * 2;
        }

        static double easeIn(double x, double p)
        {
            return easeInOut(x / 2, p) * 2;
        }

        void animTimer_Tick(object sender, EventArgs e)
        {
            double elapsed = (DateTime.Now - animStart).TotalSeconds;
            if (elapsed >= animEnd)
            {
                SetVisibleRange(targetView);
                animTimer.Stop();
            }
            else
            {
                double n = elapsed / animEnd;
                double s = easeInOut(Clamp(n, 0, 1), 3);
                Span d = Span.Diff(targetView, sourceView);
                SetVisibleRange(sourceView.Low + d.Low * s, sourceView.High + d.High * s);
            }
            OnScaleChanged?.Invoke(this);
        }

        public bool Animating
        {
            get
            {
                return animTimer.Enabled;
            }
        }

    }
}
