﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using static Locomo.Extensions;

namespace Locomo.Ctrls
{
    public class DragTextBox : TextBox
    {
        const int WM_MOUSEMOVE = 0x0200;
        const int WM_LBUTTONDOWN = 0x0201; //513
        const int WM_LBUTTONUP = 0x0202; //514

        const int WM_LBUTTONDBLCLK = 0x0203; //515

        public delegate void Drag(object sender, ref Point pos);
        public delegate void DragFinished(object sender, ref Point pos);
        public delegate void NewText(object sender, string text);
        public event NewText OnNewText;

        public DragTextBox(Font f)
        {
            Multiline = true;
            BorderStyle = BorderStyle.None;
            Font = f;
            Height = Font.Height;
            TextAlign = HorizontalAlignment.Center;
            Text = string.Empty;
            Width = (int)(f.MeasureString("00.0000").Width);
            Cursor = Cursors.Arrow;
            AcceptsReturn = true;
            KeyPress += DragTextBox_KeyPress;
            Invalidate();
        }

        public override Font Font
        {
            get
            {
                return base.Font;
            }

            set
            {
                base.Font = value;
                Invalidate();
            }
        }

        void DragTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Return || e.KeyChar == (char)Keys.Tab)
            {
                SelectionLength = 0;
                Parent.SelectNextControl(this, true, false, false, true);
                OnNewText?.Invoke(this, Text);
                e.Handled = true;
            }
        }

        protected override void OnLostFocus(EventArgs e)
        {
            SelectionLength = 0;
        }

        protected override void WndProc(ref Message m)
        {
            switch (m.Msg)
            {
                case WM_LBUTTONDOWN:
                    ((DragValue)Parent).OnMouseLeftButtonDown(this.PointFromMessage(m));
                    Capture = true;
                    break;
                
                case WM_LBUTTONUP:
                    ((DragValue)Parent).OnMouseLeftButtonUp(this.PointFromMessage(m));
                    Capture = false;
                    break;

                case WM_MOUSEMOVE:
                    ((DragValue)Parent).OnMouseMoved(this.PointFromMessage(m));
                    break;
                case WM_LBUTTONDBLCLK:
                    SelectAll();
                    Focus();
                    break;
                default:
                    base.WndProc(ref m);
                    break;
            }
        }
    }
}
