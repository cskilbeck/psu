﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using static Locomo.Extensions;

namespace Locomo.Ctrls
{
    public partial class Tick : Control
    {
        public Tick()
        {
            SetStyle(ControlStyles.AllPaintingInWmPaint, true);
            SetStyle(ControlStyles.UserPaint, true);
            SetStyle(ControlStyles.SupportsTransparentBackColor, true);
            SetStyle(ControlStyles.Opaque, true);
            InitializeComponent();
        }

        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.ExStyle = cp.ExStyle | WS_EX_TRANSPARENT;
                return cp;
            }
        }

        protected override void OnPaint(PaintEventArgs pe)
        {
            pe.Graphics.FillPolygon(Brushes.Black, new Point[]
            {
                new Point(ClientRectangle.Left, ClientRectangle.Height / 2),
                new Point(ClientRectangle.Right, 0),
                new Point(ClientRectangle.Right, ClientRectangle.Height)
            });
            base.OnPaint(pe);
        }
    }
}
