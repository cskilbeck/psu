﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using System.Diagnostics;
using System.Xml;
using System.Linq;

using Managed.Graphics.Forms;
using Managed.Graphics.DirectWrite;

using static Locomo.Extensions;

using D2DColor = Managed.Graphics.Direct2D.Color;
using D2DPointF = Managed.Graphics.Direct2D.PointF;
using D2DSizeF = Managed.Graphics.Direct2D.SizeF;
using D2DBrush = Managed.Graphics.Direct2D.Brush;
using StrokeStyle = Managed.Graphics.Direct2D.StrokeStyle;
using WindowRenderTarget = Managed.Graphics.Direct2D.WindowRenderTarget;
using RectF = Managed.Graphics.Direct2D.RectF;
using AntialiasMode = Managed.Graphics.Direct2D.AntialiasMode;
using Colors = Managed.Graphics.Direct2D.Colors;
using DrawTextOptions = Managed.Graphics.Direct2D.DrawTextOptions;
using SolidColorBrush = Managed.Graphics.Direct2D.SolidColorBrush;
using StrokeStyleProperties = Managed.Graphics.Direct2D.StrokeStyleProperties;
using Direct2DFactory = Managed.Graphics.Direct2D.Direct2DFactory;

using PathGeometry = Managed.Graphics.Direct2D.PathGeometry;
using GeometrySink = Managed.Graphics.Direct2D.GeometrySink;
using PathSegment = Managed.Graphics.Direct2D.PathSegment;
using FigureBegin = Managed.Graphics.Direct2D.FigureBegin;
using FigureEnd = Managed.Graphics.Direct2D.FigureEnd;
using D2DFont = Managed.Graphics.DirectWrite.Font;

namespace Locomo.Ctrls
{
    public partial class GraphControl : Direct2DControl, Draggable
    {
        public event PropertyChangedEventHandler PausedChanged;
        public event PropertyChangedEventHandler RightLockChanged;

        public bool show_zero_lines = true;

        private double border_left = 0;  // TODO (chs): make these configurable somehow?
        private double border_right = 0;
        private double border_top = 0;
        private double border_bottom = 32;

        private double range_low;
        private double range_high;
        private int old_width;

        private Graph selected_graph = null;
        private Graph hover_graph = null;
        private Graph click_graph = null;

        private StrokeStyle grid_line_strokestyle;
        private TextFormat label_text_format;
        private D2DBrush label_brush;
        private D2DBrush grid_line_brush;
        private D2DBrush now_brush;

        private MouseButtons drag_button;
        private Point drag_point;
        private Point drag_end_point;
        private Rectangle drag_rectangle;
        private bool dragging_rectangle;
        private Point last_drag_point;

        private bool halt = false;
        private bool drag = false;
        private bool paused = false;
        private bool right_lock = false;

        private Draggable draggee;

        private double min_range;
        private double max_range;

        private double time_label_width;

        private DateTime zero_time;

        private Stopwatch stopwatch;
        private Timer refresh_timer;
        private int framerate = 60;
        private double deltatime;
        private double velocity;
        private double target_velocity;

        private Point click_point;
        private MouseButtons click_button;

        private List<Graph> Graphs;

        D2DBrush[] timeLabelBrush = new D2DBrush[4];

        const int MIN = 60;
        const int HR = MIN * 60;
        const int DAY = HR * 24;

        static double[] scales = new double[]
        {
            0.00001,
            0.0001,
            0.001,
            0.01,
            0.1,
            1,
            10,
            30,
            MIN,
            MIN * 2,
            MIN * 5,
            MIN * 10,
            MIN * 30,
            HR,
            HR * 2,
            HR * 4,
            HR * 8,
            HR * 12,
            DAY,
            DAY * 2,
            DAY * 5,
            DAY * 10
        };

        public GraphControl(): base()
        {
            Graphs = new List<Graph>();
            old_width = Width;
            InitializeComponent();
            SizeChanged += Graph_SizeChanged;
            MouseWheel += Graph_MouseWheel;
            MouseDown += Graph_MouseDown;
            MouseMove += Graph_MouseMove;
            MouseUp += Graph_MouseUp;
            stopwatch = new Stopwatch();
            refresh_timer = new Timer();
            refresh_timer.Interval = 1000 / framerate;
            refresh_timer.Tick += refresh_timer_tick;
            this.HandleCreated += Graph_Load;
            range_low = -10;
            range_high = 0;
            zero_time = DateTime.Now;
            min_range = 0.0001f;
            max_range = TimeToPixels(60 * 60 * 24 * 100);
            velocity = 1;
            target_velocity = 1;
            ToNow();
            AllowDrop = true;
            DragEnter += GraphControl_DragEnter;
            DragDrop += GraphControl_DragDrop;
        }

        private void GraphControl_DragDrop(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent("System.Windows.Forms.ListView+SelectedListViewItemCollection", false))
            {
                foreach (ListViewItem item in e.Data.GetData("System.Windows.Forms.ListView+SelectedListViewItemCollection") as ListView.SelectedListViewItemCollection)
                {
                    AddStream(item.Tag as Stream);
                }
            }
        }

        private void GraphControl_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent("System.Windows.Forms.ListView+SelectedListViewItemCollection", false))
            {
                e.Effect = DragDropEffects.Move;
            }
        }

        public bool IsShowing(Stream stream)
        {
            return Graphs.FindIndex(s => { return s.stream == stream; }) != -1;
        }

        public void SaveTo(XmlElement e)
        {
            e.Save("Paused", Paused);
            e.Save("RightLock", RightLock);
            foreach (Graph g in Graphs)
            {
                if(g.device != null)
                {
                    XmlElement x = e.Elem("Graph");
                    g.SaveTo(x);
                    e.AppendChild(x);
                }
            }
        }

        public void LoadFrom(XmlElement e)
        {
            Paused = e.Load<bool>("Paused");
        }

        private void Graph_Load(object sender, EventArgs e)
        {
            stopwatch.Start();
            refresh_timer.Start();
        }

        private void refresh_timer_tick(object sender, EventArgs e)
        {
            if (RenderTarget != null)
            {
                deltatime = stopwatch.Elapsed.TotalSeconds;
                stopwatch.Restart();
                if (drag)
                {
                    last_drag_point.X = drag_point.X;
                    velocity = 0;
                }
                if (paused)
                {
                    target_velocity = 0;
                }
                if (!halt && !drag)
                {
                    double vdelta = (target_velocity - velocity) * deltatime * 6;
                    velocity += vdelta;
                    if (Math.Abs(velocity - target_velocity) < 0.01f)
                    {
                        velocity = target_velocity;
                    }
                    range_low += velocity * deltatime;
                    range_high += velocity * deltatime;
                }
                if (Visible)
                {
                    Refresh();
                }
            }
        }

        public Graph GraphFromToolStripMenuItem(object sender)
        {
            var item = sender as ToolStripMenuItem;
            return item?.Owner.Tag as Graph;
        }

        public void AddMarker(object sender, EventArgs e)
        {
            var g = GraphFromToolStripMenuItem(sender);
            g?.AddMarker();
        }

        public void RemoveMarker(object sender, EventArgs e)
        {
            var g = GraphFromToolStripMenuItem(sender);
            g?.RemoveMarker();
        }

        public void DeleteGraph(object sender, EventArgs e)
        {
            var current_graph = GraphFromToolStripMenuItem(sender);
            if(current_graph != null)
            {
                Graphs.Remove(current_graph);
            }
        }

        public void SendGraphData(object sender, EventArgs e)
        {
            var current_graph = GraphFromToolStripMenuItem(sender);
            var chooser = new Ctrls.ContainerChooser(FindForm(), typeof(Ctrls.ChildPanel), true);
            chooser.Click += (s, e2) =>
            {
                ContainerChooser.SplitType split_type;
                var chosen = chooser.End(out split_type);
                if ((e2 as MouseEventArgs)?.Button == MouseButtons.Left)
                {
                    var new_panel = (chosen?.ActivePanel) as Panels.DataGridPanel;
                    if (new_panel == null)
                    {
                        new_panel = new Panels.DataGridPanel();
                        chosen.AddControl(new_panel, true);
                    }
                    // what panel do they want to drop it on?
                    new_panel.AddStream(current_graph.stream);
                }
            };
        }

        public void MoveGraph(object sender, EventArgs e)
        {
            var current_graph = GraphFromToolStripMenuItem(sender);
            var chooser = new ContainerChooser(FindForm(), typeof(ChildPanel), true);
            chooser.Click += (s, e2) =>
            {
                ContainerChooser.SplitType split_type;
                var chosen = chooser.End(out split_type);
                if((e2 as MouseEventArgs)?.Button == MouseButtons.Left)
                {
                    // what panel do they want to drop it on?
                    Panels.GraphPanel p = chosen?.ActivePanel as Panels.GraphPanel;

                    // not moving it? do nothing
                    if (p != null && p.GraphControl == this)
                    {
                        return;
                    }

                    // moving to a non-graph panel (or an empty one?)
                    if (p == null)
                    {
                        // ok, create a graph panel
                        p = new Panels.GraphPanel();
                        chooser.TargetContainer.AddControl(p, true);
                    }
                    // take it away from me
                    current_graph.CleanupResources();
                    Graphs.Remove(current_graph);

                    //add it to the new one
                    p.GraphControl.AddExistingGraph(current_graph);
                }
            };
        }

        public IEnumerable<Stream> AllStreams
        {
            get
            {
                foreach(Graph g in Graphs)
                {
                    yield return g.stream;
                }
            }

        }

        public void SendTo(object sender, int n)
        {
            Graph g = GraphFromToolStripMenuItem(sender);
            if (g != null)
            {
                Graphs.Remove(g);
                Graphs.Insert(Graphs.Count * n, g);
            }
        }

        public void BringToFront(object sender, EventArgs e)
        {
            SendTo(sender, 1);
        }

        public void SendToBack(object sender, EventArgs e)
        {
            SendTo(sender, 0);
        }

        public void SetGraphColor(object sender, EventArgs e)
        {
            Graph g = GraphFromToolStripMenuItem(sender);
            if (g != null)
            {
                halt = true;
                ColorDialog d = new ColorDialog();
                d.AllowFullOpen = true;
                d.AnyColor = true;
                d.Color = COLOR(g.Color);
                d.FullOpen = true;
                d.SolidColorOnly = false;
                if (d.ShowDialog() == DialogResult.OK)
                {
                    g.Color = COLOR(d.Color);
                }
                halt = false;
            }
        }

        public bool DragThreshold(MouseEventArgs e)
        {
            int max_x = SystemInformation.DragSize.Width / 2;
            int max_y = SystemInformation.DragSize.Height / 2;
            Point diff = e.Location.Sub(click_point);
            return (Math.Abs(diff.X) > max_x || Math.Abs(diff.Y) > max_y);
        }

        void Graph_SizeChanged(object sender, EventArgs e)
        {
            if (GraphWidth > 0)
            {
                // minRange/maxRange are the smallest ranges which would allow Min, Max pixels per second
                min_range = GraphWidth / 1000000;
                max_range = GraphWidth * 10000;
                double scale = GraphWidth / old_width;
                range_high = Low + Range * scale;
                old_width = (int)GraphWidth;
                if (RightLock && !paused)
                {
                    ToNow();
                }
            }
        }

        void Graph_MouseDown(object sender, MouseEventArgs e)
        {
            if (dragging_rectangle)
            {
                dragging_rectangle = false;
            }
            else
            {
                click_point = e.Location;
                click_button = e.Button;
                click_graph = hover_graph;
                halt = true;
            }
        }

        void Graph_MouseMove(object sender, MouseEventArgs e)
        {
            if (draggee != null)
            {
                halt = false;
                draggee.MouseMoved(e);
            }
            else if (click_button == MouseButtons.Left)
            {
                if (DragThreshold(e))
                {
                    halt = false;
                    click_graph = null;
                    click_button = MouseButtons.None;
                    if (hover_graph != null)
                    {
                        draggee = hover_graph;
                    }
                    else
                    {
                        draggee = this;
                    }
                    if (draggee.StartDrag(e, click_point))
                    {
                        Capture = true;
                    }
                    else
                    {
                        draggee = null;
                    }
                }
            }
            else if(click_button == MouseButtons.Right && DragThreshold(e))
            {
                Capture = true;
                draggee = this;
                paused = true;
                target_velocity = 0;
                drag = false;
                draggee.StartDrag(e, click_point);
            }
        }

        void Graph_MouseUp(object sender, MouseEventArgs e)
        {
            Capture = false;
            halt = false;
            if (draggee != null)
            {
                click_button = MouseButtons.None;
                draggee.EndDrag(e);
                draggee = null;
            }
            else if(click_button == MouseButtons.Right)
            {
                click_button = MouseButtons.None;

                if (hover_graph != null)
                {
                    halt = true;
                    ContextMenuStrip m = new ContextMenuStrip();
                    m.Tag = hover_graph;
                    m.Items.Add(hover_graph.IsHoverMarked ? new ToolStripMenuItem("Unmark", null, RemoveMarker) : new ToolStripMenuItem("Mark", null, AddMarker));
                    m.Items.Add(new ToolStripSeparator());
                    m.Items.Add(new ToolStripMenuItem("Bring to front", null, BringToFront));
                    m.Items.Add(new ToolStripMenuItem("Send to back", null, SendToBack));
                    m.Items.Add(new ToolStripMenuItem("Color", null, SetGraphColor));
                    m.Items.Add(new ToolStripMenuItem("Move to", null, MoveGraph));
                    m.Items.Add(new ToolStripMenuItem("Data to", null, SendGraphData));
                    m.Items.Add(new ToolStripMenuItem("Delete", null, DeleteGraph));
                    m.Closed += (foo, bar) => { halt = false; };
                    m.Show(this, this.MousePos());
                }
            }
            else if (click_button == MouseButtons.Left)
            {
                SelectGraph(click_graph);
                click_button = MouseButtons.None;
            }
        }

        public void SelectGraph(Graph g)
        {
            if (selected_graph != null)
            {
                selected_graph.selected = false;
            }
            selected_graph = g;
            if (selected_graph != null)
            {
                selected_graph.selected = true;
            }
        }

        void SetNewRange(double newRange, double offset)
        {
            if (newRange < min_range)
            {
                newRange = min_range;
            }
            if (newRange > max_range)
            {
                newRange = max_range;
            }
            double time = Low + Range * offset;
            range_low = time - newRange * offset;
            range_high = time + newRange * (1 - offset);
        }

        void Wrap(int w)
        {
            Point p = Cursor.Position;
            p.X += w;
            Cursor.Position = p;
            drag_point.X += w;
            last_drag_point.X += w;
        }

        public bool StartDrag(MouseEventArgs e, Point click_point)
        {
            if (dragging_rectangle && e.Button == MouseButtons.Left)
            {
                draggee = null;
                EndDrag(e);
                dragging_rectangle = false;
                drag = false;
            }
            else
            {
                StartMouseDrag(e, click_point);
                if (e.Button == MouseButtons.Left)
                {
                    if (click_point.Y < GraphBottom)
                    {
                        for (int i = 0; i < Graphs.Count; ++i)
                        {
                            Graphs[i].StartDrag(e, click_point);
                        }
                    }
                }
            }
            return drag;
        }

        public void StartMouseDrag(MouseEventArgs e, Point click_point)
        {
            drag = true;
            drag_point = click_point;
            drag_button = e.Button;
        }

        public void DoMouseDrag(MouseEventArgs e)
        {
            RightLock = false;
            double offset = PixelsToTime(drag_point.X - e.X);
            velocity = offset * deltatime;
            range_low += offset;
            range_high += offset;
            last_drag_point = drag_point;
            drag_point = e.Location;
            if (e.X < 0)
            {
                Wrap(Width);
            }
            else if (e.X > Width)
            {
                Wrap(-Width);
            }
        }

        public void DoEndMouseDrag()
        {
            drag = false;
            Capture = false;
        }

        public void MouseMoved(MouseEventArgs e)
        {
            if (Created && drag)
            {
                if (drag_button == MouseButtons.Left)
                {
                    if (click_point.Y < GraphBottom)
                    {
                        for (int i = 0; i < Graphs.Count; ++i)
                        {
                            Graphs[i].MouseMoved(e);
                        }
                    }
                    DoMouseDrag(e);
                }
                else if (drag_button == MouseButtons.Right)
                {
                    drag_end_point = e.Location;
                    drag_rectangle = new Rectangle(Math.Min(drag_point.X, drag_end_point.X),
                                                    Math.Min(drag_point.Y, drag_end_point.Y),
                                                    Math.Abs(drag_point.X - drag_end_point.X),
                                                    Math.Abs(drag_point.Y - drag_end_point.Y));
                    dragging_rectangle = true;
                }
            }
        }

        public void EndDrag(MouseEventArgs e)
        {
            if(dragging_rectangle)
            {
                dragging_rectangle = false;
                // now work out the zoom factor (for all graphs, I guess)
                foreach(Graph g in Graphs)
                {
                    g.ZoomTo(drag_rectangle.Top, drag_rectangle.Bottom);
                }
                double left = PixelsToTime(drag_rectangle.Left - GraphLeft) + range_low;
                double right = PixelsToTime(drag_rectangle.Right - GraphLeft) + range_low;
                if (Math.Abs(right - left) < min_range)
                {
                    double m = (left + right) / 2;
                    left = m - min_range / 2;
                    right = m + min_range / 2;
                }
                Paused = true;
                RightLock = false;
                range_low = left;
                range_high = right;
            }
            else if(drag_button == MouseButtons.Left)
            {
                double offset = PixelsToTime(last_drag_point.X - e.X);
                velocity = offset / deltatime;
            }
            DoEndMouseDrag();
        }

        //

        DateTime last_scroll = DateTime.Now - new TimeSpan(0, 0, 100);
        float zoom_velocity = 0.05f;

        void Graph_MouseWheel(object sender, MouseEventArgs e)
        {
            double x = e.X - GraphLeft;
            double offset = x / GraphWidth;
            DateTime now = DateTime.Now;
            TimeSpan since = now - last_scroll;
            last_scroll = now;
            if(since.TotalMilliseconds < 200)
            {
                zoom_velocity = Math.Min(3, zoom_velocity * 2);
            }
            else
            {
                zoom_velocity = 0.05f;
            }
            double new_range = Range * (1 - e.Delta / 600.0f * zoom_velocity);
            if(e.X > GraphRight - 32 && selected_graph != null)
            {
                ZoomScale(selected_graph, e, zoom_velocity);
            }
            else if (e.Y < GraphBottom)
            {
                foreach(Graph c in Graphs)
                {
                    SetNewRange(new_range, offset);
                    RightLock = false;
                    ZoomScale(c, e, zoom_velocity);
                }
            }
            else
            {
                SetNewRange(new_range, offset);
            }
        }

        void ZoomScale(Graph which, MouseEventArgs e, float zoom_velocity)
        {
            if(which != null)
            {
                which.auto_range = false;
                double d = Clamp((GraphBottom - e.Y) / GraphHeight, 0.0f, 1.0f);
                double y = which.View.Low + which.Range * d;
                double r = Clamp(which.Range * (1 - e.Delta / 600.0f * zoom_velocity), which.min_range, which.max_range);
                which.View.Set(y - r * d, y + r * (1 - d));
            }
        }

        public Graph FindDataSet(Device device, Stream stream)
        {
            foreach (Graph c in Graphs)
            {
                if (c.stream == stream && c.device == device)
                {
                    return c;
                }
            }
            return null;
        }

        public void AddStream(Stream stream)
        {
            AddNewGraph(stream.Device as Device, stream, global_timebase);
        }

        public void AddExistingGraph(Graph g)
        {
            g.graph_Control = this; ;
            Graphs.Add(g);
        }

        public Graph AddNewGraph(Device device, Stream stream, DateTime time_base)  // DODO
        {
            Graph d = new Graph(this, stream, device, time_base);
            Graphs.Add(d);
            return d;
        }

        public Graph AddNewGraph(Device device, Stream stream, DateTime time_base, System.Drawing.Color color)  // DODO
        {
            Graph d = new Graph(this, stream, device, time_base, COLOR(color));
            Graphs.Add(d);
            return d;
        }

        public bool RemoveDataSet(Device device, Stream stream)
        {
            var c = FindDataSet(device, stream);
            if (c != null)
            {
                Graphs.Remove(c);
                return true;
            }
            return false;
        }

        protected virtual void RenderHorizontalAxis(WindowRenderTarget renderTarget)
        {
            double graphScale = scales[scales.Length - 1];

            for (int i = 0; i < scales.Length; ++i)
            {
                if (TimeToPixels(scales[i]) >= time_label_width)
                {
                    graphScale = scales[i];
                    break;
                }
            }

            // get to a useful time for the time labels
            double diff = (int)(range_low / graphScale) * graphScale;

            D2DSizeF fs = new D2DSizeF(32, 20);
            double mw = fs.Width / 2;
            double mh = fs.Height / 2;

            RectF bounds = new RectF((int)GraphLeft, (int)GraphTop, (int)GraphWidth, (int)GraphHeight);

            renderTarget.AntialiasMode = AntialiasMode.PerPrimitive;
            renderTarget.Clear(D2DColor.FromKnown(Colors.Black, 1));
            int w = ClientSize.Width;
            int h = ClientSize.Height;

            // draw the graph(s)
            double best_distance = Graph.hover_threshold_distance;
            Graph closest_graph = null;
            foreach (Graph g in Graphs)
            {
                g.hover = false;
                g.PreDraw();
                if (g.best_distance < best_distance)
                {
                    best_distance = g.best_distance;
                    closest_graph = g;
                }
            }

            if (!halt)
            {
                hover_graph = null;
                if (closest_graph != null)
                {
                    hover_graph = closest_graph;
                    closest_graph.hover = true;
                    closest_graph.SnapshotHover();
                }
            }

            if (hover_graph == null)
            {
                PointF mouse_pos = this.MousePos();
                if (mouse_pos.X > GraphLeft && mouse_pos.X < GraphRight)
                {
                    float best_y = Graph.hover_threshold_distance;
                    foreach (Graph g in Graphs)
                    {
                        float y = (float)Y(0, g);
                        float ydiff = Math.Abs(y - mouse_pos.Y);
                        if (ydiff < best_y)
                        {
                            best_y = ydiff;
                            hover_graph = g;
                            hover_graph.hover = true;
                        }
                    }
                }
            }

            var bigBounds = new RectF(bounds.X, bounds.Y, bounds.Width, bounds.Height);

            // Draw vertical time lines
            TimeSpan t = TimeSpan.FromSeconds(diff);

            // draw the time labels
            diff -= graphScale;
            t = TimeSpan.FromSeconds(diff);
            long ticker = (long)(graphScale * TimeSpan.TicksPerSecond);
            for (double xtime = diff; xtime < range_high + graphScale; xtime += graphScale)
            {
                double xpos = X(xtime);
                string fmt = "\\.fffff";
                int bi = 0;
                bool centis = (t.Ticks % 1000) == 0;
                bool millis = (t.Ticks % 10000) == 0;
                bool hundredths = (t.Ticks % 100000) == 0;
                bool tenths = (t.Ticks % 1000000) == 0;
                bool second = Math.Floor(t.TotalSeconds) == t.TotalSeconds;
                bool minute = Math.Floor(t.TotalMinutes) == t.TotalMinutes;
                bool hour = Math.Floor(t.TotalHours) == t.TotalHours;
                bool day = Math.Floor(t.TotalDays) == t.TotalDays;
                if (centis)
                {
                    fmt = "\\.ffff";
                }
                if (millis)
                {
                    fmt = "\\.fff";
                }
                if (hundredths)
                {
                    fmt = "\\.ff";
                }
                if (tenths)
                {
                    fmt = "\\.f";
                }
                if (second)
                {
                    fmt = "s\\\"";
                }
                if (minute)
                {
                    bi = 1;
                    fmt = "m\\'";
                }
                if (hour)
                {
                    bi = 2;
                    fmt = "h\\h";
                }
                if (day)
                {
                    bi = 3;
                    fmt = "d\\d";
                }

                // Hmph, seems these can wander in terms of the factory used or something?
                string al = t.ToString(fmt);
                using (TextLayout tl = DW.CreateTextLayout(al, label_text_format, (float)GraphWidth, (float)GraphHeight))
                {
                    float aw = tl.Metrics.Width / 2;
                    renderTarget.DrawTextLayout(new D2DPointF((float)xpos - aw, Height - 25), tl, timeLabelBrush[bi], DrawTextOptions.None);
                }
                t += TimeSpan.FromTicks(ticker);
            }

            renderTarget.DrawRect(grid_line_brush, 1, bigBounds);

            renderTarget.PushAxisAlignedClip(bigBounds.Shrink(1, 1), AntialiasMode.PerPrimitive);

            //renderTarget.AntialiasMode = AntialiasMode.Aliased;
            for (double xtime = diff; xtime < range_high + graphScale; xtime += graphScale)
            {
                double xpos = X(xtime);
                renderTarget.DrawLine(grid_line_brush, 2, grid_line_strokestyle, new D2DPointF((float)xpos, (float)GraphTop - 1), new D2DPointF((float)xpos, (float)GraphBottom));
                t += TimeSpan.FromSeconds(graphScale);
            }

            // draw the 'now' line
            double now = (DateTime.Now - global_timebase).TotalSeconds;
            double x = X(now);
            renderTarget.DrawLine(now_brush, 1, grid_line_strokestyle, new D2DPointF((float)x, (float)GraphTop - 1), new D2DPointF((float)x, (float)GraphBottom));
        }

        protected override void OnRender(WindowRenderTarget renderTarget)
        {
            RenderHorizontalAxis(renderTarget);
            renderTarget.AntialiasMode = AntialiasMode.PerPrimitive;
            foreach (Graph c in Graphs)
            {
                c.SetupScale(GraphRight, true);
                if (selected_graph == c)
                {
                    selected_graph.DrawScale(renderTarget);
                    selected_graph.DrawZeroLine(renderTarget);
                }
                else if (show_zero_lines)
                {
                    c.DrawZeroLine(renderTarget);
                }
            }

            foreach (Graph g in Graphs)
            {
                g.Draw(renderTarget);
            }

            foreach (Graph g in Graphs)
            {
                g.DrawMarkers(renderTarget);
            }

            if (dragging_rectangle)
            {
                RectF r = new RectF(drag_rectangle.Left, drag_rectangle.Top, drag_rectangle.Width, drag_rectangle.Height);
                using (SolidColorBrush brush = renderTarget.CreateSolidColorBrush(D2DColor.FromRGB(255, 255, 0), 0.25f))
                {
                    renderTarget.FillRect(brush, r);
                }
                using (SolidColorBrush brush = renderTarget.CreateSolidColorBrush(D2DColor.FromRGB(255, 255, 0)))
                {
                    renderTarget.DrawRect(brush, 2, r);
                }
            }
            else if (draggee == null)
            {
                hover_graph?.DrawValuePopup(renderTarget);
            }
            renderTarget.PopAxisAlignedClip();
            base.OnRender(renderTarget);
        }

        protected override void OnCreateDeviceIndependentResources(Direct2DFactory factory)
        {
            grid_line_strokestyle = factory.CreateStrokeStyle(new StrokeStyleProperties(), null);
            label_text_format = DW.CreateTextFormat("Lucida Sans Typewriter", 9 * 96.0f / 72.0f, FontWeight.Medium, Managed.Graphics.DirectWrite.FontStyle.Normal);
            using (TextLayout tl = DW.CreateTextLayout("00w", label_text_format, (float)GraphWidth, (float)GraphHeight))
            {
                time_label_width = tl.Metrics.Width + 1;
            }
            base.OnCreateDeviceIndependentResources(factory);
        }

        protected override void OnCleanUpDeviceIndependentResources()
        {
            DisposeOf(ref grid_line_strokestyle);
            DisposeOf(ref label_text_format);
            base.OnCleanUpDeviceIndependentResources();
        }

        protected override void OnCreateDeviceResources(WindowRenderTarget renderTarget)
        {
            grid_line_brush = renderTarget.CreateSolidColorBrush(D2DColor.FromRGB(255, 255, 255));
            grid_line_brush.Opacity = 0.25f;

            now_brush = renderTarget.CreateSolidColorBrush(D2DColor.FromRGB(255, 255, 255));

            timeLabelBrush[0] = renderTarget.CreateSolidColorBrush(D2DColor.FromRGB(128, 128, 128));
            timeLabelBrush[1] = renderTarget.CreateSolidColorBrush(D2DColor.FromRGB(160, 160, 160));
            timeLabelBrush[2] = renderTarget.CreateSolidColorBrush(D2DColor.FromRGB(255, 255, 255));
            timeLabelBrush[3] = renderTarget.CreateSolidColorBrush(D2DColor.FromRGB(128, 255, 192));

            label_brush = renderTarget.CreateSolidColorBrush(D2DColor.FromRGB(255, 255, 255));
            base.OnCreateDeviceResources(renderTarget);
        }

        protected override void OnCleanUpDeviceResources()
        {
            foreach (Graph g in Graphs)
            {
                g.CleanupResources();
            }
            DisposeOf(ref label_brush);
            DisposeOf(ref grid_line_brush);
            DisposeOf(ref now_brush);
            DisposeOf(ref timeLabelBrush[0]);
            DisposeOf(ref timeLabelBrush[1]);
            DisposeOf(ref timeLabelBrush[2]);
            DisposeOf(ref timeLabelBrush[3]);
            base.OnCleanUpDeviceResources();
        }

        public void ToNow()
        {
            TimeSpan t = DateTime.Now - global_timebase;
            range_high = t.TotalSeconds;
            range_low = range_high - Range;
            if (paused)
            {
                paused = false;
                target_velocity = 1;
                PausedChanged?.Invoke(this, new PropertyChangedEventArgs("Paused")); // @yuck update the button
            }
            right_lock = true;
        }

        public double TimeFromX(int x)
        {
            return PixelsToTime(x - GraphLeft) - Low;
        }

        public double TimeToPixels(double t)
        {
            return (t / Range) * GraphWidth;
        }

        public double PixelsToTime(double x)
        {
            return (x / GraphWidth) * Range;
        }

        public bool Paused
        {
            get
            {
                return paused;
            }
            set
            {
                if (paused != value)
                {
                    paused = value;
                    if (!paused && right_lock)
                    {
                        ToNow();
                    }
                    if (paused)
                    {
                        target_velocity = 0;
                    }
                    else
                    {
                        target_velocity = 1;
                        stopwatch.Reset();
                        stopwatch.Start();
                        refresh_timer.Start();
                    }
                    PausedChanged?.Invoke(this, new PropertyChangedEventArgs("RightLock"));
                }
            }
        }

        public bool RightLock
        {
            get
            {
                return right_lock;
            }
            set
            {
                if(right_lock != value)
                {
                    if (value)
                    {
                        ToNow();
                    }
                    right_lock = value;
                    RightLockChanged?.Invoke(this, new PropertyChangedEventArgs("RightLock"));
                }
            }
        }

        double Range
        {
            get
            {
                return High - Low;
            }
        }

        double Zoom
        {
            get
            {
                return Range / GraphWidth;
            }
        }

        double Low
        {
            get
            {
                return range_low;
            }
        }

        double High
        {
            get
            {
                return range_high;
            }
        }

        public double X(double x)
        {
            return (x - Low) * GraphWidth / Range + GraphLeft;
        }

        public double X(TimeSpan t)
        {
            return (t.TotalSeconds - Low) * GraphWidth / Range + GraphLeft;
        }

        public double X(Reading r)
        {
            return (r.Time.TotalSeconds - Low) * GraphWidth / Range + GraphLeft;
        }

        public double Y(double y, Graph d)
        {
            return GraphBottom - (y - d.View.Low) / d.Range * GraphHeight;
        }

        public double invY(double y, Graph d)
        {
            return ((GraphBottom - y) / GraphHeight) * d.Range + d.View.Low;
        }

        // get the pixel position of a reading's mean value
        public PointF Pos(Reading r, Graph d)
        {
            return PointF(X((double)r.Time.TotalSeconds), Y(r.Max, d));
        }

        // get the pixel position of a reading's mean value
        public D2DPointF PosF(Reading r, Graph d)
        {
            return new D2DPointF((float)X((double)r.Time.TotalSeconds), (float)Y(r.Max, d));
        }

        // get the pixel position of a reading's mean value
        public PointF MinPos(Reading r, Graph d)
        {
            return PointF(X((double)r.Time.TotalSeconds), Y(r.Min, d));
        }

        // get the pixel position of a reading's mean value
        public PointF MaxPos(Reading r, Graph d)
        {
            return PointF(X((double)r.Time.TotalSeconds), Y(r.Max, d));
        }

        // get the pixel position of a reading's mean value
        public PointF ZeroPos(Reading r, Graph d)
        {
            return PointF(X((double)r.Time.TotalSeconds), Y(0, d));
        }

        public double BorderHeight
        {
            get
            {
                return border_bottom + border_top;
            }
        }

        public double BorderWidth
        {
            get
            {
                return border_left + border_right;
            }
        }

        public double GraphWidth
        {
            get
            {
                return Width - BorderWidth;
            }
        }

        public double GraphHeight
        {
            get
            {
                return Height - BorderHeight;
            }
        }

        public double GraphTop
        {
            get
            {
                return border_top;
            }
        }

        public double GraphBottom
        {
            get
            {
                return Height - border_bottom;
            }
        }

        public double GraphRight
        {
            get
            {
                return Width - border_right;
            }
        }

        public double GraphLeft
        {
            get
            {
                return border_left;
            }
        }

        public Rectangle GraphRect
        {
            get
            {
                return new Rectangle((int)GraphLeft, (int)GraphTop, (int)GraphWidth, (int)GraphHeight);
            }
        }

        public RectF GraphRectF
        {
            get
            {
                return new RectF((float)GraphLeft, (float)GraphTop, (float)GraphWidth, (float)GraphHeight);
            }
        }

    }
}
