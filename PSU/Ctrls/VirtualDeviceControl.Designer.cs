﻿namespace Locomo.Ctrls
{
    partial class VirtualDeviceControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.channels_listBox = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.add_button = new System.Windows.Forms.Button();
            this.delete_button = new System.Windows.Forms.Button();
            this.virtualInputs_Control = new Locomo.Ctrls.VirtualInputsControl();
            this.SuspendLayout();
            // 
            // channels_listBox
            // 
            this.channels_listBox.FormattingEnabled = true;
            this.channels_listBox.ItemHeight = 20;
            this.channels_listBox.Location = new System.Drawing.Point(4, 46);
            this.channels_listBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.channels_listBox.Name = "channels_listBox";
            this.channels_listBox.Size = new System.Drawing.Size(279, 244);
            this.channels_listBox.TabIndex = 0;
            this.channels_listBox.SelectedIndexChanged += new System.EventHandler(this.channels_listBox_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 14);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(76, 20);
            this.label1.TabIndex = 1;
            this.label1.Text = "Channels";
            // 
            // add_button
            // 
            this.add_button.Image = global::Locomo.Properties.Resources.PlusButton;
            this.add_button.Location = new System.Drawing.Point(99, 7);
            this.add_button.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.add_button.Name = "add_button";
            this.add_button.Size = new System.Drawing.Size(36, 35);
            this.add_button.TabIndex = 2;
            this.add_button.UseVisualStyleBackColor = true;
            this.add_button.Click += new System.EventHandler(this.Add_button_Click);
            // 
            // delete_button
            // 
            this.delete_button.Image = global::Locomo.Properties.Resources.CloseButton;
            this.delete_button.Location = new System.Drawing.Point(143, 7);
            this.delete_button.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.delete_button.Name = "delete_button";
            this.delete_button.Size = new System.Drawing.Size(36, 35);
            this.delete_button.TabIndex = 3;
            this.delete_button.UseVisualStyleBackColor = true;
            this.delete_button.Click += new System.EventHandler(this.Delete_button_Click);
            // 
            // virtualInputs_Control
            // 
            this.virtualInputs_Control.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.virtualInputs_Control.Location = new System.Drawing.Point(4, 298);
            this.virtualInputs_Control.Name = "virtualInputs_Control";
            this.virtualInputs_Control.Size = new System.Drawing.Size(280, 128);
            this.virtualInputs_Control.TabIndex = 4;
            // 
            // VirtualDeviceControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.virtualInputs_Control);
            this.Controls.Add(this.delete_button);
            this.Controls.Add(this.add_button);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.channels_listBox);
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "VirtualDeviceControl";
            this.Size = new System.Drawing.Size(287, 433);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox channels_listBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button add_button;
        private System.Windows.Forms.Button delete_button;
        private VirtualInputsControl virtualInputs_Control;
    }
}
