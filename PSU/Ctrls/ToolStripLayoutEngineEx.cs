﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.Layout;

using static Locomo.Extensions;

namespace Locomo.Ctrls
{
    public class ToolStripLayoutEngineEx : LayoutEngine
    {
        public override bool Layout(object container, LayoutEventArgs layoutEventArgs)
        {
            ToolStripPanelEx parent = container as ToolStripPanelEx;
            int left = 0;
            int right = parent.DisplayRectangle.Width;
            for(int i = parent.Controls.Count - 1; i >= 0; --i)
            {
                Ctrls.ToolStripEx c = parent.Controls[i] as Ctrls.ToolStripEx;
                if(c != null)
                {
                    switch (c.Align)
                    {
                        case HorizontalAlignment.Right:
                            c.Left = right - c.Width;
                            right -= c.Width;
                            break;
                        default:
                            c.Left = left;
                            left += c.Width;
                            break;
                    }
                }
            }
            return true;
        }
    }
}
