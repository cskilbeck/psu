﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace Locomo.Devices
{
    [Device("Power Supply", "HP", "6612C", typeof(Ctrls.SerialPortConfigControl), new Channel[] {
        Channel.VoltsDC,
        Channel.SetVolts,
        Channel.AmpsDC,
        Channel.SetAmps,
        Channel.Output,
        Channel.ConstantCurrent,
        Channel.ConstantVoltage
    })]
    public class Device_6612C : SCPIDevice
    {
        Monitor monitor;
        Monitor.ItemBase[] monitorItems;
        MultiMonitor getMisc;

        public const int CC_MINUS = 2048;
        public const int CC_PLUS = 1024;
        public const int CV = 256;

        public const int TRACK_SETVOLTS = 1;
        public const int TRACK_SETAMPS = 2;
        public const int TRACK_OUTPUT = 4;
        public const int TRACK_STATUS = 8;
        public const int TRACK_MULTI_ALL = 15;

        public Device_6612C(System.Windows.Forms.TreeNode node) : base(node)
        {
            // get TypedDataStream<>s of them
            var volts_datastream = GetStream(Channel.VoltsDC);
            var setvolts_datastream = GetStream(Channel.SetVolts);
            var amps_datastream = GetStream(Channel.AmpsDC);
            var setamps_datastream = GetStream(Channel.SetAmps);
            var output_datastream = GetStream(Channel.Output);
            var cc_datastream = GetStream(Channel.ConstantCurrent);
            var cv_dataStream = GetStream(Channel.ConstantVoltage);

            volts_datastream.Range = 20;
            setvolts_datastream.Range = 20;
            amps_datastream.Range = 2;
            setamps_datastream.Range = 2;

            // setup monitoring for volts and amps (they can't go in a MultiMonitor - see user manual)
            var getVolts = new Monitor.Item<double>("meas:volt?", volts_datastream);
            var getAmps = new Monitor.Item<double>("meas:curr?", amps_datastream);

            // MultiMonitor for the rest of them
            getMisc = new MultiMonitor(new string[] { "volt?", "curr?", "outp?", "stat:oper:cond?" }, new object[] {
                setvolts_datastream,
                setamps_datastream,
                output_datastream,
                new Action<string>(s =>
                {
                    int status;
                    if (int.TryParse(s, out status))
                    {
                        cc_datastream.Put(new Result<int>(((status & CC_MINUS) != 0) ? -1 : ((status & CC_PLUS) != 0) ? 1 : 0));
                        cv_dataStream.Put(new Result<bool>((status & CV) != 0));
                    }
                })

            });
            monitorItems = new Monitor.ItemBase[] { getVolts, getAmps, getMisc };
        }

        public void StartTracking(int mask)
        {
            getMisc.mask |= mask;
        }

        public void StopTracking(int mask)
        {
            getMisc.mask &= ~mask;
        }

        public void SetTracking(int mask, bool enable_disable)
        {
            if (enable_disable) StartTracking(mask);
            else StopTracking(mask);
        }

        public override void StartMonitor()
        {
            if (monitor == null)
            {
                monitor = new Monitor(monitorItems, this);
            }
            monitor.Start();
        }

        public override void StopMonitor()
        {
            if (monitor != null)
            {
                monitor.Stop();
                monitor = null;
            }
        }

        public override void Disconnect()
        {
            StopMonitor();
            base.Disconnect();
        }

        static Dictionary<Channel, string> GetCommands = new Dictionary<Channel, string>()
        {
            { Channel.Output, "outp?" }
        };

        public override void GetValue<T>(Channel t, Completed<T> onCompleted)
        {
            string cmd;
            if (GetCommands.TryGetValue(t, out cmd))
            {
                Get<T>(cmd, onCompleted);
            }
        }
    }
}
