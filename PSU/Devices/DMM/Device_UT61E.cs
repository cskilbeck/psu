﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO.Ports;
using System.Runtime.InteropServices;
using System.Text;
using Locomo.Devices.DMM;
using Logging;

// UT61x serial: connect RTS to ground(Pin 5 and Pin 7), DTR to 5V or 3,3V and invert the output


// UT61C 2400 7N2
// http://www.produktinfo.conrad.com/datenblaetter/100000-124999/124601-in-01-en-RS232_com_protocol_VC_850.pdf

namespace Locomo.Devices
{
    [Device("Multimeter", "Uni-T", "UT61E", typeof(Ctrls.SerialPortConfigControl), new Channel[]
                                                            {
                                                                Channel.VoltsDC,
                                                                Channel.VoltsAC,
                                                                Channel.AmpsDC,
                                                                Channel.AmpsAC,
                                                                Channel.Ohms,
                                                                Channel.Farads,
                                                                Channel.Degrees
                                                            })]
    public class Device_UT61E : DMM_Serial, DMM.IDMM
    {
        public Device_UT61E(System.Windows.Forms.TreeNode node) : base(node)
        {
            listener = new MyListener(this);
        }

        public override SerialPortConfig DefaultConfig
        {
            get
            {   // UT61E: BaudRate = 19200, 7 data bits, odd parity, 1 stop bit, no flow control (set DTR to switch it on)
                SerialPortConfig c = new SerialPortConfig();
                c.BaudRate = 19200;
                c.DataBits = 7;
                c.StopBits = StopBits.One;
                c.Parity = Parity.Odd;
                c.FlowControl = Handshake.None;
                return c;
            }
        }

        public override SerialSettingsMask SettingsMask
        {
            get
            {
                return SerialSettingsMask.BAUDRATE |
                        SerialSettingsMask.DATABITS |
                        SerialSettingsMask.STOPBITS |
                        SerialSettingsMask.PARITY |
                        SerialSettingsMask.FLOWCONTROL;
            }
        }

        [Flags]
        public enum Flags
        {
            OL = 0x00001,       // input overflow
            BATT = 0x00002,     // 1-battery low
            SIGN = 0x00004,     // 1-minus sign, 0-no sign
            SCALE = 0x00008,    // 1-°C, 0-°F.

            RMR = 0x00010,      // current value
            REL = 0x00020,      // relative/zero mode
            MIN = 0x00040,      // minimum
            MAX = 0x00080,      // maximum

            ZERO1 = 0x00100,    // should be zero
            PMIN = 0x00200,     // minimum peak value
            PMAX = 0x00400,     // maximum peak value
            UL = 0x00800,       // 1 -at 22.00Hz <2.00Hz., at 220.0Hz <20.0Hz,duty cycle <10.0%.

            VAHZ = 0x01000,     // Volts AC Hz mode
            AUTO = 0x02000,     // 1-automatic mode, 0-manual
            AC = 0x04000,       // AC measurement mode, either voltage or current.
            DC = 0x08000,       // DC measurement mode, either voltage or current.

            LPF = 0x10000,      // low-pass-filter feature is activated.
            HOLD = 0x20000,     // hold mode
            VBAR = 0x40000,     // 1-VBAR pin is connected to V-.
            ZERO2 = 0x80000,    // should be zero
        }

        public class Range
        {
            public double multiplier;
            public int power;
            public string display_unit;

            public Range(double multiplier, int power, string display_unit)
            {
                this.multiplier = multiplier;
                this.power = power;
                this.display_unit = display_unit;
            }
        }

        public class Function
        {
            public string name;
            public Channel channel;
            public Range[] ranges;

            public Function(string name, Channel channel, Range[] ranges)
            {
                this.name = name;
                this.channel = channel;
                this.ranges = ranges;
            }
        };

        static Function[] Functions = new Function[16]
        {
            // 0
            new Function("current", Channel.AmpsDC, new Range[]
            {
                new Range(1e0, 3, "A") //22.000 A
            }),
            // 1
            new Function("diode", Channel.VoltsDC, new Range[]
            {
                new Range(1e0, 4, "V"),  //2.2000V
            }),
            // 2
            new Function("frequency", Channel.Hertz, new Range[]
            {
                new Range(1e0, 2, "Hz" ), //22.00Hz
                new Range(1e0, 1, "Hz" ), //220.0Hz
                null,
                new Range(1e3, 3, "kHz"), //22.000KHz
                new Range(1e3, 2, "kHz"), //220.00KHz
                new Range(1e6, 4, "MHz"), //2.2000MHz
                new Range(1e6, 3, "MHz"), //22.000MHz
                new Range(1e6, 2, "MHz"), //220.00MHz
            }),
            // 3
            new Function("resistance", Channel.Ohms, new Range[]
            {
                new Range(1e0, 2, "Ω" ), //220.00Ω
                new Range(1e3, 4, "kΩ"), //2.2000KΩ
                new Range(1e3, 3, "kΩ"), //22.000KΩ
                new Range(1e3, 2, "kΩ"), //220.00KΩ
                new Range(1e6, 4, "MΩ"), //2.2000MΩ
                new Range(1e6, 3, "MΩ"), //22.000MΩ
                new Range(1e6, 2, "MΩ"), //220.00MΩ
            }),
            // 4
            new Function("temperature", Channel.Degrees, null),
            // 5
            new Function("continuity", Channel.Ohms, new Range[]
            {
                new Range(1e0, 2, "Ω"), //220.00Ω
            }),
            // 6
            new Function("capacitance", Channel.Farads, new Range[]
            {
                new Range(1e-9, 3, "nF"), //22.000nF
                new Range(1e-9, 2, "nF"), //220.00nF
                new Range(1e-6, 4, "µF"), //2.2000μF
                new Range(1e-6, 3, "µF"), //22.000μF
                new Range(1e-6, 2, "µF"), //220.00μF
                new Range(1e-3, 4, "mF"), //2.2000mF
                new Range(1e-3, 3, "mF"), //22.000mF
                new Range(1e-3, 2, "mF"), //220.00mF
            }),
            // 7
            null,
            // 8
            null,
            // 9
            new Function("current", Channel.AmpsDC, new Range[]
            {
                new Range(1e0, 4, "A"), //2.2000A
                new Range(1e0, 3, "A"), //22.000A
                new Range(1e0, 2, "A"), //220.00A
                new Range(1e0, 1, "A"), //2200.0A
                new Range(1e0, 0, "A"), //22000A
            }),
            // 10
            null,
            // 11
            new Function("voltage", Channel.VoltsDC, new Range[]
            {
                new Range(1e0, 4, "V" ),  // 2.2000V
                new Range(1e0, 3, "V" ),  // 22.000V
                new Range(1e0, 2, "V" ),  // 220.00V
                new Range(1e0, 1, "V" ),  // 2200.0V
                new Range(1e-3, 2,"mV"), // 220.00mV
            }),
            // 12
            null,
            // 13
            new Function("current", Channel.AmpsDC, new Range[]
            {
                new Range(1e-6, 2, "µA"), //
                new Range(1e-6, 1, "µA"), //2
            }),
            // 14
            new Function("ADP", Channel.None, new Range[]
            {
                new Range(1, 1, "ADP4"),
                new Range(1, 1, "ADP3"),
                new Range(1, 1, "ADP2"),
                new Range(1, 1, "ADP1"),
                new Range(1, 1, "ADP0"),
            }),
            // 15
            new Function("current", Channel.AmpsDC, new Range[]
            {
                new Range(1e-3, 3, "mA"), //
                new Range(1e-3, 2, "mA"), //2
            }),
        };

        static Function duty_cycle = new Function("duty_cycle", Channel.Percent, new Range[] { new Range(1e0, 1, "%") });

        //public event on_data_delegate on_data = null;

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct Packet
        {
            public byte range;
            public byte d4, d3, d2, d1, d0;
            public byte func;
            public byte status;
            public byte option1, option2, option3, option4;
        }

        public override void ReadData()
        {
            try
            {
                string line = Connection.ReadLine();
                Throw.If(line == null, "Null result from " + Connection.Name);
                Throw.If(line.Length != 13, "Bad line length (" + line.Length + "): [" + line + "]");

                double value = 0;

                // stuff it into a struct
                Packet s = Loader.Load<Packet>(line, Encoding.ASCII);

                if(!on_data_is_null())
                {
                    int size = Marshal.SizeOf(s);
                    byte[] arr = new byte[size];
                    IntPtr ptr = Marshal.AllocHGlobal(size);
                    Marshal.StructureToPtr(s, ptr, true);
                    Marshal.Copy(ptr, arr, 0, size);
                    Marshal.FreeHGlobal(ptr);
                    invoke_on_data(arr);
                }

                // extract the flags into a Flags enum
                Flags flags = 0;
                foreach (var val in new int[] { s.option4, s.option3, s.option2, s.option1, s.status })
                {
                    Throw.If((val & 0x30) != 0x30, "Invalid byte in status/options");
                    flags = (Flags)(((int)flags << 4) | (val & 0xf));
                }
                Throw.If(flags.Any(Flags.ZERO1 | Flags.ZERO2), "Zero bits not zero");

                // get decoder
                var func_id = s.func - 0x30;
                if (flags.On(Flags.VAHZ))
                {
                    func_id = 2;
                }
                Throw.If(func_id.OutOfRange(Functions), "Invalid function ID");

                var func = Functions[func_id];
                Throw.If(func == null, "Non existent function");

                if (func_id == 2 /* frequency */ && flags.On(Flags.SCALE))
                {
                    func = duty_cycle;
                }

                // get the right ranger
                var range_id = s.range - 0x30;
                Throw.If(range_id.OutOfRange(func.ranges), "Invalid range");

                var range = func.ranges[range_id];
                Throw.If(range == null, "Non existent range");

                if (func_id == 4 /*temperature*/)
                {
                    range = new Range(1e0, flags.On(Flags.VBAR) ? 1 : 2, "deg");
                }

                // get the actual number displayed
                Throw.If(!double.TryParse(Encoding.ASCII.GetString(new byte[] { s.d4, s.d3, s.d2, s.d1, s.d0 }), out value), "Invalid digits");

                // apply ranger
                value = value / Math.Pow(10, range.power) * range.multiplier * (flags.On(Flags.SIGN) ? -1 : 1);

                // or just sack it off if under or overflow
                if (flags.Any(Flags.OL | Flags.UL))
                {
                    value = 0;
                }

                Channel channel = func.channel;
                if (flags.On(Flags.AC))
                {
                    if (channel == Channel.AmpsDC)
                    {
                        channel = Channel.AmpsAC;
                    }
                    else if (channel == Channel.VoltsDC)
                    {
                        channel = Channel.VoltsAC;
                    }
                }

                // stuff it into the right datastream
                var d = GetStream(channel);

                if (d != null)
                {
                    if (channel != last_channel)
                    {
                        d.ResetPacketCount();
                        last_channel = channel;
                    }
                    d.Put(new Result<double>(value));
                    //ConnectionState = State.Connected;
                }
            }
            catch (Error e)
            {
                Log.Error(e.Message.Replace("\r", ""));
                if(Connection.State == ConnectionState.Disconnected)
                {
                    listener.Stop();
                }
            }
            catch (TimeoutException)
            {
                Log.Write(Log.Severity.Error, "Timeout", $"{Name} timed out on {(Connection.Name != null ? Connection.Name : "NULL Connection?")}");

                // if you like, you can disconnect here...
                // TODO (chs): setting to auto-disconnect on serial port timeout? (global, and per device override?)
                // Disconnect();
            }
        }
    }
}
