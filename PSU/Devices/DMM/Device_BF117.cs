﻿//////////////////////////////////////////////////////////////////////
// DMM
// Bolyfa BF117
// 

/*
Similar to HYELEC MS8236

Source: https://sigrok.org/wiki/HYELEC_MS8236

Packet length: 23 bytes

Bytes 0 to 6: AA 55 52 24 01 10 10

Bytes 7 to 10: Next four bytes are the numbers, from right to left, in the format 

7   6   5   4   3   2   1   0
DP  A   B   C   D   E   F   G

 ---G---
|       |
C       F
|       |
 ---B---
|       |
A       E
|       |
 ---D---

Byte 11:    00000000 
0           ||||||||--> Diode icon
1           |||||||---> AC
2           ||||||----> DC
3           |||||-----> - (minus
4           ||||------> - (minus, duplicate of bit 3)
5           |||-------> Progess bar cue - the bar with the 0 - 10 - 20 - ...
6           ||--------> Continuity tester "sound" icon
7           |---------> Battery

Bytes 12 to 18: first 56 bits of the progress bar

Byte 19:    00000000
0           ||||||||--> progress bar bit 56
1           |||||||---> progress bar bit 57
2           ||||||----> progress bar bit 58
3           |||||-----> progress bar bit 59
4           ||||------> Wait... (?)
5           |||-------> Auto
6           ||--------> HOLD (USB transmission stops when hold is activated)
7           |---------> REL

Byte 20:    00000000
0           ||||||||--> USB Icon (always on)
1           |||||||---> MAX
2           ||||||----> - (hyphen between MAX and MIN)
3           |||||-----> MIN
4           ||||------> N/A (?)
5           |||-------> %
6           ||--------> hFE
7           |---------> N/A (?)

Byte 21:    00000000
0           ||||||||--> C (temp)
1           |||||||---> F (temp)
2           ||||||----> N/A (?)
3           |||||-----> N/A (?)
4           ||||------> m (in the Farads line)
5           |||-------> u (in the Farads line)
6           ||--------> n (in the Farads line)
7           |---------> F (Farads)

Byte 22:    00000000
0           ||||||||--> u (in the A line)
1           |||||||---> m (in the A line)
2           ||||||----> A
3           |||||-----> V (in diode meter mode)
4           ||||------> M (in the Ohms line)
5           |||-------> k (in the Ohms line)
6           ||--------> Ohms
7           |---------> Hz

*/

using System;
using System.IO.Ports;
using Locomo.Devices.DMM;
using Logging;
using DMM.LCD;

namespace Locomo.Devices
{
    //////////////////////////////////////////////////////////////////////

    [Device("Multimeter", "Bolyfa", "BF117", typeof(Ctrls.SerialPortConfigControl), new Channel[]
                                                            {
                                                                Channel.VoltsDC,
                                                                Channel.VoltsAC,
                                                                Channel.AmpsDC,
                                                                Channel.AmpsAC,
                                                                Channel.Ohms,
                                                                Channel.Farads,
                                                                Channel.Degrees
                                                            })]
    public class Device_BF117 : DMM_LCDMapped, DMM.IDMM
    {
        //////////////////////////////////////////////////////////////////////

        byte[] ident = new byte[] { 0xaa, 0x55, 0x52, 0x24, 0x01, 0x10 };
        private byte[] buffer = new byte[23];

        //////////////////////////////////////////////////////////////////////
        // the IDs of the bits in the buffer

        bit[] bits = new bit[]
        {
            new bit {offset = 7, bit_number = 7, id = segment.decimal_point3 },
            new bit {offset = 8, bit_number = 7, id = segment.decimal_point2 },
            new bit {offset = 9, bit_number = 7, id = segment.decimal_point1 },

            new bit {offset = 11, bit_number = 0, id = segment.diode },
            new bit {offset = 11, bit_number = 1, id = segment.ac },
            new bit {offset = 11, bit_number = 2, id = segment.dc },
            new bit {offset = 11, bit_number = 3, id = segment.minus },
            new bit {offset = 11, bit_number = 4, id = segment.minus },
            new bit {offset = 11, bit_number = 5, id = segment.none },
            new bit {offset = 11, bit_number = 6, id = segment.beep },
            new bit {offset = 11, bit_number = 7, id = segment.battery },

            new bit {offset = 19, bit_number = 5, id = segment.auto },
            new bit {offset = 19, bit_number = 6, id = segment.hold },
            new bit {offset = 19, bit_number = 7, id = segment.relative },

            new bit {offset = 20, bit_number = 0, id = segment.usb },
            new bit {offset = 20, bit_number = 1, id = segment.max },
            new bit {offset = 20, bit_number = 2, id = segment.minmax },
            new bit {offset = 20, bit_number = 3, id = segment.min },
            new bit {offset = 20, bit_number = 4, id = segment.none },
            new bit {offset = 20, bit_number = 5, id = segment.percent },
            new bit {offset = 20, bit_number = 6, id = segment.hFE },
            new bit {offset = 20, bit_number = 7, id = segment.none },

            new bit {offset = 21, bit_number = 0, id = segment.celsius },
            new bit {offset = 21, bit_number = 1, id = segment.fahrenheit },
            new bit {offset = 21, bit_number = 2, id = segment.none },
            new bit {offset = 21, bit_number = 3, id = segment.none },
            new bit {offset = 21, bit_number = 4, id = segment.milli },
            new bit {offset = 21, bit_number = 5, id = segment.micro },
            new bit {offset = 21, bit_number = 6, id = segment.nano },
            new bit {offset = 21, bit_number = 7, id = segment.farads },

            new bit {offset = 22, bit_number = 0, id = segment.micro },
            new bit {offset = 22, bit_number = 1, id = segment.milli },
            new bit {offset = 22, bit_number = 2, id = segment.amps },
            new bit {offset = 22, bit_number = 3, id = segment.volts },
            new bit {offset = 22, bit_number = 4, id = segment.meg },
            new bit {offset = 22, bit_number = 5, id = segment.kilo },
            new bit {offset = 22, bit_number = 6, id = segment.ohms },
            new bit {offset = 22, bit_number = 7, id = segment.hertz }
        };

        //////////////////////////////////////////////////////////////////////
        // identification of the 7 segment digits

        const int seg_A = 1 << 6;
        const int seg_B = 1 << 5;
        const int seg_C = 1 << 4;
        const int seg_D = 1 << 3;
        const int seg_E = 1 << 2;
        const int seg_F = 1 << 1;
        const int seg_G = 1 << 0;

        int[] digits = new int[]
        {
            seg_A | seg_C | seg_G | seg_F | seg_E | seg_D,          // 0
            seg_F | seg_E,                                          // 1
            seg_G | seg_F | seg_B | seg_A | seg_D,                  // 2
            seg_G | seg_F | seg_B | seg_E | seg_D,                  // 3
            seg_C | seg_B | seg_F | seg_E,                          // 4
            seg_G | seg_C | seg_B | seg_E | seg_D,                  // 5
            seg_G | seg_C | seg_B | seg_A | seg_E | seg_D,          // 6
            seg_G | seg_F | seg_E,                                  // 7
            seg_A | seg_B | seg_C | seg_D | seg_E | seg_F | seg_G,  // 8
            seg_B | seg_C | seg_D | seg_E | seg_F | seg_G,          // 9
            seg_C | seg_A | seg_D,                                  // L
        };

        //////////////////////////////////////////////////////////////////////

        channel_bit[] channel_bits = new channel_bit[]
        {
            new channel_bit { bits = segment.ac | segment.volts, c = Locomo.Channel.VoltsAC },
            new channel_bit { bits = segment.dc | segment.volts, c = Locomo.Channel.VoltsDC },
            new channel_bit { bits = segment.ac | segment.amps, c = Locomo.Channel.AmpsAC },
            new channel_bit { bits = segment.dc | segment.amps, c = Locomo.Channel.AmpsDC },
            new channel_bit { bits = segment.ohms, c = Locomo.Channel.Ohms },
            new channel_bit { bits = segment.celsius, c = Locomo.Channel.Degrees },
            new channel_bit { bits = segment.celsius, c = Locomo.Channel.Degrees },
            new channel_bit { bits = segment.farads, c = Locomo.Channel.Farads },
            new channel_bit { bits = segment.hertz, c = Locomo.Channel.Hertz },
            new channel_bit { bits = segment.percent, c = Locomo.Channel.Percent }
        };

        //////////////////////////////////////////////////////////////////////

        protected override bit[] Bits
        {
            get
            {
                return bits;
            }
        }

        protected override int[] DigitMasks
        {
            get
            {
                return digits;
            }
        }

        protected override channel_bit[] ChannelBits
        {
            get
            {
                return channel_bits;
            }
        }

        //////////////////////////////////////////////////////////////////////

        public Device_BF117(System.Windows.Forms.TreeNode node) : base(node)
        {
        }

        //////////////////////////////////////////////////////////////////////

        public override SerialPortConfig DefaultConfig
        {
            get
            {   // DT400ZC: BaudRate = 2400, 8 data bits, no parity, 1 stop bit, no flow control
                SerialPortConfig c = new SerialPortConfig();
                c.BaudRate = 2400;
                c.DataBits = 8;
                c.StopBits = StopBits.One;
                c.Parity = Parity.None;
                c.FlowControl = Handshake.None;
                return c;
            }
        }

        //////////////////////////////////////////////////////////////////////

        public override SerialSettingsMask SettingsMask
        {
            get
            {
                return SerialSettingsMask.BAUDRATE |
                        SerialSettingsMask.DATABITS |
                        SerialSettingsMask.STOPBITS |
                        SerialSettingsMask.PARITY |
                        SerialSettingsMask.FLOWCONTROL;
            }
        }

        //////////////////////////////////////////////////////////////////////

        protected override double get_number(byte[] buffer)
        {
            double value = 0;
            for (int i = 11; i >= 7; i -= 1)
            {
                value *= 10.0;
                int v = buffer[i] & 0x7f;
                int d = get_digit_from_byte(v);
                if (d == 10)
                {
                    return -1;  // overload
                }
                value += d;
            }
            return value;
        }

        //////////////////////////////////////////////////////////////////////

        int ident_scan = 0;
        int buffer_count = 0;

        public override void ReadData()
        {
            try
            {
                int b = Connection.ReadByte();
                if(ident_scan < ident.Length)
                {
                    if(b == ident[ident_scan])
                    {
                        ident_scan += 1;
                        //Log.Debug($"IDENT: {b}");
                    }
                    else
                    {
                        ident_scan = 0;
                    }
                }
                if (ident_scan == ident.Length)
                {
                    Array.Copy(ident, 0, buffer, 0, ident.Length);
                    buffer[buffer_count] = (byte)b;
                    buffer_count += 1;
                    if (buffer_count == buffer.Length)
                    {
                        buffer_count = ident.Length;
                        ident_scan = 0;

                        invoke_on_data(buffer);

                        segments = 0;
                        foreach (bit i in bits)
                        {
                            if(((buffer[i.offset] & (1 << i.bit_number)) != 0))
                            {
                                segments |= (ulong)i.id;
                            }
                        }

                        update_value(buffer);
                    }
                }
            }
            catch (Error e)
            {
                Log.Error(e.Message.Replace("\r", ""));
                if (Connection.State == ConnectionState.Disconnected)
                {
                    listener.Stop();
                }
            }
            catch (TimeoutException)
            {
                Log.Write(Log.Severity.Error, "Timeout", $"{Name} timed out on {(Connection.Name != null ? Connection.Name : "NULL Connection?")}");

                // if you like, you can disconnect here...
                // TODO (chs): setting to auto-disconnect on serial port timeout? (global, and per device override?)
                // Disconnect();
            }
        }
    }
}
