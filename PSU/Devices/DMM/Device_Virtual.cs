﻿//////////////////////////////////////////////////////////////////////
// Virtual Device
// User can create virtual channels which take 2 other channels as inputs and specify a combiner function (eg multiply)
// Virtual channels can be fed into other virtual channels
// Virtual channel specifies connection id and device type - if either doesn't match up, warn and kill the channel (and dependent channels)

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using Locomo.Devices.DMM;

namespace Locomo.Devices
{
    public class Virtual_Channel
    {
        public enum operation
        {
            Add,
            Subtract,
            Multiply,
            Divide
        }

        public string name;
        public Stream A;
        public Stream B;
        public operation op;

        public override string ToString()
        {
            return name;
        }
    };

    // TODO (chs): might need IDMM.DMM here when scanning lists of device
    [Device("Other", "Virtual", "Device", typeof(Ctrls.VirtualDeviceControl), null)]
    public class Device_Virtual : Device
    {
        public List<Virtual_Channel> virtual_channels = new List<Virtual_Channel>();

        List<Channel> bogus_channels = new List<Channel>();

        public override Channel[] Channels
        {
            get
            {
                return bogus_channels.ToArray();
            }
        }

        public Device_Virtual(System.Windows.Forms.TreeNode node) : base(node)
        {
            Connection = null;
        }

        public override bool Controllable
        {
            get
            {
                return false;
            }
        }

        public override bool Connect()
        {
            return true;
        }

        public override void Disconnect()
        {
        }

        public override void InitConfigControl(Control c)
        {
            Ctrls.VirtualDeviceControl v = (Ctrls.VirtualDeviceControl)c;
            v.set_device(this);
        }

        public override void Load(XmlElement e)
        {
        }

        public override void Save(XmlElement e)
        {
        }

        public override void Setup(Control o)
        {
        }
    }
}
