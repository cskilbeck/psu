﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO.Ports;
using System.Runtime.InteropServices;
using System.Text;
using Locomo.Devices.DMM;
using Logging;

// UT61x serial: connect RTS to ground(Pin 5 and Pin 7), DTR to 5V or 3,3V and invert the output


// UT61C 2400 7N2
// http://www.produktinfo.conrad.com/datenblaetter/100000-124999/124601-in-01-en-RS232_com_protocol_VC_850.pdf

namespace Locomo.Devices
{
    [Device("Multimeter", "Uni-T", "UT61C", typeof(Ctrls.SerialPortConfigControl), new Channel[]
                                                            {
                                                                Channel.VoltsDC,
                                                                Channel.VoltsAC,
                                                                Channel.AmpsDC,
                                                                Channel.AmpsAC,
                                                                Channel.Ohms,
                                                                Channel.Farads,
                                                                Channel.Degrees
                                                            })]
    public class Device_UT61C : DMM_Serial, DMM.IDMM
    {
        public Device_UT61C(System.Windows.Forms.TreeNode node) : base(node)
        {
        }

        public override void ReadData()
        {
        }
    }
}
