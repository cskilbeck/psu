﻿//////////////////////////////////////////////////////////////////////
// https://www.forward.com.au/pfod/pfodDataLogging/tp4000zc_serial_protocol.pdf

using System;
using System.IO.Ports;
using Locomo.Devices.DMM;
using Logging;
using DMM.LCD;

//////////////////////////////////////////////////////////////////////

namespace Locomo.Devices
{
    //////////////////////////////////////////////////////////////////////

    [Device("Multimeter", "Digitek", "DT400ZC", typeof(Ctrls.SerialPortConfigControl), new Channel[]
                                                            {
                                                                Channel.VoltsDC,
                                                                Channel.VoltsAC,
                                                                Channel.AmpsDC,
                                                                Channel.AmpsAC,
                                                                Channel.Ohms,
                                                                Channel.Farads,
                                                                Channel.Degrees
                                                            })]
    public class Device_DT4000ZC : DMM_LCDMapped, DMM.IDMM
    {
        //////////////////////////////////////////////////////////////////////

        private byte[] buffer = new byte[14];

        //////////////////////////////////////////////////////////////////////
        // the positions of the bits in the buffer

        bit[] bits = new bit[]
        {
            new bit {offset = 0, bit_number = 3, id = segment.ac },
            new bit {offset = 0, bit_number = 2, id = segment.dc },
            new bit {offset = 0, bit_number = 1, id = segment.auto },
            new bit {offset = 0, bit_number = 0, id = segment.rs232 },

            new bit {offset = 1, bit_number = 3, id = segment.minus },

            new bit {offset = 3, bit_number = 3, id = segment.decimal_point1 },
            new bit {offset = 5, bit_number = 3, id = segment.decimal_point2 },
            new bit {offset = 7, bit_number = 3, id = segment.decimal_point3 },

            new bit {offset = 9, bit_number = 3, id = segment.micro },
            new bit {offset = 9, bit_number = 2, id = segment.nano },
            new bit {offset = 9, bit_number = 1, id = segment.kilo },
            new bit {offset = 9, bit_number = 0, id = segment.diode },

            new bit {offset = 10, bit_number = 3, id = segment.milli },
            new bit {offset = 10, bit_number = 2, id = segment.percent },
            new bit {offset = 10, bit_number = 1, id = segment.meg },
            new bit {offset = 10, bit_number = 0, id = segment.beep },

            new bit {offset = 11, bit_number = 3, id = segment.farads },
            new bit {offset = 11, bit_number = 2, id = segment.ohms },
            new bit {offset = 11, bit_number = 1, id = segment.relative },
            new bit {offset = 11, bit_number = 0, id = segment.hold },

            new bit {offset = 12, bit_number = 3, id = segment.amps },
            new bit {offset = 12, bit_number = 2, id = segment.volts },
            new bit {offset = 12, bit_number = 1, id = segment.hertz },
            new bit {offset = 12, bit_number = 0, id = segment.battery },

            new bit {offset = 13, bit_number = 2, id = segment.celsius },

            new bit {offset = 0, bit_number = 8, id = segment.none }
        };

        //////////////////////////////////////////////////////////////////////
        // identification of the 7 segment digits

        const int seg_A = 1 << 6;
        const int seg_B = 1 << 5;
        const int seg_C = 1 << 4;
        const int seg_D = 1 << 3;
        const int seg_E = 1 << 2;
        const int seg_F = 1 << 1;
        const int seg_G = 1 << 0;

        int[] digits = new int[]
        {
            seg_A | seg_B | seg_C | seg_G | seg_E | seg_D,          // 0
            seg_G | seg_E,                                          // 1
            seg_C | seg_G | seg_F | seg_A | seg_D,                  // 2
            seg_C | seg_G | seg_F | seg_E | seg_D,                  // 3
            seg_B | seg_G | seg_F | seg_E,                          // 4
            seg_C | seg_B | seg_F | seg_E | seg_D,                  // 5
            seg_C | seg_B | seg_F | seg_A | seg_E | seg_D,          // 6
            seg_C | seg_G | seg_E,                                  // 7
            seg_A | seg_B | seg_C | seg_D | seg_E | seg_F | seg_G,  // 8
            seg_B | seg_C | seg_D | seg_E | seg_F | seg_G,          // 9
            seg_B | seg_A | seg_D,                                  // L
        };

        //////////////////////////////////////////////////////////////////////
        // scaling values

        scaler[] scales = new scaler[]
        {
            new scaler { bit = segment.nano, scale = 0.000000001 },
            new scaler { bit = segment.micro, scale = 0.000001 },
            new scaler { bit = segment.milli, scale = 0.001 },
            new scaler { bit = segment.kilo, scale = 1000.0 },
            new scaler { bit = segment.meg, scale = 1000000.0 }
        };

        //////////////////////////////////////////////////////////////////////
        // feed to the right channel based on which anunciators are active

        channel_bit[] channel_bits = new channel_bit[]
        {
            new channel_bit { bits = segment.ac | segment.volts, c = Locomo.Channel.VoltsAC },
            new channel_bit { bits = segment.dc | segment.volts, c = Locomo.Channel.VoltsDC },
            new channel_bit { bits = segment.ac | segment.amps, c = Locomo.Channel.AmpsAC },
            new channel_bit { bits = segment.dc | segment.amps, c = Locomo.Channel.AmpsDC },
            new channel_bit { bits = segment.ohms, c = Locomo.Channel.Ohms },
            new channel_bit { bits = segment.celsius, c = Locomo.Channel.Degrees },
            new channel_bit { bits = segment.celsius, c = Locomo.Channel.Degrees },
            new channel_bit { bits = segment.farads, c = Locomo.Channel.Farads },
            new channel_bit { bits = segment.hertz, c = Locomo.Channel.Hertz },
            new channel_bit { bits = segment.percent, c = Locomo.Channel.Percent }
        };

        //////////////////////////////////////////////////////////////////////

        protected override bit[] Bits
        {
            get
            {
                return bits;
            }
        }

        protected override int[] DigitMasks
        {
            get
            {
                return digits;
            }
        }

        protected override channel_bit[] ChannelBits
        {
            get
            {
                return channel_bits;
            }
        }

        //////////////////////////////////////////////////////////////////////

        public Device_DT4000ZC(System.Windows.Forms.TreeNode node) : base(node)
        {
            listener = new MyListener(this);
        }

        //////////////////////////////////////////////////////////////////////

        public override bool Controllable
        {
            get
            {
                return false;
            }
        }

        //////////////////////////////////////////////////////////////////////

        public override SerialPortConfig DefaultConfig
        {
            get
            {   // DT400ZC: BaudRate = 2400, 8 data bits, no parity, 1 stop bit, no flow control
                SerialPortConfig c = new SerialPortConfig();
                c.BaudRate = 2400;
                c.DataBits = 8;
                c.StopBits = StopBits.One;
                c.Parity = Parity.None;
                c.FlowControl = Handshake.None;
                return c;
            }
        }

        //////////////////////////////////////////////////////////////////////

        public override SerialSettingsMask SettingsMask
        {
            get
            {
                return SerialSettingsMask.BAUDRATE |
                        SerialSettingsMask.DATABITS |
                        SerialSettingsMask.STOPBITS |
                        SerialSettingsMask.PARITY |
                        SerialSettingsMask.FLOWCONTROL;
            }
        }

        //////////////////////////////////////////////////////////////////////

        protected override double get_number(byte[] buffer)
        {
            double value = 0;
            for (int i = 1; i < 9; i += 2)
            {
                value *= 10.0;
                int v = ((buffer[i] & 0x7) << 4) + (buffer[i + 1] & 0xf);
                int d = get_digit_from_byte(v);
                if (d == 10)
                {
                    return -1;  // overload
                }
                value += d;
            }
            return value;
        }

        // one packet is 14 bytes
        // top nibble of each byte is the index of the byte in the array
        // so, check for the 0th one and reset mask of received bytes when we see it
        // then set bit in mask for each byte that arrives
        // when we see the mask is full (0x1fff), we have a whole packet, so decode/send it

        int packet_mask = 0;

        public override void ReadData()
        {
            try
            {
                int b = Connection.ReadByte();
                int index = ((b >> 4) & 0xf) - 1;
                if(index == 0)
                {
                    packet_mask = 0;
                }
                if(index >= 0 && index <= 13)
                {
                    packet_mask |= 1 << index;
                    buffer[index] = (byte)b;
                }
                if(packet_mask == 0x3fff)
                {
                    invoke_on_data(buffer);

                    foreach(bit bit in bits)
                    {
                        int mask = 1 << bit.bit_number;
                        if((buffer[bit.offset] & mask) != 0)
                        {
                            segments |= (ulong)bit.id;
                        }
                    }

                    packet_mask = 0;
                    double value = get_value(buffer);
                    Channel channel = get_channel();

                    var d = GetStream(channel);

                    if (d != null)
                    {
                        if (channel != last_channel)
                        {
                            d.ResetPacketCount();
                            last_channel = channel;
                        }
                        d.Put(new Result<double>(value));
                        //ConnectionState = State.Connected;
                    }

                }
            }
            catch (Error e)
            {
                Log.Error(e.Message.Replace("\r", ""));
                if (Connection.State == ConnectionState.Disconnected)
                {
                    listener.Stop();
                }
            }
            catch (TimeoutException)
            {
                Log.Write(Log.Severity.Error, "Timeout", $"{Name} timed out on {(Connection.Name != null ? Connection.Name : "NULL Connection?")}");

                // if you like, you can disconnect here...
                // TODO (chs): setting to auto-disconnect on serial port timeout? (global, and per device override?)
                // Disconnect();
            }
        }
    }
}
