﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DMM.LCD;

namespace Locomo.Devices.DMM
{
    public abstract class DMM_LCDMapped : DMM_Serial
    {
        //////////////////////////////////////////////////////////////////////
        // scaling values

        protected struct scaler
        {
            public segment bit;
            public double scale;
        };

        //////////////////////////////////////////////////////////////////////
        // feed to the right channel based on which anunciators are active

        protected struct channel_bit
        {
            public segment bits;
            public Locomo.Channel c;
        };

        //////////////////////////////////////////////////////////////////////
        // a bit in the buffer with an id

        protected struct bit
        {
            public int offset;
            public int bit_number;
            public segment id;
        };

        //////////////////////////////////////////////////////////////////////

        protected abstract double get_number(byte[] buffer);
        protected ulong segments;

        //////////////////////////////////////////////////////////////////////

        protected abstract int[] DigitMasks
        {
            get;
        }

        //////////////////////////////////////////////////////////////////////

        protected virtual channel_bit[] ChannelBits
        {
            get;
        }

        protected abstract bit[] Bits
        {
            get;
        }

        //////////////////////////////////////////////////////////////////////

        static scaler[] scales = new scaler[]
        {
            new scaler { bit = segment.nano, scale = 0.000000001 },
            new scaler { bit = segment.micro, scale = 0.000001 },
            new scaler { bit = segment.milli, scale = 0.001 },
            new scaler { bit = segment.kilo, scale = 1000.0 },
            new scaler { bit = segment.meg, scale = 1000000.0 }
        };

        //////////////////////////////////////////////////////////////////////

        public DMM_LCDMapped(System.Windows.Forms.TreeNode node) : base(node)
        {
        }

        //////////////////////////////////////////////////////////////////////

        protected double get_value(byte[] buffer)
        {
            double value = get_number(buffer);

            if (get_bit(segment.minus))
            {
                value = -value;
            }
            if (get_bit(segment.decimal_point1))
            {
                value /= 1000.0;
            }
            else if (get_bit(segment.decimal_point2))
            {
                value /= 100.0;
            }
            else if (get_bit(segment.decimal_point3))
            {
                value /= 10.0;
            }
            foreach (scaler s in scales)
            {
                if (get_bit(s.bit))
                {
                    value *= s.scale;
                    break;
                }
            }
            return value;
        }

        //////////////////////////////////////////////////////////////////////
        // get which channel the value should be fed into

        public Locomo.Channel get_channel()
        {
            foreach (channel_bit b in ChannelBits)
            {
                if (((segments & (ulong)b.bits)) == (ulong)b.bits)
                {
                    return b.c;
                }
            }
            return Locomo.Channel.None;
        }

        //////////////////////////////////////////////////////////////////////
        // is the bit with a certin ID set?

        bool get_bit(segment index)
        {
            return (segments & (ulong)index) != 0;
        }

        //////////////////////////////////////////////////////////////////////
        // lookup a 7 segment digit

        protected int get_digit_from_byte(int mask)
        {
            int n = 0;
            foreach (int d in DigitMasks)
            {
                if (d == mask)
                {
                    return n;
                }
                n += 1;
            }
            return 0;
        }

        //////////////////////////////////////////////////////////////////////

        protected void update_value(byte[] buffer)
        {
            double value = get_value(buffer);
            Channel channel = get_channel();
            var d = GetStream(channel);
            if (d != null)
            {
                if (channel != last_channel)
                {
                    d.ResetPacketCount();
                    last_channel = channel;
                }
                d.Put(new Result<double>(value));
                //ConnectionState = State.Connected;
            }
        }
    }
}
