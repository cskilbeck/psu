﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Locomo.Devices.DMM
{
    public delegate void on_data_delegate(byte[] data);

    public interface IDMM
    {
        event on_data_delegate on_data;
    }
}
