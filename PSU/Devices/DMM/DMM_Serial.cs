﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Locomo.Devices.DMM
{
    public abstract class DMM_Serial : SerialDevice
    {
        protected MyListener listener;
        protected Channel last_channel = Channel.None;

        public abstract void ReadData();

        public event on_data_delegate on_data;

        //////////////////////////////////////////////////////////////////////

        public class MyListener : Listener
        {
            DMM_Serial device;

            public MyListener(DMM_Serial device) : base()
            {
                this.device = device;
            }

            public override void Read()
            {
                device.ReadData();
            }
        }

        //////////////////////////////////////////////////////////////////////

        public DMM_Serial(System.Windows.Forms.TreeNode node) : base(node)
        {
            listener = new MyListener(this);
        }

        //////////////////////////////////////////////////////////////////////

        public static System.Windows.Forms.Control GetConfigControl()
        {
            return new Ctrls.SerialPortConfigControl();
        }

        //////////////////////////////////////////////////////////////////////

        public override bool Controllable
        {
            get
            {
                return false;
            }
        }

        //////////////////////////////////////////////////////////////////////

        public override bool Connect()
        {
            bool rc = base.Connect();
            if (rc)
            {
                StartMonitor();
            }
            return rc;
        }

        //////////////////////////////////////////////////////////////////////

        public override void Disconnect()
        {
            StopMonitor();
            base.Disconnect();
        }

        //////////////////////////////////////////////////////////////////////

        public override void StartMonitor()
        {
            if (IsConnected)
            {
                ((SerialConnection)Connection).Port.ReadTimeOut = 1000;
                listener.Start();
            }
        }

        //////////////////////////////////////////////////////////////////////

        public override void StopMonitor()
        {
            listener.Stop();
        }

        //////////////////////////////////////////////////////////////////////

        protected void invoke_on_data(byte[] buffer)
        {
            on_data?.Invoke(buffer);
        }

        protected bool on_data_is_null()
        {
            return on_data == null;
        }
    }
}
