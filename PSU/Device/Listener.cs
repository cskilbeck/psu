﻿using System.Threading;

namespace Locomo
{
    public abstract class Listener
    {
        private Thread thread;
        private CancellationTokenSource killer;

        public Listener()
        {
        }

        public abstract void Read();

        public void Start()
        {
            if (thread == null || !thread.IsAlive)
            {
                thread = new Thread(() =>
                {
                    while (!killer.IsCancellationRequested)
                    {
                        Read();
                    }
                });

                killer = new CancellationTokenSource();
                thread.Start();
            }
        }

        public void Stop()
        {
            if (thread != null && thread.IsAlive)
            {
                killer.Cancel(true);
                if (!thread.Join(1100))     // @note must be > port timeout value!
                {
                    thread?.Abort();
                }
                thread = null;
            }
        }
    }
}
