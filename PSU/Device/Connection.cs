﻿using System;
using System.Diagnostics;
using System.Windows.Forms;
using System.Xml;

namespace Locomo
{
    public enum ConnectionState
    {
        Initializing,
        Disconnected,
        Connecting,
        Connected
    }

    public abstract class Connection: IEquatable<Connection>
    {
        public delegate void ConnectionStateChangeDelegate(object sender);

        public event ConnectionStateChangeDelegate ConnectionStateChange;

        private ConnectionState state;
        private Timer auto_connect_timer;
        private bool auto_connect;

        public Device Device;

        public Connection(Device device)
        {
            Device = device;
            State = ConnectionState.Initializing;
            AutoConnect = false;
        }

        public bool AutoConnect
        {
            get
            {
                return auto_connect;
            }
            set
            {
                auto_connect = value;
                if (auto_connect)
                {
                    if(auto_connect_timer == null)
                    {
                        auto_connect_timer = new Timer();
                        auto_connect_timer.Interval = 1000;
                        auto_connect_timer.Tick += (s, e) =>
                        {
                            if (State != ConnectionState.Connected && Device != null)
                            {
                                Device.Connect();
                            }
                        };
                        auto_connect_timer.Start();
                    }
                }
                else if(auto_connect_timer != null)
                {
                    auto_connect_timer.Stop();
                    auto_connect_timer.Dispose();
                    auto_connect_timer = null;
                }
            }
        }

        private void T_Tick(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        public ConnectionState State
        {
            get
            {
                return state;
            }
            set
            {
                if (state != value)
                {
                    state = value;
                    OnConnectionStateChange();
                }
            }
        }

        public bool CompareTo(Connection b)
        {
            return ReferenceEquals(GetType(), b.GetType()) && b.Equals(this);
        }

        protected virtual void OnConnectionStateChange()
        {
            var csc = ConnectionStateChange;
            if (csc != null)
            {
                foreach (Delegate d in csc.GetInvocationList())
                {
                    d.DynamicInvoke(this);
                }
            }
        }

        public abstract bool Connect();
        public abstract void Disconnect();

        public abstract bool IsConnected { get; }
        public abstract string Name { get; }

        public virtual void Load(XmlElement e)
        {
            AutoConnect = e.Load("AutoConnect", false);
        }

        public virtual void Save(XmlElement e)
        {
            e.Save("AutoConnect", AutoConnect);
        }

        public abstract bool WriteLine(string s);
        public abstract void WriteBytes(byte[] data, int offset, int length);

        public abstract string ReadLine();
        public abstract int ReadBytes(byte[] buffer, int num);
        public abstract int ReadByte();

        public abstract bool Equals(Connection other);
    }
}
