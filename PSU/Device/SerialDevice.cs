﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.IO.Ports;
using System.Threading;
using System.Xml;
using Locomo.Ctrls;

namespace Locomo
{
    public abstract class SerialDevice : Device
    {
        [Flags]
        public enum SerialSettingsMask
        {
            NONEFIXED = 0,
            BAUDRATE = 1,
            DATABITS = 2,
            PARITY = 4,
            STARTBITS = 8,
            STOPBITS = 16,
            FLOWCONTROL = 32,
            ALL = 63
        }

        public SerialDevice(System.Windows.Forms.TreeNode node) : base(node)
        {
        }

        protected SerialConnection MySerialConnection
        {
            get
            {
                return Connection as SerialConnection;
            }
        }

        public override void Setup(System.Windows.Forms.Control o)
        {
            var spcc = o as SerialPortConfigControl;
            if(spcc != null)
            {
                Connection = new SerialConnection(this, spcc.GetConfig());
                Connection.ConnectionStateChange -= Connection_ConnectionStateChange;
                Connection.ConnectionStateChange += Connection_ConnectionStateChange;
            }
        }

        private void Connection_ConnectionStateChange(object sender)
        {
            if (Connection.State == ConnectionState.Connected)
            {
                ResetAllStreams();
            }
        }

        public SerialPortConfig PortConfig
        {
            get
            {
                return MySerialConnection.Config;
            }
        }


        public string PortName
        {
            get
            {
                return MySerialConnection.Port?.Name;
            }
        }

        public override string DisplayName
        {
            get
            {
                return Name + ": " + PortName;
            }
        }

        public override void Save(XmlElement e)
        {
            e.Save("Connected", IsConnected);
            Connection.Save(e);
        }

        public override void SaveID(XmlElement e)
        {
//            e.Save("Port", MySerialConnection.Port.Name);
            base.SaveID(e);
        }

        public override void Load(XmlElement e)
        {
            Connection = new SerialConnection(this, XmlExtensions.Deserialize<SerialPortConfig>(e) ?? DefaultConfig);
            Connection.ConnectionStateChange += Connection_ConnectionStateChange;
            Connection.Load(e);
            if (e.Load<bool>("Connected"))
            {
                if (Connect())
                {
                    StartMonitor();
                }
            }
            else
            {
                StopMonitor();
                Disconnect();
            }
        }

        public override void InitConfigControl(System.Windows.Forms.Control c)
        {
            SerialPortConfigControl s = (SerialPortConfigControl)c;
            s?.SetConfig(DefaultConfig, SettingsMask);
        }

        public static SerialDevice FindConnectedDevice(Type type, string port_name)
        {
            foreach (SerialDevice d in Devices)
            {
                if (d.GetType() == type && d.PortName == port_name && d.Connection.State != ConnectionState.Disconnected)
                {
                    return d;
                }
            }
            return null;
        }

        // get which serial port settings are fixed for this device
        public virtual SerialSettingsMask SettingsMask
        {
            get
            {
                return SerialSettingsMask.NONEFIXED;   // nothing fixed
            }
        }

        // get the default serial port settings for this device
        public virtual SerialPortConfig DefaultConfig
        {
            get
            {
                return new SerialPortConfig();
            }
        }

        public override bool Connect()
        {
            return Connection.Connect();
        }

        public override void Disconnect()
        {
            ResetAllStreams();
            Connection?.Disconnect();
            OnClosed();
        }

        protected bool WriteLine(string s)
        {
            return Connection.WriteLine(s);
        }

        protected string ReadLine()
        {
            return Connection.ReadLine();
        }

        protected int ReadBytes(byte[] buffer, int num)
        {
            return Connection.ReadBytes(buffer, num);
        }
    }
}
