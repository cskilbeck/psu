﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using static Locomo.Extensions;

namespace Locomo
{
    public class DeviceAttribute : Attribute
    {
        public string kind;
        public string manufacturer;
        public string name;
        public Type config_control_type;
        public Channel[] channels;

        public DeviceAttribute(string kind, string manufacturer, string name, Type config_control_type, Channel[] channels)
        {
            this.kind = kind;
            this.manufacturer = manufacturer;
            this.name = name;
            this.config_control_type = config_control_type;
            this.channels = channels as Channel[];
        }
    }

    public abstract class Device
    {
        static Dictionary<int, Device> DevicesByID = new Dictionary<int, Device>();

        public static List<Device> Devices = new List<Device>();

        // Type -> DeviceAttribute
        public static Dictionary<Type, DeviceAttribute> DeviceAttributes = new Dictionary<Type, DeviceAttribute>();

        // All the types, kinds and manufacturers
        public static List<Type> DeviceTypes = new List<Type>();
        private static SortedSet<string> DeviceKinds = new SortedSet<string>();
        private static SortedSet<string> DeviceManufacturers = new SortedSet<string>();

        public delegate void DeviceClosed();
        public delegate void Completed<T>(Result<T> o);

        public event DeviceClosed Closed;

        public List<Stream> Streams;
        public Connection Connection;
        public int ConnectionID;
        public TreeNode tree_node;

        public abstract bool Controllable { get; }
        public abstract void Load(XmlElement e);
        public abstract void Save(XmlElement e);
        public abstract void InitConfigControl(Control c);
        public abstract void Disconnect();
        public abstract bool Connect();
        public abstract void Setup(Control o); // set up connection from the config control

        public Device(TreeNode node)
        {
            tree_node = node;
            Devices.Add(this);
            ConnectionID = GetAConnectionID();
            Streams = new List<Stream>();
            foreach (Channel channel in Channels)
            {
                Streams.Add(Stream.Create(channel, this, new TimeSpan(0, 0, 1), 100000));
            }
        }

        public virtual Channel[] Channels
        {
            get
            {
                return GetChannels(GetType());
            }
        }

        public Stream GetStream(Channel channel)
        {
            foreach(Stream s in Streams)
            {
                if(s.Channel == channel)
                {
                    return s;
                }
            }
            return null;
        }

        public void Delete()
        {
            StopMonitor();
            Disconnect();
            Devices.Remove(this);
        }

        public static string GetDeviceName(Type t)
        {
            return DeviceAttributes.Get(t, null)?.name;
        }

        public static string GetDeviceKind(Type t)
        {
            return DeviceAttributes.Get(t, null)?.kind;
        }

        public string Kind
        {
            get
            {
                return GetDeviceKind(GetType());
            }
        }

        public bool IsConnected
        {
            get
            {
                return Connection == null || Connection.State == ConnectionState.Connected || Connection.State == ConnectionState.Connecting;
            }
        }

        public static DeviceAttribute GetDeviceAttribute(Type t)
        {
            return DeviceAttributes.Get(t, null);
        }

        public static SortedSet<string> AllKinds
        {
            get
            {
                return DeviceKinds;
            }
        }

        public static SortedSet<string> AllManufacturers
        {
            get
            {
                return DeviceManufacturers;
            }
        }

        public static Control GetConfigControl(Type t)
        {
            DeviceAttribute a;
            if(DeviceAttributes.TryGetValue(t, out a))
            {
                var ctor = a.config_control_type.GetConstructor(Type.EmptyTypes);
                return (Control)ctor?.Invoke(null);
            }
            return null;
        }

        public static string GetDeviceManufacturer(Type t)
        {
            return DeviceAttributes.Get(t, null)?.manufacturer;
        }

        // @yuck remove this duplicate
        public static Channel[] GetChannels(Type t)
        {
            Channel[] rc = null;
            DeviceAttribute a;
            if(DeviceAttributes.TryGetValue(t, out a))
            {
                rc = a.channels;
            }
            return rc;
        }

        public static void RegisterDevice(Type type, DeviceAttribute attr)
        {
            Type config_control_type = attr.config_control_type;
            DeviceAttributes.Add(type, attr);
            AllKinds.Add(attr.kind);
            AllManufacturers.Add(attr.manufacturer);
            DeviceTypes.Add(type);
        }

        public static int GetAConnectionID()
        {
            return Devices.Max(s => s.ConnectionID) + 1;
        }

        public static Device FindDeviceByConnectionID(int id)
        {
            return Devices.FirstOrDefault(d => d.ConnectionID == id);
        }

        public bool SetConnectionID(int id)
        {
            Device d = FindDeviceByConnectionID(id);
            if (d == null || d == this)
            {
                ConnectionID = id;
                return true;
            }
            return false;
        }

        public virtual void SaveID(XmlElement e)
        {
            e.Save("Type", GetType().ToString());
            Connection.Save(e);
        }

        // create a device by type
        public static Device CreateDevice(Type type, int id, TreeNode node)
        {
            Device d = null;
            if (type.IsSubclassOf(typeof(Device)))
            {
                var c = type.GetConstructor(new Type[] { typeof(TreeNode)});
                d = (c != null) ? (Device)c.Invoke(new object[] { node }) : null;
                if(d != null && id != -1)
                {
                    if (!d.SetConnectionID(id))
                    {
                        Debugger.Break();
                    }
                }
               
            }
            return d;
        }

        public static void RegisterClasses()
        {
            foreach(Pair<Type, DeviceAttribute> type in FindTypesWithAttribute<DeviceAttribute>())
            {
                try
                {
                    RegisterDevice(type.First, type.Second);
                }
                catch(Exception e)
                {
                    MessageBox.Show("Error registering device " + type.First + "\n\n" + e.Message);
                }
            }
        }

        public static void SaveSettings(XmlElement settings)
        {
            XmlElement e = settings.Elem("Connections");
            foreach (Device device in Devices)
            {
                XmlElement connection_element = e.Elem("Connection");
                connection_element.Save("ID", device.ConnectionID);
                XmlElement elem = connection_element.Elem("Device");
                elem.Save("Type", device.GetType().ToString());
                device.Save(elem);
                connection_element.AppendChild(elem);
                e.AppendChild(connection_element);
                foreach(Stream s in device.Streams)
                {
                    s.Save(elem);
                }
            }
            settings.AppendChild(e);
        }

        public static void LoadSettings(XmlElement settings)
        {
            XmlElement connections = (XmlElement)settings.SelectSingleNode("Connections");
            if (connections != null)
            {
                foreach (XmlElement connection_element in connections.SelectNodes("Connection"))
                {
                    var id = connection_element.Load("ID", -1);
                    var elem = connection_element.SelectSingleNode("Device") as XmlElement;
                    Type device_type = null;
                    if(elem.LoadType("Type", ref device_type))
                    {
                        Device device = CreateDevice(device_type, id, null);
                        if (device != null)
                        {
                            device.Load(elem);
                            foreach (XmlElement stream_element in elem.SelectNodes("Stream"))
                            {
                                var channel = stream_element.Load("Channel", Channel.None);
                                var stream = device.GetStream(channel);
                                if (stream != null)
                                {
                                    stream.Load(stream_element);
                                }
                            }
                        }
                    }
                }
            }
        }

        public void ResetAllStreams()
        {
            foreach(Stream d in Streams)
            {
                d.ResetPacketCount();
            }
        }

        public static string FullName(Type t)
        {
            return GetDeviceManufacturer(t) + " " + GetDeviceName(t);
        }

        public virtual string Name
        {
            get
            {
                return FullName(GetType());
            }
        }

        public virtual string DisplayName
        {
            get
            {
                return Name;
            }
        }

        public virtual void OnClosed()
        {
            Closed?.Invoke();
        }

        public virtual void StartMonitor()
        {
        }

        public virtual void StopMonitor()
        {
//            Debugger.Break();
        }

        public static void DisconnectAll()
        {
            foreach (Device d in Devices)
            {
                d.Disconnect();
            }
        }

        public string ConnectionStateString
        {
            get
            {
                if(Connection != null)
                {
                    return Connection.State.ToString();
                }
                return "Connected";
            }
        }

        public void AddCallback(Channel stream_type, Stream.DataReadyDelegate callback)
        {
            var stream = GetStream(stream_type);
            stream.DataReady += callback;
        }

        public static Device FindConnectedDevice(Type t, Connection c)
        {
            foreach(Device d in Devices)
            {
                if(ReferenceEquals(d.GetType(), t) && d.Connection != null && d.Connection.CompareTo(c))
                {
                    return d;
                }
            }
            return null;
        }
    }
}
