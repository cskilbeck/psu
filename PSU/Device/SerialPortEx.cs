﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Logging;

namespace Locomo
{
    public abstract class SerialPortEx
    {
        public abstract void Open();
        public abstract void Close();
        public abstract string ReadLine();
        public abstract int Read(byte[] buffer, int offset, int num);
        public abstract int ReadByte();
        public abstract void WriteLine(string text);
        public abstract bool IsOpen { get; }
        public abstract string Name { get; }
        public abstract int ReadTimeOut { set; }
        public abstract int WriteTimeOut { set; }

        public static SerialPortEx CreatePort(SerialPortConfig config)
        {
            if (config == null)
            {
                return null;
            }
            if (config.Port == "DUMMY")
            {
                return new DummySerialPort();
            }
            else
            {
                SerialPort sport = new SerialPort(config.Port, config.BaudRate, config.Parity, config.DataBits, config.StopBits);
                sport.Handshake = config.FlowControl;
                sport.ReadTimeout = 500;
                sport.WriteTimeout = 500;
                return new NormalSerialPort(sport);
            }
        }
    }

    public class NormalSerialPort : SerialPortEx
    {
        public SerialPort Port;

        public override string Name
        {
            get
            {
                return Port.PortName;
            }
        }

        public NormalSerialPort(SerialPort port)
        {
            Port = port;
        }

        public override void Open()
        {
            try
            {
                Port.Open();
            }
            catch (ArgumentException e)
            {
                Log.Error("PSU::PSU() -> ArgumentException " + e.Message);
            }
            catch (UnauthorizedAccessException e)
            {
                Log.Error("PSU::PSU() -> UnauthorizedAccessException " + e.Message);
            }
            catch (InvalidOperationException e)
            {
                Log.Error("PSU::PSU() -> InvalidOperationException " + e.Message);
            }
            catch (IOException e)
            {
                // huh?
                Log.Error("PSU::PSU() -> IOException " + e.Message);
            }
        }

        public override void Close()
        {
            Port.Close();
        }

        public override string ReadLine()
        {
            try
            {
                return IsOpen ? Port.ReadLine() : string.Empty;
            }
            catch (UnauthorizedAccessException e)
            {
                Log.Error("Port.Readline: " + e.Message);
            }
            return string.Empty;
        }

        public override void WriteLine(string text)
        {
            if (IsOpen)
            {
                Port.WriteLine(text);
            }
        }

        public override int Read(byte[] buffer, int offset, int num)
        {
            return IsOpen ? Port.Read(buffer, offset, num) : 0;
        }

        public override int ReadByte()
        {
            if (!IsOpen)
            {
                throw new InvalidOperationException("Port is not open");
            }
            return Port.ReadByte();
        }

        public override bool IsOpen
        {
            get
            {
                return Port.IsOpen;
            }
        }

        public override int ReadTimeOut
        {
            set
            {
                Port.ReadTimeout = value;
            }
        }

        public override int WriteTimeOut
        {
            set
            {
                Port.WriteTimeout = value;
            }
        }
    }

    public class DummySerialPort : SerialPortEx
    {
        class Command
        {
            public delegate void Execute(string[] cmd);
            public string prefix;
            public Execute OnExecute;

            public Command(string prefix, Execute execute)
            {
                this.prefix = prefix;
                this.OnExecute = execute;
            }
        }

        Command[] commands;
        float volts;
        float amps;
        int on;
        CircularList<string> output;
        List<string> curLine = new List<string>();

        const int SerialDelay = 1;

        public override string Name
        {
            get
            {
                return "DUMMY";
            }
        }

        public void ResetOutput()
        {
            curLine.Clear();
        }

        public void Output(string s)
        {
            curLine.Add(s);
        }

        public void FlushOutput()
        {
            if (curLine.Count > 0)
            {
                lock (output)
                {
                    output.Put(string.Join(";", curLine) + "\n");
                    ResetOutput();
                }
            }
        }

        public string GetOutput()
        {
            lock (output)
            {
                return output.IsEmpty ? string.Empty : output.Get();
            }
        }

        public override bool IsOpen
        {
            get
            {
                return isOpen;
            }
        }

        public override int ReadTimeOut
        {
            set
            {
            }
        }

        public override int WriteTimeOut
        {
            set
            {
            }
        }

        public DummySerialPort()
        {
            output = new CircularList<string>(10);
            ResetOutput();
            commands = new Command[] {
                new Command("volt", cmd => {
                    volts = float.Parse(cmd[1]);
                }),
                new Command("volt?", cmd => {
                    Output(volts.ToString());
                }),
                new Command("meas:volt?", cmd => {
                    Output(volts.ToString());
                }),
                new Command("curr", cmd => {
                    amps = float.Parse(cmd[1]);
                }),
                new Command("curr?", cmd => {
                    Output(amps.ToString());
                }),
                new Command("outp", cmd => {
                    on = int.Parse(cmd[1]);
                }),
                new Command("outp?", cmd => {
                    Output(on.ToString());
                }),
                new Command("meas:volt?", cmd => {
                    Output(volts.ToString());
                }),
                new Command("meas:curr?", cmd => {
                    Output(amps.ToString());
                }),
                new Command("stat:oper:cond?", cmd => {
                    Output(0.ToString());
                }),
                new Command("*idn?", cmd => {
                    Output("DUMMY PSU");
                }),
                new Command("*cls", cmd => {
                })
            };
            isOpen = true;
        }

        bool isOpen;

        public override void Open()
        {
            isOpen = true;
        }

        public override void Close()
        {
            isOpen = false;
        }

        public override void WriteLine(string s)
        {
            if (isOpen)
            {
                Thread.Sleep(SerialDelay * s.Length);  // serial delay
                string[] cmds = s.Split(';');
                lock (curLine)
                {
                    ResetOutput();
                    foreach (var command in cmds)
                    {
                        string[] parts = command.Split(' ');
                        foreach (var c in commands)
                        {
                            if (c.prefix.CompareTo(parts[0]) == 0)
                            {
                                c.OnExecute(parts);
                                break;
                            }
                        }
                    }
                    FlushOutput();
                }
            }
        }

        public override string ReadLine()
        {
            string s = string.Empty;
            if (isOpen)
            {
                s = GetOutput();
                Thread.Sleep(SerialDelay * s.Length);
            }
            return s;
        }

        public override int ReadByte()
        {
            if(!IsOpen)
            {
                throw new InvalidOperationException("DUMMY port is not open");
            }
            Thread.Sleep(SerialDelay * 1);
            return 0;
        }

        public override int Read(byte[] buffer, int offset, int num)
        {
            int got = 0;
            if (isOpen)
            {
                Thread.Sleep(SerialDelay * num);
                buffer = new byte[num + offset];
                for (int i = 0; i < num; ++i)
                {
                    buffer[offset + i] = 0;
                }
                got = num;
            }
            return got;
        }
    }
}
