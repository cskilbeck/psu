﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Locomo;

namespace Locomo.Virtual
{
    public class VirtualChannel
    {
        public enum Combiner
        {
            Add,
            Subtract,
            Multiply,
            Divide
        }

        public class Source
        {
            public Device device;
            public Channel channel;
        }

        public string name;
        public Source[] sources;
        public Stream[] streams;
        Combiner combiner;

        // ctor, tostring, start(attach), stop
    }
}
