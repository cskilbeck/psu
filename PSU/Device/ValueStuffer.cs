﻿namespace Locomo
{
    // for sending values to a device without clogging up the commandqueue but always sending the last one
    public class ValueStuffer
    {
        public bool sending;
        public bool got_final;
        public double final_value;
        public string value_send_command;

        public delegate void StuffingDelegate(bool is_final);
        public event StuffingDelegate Stuffing;
        public virtual void OnStuffing(bool is_final)
        {
            Stuffing?.Invoke(is_final);
        }

        public void StuffIt(double v, bool isFinalValueMouseUp, SCPIDevice psu)
        {
            // disable/enable monitor when mouse is dragging the value
            if (psu != null)
            {
                OnStuffing(isFinalValueMouseUp);
            }

            // if we're sending a value at the moment
            if (sending)
            {
                // stash this one for sending when the queue is empty
                got_final = true;
                final_value = v;
            }

            // if not busy || mouse released, send it
            if (isFinalValueMouseUp || !sending)
            {
                // now we're busy
                sending = true;

                if (psu != null)
                {
                    // start the send
                    psu.PutString(string.Format("{0} {1}\n", value_send_command, v), result =>
                    {
                        // send is complete
                        sending = false;

                        // was there a dangling one?
                        if (got_final)
                        {
                            // ok, send that (but don't say we're sending, worst case we queue up 2, no biggie)
                            psu?.PutString(string.Format("{0} {1}\n", value_send_command, final_value));

                            // final value has been sent, what happens if another comes in now? it's ok because Sending = false
                            got_final = false;
                        }
                    });
                }
            }
        }
    }
}
