﻿using System;

namespace DMM.LCD
{
    [Flags]
    public enum segment : ulong
    {
        none = 0,
        ac = 1 << 0,
        dc = 1 << 1,
        auto = 1 << 2,
        rs232 = 1 << 3,
        minus = 1 << 4,
        decimal_point1 = 1 << 5,
        decimal_point2 = 1 << 6,
        decimal_point3 = 1 << 7,
        micro = 1 << 8,
        nano = 1 << 9,
        kilo = 1 << 10,
        diode = 1 << 11,
        milli = 1 << 12,
        percent = 1 << 13,
        meg = 1 << 14,
        beep = 1 << 15,
        farads = 1 << 16,
        ohms = 1 << 17,
        relative = 1 << 18,
        hold = 1 << 19,
        amps = 1 << 20,
        volts = 1 << 21,
        hertz = 1 << 22,
        battery = 1 << 23,
        celsius = 1 << 24,
        usb = 1 << 25,
        max = 1 << 26,
        minmax = 1 << 27,
        min = 1 << 28,
        hFE = 1 << 29,
        fahrenheit = 1 << 30
    };
}
