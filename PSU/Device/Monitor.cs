﻿using System;
using System.Diagnostics;
using System.Threading;
using System.Windows.Forms;
using Logging;

namespace Locomo
{
    public class Monitor
    {
        private Thread thread;
        private CancellationTokenSource killer;
        private ItemBase[] items;
        private int currentItem;
        private ManualResetEvent waiter;
        private SCPIDevice device;

        public abstract class ItemBase
        {
            public string Command;
            public abstract void Execute(SCPIDevice psu, Monitor monitor);
        }

        public class Item<T> : ItemBase where T : IConvertible
        {
            public Action<Result<T>> Update;

            public Item(string cmd, Action<ResultBase> func)
            {
                Command = cmd;
                Update = func;
            }

            public Item(string cmd, Stream stream)
            {
                Command = cmd;
                Update = result =>
                {
                    stream.Put(result);
                };
            }

            public virtual void RunUpdate(Result<T> result)
            {
                // update the client asynchronously so we can get on with requesting the next reading
                Update.Invoke(result);
            }

            public override void Execute(SCPIDevice psu, Monitor monitor)
            {
                psu.Get<T>(Command, result =>
                {
                    // there's a little race here I think, hence the try/catch just in case
                    if (!monitor.Killer.IsCancellationRequested)
                    {
                        try
                        {
                            RunUpdate(result);
                        }
                        catch (ObjectDisposedException e1)
                        {
                            Log.Info("COMMAND was " + Command + "ObjectDisposedException said " + e1.Message);
                        }
                        catch (InvalidOperationException e2)
                        {
                            Log.Info("COMMAND was " + Command + "InvalidOperationException said " + e2.Message);
                        }
                    }
                    // it's done, kick off the next one
                    monitor.Waiter.Set();
                });
            }
        }

        public Monitor(ItemBase[] monitorItems, SCPIDevice device)
        {
            waiter = new ManualResetEvent(false);
            items = monitorItems;
            killer = new CancellationTokenSource();
            this.device = device;
            Start();
        }

        public void Start()
        {
            if (thread == null)
            {
                thread = new Thread(new ThreadStart(() =>
                {
                    try
                    {
                        while (!killer.IsCancellationRequested && device.Connection.State != ConnectionState.Disconnected)
                        {
                            ItemBase m;
                            lock (items)
                            {
                                m = items[currentItem];
                                currentItem = ++currentItem % items.Length;
                            }
                            m.Execute(device, this);
                            waiter.WaitOne();
                            waiter.Reset();
                        }
                    }
                    catch (ThreadAbortException)
                    {
                        Log.Warning("Monitor thread was aborted");
                    }
                    finally
                    {
                        thread = null;
                    }
                    Log.Info("Monitor thread for " + device.Name + " is exiting");
                }))
                { Name = "Monitor" };
                thread.Start();
            }
            else
            {
                lock (items)
                {
                    currentItem = 0;
                    waiter.Set();
                }
            }
        }

        public void Stop()
        {
            if (thread != null)
            {
                Log.Info("Stopping Monitor");
                killer.Cancel();
                waiter.Set();
                if (!thread.Join(500))
                {
                    Log.Warning("Aborting Monitor");
                    thread.Abort();
                }
                Log.Info("Stopped Monitor");
            }
        }

        public CancellationTokenSource Killer
        {
            get
            {
                return killer;
            }
        }

        public ManualResetEvent Waiter
        {
            get
            {
                return waiter;
            }
        }
    }
}
