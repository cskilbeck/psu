﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Xml;
using static Locomo.Extensions;
using static Locomo.XmlExtensions;

namespace Locomo
{
    public enum Channel
    {
        Output,
        ConstantCurrent,
        ConstantVoltage,
        VoltsDC,
        VoltsAC,
        SetVolts,
        AmpsDC,
        AmpsAC,
        SetAmps,
        Ohms,
        Degrees,
        Farads,
        Hertz,
        Percent,
        None
    }

    public abstract class Stream
    {
        public delegate void SampleAddedDelegate(Stream d, Reading result);
        public event SampleAddedDelegate SampleAdded;

        private Device device;

        public Channel channel;
        public TimeSpan resolution;
        public int capacity;
        public Color color;

        protected List<int> sections;
        protected DateTime time_base;
        protected DateTime most_recent_addition;

        protected double min;
        protected double max;
        protected double first;
        protected double last;
        protected double total;
        protected int readings;
        protected double global_min;
        protected double global_max;
        protected double range;
        protected int PacketsSent;

        public class Details
        {
            public Color Color;
            public double range;
            public Type Type;
            public string Name;
            public string Description;

            public Details(Color color, double range, Type type, string name, string description)
            {
                Color = color;
                this.range = range;
                Type = type;
                Name = name;
                Description = description;
            }
        }

        public virtual Device Device
        {
            get
            {
                return device;
            }
            set
            {
                device = value;
            }
        }

        public Channel Channel
        {
            get
            {
                return channel;
            }
        }

        public string Name
        {
            get
            {
                return Channel.ToString();
            }
        }

        // TODO (chs): allow devices to override these default ranges
        // TODO (chs): allow users to assign/override default colors
        protected static Dictionary<Channel, Details> details = new Dictionary<Channel, Details>()
        {
            { Channel.Output, new Details(Color.AliceBlue, 1, typeof(bool), "Output Status",  "Output is on/off") },
            { Channel.ConstantCurrent, new Details(Color.Beige, 2, typeof(int), "CC",  "1: +ive, -1: -ive, 0: not in CC") },
            { Channel.ConstantVoltage, new Details(Color.Bisque, 1, typeof(bool), "CV",  "Constant Voltage") },
            { Channel.VoltsDC, new Details(Color.MediumSpringGreen, 20, typeof(double), "Volts DC",  "Measured voltage") },
            { Channel.VoltsAC, new Details(Color.MediumSpringGreen, 20, typeof(double), "Volts AC",  "Measured voltage") },
            { Channel.SetVolts, new Details(Color.MediumSeaGreen, 20, typeof(double), "Set Volts",  "Voltage set level") },
            { Channel.AmpsDC, new Details(Color.Aqua, 2, typeof(double), "Amps DC",  "Measured current") },
            { Channel.AmpsAC, new Details(Color.Aqua, 2, typeof(double), "Amps AC",  "Measured current") },
            { Channel.SetAmps, new Details(Color.Aquamarine, 2, typeof(double), "Set Amps",  "Current limit") },
            { Channel.Ohms, new Details(Color.Red, 100000, typeof(double), "Ohms",  "Resistance") },
            { Channel.Degrees, new Details(Color.OrangeRed, 500, typeof(double), "Degrees C",  "Temperature ") },
            { Channel.Farads, new Details(Color.PaleGoldenrod, 2, typeof(double), "Farads",  "Capacitance") },
            { Channel.Hertz, new Details(Color.Cyan, 20000, typeof(double), "Hertz",  "Frequency") },
            { Channel.Percent, new Details(Color.Magenta, 100, typeof(double), "Duty Cycle",  "Duty Cycle (usually)") },
            { Channel.None, new Details(Color.Yellow, 0, typeof(int), "NONE!?", "Huh? you shouldn't be seeing this") }
        };

        public static Details GetDetails(Channel c)
        {
            return details[c];
        }

        public static string ChannelName(Channel c)
        {
            return GetDetails(c).Name;
        }

        // @yuck switch case on the type of results the stream contains based on which channel it is for
        public static Stream Create(Channel channel, Device device, TimeSpan resolution, int capacity)
        {
            switch (Type.GetTypeCode(details[channel].Type))
            {
                case TypeCode.Boolean:
                    return new TypedStream<bool>(device, channel, capacity, resolution, global_timebase);
                case TypeCode.Int32:
                    return new TypedStream<int>(device, channel, capacity, resolution, global_timebase);
                case TypeCode.Double:
                    return new TypedStream<double>(device, channel, capacity, resolution, global_timebase);
            }
            return null;
        }

        protected virtual void OnSampleAdded(Reading result)
        {
            SampleAdded?.Invoke(this, result);
        }

        public Stream(Device device, Channel channel, int capacity, TimeSpan resolution, DateTime time_base)
        {
            this.channel = channel;
            this.time_base = time_base;
            this.resolution = resolution;
            this.sections = new List<int>();
            this.range = details[channel].range;
            this.color = details[channel].Color;

            this.Device = device;
        }

        public virtual void Save(XmlElement e)
        {
            // save whatever is different from the defaults
            var elem = e.Elem("Stream");
            elem.Save("Channel", channel);
            elem.Save("Resolution", resolution);
            elem.Save("Samples", capacity);
            elem.Save("Color", ColorToARGBString(color));
            e.AppendChild(elem);
        }

        public virtual void SaveID(XmlElement e)
        {
            if(Device != null)
            {
                e.Save("Channel", channel);
                e.Save("ConnectionID", device.ConnectionID);
            }
        }

        public virtual void Load(XmlElement e)
        {
            e.Load("Resolution", ref resolution);
            e.Load("Capacity", ref capacity);
            e.LoadColor("Color", ref color);
        }

        public virtual void Reset()
        {
            sections.Clear();
            readings = 0;
            min = 0;
            max = 0;
            first = 0;
            last = 0;
            global_min = 0;
            global_max = 0;
            total = 0;
            PacketsSent = 0;
            //time_base = DateTime.Now; // @bugfix
            most_recent_addition = DateTime.Now;
        }

        public void ResetPacketCount()
        {
            PacketsSent = 0;
        }

        public virtual Type ResultType
        {
            get
            {
                return null;
            }
        }

        public virtual void StartNewSection()
        {

        }

        public IEnumerable<int> Sections
        {
            get
            {
                for (int i = 0; i < sections.Count; ++i)
                {
                    yield return sections[i];
                }
            }
        }

        public int SectionLength(int section)
        {
            return SectionEnd(section) - SectionBase(section);
        }

        public int SectionEnd(int section)
        {
            return (section < sections.Count - 1) ? sections[section + 1] : DataLength;
        }

        public virtual Reading Get(int index)
        {
            return default(Reading);
        }

        public int SectionBase(int section)
        {
            return sections[section];
        }

        public abstract int DataLength { get;  }

        public int SectionCount
        {
            get
            {
                return sections.Count;
            }
        }

        public double Range
        {
            get
            {
                return range;
            }
            set
            {
                range = value;
            }
        }

        public delegate void DataReadyDelegate(Stream sender, ResultBase value);   // HMPH! TODO (chs): fix this
        public event DataReadyDelegate DataReady;

        public virtual void OnDataReady(ResultBase value)
        {
            DataReady?.Invoke(this, value);
        }

        public abstract void Put(ResultBase r);
    }

    public class TypedStream<T> : Stream
    {
        public delegate void NewStreamIncomingDelegate(TypedStream<T> sender);

        public event NewStreamIncomingDelegate NewStreamIncoming;

        protected CircularList<Reading> data;

        public TypedStream(Device device, Channel channel, int capacity, TimeSpan resolution, DateTime time_base): base(device, channel, capacity, resolution, time_base)
        {
            data = new CircularList<Reading>(capacity);
            Reset();
        }

        public override int DataLength
        {
            get
            {
                return data.Length;
            }
        }

        public override Type ResultType
        {
            get
            {
                return typeof(T);
            }
        }

        public override void Reset()
        {
            base.Reset();
            data.Reset();
        }

        public TimeSpan Resolution
        {
            get
            {
                return resolution;
            }
            set
            {
                resolution = value;
            }
        }

        public IEnumerable<Reading> Section(int section)
        {
            int end = SectionEnd(section);
            for (int i = SectionBase(section); i < end; ++i)
            {
                yield return data.GetAt(i);
            }
        }

        public TimeSpan MaxTime
        {
            get
            {
                return most_recent_addition - time_base;
            }
        }

        public CircularList<Reading> Data
        {
            get
            {
                return data;
            }
        }

        public DateTime TimeBase
        {
            get
            {
                return time_base;
            }
            set
            {
                time_base = value;
            }
        }

        public int Length
        {
            get
            {
                return DataLength;
            }
        }

        public double Min
        {
            get
            {
                return global_min;
            }
        }

        public double Max
        {
            get
            {
                return global_max;
            }
        }

        public double DefaultRange
        {
            get
            {
                return global_max - global_min;
            }
        }

        public override Reading Get(int index)
        {
            return data.GetAt(index);
        }

        // TODO (chs): add handler for Device.COnnectionStateChange - when it goes to disconnected, set packetssent to 0
        // so next time get a packet, a new section will be started
        public override void StartNewSection()
        {
            if (PacketsSent == 0)
            {
                sections.Add(DataLength);
                OnNewStreamIncoming();
            }
            ++PacketsSent;
        }

        public virtual bool Add(double value)
        {
            // add the value into the current reading
            DateTime now = DateTime.Now;
            total += value;
            if (readings == 0)
            {
                first = value;
                last = value;
                min = value;
                max = value;
                if (Length == 0)
                {
                    global_max = value;
                    global_min = value;
                }
                else
                {
                    if (value < global_min)
                    {
                        global_min = value;
                    }
                    if (value > global_max)
                    {
                        global_max = value;
                    }
                }
            }
            else
            {
                if (value < min)
                {
                    min = value;
                }
                if (value > max)
                {
                    max = value;
                }
                if (value < global_min)
                {
                    global_min = value;
                }
                if (value > global_max)
                {
                    global_max = value;
                }
                last = value;
            }
            ++readings;

            // add a new datapoint if necessary
            if (now - most_recent_addition >= resolution)
            {
                Reading r = new Reading(now - time_base, min, max, total / readings, first, last, readings);
                data.Put(r);
                most_recent_addition = now;
                readings = 0;
                total = 0;
                OnSampleAdded(r);
                return true;
            }
            return false;
        }

        public void AddReading(TimeSpan t, double value)
        {
            StartNewSection();
            Reading r = new Reading(t, value, value, value, value, value, 1);
            data.Put(r);
            readings = 0;
            total = 0;
        }

        public virtual void OnNewStreamIncoming()
        {
            NewStreamIncoming?.Invoke(this);
        }

        public override void Put(ResultBase result)
        {
            Result<T> r = (Result<T>)result;
            StartNewSection();
            DoubleConverter d = new DoubleConverter();
            Add((double)d.ConvertFrom(r.value));
            OnDataReady(result);
        }

        // parse a string into a Result<T>, with some checking
        public static Result<T> CreateResult(string response)
        {
            Result<T> r = new Result<T>(default(T), false);
            if (response != null)
            {
                try
                {
                    TypeConverter cnv = (typeof(T) == typeof(bool)) ? new BoolConverter() : TypeDescriptor.GetConverter(typeof(T));
                    try
                    {
                        r.value = (T)cnv.ConvertFromString(response);
                        r.success = true;
                    }
                    catch(Exception)
                    {
                    }
                }
                catch (FormatException)
                {
                }
            }
            return r;
        }
    }

    // this is a stream which has been loaded from a file, it's not connected to a device
    public class DummyStream<T> : TypedStream<T>
    {
        public DummyStream(Device device, Channel channel, int capacity, TimeSpan resolution, DateTime time_base): base(device, channel, capacity, resolution, time_base)
        {
            data = new CircularList<Reading>(capacity);
            Reset();
        }

        public override Device Device
        {
            get
            {
                return base.Device;
            }

            set
            {
                base.Device = value;
            }
        }
    }
}
