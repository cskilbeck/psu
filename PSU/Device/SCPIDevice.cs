﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Xml;
using Locomo;

namespace Locomo
{
    public abstract class SCPIDevice : SerialDevice
    {
        protected CommandProcessor command_processor;

        public SCPIDevice(System.Windows.Forms.TreeNode node) : base(node)
        {
        }

        public override bool Controllable
        {
            get
            {
                return true;
            }
        }

        public override bool Connect()
        {
            var rc = base.Connect();
            if (rc)
            {
                if(command_processor == null)
                {
                    command_processor = new CommandProcessor();
                }
                StartMonitor();
            }
            return rc;
        }

        public override void Disconnect()
        {
            command_processor?.Complete();
            command_processor = null;
            base.Disconnect();
        }

        protected string Query(string q)
        {
            WriteLine(q);
            var response = ReadLine();
            return response;
        }

        public void Get<T>(string query, Completed<T> onCompleted) where T: IConvertible
        {
            try
            {
                if (command_processor != null)
                {
                    command_processor.Schedule(() =>
                    {
                        string response = Query(query);
                        var r = TypedStream<T>.CreateResult(response);
                        if (r.success)
                        {
                            onCompleted(r);
                        }
                        return r;
                    });
                }
            }
            catch (TaskCanceledException)
            {
            }
        }

        public void PutString(string s, Completed<bool> onCompleted)
        {
            command_processor?.Schedule(() =>
            {
                WriteLine(s);
                onCompleted(new Result<bool>(true));
                return true;
            });
        }

        public void PutString(string s)
        {
            command_processor?.Schedule(() =>
            {
                return WriteLine(s);
            });
        }

        public virtual void GetValue<T>(Channel t, Completed<T> onCompleted) where T: IConvertible
        {
            Debugger.Break();
        }
    }
}
