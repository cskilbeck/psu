﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Locomo
{
    public struct Reading
    {
        private TimeSpan time;
        private double min;
        private double max;
        private double mean;    // mean of bools looks like what?
        private double first;
        private double last;
        private int readings;

        public TimeSpan Time
        {
            get
            {
                return time;
            }
        }

        public double Median
        {
            get
            {
                return (min + max) / 2;
            }
        }

        public double Mean
        {
            get
            {
                return mean;
            }
        }

        public double Range
        {
            get
            {
                return max - min;
            }
        }

        public double Min
        {
            get
            {
                return min;
            }
        }

        public double Max
        {
            get
            {
                return max;
            }
        }

        public Reading(TimeSpan time, double min, double max, double mean, double first, double last, int readings)
        {
            this.time = time;
            this.min = min;
            this.max = max;
            this.mean = mean;
            this.first = first;
            this.last = last;
            this.readings = readings;
        }
    }
}
