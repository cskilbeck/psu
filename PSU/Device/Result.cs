﻿namespace Locomo
{
    public class ResultBase
    {

    }

    public class Result<T> : ResultBase
    {
        public T value;
        public bool success;

        public Result(T val)
        {
            value = val;
            success = true;
        }

        public Result(T val, bool success)
        {
            value = val;
            this.success = success;
        }
    };
}
