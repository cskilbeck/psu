﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace Locomo
{
    [XmlType(Namespace=null, TypeName=null)]
    public class SerialPortConfig : IEquatable<SerialPortConfig>
    {
        public string Port;
        public int BaudRate;        
        public Parity Parity;        
        public Handshake FlowControl;
        public StopBits StopBits;
        public int DataBits;
        public bool rtsEnable;

        public SerialPortConfig()
        {
            Port = "DUMMY";
            BaudRate = 9600;
            DataBits = 8;
            Parity = Parity.None;
            StopBits = StopBits.One;
            FlowControl = Handshake.None;
            rtsEnable = false;
        }

        public SerialPortConfig(string portName, int baudRate, Parity parity, Handshake flowControl)
        {
            Port = portName;
            BaudRate = baudRate;
            DataBits = 8;
            Parity = parity;
            StopBits = StopBits.One;
            FlowControl = flowControl;
            rtsEnable = false;
        }

        public override string ToString()
        {
            return "SerialPortConfig { " +
                "Port = " + Port +
                ", BaudRate = " + BaudRate +
                ", DataBits = " + DataBits +
                ", Parity = " + Parity +
                ", StopBits = " + StopBits +
                ", FlowControl = " + FlowControl +
                ", rtsEnable = " + rtsEnable + "}";
        }

        public bool Equals(SerialPortConfig other)
        {
            return Port == other.Port &&
                    BaudRate == other.BaudRate &&
                    Parity == other.Parity &&
                    FlowControl == other.FlowControl &&
                    StopBits == other.StopBits &&
                    DataBits == other.DataBits &&
                    rtsEnable == other.rtsEnable;
        }
    };
}
