﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Logging;

namespace Locomo
{
    public class SerialConnection : Connection
    {
        public SerialPortConfig Config;
        public SerialPortEx Port;

        public SerialConnection(Device device, SerialPortConfig config) : base(device)
        {
            Config = config;
            Port = SerialPortEx.CreatePort(config);
        }

        public override bool IsConnected
        {
            get
            {
                return Port.IsOpen && State != ConnectionState.Disconnected;
            }
        }

        public override string Name
        {
            get
            {
                return Port.Name;
            }
        }

        public override bool Connect()
        {
            if (Port == null)
            {
                Port = SerialPortEx.CreatePort(Config);
            }
            if (Port != null)
            {
                if (!Port.IsOpen)
                {
                    Port.Open();
                }
                State = ConnectionState.Connecting;
                return true;
            }
            else
            {
                State = ConnectionState.Disconnected;
                return false;
            }
        }

        public override void Disconnect()
        {
            if (Port != null && Port.IsOpen)
            {
                Port.Close();
            }
            State = ConnectionState.Disconnected;
        }

        public override void Load(XmlElement e)
        {
            base.Load(e);
            Config = XmlExtensions.Deserialize<SerialPortConfig>(e);
        }

        public override void Save(XmlElement e)
        {
            base.Save(e);
            XmlExtensions.SerializeInto(e, Config);
        }

        public override bool WriteLine(string s)
        {
            try
            {
                if (IsConnected)
                {
                    Port.WriteLine(s);
                    //Log.Verbose("SEND:" + s, Port.Name);
                    return true;
                }
            }
            catch (TimeoutException)
            {
                State = ConnectionState.Disconnected;
            }
            return false;
        }

        public override void WriteBytes(byte[] data, int offset, int length)
        {
        }

        public override string ReadLine()
        {
            string s = null;
            try
            {
                if (State != ConnectionState.Disconnected)
                {
                    s = Port.ReadLine();
                    State = ConnectionState.Connected;
                    //Log.Verbose("RECV:" + s.Trim(), Port.Name);
                }
            }
            catch (TimeoutException)
            {
                State = ConnectionState.Disconnected;
            }
            return s;
        }

        public override int ReadBytes(byte[] buffer, int num)
        {
            int got = 0;
            if (State != ConnectionState.Disconnected)
            {
                try
                {
                    got = Port.Read(buffer, 0, num);
                    State = ConnectionState.Connected;
                    Log.Verbose(got + " bytes", Port.Name);
                }
                catch (TimeoutException)
                {
                    State = ConnectionState.Disconnected;
                }
            }
            return got;
        }

        public override int ReadByte()
        {
            int got = 0;
            if (State != ConnectionState.Disconnected)
            {
                try
                {
                    got = Port.ReadByte();
                    State = ConnectionState.Connected;
                }
                catch(TimeoutException)
                {
                    State = ConnectionState.Disconnected;
                }
            }
                return got;
            }

        public override bool Equals(Connection other)
        {
            return Config.Equals(((SerialConnection)other).Config);
        }
    }
}
