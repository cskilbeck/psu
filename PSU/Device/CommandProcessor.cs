﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;

namespace Locomo
{
    public sealed class CommandProcessor : TaskScheduler
    {
        CancellationTokenSource killer;
        BlockingCollection<Task> taskQueue;
        Thread taskThread;
        object lockObject;

        public CommandProcessor()
        {
            lockObject = new object();
            killer = new CancellationTokenSource();
            taskQueue = new BlockingCollection<Task>();
            taskThread = new Thread(() =>
            {
                try
                {
                    while (!killer.IsCancellationRequested)
                    {
                        foreach (var task in taskQueue.GetConsumingEnumerable(killer.Token))
                        {
                            TryExecuteTask(task);
                            if (killer.IsCancellationRequested)
                            {
                                break;
                            }
                        }
                    }
                }
                catch (InvalidOperationException)
                {
                }
                catch (OperationCanceledException)
                {
                }
            })
            { Name = "PSU Thread" };
            taskThread.Start();
        }

        public Task<T> Schedule<T>(Func<T> action)
        {
            lock (lockObject)
            {
                return Task.Factory.StartNew(action, killer.Token, TaskCreationOptions.None, this);
            }
        }

        public Task Schedule(Action action)
        {
            lock (lockObject)
            {
                return Task.Factory.StartNew(action, killer.Token, TaskCreationOptions.None, this);
            }
        }

        // schedule a task which is guaranteed to be the last task it executes
        public Task ScheduleSentinelTask(Action action)
        {
            lock (lockObject)
            {
                var t = Task.Factory.StartNew(action, killer.Token, TaskCreationOptions.None, this);
                taskQueue.CompleteAdding();
                return t;
            }
        }

        public void Complete()
        {
            taskQueue.CompleteAdding();
            killer.Cancel(true);
            if (!taskThread.Join(500))
            {
                taskThread.Abort();
            }
        }

        protected override void QueueTask(Task task)
        {
            try
            {
                taskQueue.Add(task, killer.Token);
            }
            catch (OperationCanceledException)
            {
            }
        }

        protected override IEnumerable<Task> GetScheduledTasks()
        {
            return null;
        }

        protected override bool TryExecuteTaskInline(Task task, bool taskWasPreviouslyQueued)
        {
            if(taskWasPreviouslyQueued)
            {
                return false;
            }
            Debugger.Break();
            return TryExecuteTask(task);
        }
    }
}
