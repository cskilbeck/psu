﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Logging;

namespace Locomo
{
    // a monitor item which monitors multiple things with one string of semicolon separated commands
    public class MultiMonitor : Monitor.Item<string>
    {
        // these are the commands which can be concatenated into the string
        public string[] command_strings;

        private int next_mask;        // command set is fungible, can be switched on and off with mask bits
        private string next_command;
        private int current_mask;

        // switch them on and off by changing the bits in this property
        public int mask
        {
            get
            {
                return next_mask;
            }
            set
            {
                if (next_mask != value)
                {
                    SetupMiscString(value);
                }
            }
        }

        // create the string
        private void SetupMiscString(int newMask)
        {
            List<string> miscStrings = new List<string>();
            int id = 0;
            for (int i = 1; i != 0x10; i <<= 1)
            {
                if ((newMask & i) != 0)
                {
                    miscStrings.Add(command_strings[id]);
                }
                id++;
            }
            lock (this)
            {
                next_command = string.Join(";", miscStrings);
                next_mask = newMask;
            }
        }

        public void HandleResults(Result<string> result)
        {
        }

        public MultiMonitor(string[] commands, object[] streams_or_actions) : base(string.Empty, (Action<ResultBase>)null)
        {
            Update = (result =>
            {
                if (result.success)
                {
                    double dv;
                    int iv;
                    int bv;

                    string[] s = result.value.Split(';');
                    int index = 0;
                    int cmd_mask = 1;
                    for (int cmd = 0; cmd < commands.Length; ++cmd) // H
                    {
                        if (index >= s.Length)
                        {
                            Log.Error("Index " + index + " out of range for " + result);
                            break;
                        }
                        if ((cmd_mask & mask) != 0)
                        {
                            if(streams_or_actions[cmd] is Stream)
                            {
                                Stream stream = (Stream)streams_or_actions[cmd];

                                switch (Type.GetTypeCode(stream.ResultType))
                                {
                                    case TypeCode.Double:
                                        TypedStream<double> td = (TypedStream<double>)stream;
                                        if (double.TryParse(s[index], out dv)) td.Put(new Result<double>(dv));
                                        break;
                                    case TypeCode.Int32:
                                        TypedStream<int> ti = (TypedStream<int>)stream;
                                        if (int.TryParse(s[index], out iv)) ti.Put(new Result<int>(iv));
                                        break;
                                    case TypeCode.Boolean:
                                        TypedStream<bool> tb = (TypedStream<bool>)stream;
                                        if (int.TryParse(s[index], out bv)) tb.Put(new Result<bool>(bv != 0));
                                        break;
                                }
                            }
                            else if(streams_or_actions[cmd] is Action<string>)
                            {
                                ((Action<string>)(streams_or_actions[cmd])).Invoke(s[index]);
                            }
                            index++;
                        }
                        cmd_mask <<= 1;
                    }
                }
            });
            command_strings = commands;
            SetupMiscString(0xf);
        }

        public override void RunUpdate(Result<string> result)
        {
            base.RunUpdate(result);
        }

        public override void Execute(SCPIDevice psu, Monitor monitor)
        {
            lock (this)
            {
                current_mask = next_mask;
                Command = next_command;
            }
            base.Execute(psu, monitor);
        }
    }

}
