﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Logging
{
    public class Log
    {
        public static string Filename;

        public static WriteToFile WriteToFileOption;

        public static void Debug(object text, [CallerMemberName] string channel = "", [CallerFilePath] string f = "", [CallerLineNumber] int l = 0)
        {
            System.Diagnostics.Debug.WriteLine($"DBG:{channel}:{text}");
        }
        public static void Error(object text, [CallerMemberName] string channel = "", [CallerFilePath] string f = "", [CallerLineNumber] int l = 0)
        {
            System.Diagnostics.Debug.WriteLine($"ERR:{channel}:{text}");
        }
        public static void Info(object text, [CallerMemberName] string channel = "", [CallerFilePath] string f = "", [CallerLineNumber] int l = 0)
        {
            System.Diagnostics.Debug.WriteLine($"INF:{channel}:{text}");
        }
        public static void Verbose(object text, [CallerMemberName] string channel = "", [CallerFilePath] string f = "", [CallerLineNumber] int l = 0)
        {
            System.Diagnostics.Debug.WriteLine($"VRB:{channel}:{text}");
        }
        public static void Warning(object text, [CallerMemberName] string channel = "", [CallerFilePath] string f = "", [CallerLineNumber] int l = 0)
        {
            System.Diagnostics.Debug.WriteLine($"WRN:{channel}:{text}");
        }
        public static void Write(Severity severity, object text, [CallerMemberName] string channel = "", [CallerFilePath] string f = "", [CallerLineNumber] int l = 0)
        {
            System.Diagnostics.Debug.WriteLine($"{severity,3}:{channel}:{text}");
        }

        public enum Severity
        {
            Verbose = 0,
            Debug = 1,
            Info = 2,
            Warning = 3,
            Error = 4
        }
        public enum WriteToFile
        {
            Never = 0,
            Always = 1,
            IfNoDebugger = 2
        }
    }
}
