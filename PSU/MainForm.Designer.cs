﻿namespace Locomo
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.rootContainer = new Locomo.Ctrls.PanelContainer();
            this.SuspendLayout();
            // 
            // rootContainer
            // 
            this.rootContainer.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.rootContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rootContainer.Fixed = false;
            this.rootContainer.Location = new System.Drawing.Point(0, 0);
            this.rootContainer.Name = "rootContainer";
            this.rootContainer.Pinned = true;
            this.rootContainer.Size = new System.Drawing.Size(775, 716);
            this.rootContainer.TabIndex = 0;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(775, 716);
            this.Controls.Add(this.rootContainer);
            this.KeyPreview = true;
            this.Name = "MainForm";
            this.Text = "Locomo";
            this.WindowState = System.Windows.Forms.FormWindowState.Minimized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.MainForm_KeyPress);
            this.ResumeLayout(false);
        }

        #endregion

        private Ctrls.PanelContainer rootContainer;
    }
}